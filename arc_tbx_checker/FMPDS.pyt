import arcpy

import os,sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from checker import productDict, fmpSpecDict, reportDetailList # used to fill the combo boxes...
from stack.static_db import static_db 

class Toolbox(object):
	def __init__(self):
		"""Define the toolbox (the name of the toolbox is the name of the .pyt file)."""
		self.label = "FMPDS Tools"
		self.alias = "MNRF FMPDS Toolbox"
		# List of tool classes associated
		# with this toolbox
		self.tools = [ Checker ]


class Checker(object):
	def __init__(self):
		"""Define the tool (tool name is the name of the class)."""
		self.label = "Checker"
		self.description = "Checker reviews data files for FIM Tech Spec Validation"
		self.canRunInBackground = False
		
	def getParameterInfo(self):
		"""Define parameter definitions"""
		# 0 is Product fullname ( productDict.keys() ) Default Annual Report
		# 1 is forest (fmu - static_db.keys() Can we get that here in a .pyt?  ) Default Algonquin_Park
		# Needs Combobox - see here for a two methods
		# https://gis.stackexchange.com/questions/217755/choosing-string-from-dropdown-list-rather-than-typing-it-into-python-script-tool
		# 2 is year - default to 2016
		# 3 is fmpstartyear  ## Optional
		# 4 is customPath  ## Optional
		# 5 is fmpmVersion ## Mandatory. fmpSpecDict.keys()
		# 6 is reportDetail ## Mandatory. reportDetailList
		
		
		params = [
			arcpy.Parameter(
				displayName="FIM Data Product",
				name="product_fullname",
				datatype="GPString",
				parameterType="Required",
				direction="Input"
				),
			arcpy.Parameter(
				displayName="Forest Management Unit",
				name="forest",
				datatype="GPString",
				parameterType="Required",
				direction="Input"
				),
			arcpy.Parameter(
				displayName="Submission Year",
				name="year",
				datatype="GPString",
				parameterType="Required",
				direction="Input"
				),
			arcpy.Parameter(
				displayName="Forest Management Plan Year",
				name="fmpstartyear",
				datatype="GPString",
				parameterType="Optional",
				direction="Input"
				),
			arcpy.Parameter(
				displayName="Custom Path",
				name="customPath",
				datatype="GPString",
				parameterType="Optional",
				direction="Input"
				),
			arcpy.Parameter(
				displayName="FIM Tech Spec Version",
				name="fmpmVersion",
				datatype="GPString",
				parameterType="Optional",
				direction="Input"
				),
			arcpy.Parameter(
				displayName="Level of Report Detail",
				name="reportDetail",
				datatype="GPString",
				parameterType="Optional",
				direction="Input"
				)
			]
			
		# params[i].parameterDependencies= [ params[j].name ]
		
		return params
		
	def isLicensed(self):
		"""Set whether tool is licensed to execute."""
		return True
		
	def updateParameters(self, parameters):
		"""Modify the values and properties of parameters before internal
		validation is performed.  This method is called whenever a parameter
		has been changed."""
		return
		
	def updateMessages(self, parameters):
		"""Modify the messages created by internal validation for each tool
		parameter.  This method is called after internal validation."""
		return
		
	def execute(self, parameters, messages):
		"""The source code of the tool."""
		return
