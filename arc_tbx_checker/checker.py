# this python script receives the inputs from the Arc tool and sends it off to
# initialize_checker.py to run the validation checks
import sys
import os

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

import stack.paths
from checker.initialize_checker import submission_checker
from stack.static_db import static_db
import arcpy

fmpSpecDict = { "2017 (Current)": "2017", "2009 (Old)":"2009" }

productDict = { 'Annual Report':'ar',
                'Annual Work Schedule':'aws',
                'Base Model Inventory':'bmi',
                'Enhanced Forest Resource Inventory':'efri',
                'Forest Management Plan':'fmp',
                'Operational Planning Inventory':'opi',
                'Planning Composite Inventory':'pci'
                }
reportDetailList = [ "None", "Short", "Long" ]


try:
	product = productDict[str(arcpy.GetParameterAsText(0))]
except KeyError:
	product = productDict.values()[0]


forest = str(arcpy.GetParameterAsText(1))
if forest in ['None','']: forest = static_db.keys()[0]  #'Algonquin_Park'

try:
	year = int(arcpy.GetParameterAsText(2))
except ValueError:
	year = 2016

basepaths = stack.paths.basepaths

fmpstartyear = arcpy.GetParameterAsText(3) ## Optional

customPath = arcpy.GetParameterAsText(4) ## Optional

fmpmVersion = arcpy.GetParameterAsText(5) ## Mandatory. either "2017 (Current)" or "2009 (Old)"

reportDetail = arcpy.GetParameterAsText(6) ## Mandatory. 3 options: "None", "Short", "Long"



if len(customPath) > 0: ## if customPath is filled out
	basepaths = [customPath]
	arcpy.AddMessage("- User specified base path = " + customPath)


if len(fmpstartyear) > 0: ## if fmpstartyear is filled out
    fmpstartyear = int(fmpstartyear)
else:
    fmpstartyear = None

arcpy.AddMessage('- Running the %s validation check on %s %s on %s...'%(fmpmVersion, product, str(year), forest))

s = submission_checker(reportDetail=reportDetail)
s.do_one(basepaths,forest,product,year,fmpstartyear,fmpmVersion)

import webbrowser
output = os.path.join(s.outputFilePath,s.outputFileName)
webbrowser.open(output)

"""
c:\Python27\ArcGIS10.3\python.exe
import sys
sys.path.insert(0, '..')
# start it out... it should run
import checker

# reload runs it again, with updated code here, but not in submodules
# so it doesn't reload arcpy...
reload( checker )

"""
