@echo off
SETLOCAL

:: This file removes the environment variables that are conflicting with
:: arcpy when called from the OsGeo4W Shell
if exist "c:\Python27\ArcGIS10.3\python.exe" (
set ARCPY=c:\Python27\ArcGIS10.3\python.exe
set PYTHONHOME=c:\Python27\ArcGIS10.3
) else if exist "c:\Python27\ArcGIS10.4\python.exe" (
set ARCPY=c:\Python27\ArcGIS10.4\python.exe
set PYTHONHOME=c:\Python27\ArcGIS10.4
) else if exist "c:\Python27\ArcGIS10.5\python.exe" (
set ARCPY=c:\Python27\ArcGIS10.5\python.exe
set PYTHONHOME=c:\Python27\ArcGIS10.5
) else if exist "c:\Python27\ArcGIS10.6\python.exe" (
set ARCPY=c:\Python27\ArcGIS10.6\python.exe
set PYTHONHOME=c:\Python27\ArcGIS10.6
) else if exist "c:\Python27\ArcGIS10.7\python.exe" (
set ARCPY=c:\Python27\ArcGIS10.7\python.exe
set PYTHONHOME=c:\Python27\ArcGIS10.7
) else (
echo "Arcpy Not Found. Exitting."
EXIT /B 2
)



set GDAL_DATA=
set GDAL_DRIVER_PATH=
set GEOTIFF_CSV=
set OSGEO4W_ROOT=
set PROJ_LIB=

:: The standard path in the Loch Lomond machine...
set  Path=C:\WINDOWS\system32;C:\WINDOWS;C:\WINDOWS\System32\Wbem;C:\WINDOWS\System32\WindowsPowerShell\v1.0\;C:\Program Files (x86)\Enterprise Vault\EVClient\;

::C:\Users\carran\AppData\Local\Microsoft\WindowsApps;C:\Users\carran\AppData\Local\Programs\Git\cmd;


rem List available o4w programs
rem but only if osgeo4w called without parameters
@echo on
@if [%1]==[] (
	echo "Arc python (%%ARCPY%%) is %ARCPY%" & cmd.exe /k
) else (
	cmd /c "%ARCPY% %*"
)

ENDLOCAL
EXIT /B 0
