import shutil, os, sys, datetime
import glob
from os import path

import socket

static_db = {
#           fmu                     code        plan start years    region
        'Abitibi_River':        [   '110',      [2012, 2022],       'NE'    ],
        'Algoma':               [   '615',      [2010, 2020],       'NE'    ],
        'Algonquin_Park':       [   '451',      [2010, 2020],       'S'     ],
        'Armstrong':            [   '444',      [2005],             'NW'    ], #outdated?
        'Bancroft-Minden':      [   '220',      [2011, 2021],       'S'     ],
        'Big_Pic':              [   '067',      [2007, 2017],       'NE'    ], # will be a part of Pic_Forest in 2019
        'Black_River':          [   '370',      [2006],             'NW'    ], #outdated?
        'Black_Spruce':         [   '035',      [2011, 2021],       'NW'    ],
        'Black_Sturgeon':       [   '815',      [2011],             'NW'    ],
        'Caribou':              [   '175',      [2008, 2018],       'NW'    ],
        'Crossroute':           [   '405',      [2007, 2017],       'NW'    ],
        'Dog_River-Matawin':    [   '177',      [2009, 2019],       'NW'    ],
        'Dryden':               [   '535',      [2011, 2021],       'NW'    ],
        'English_River':        [   '230',      [2009, 2019],       'NW'    ],
        'French_Severn':        [   '360',      [2009, 2019],       'S'     ],
        'Gordon_Cosens':        [   '438',      [2010, 2020],       'NE'    ],
        'Hearst':               [   '601',      [2007, 2017],       'NE'    ],
        'Kenogami':             [   '350',      [2011, 2021],       'NW'    ],
        'Kenora':               [   '644',      [2012, 2022],       'NW'    ],
        'Lac_Seul':             [   '702',      [2011, 2021],       'NW'    ],
        'Lake_Nipigon':         [   '815',      [2011, 2021],       'NW'    ],
        'Lakehead':             [   '796',      [2007, 2017],       'NW'    ],
        'Magpie':               [   '565',      [2009, 2019],       'NE'    ],
        'Martel':               [   '509',      [2011, 2021],       'NE'    ],
        'Mazinaw_Lanark':       [   '140',      [2011, 2021],       'S'     ],
        'Nagagami':             [   '390',      [2011, 2021],       'NE'    ],
        'Nipissing':            [   '754',      [2009, 2019],       'NE'    ],
        'Northshore':           [   '680',      [2010, 2020],       'NE'    ],
        'Ogoki':                [   '415',      [2008, 2018],       'NW'    ],
        'Ottawa_Valley':        [   '780',      [2011, 2021],       'S'     ],
        'Pic_Forest':           [   '966',      [2019],             'NE'    ], # Amalgamation of Big_Pic and Pic_River as of 2019 plan
        'Pic_River':            [   '965',      [2006, 2013],       'NE'    ], # will be a part of Pic_Forest in 2019
        'Pineland':             [   '421',      [2011, 2021],       'NW'    ],
        'Red_Lake':             [   '840',      [2008, 2018],       'NW'    ],
        'Romeo_Malette':        [   '930',      [2009, 2019],       'NE'    ],
        'Sapawe':               [   '853',      [2010, 2020],       'NW'    ],
        'Spanish':              [   '210',      [2010, 2020],       'NE'    ],
        'Sudbury':              [   '889',      [2010, 2020],       'NE'    ],
        'Temagami':             [   '898',      [2009, 2019],       'NE'    ],
        'Timiskaming':          [   '280',      [2011, 2021],       'NE'    ],
        'Trout_Lake':           [   '120',      [2009, 2019],       'NW'    ],
        'Wabigoon':             [   '130',      [2008, 2018],       'NW'    ],
        'Whiskey_Jack':         [   '490',      [2012, 2022],       'NW'    ],
        'White_River':          [   '060',      [2008, 2018],       'NE'    ],
        'Whitefeather':         [   '994',      [2012, 2022],       'NW'    ]
        }




# ====================================================================================================
def unit_number_from_name(fmu):
	try:
		return static_db[fmu][0]
	except KeyError:
		return '999'
# =====================================================================================================

def MakeRelativeLYR(fmu, product, year, FMUnumb, AOIyrBase, PS, lyrpath, gdbpath, lyr_src=None, IsDraft=None, **kwargs):
	###################
	# newDataPath - data file dirname
	# oldDataPath - FIM TS filename of data
	###################

	print "MakeRelativeLYR"
	print "\t{0:>12}: {1}".format('fmu',fmu)
	print "\t{0:>12}: {1}".format('product',product)
	print "\t{0:>12}: {1}".format('year',year)
	print "\t{0:>12}: {1}".format('FMUnumb',FMUnumb)
	print "\t{0:>12}: {1}".format('AOIyrBase',AOIyrBase)
	print "\t{0:>12}: {1}".format('PS',PS)
	#print "\t{0:>12}: {1}".format('outpath',outpath)
	print "\t{0:>12}: {1}".format('lyrpath',lyrpath)
	print "\t{0:>12}: {1}".format('gdbpath',gdbpath)
	print "\t{0:>12}: {1}".format('lyr_src',lyr_src)



	lyrpath2    = lyrpath.replace(".lyr","_1.lyr")
	rellyrPath  = lyrpath  #.replace(".lyr","_r.lyr")

	if not os.path.exists(gdbpath):
		print "\n"+ gdbpath + " doesn't exist.\n"
		raise IOError("gdbpath does not exist: {0}".format(gdbpath))

	#Copy a Layer file in to place
	lyr_src_filename = validate_lyr_src(lyr_src,year=int(year),product=product)
	if lyr_src is None:
		lyr_src = os.path.dirname(lyr_src_filename)
	copy_lyrfile_from_source(dst=lyrpath2,lyr_src=lyr_src_filename,year=int(year),product=product)

	#replace layer path names in product
	lyrFile = replace_lyr_sources(lyrpath=lyrpath2,gdbpath=gdbpath)

	try:
		arcpy.env.workspace = os.path.dirname(gdbpath)
	except NameError:
		import arcpy
		arcpy.env.workspace = os.path.dirname(gdbpath)


	if arcpy.Exists(rellyrPath):
		arcpy.Delete_management(rellyrPath)
		#
	lyrFile.name = lyrpath2
	#print lyrFile.name
	#print "Saving to new file ..."
	lyrFile.saveACopy(lyrpath2)
	#arcpy.Delete_management(PT+"_"+FMUname+"_("+FMUnumb+")_"+year+"_X.lyr")

	# print "Open LYR in MXD. Save rel paths,  Save mxd."
	#
	mxdLYR = arcpy.mapping.MapDocument(os.path.join(lyr_src,"MXD4FMP.mxd"))
	df = arcpy.mapping.ListDataFrames(mxdLYR, "*")[0]
	newlayer = arcpy.mapping.Layer(lyrpath2)
	arcpy.mapping.AddLayer(df, newlayer, "TOP")
	mxdLYR.relativePaths="True"
	print mxdLYR.relativePaths
	mxdLYR2 = os.path.splitext(lyrpath2)[0]+".mxd"
	print "mxd2 = " + mxdLYR2
	if arcpy.Exists(mxdLYR2):
		arcpy.Delete_management(mxdLYR2)
	mxdLYR.saveACopy(mxdLYR2)
	#
	newlayer.name = rellyrPath
	arcpy.SaveToLayerFile_management(newlayer, rellyrPath, is_relative_path="RELATIVE")
   # arcpy.SaveToLayerFile_management(newlayer, NotRelFile)
	del mxdLYR
	# del mxdLYR2
	#
	arcpy.Delete_management(lyrpath2)
	arcpy.Delete_management(mxdLYR2)
	del mxdLYR2
	print "\n" + '-'*50
# ========================================================================================
#

def add_yearx_attributes(gdbpath,product,year):
	try:
		arcpy.env.workspace = gdbpath
	except NameError:
		import arcpy
		arcpy.env.workspace = gdbpath

	# Adjust source data to include the "AR_yearX" attribute if is not already there.
	# Is this still a thing we need to do?
	arcpy.env.workspace = gdbpath
	FClist = arcpy.ListFeatureClasses("", "", "")
	for FC in FClist:
		if len(arcpy.ListFields(FC, product+"_yearX")) == 0:
			arcpy.AddField_management(FC, product+"_yearX", "LONG")
		arcpy.CalculateField_management(FC, product+"_yearX", year)

def replace_lyr_sources(lyrpath,gdbpath):
	try:
		arcpy.env.workspace = os.path.dirname(gdbpath)
	except NameError:
		import arcpy
		arcpy.env.workspace = os.path.dirname(gdbpath)

	#adjust the layerfile paths to absolute local...
	#arcpy.env.workspace = outpath
	lyrFile = arcpy.mapping.Layer(lyrpath)
	print ("Layerfile Spec: {name}\r\n\tIs Group Layer : {group}\r\n\tSupports NAME: {sName}\r\n\tDATASOURCE: {ds}".format(group=str(lyrFile.isGroupLayer),sName=str(lyrFile.supports("NAME")),ds=str(lyrFile.supports("DATASOURCE")),name=lyrFile.name ))

	if lyrFile.isGroupLayer:
		for lyr in lyrFile:
			print "Re-Sourcing Layer : " + lyr.name
			if len(lyr.definitionQuery) > 0:
				lyr.definitionQuery = ""
			lyr.replaceDataSource(gdbpath, 'FILEGDB_WORKSPACE', lyr.datasetName, False)
	return lyrFile


def copy_lyrfile_from_source(dst,lyr_src=None,year=2050, product=None):
	import os
	import shutil

	if not product :
		product = 'AR'

	# print ("copy_lyrfile_from_source(dst={0},lyr_src={1},year={2}, product={3})".format(dst,lyr_src,year, product))
	if os.path.exists(dst): os.remove(dst)
	shutil.copyfile(validate_lyr_src(lyr_src,year,product),dst)


def validate_lyr_src(lyr_src=None,year=None,product=None,submission_type=None,**kwargs):
	if lyr_src is None:
		#lyr_src = r"\\cihs.ad.gov.on.ca\mnrf\groups\ROD\NorthwestRegion\RIAU\Data\Extended_Data\Forest\FMP\LYRs4PY"
		lyr_src=os.path.join(os.path.abspath(os.path.dirname(__file__)),'LYRS')

	if submission_type is not None:
		if os.path.isdir(lyr_src):
			lyr_srcfilename = os.path.join(lyr_src,submission_type + '.mm')
			return lyr_srcfilename

	if not product :
		if submission_type is not None:
			product = submission_type.split('_')[0].replace('_','')
		else:
			product = 'AR'

	if product in [ 'FMPDP','PCI','BMI' ]:
		product='FMP'

	if not year :
		if submission_type is not None:
			year = int(submission_type.split('_')[1].replace('_',''))
		else:
			year = 2050

	if os.path.isdir(lyr_src):
		lyr_srcfilename = [ os.path.join(lyr_src,'_'.join([product,str(y)+'.lyr'])) for y in range(year,1990,-1) if os.path.exists(os.path.join(lyr_src,'_'.join([product,str(y)+'.lyr']))) ][0]
		print ("\tUsing template layer_file {0}".format(lyr_srcfilename ))
	elif os.path.isfile(lyr_src):
		lyr_srcfilename = lyr_src
		print ("\tUsing template layer_file {0}".format(lyr_srcfilename ))

	return lyr_srcfilename

def copyFeatureClassesFromGPKG(src,dst,overwrite=False):
	"""
	Copy the feature class and let us know what is going on...

	"""
	try:
		arcpy.env.workspace=src
	except NameError:
		import arcpy
		arcpy.env.workspace=src

	print ( "copyFeatureClassesFromGPKG:\n\t{0}\n\t{1}\n".format(src,dst))

	fc_list = arcpy.ListFeatureClasses()
	print fc_list
	if not fc_list:
		return False

	if overwrite and os.path.exists(dst) :
		try:
			arcpy.Delete_management(dst)
		except :  #arcpy.arcgisscripting.ExecuteError as e:
			#print e
			import time
			time.sleep(15)
			arcpy.Delete_management(dst)

	if not os.path.exists(dst):
		arcpy.CreateFileGDB_management(os.path.dirname(dst),os.path.basename(dst))

	arcpy.env.workspace=dst
	try:
		fc_Dict = dict(zip(fc_list,[os.path.join(dst,f.replace('main.','')) for f in fc_list ] ) )
	except TypeError as e:
		print e

	#print "!!!!!!!!!!!!!!!!!!!!!"
	#print fc_Dict
	#print "!!!!!!!!!!!!!!!!!!!!!"

	for k,v in fc_Dict.iteritems():
		print ("Copying {k} to {v}".format(k=k,v=v)),
		try:
			arcpy.CopyFeatures_management(os.path.join(src,k),v)
			print ("...")
		except Exception as e:
			print ("... Failed")
			print e

def read_submission(newDataPath):
	# is there an fi_submission*.txt file we can use for this?
	PS_List = glob.glob(os.path.join(newDataPath,'fi_submission_*.txt'))
	try:
		PS = os.path.basename(PS_List[0]).lower().replace('fi_submission','').replace('.txt','')
	except IndexError as e:
		PS=''
	return PS

def main_multiple( fmu,product,year,FMUPath,overwrite=False, gpkgname=None):
	if fmu in [None,'%']:
		fmus=static_db.keys()
	else:
		fmus=[fmu]

	products=['AR','AWS','FMP'] if product in [None,'%'] else [product]
	years=range(2001,2020) if year in [None,'%'] else [year]

	res = list()
	for f in fmus:
		for p in products:
			for y in years:
				try:
					res.append(main(fmu=f,product=p,year=y,FMUPath=FMUPath,overwrite=overwrite, gpkgname=gpkgname))
				except IOError as e:
					print ('IOError("An IO Error was detected on FPY {forest}/{product}/{year}")'.format(forest=f,product=p,year=y))
					print e
					pass
	return res

def main(  fmu , product='AR', year='2017', FMUPath='f:\\', PS=None  , overwrite=False, gpkgname=None ):
	"""
	Main function -
		* Housekeeping for names and the like
			** Tests delivery location is present
		* Initiates copy of files from existing GPKG by calling:
			** copyFeatureClassesFromGPKG(src=fileBaseName+'.gpkg',dst=gdbpath, overwrite=True)
		* Initiates creation of relative layer file with:
			** MakeRelativeLYR(fmu, product, str(year), FMUcode, AOIyrBase, PS, outpath=newDataPath, gdbpath=gdbpath, lyrpath=fileBaseName+'.lyr' )
	"""
	if FMUPath is None:
		if socket.gethostname().upper() == 'lrcgikdcwhiis06'.upper():
			FMUPath='d:\FMP'
		else:
			FMUPath=r'\\lrcgikdcwhiis06\FMP'


	#############################################
	# Housekeeping - Destination path exists?
	#############################################
	# newDataPath - new dirname for the GDB file
	newDataPath = os.path.join(FMUPath,fmu,product,str(year))
	FMUcode = str(unit_number_from_name(fmu)).zfill(3)

	if gpkgname is None:
		fileBaseName = os.path.join(newDataPath,"mu"+ FMUcode + "_" + str(year)+ '_' + product.upper())
		gpkgname=fileBaseName+'.gpkg'
	else :
		fileBaseName = gpkgname.replace('.gpkg','')
	gdbpath=fileBaseName+'.gdb'
	lyrpath=fileBaseName+'.lyr'

	if not os.path.isdir(newDataPath):
		raise IOError("Data output path does not exist {0}".format(newDataPath))
	if not os.path.isfile(gpkgname):
		raise IOError("Input GPKG does not exist {0}".format(gpkgname))
	if os.path.isfile(gdbpath) and not overwrite :
		raise IOError("Output GDB exists, and not set to overwrite")
	if os.path.isfile(lyrpath) and not overwrite :
		raise IOError("Output GDB exists, and not set to overwrite")

	PS = read_submission(newDataPath)

	print ("Building ESRI ArcGIS Layer file for {0} {1} {2} {3} {4}".format(  fmu , product, year, FMUPath, PS if PS is not None else ''  ))
	ProdType={"AR":"AnnualReports", "AWS":"AnnualWorkSchedules", "FMP":"Plan", "PCI":"Plan", "BMI":"Plan",}[product.upper()[:3]]
	AOIyrBase = str(year)[2:]

	##############################################
	#Copy files from GPKG to FGDB
	##############################################
	copyFeatureClassesFromGPKG(src=gpkgname,dst=gdbpath, overwrite=True)

	#Create Layer File for Arc
	MakeRelativeLYR(fmu, product, str(year), FMUcode, AOIyrBase, PS, outpath=newDataPath, gdbpath=gdbpath, lyrpath=lyrpath )

	return (gdbpath,lyrpath)


if __name__ == "__main__":
	#print sys.argv
	if len(sys.argv) < 5:
		print "\nArguments not provided."
		print "So we will make some assumptions"
#		print "  *** Required: FMUname  ProductType(AR, AWS, FMP)  ProductYear  FMUbasePath  ProductSubmission# or 0 *** "
#		print "     i.e.  Lac_Seul  AR 2016  Q:\fmpstuff\fmp  18970"
#		print "Quitting.\n"
#		sys.exit()
	#
	try:
		FMUname = str(sys.argv[1])
	except IndexError:
		FMUname = '%'
	try:
		PT = str(sys.argv[2]).upper()
	except IndexError:
		PT = '%'
	try:
		AOIyr = int(sys.argv[3])
	except IndexError:
		AOIyr = '%'
	try:
		FMUPath = str(sys.argv[4])
		if FMUPath=='%':
			FMUPath=None
	except IndexError:
		FMUPath=None

	overwrite = False
	try:
		for i in range(1,len(sys.argv)):
			if sys.argv[i] in [ '--overwrite', '-o' ]:
				overwrite = True
	except IndexError:
		overwrite = False

	gpkgname = None
	try:
		for i in range(1,len(sys.argv)):
			if '--input=' in sys.argv[i]:
				gpkgname = sys.argv[i].replace('--input=','')
			elif '--input' in sys.argv[i]:
				gpkgname = sys.argv[i+1]
			elif '--i' in sys.argv[i]:
				gpkgname = sys.argv[i+1]
	except IndexError:
		gpkgname = None

	res = main_multiple( fmu=FMUname , product=PT, year=AOIyr, FMUPath=FMUPath, overwrite=overwrite , gpkgname=gpkgname  )
	#print ("Processing Complete")
	#print "\n\t".join(res)

	sys.exit( 0 )
