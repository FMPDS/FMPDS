#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  checker.py
#

########################################################################
# This is the command-line version of the checker.
# It can be run in a bat file, or straight-up.
#
###############################
# Running under system python, or from the OSGEO4W prompt:
# python checker.py

###############################
# Using the python installed in QGIS:
# ps. It uses short paths because of command line path handling.
# C:\PROGRA~1\QGIS2~1.18\OSGeo4W.bat C:\PROGRA~1\QGIS2~1.18\bin\python c:\src\FMPDS\checker\checker.py

###############################
# Running under OSGeo4W python, but from a command prompt that normally uses a different python, or no python:
# c:\OSGeo4W\OSGeo4W.bat c:\OSGEO4W\bin\python c:\src\FMPDS\checker\checker.py

###############################
# As you would use it in geany:
# cmd /c "c:\OSGeo4W\OSGeo4W.bat c:\OSGEO4W\bin\python %f"
########################################################################

###############################
# Or from Arc's py
# c:\python27\ArcGIS10.4\python cmd_checker.py -j ..\tech_spec\2017\aws.json -i G:\FOREST_MGMT\FMPDS\Ottawa_Valley\AWS\2018\_data\FMP_Schema.gdb -y 2018 -f Ottawa_Valley -p AWS12:30 PM

########################################################################
# Examples of parameters:
#  python cmd_checker.py -j ..\tech_spec\2009\fmp.json -i C:\GIS\Black_Sturgeon_FMP_2011\E00\mu815_11aoc01.e00 -y 2011
#  python cmd_checker.py --forest=Lake_Nipigon --product=FMP --year=2011
########################################################################

import time

print "Running Context Checker. This may pause for an ARC License Check."

import os
import sys
import getopt

import datetime

import glob
import json

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from stack.pg_db import pg_db
from stack.static_db import static_db
import stack.paths   #.basepaths
from stack.fi_portal import findSubID

import _winreg
def get_reg(name,query_value=""):
    try:
        registry_key = _winreg.OpenKey(_winreg.HKEY_CURRENT_USER, name, 0,
                                       _winreg.KEY_READ)
        value, regtype = _winreg.QueryValueEx(registry_key, query_value)
        _winreg.CloseKey(registry_key)
        return value
    except WindowsError:
        return None

def main(argv):
	#for arg in argv: print arg

	#print "Using Database type %s"%(pg_db().db_type)

	try:
		opts, args = getopt.getopt(argv, "do:i:t:b:f:p:y:j:l:n:rws:e",
								   ["help", "debug","outputfile=",
									"input_data_file=","tablename=",
									"basepath=","forest=","product=",
									"year=", "jsonfilename=", "layer=",
									"planyear=", "reportDetail=",
									"web-browser","submission=","errors-only",
									"no-spatial-output","geojson-output","raw-json-output", "csv-output"
									])
	except getopt.GetoptError:
		pass
		usage()
		sys.exit(2)

	#Set defaults for conductor:
	#cond.debug = False
	#cond.inputfile = r'..\tech_spec\2009\FMP.mm'
	#cond.outputfile = 'clean.mm'
	#cond.overwrite = False
	#cond.verbose = True

	# Local options for convenience
	debug = False
	outputfile = "clean.html"
	tablename = None
	basepath = None
	forest = None
	fmuID = None
	product = None
	year = None
	inputfile = None
	layer=None
	planyear = None
	jsonfilename = None
	submission_type = None
	reportDetail = "Context"
	web_browser = True
	subID = None
	errors_only = False
	pause_after_running = True
	spatial_output = True
	geojson_output = False
	raw_json_output = False
	csv_output = False

	for opt, arg in opts:
		if opt in ["--help"]:
			usage()
			sys.exit()
		elif opt in ["-d", "--debug"]:
			debug = True
		elif opt in ["-o", "--outputfile"]:
			outputfile = arg
		elif opt in ["-i", "--input_data_file"]:
			print 'Using -i %s'%(arg)
			inputfile = arg
		elif opt in ["-t", "--tablename"]:
			tablename = arg
		elif opt in ["-b", "--basepath" ]:
			basepath = arg
		elif opt in ["-f", "--forest"]:
			if arg in [ "NW" , "NE", "S" ]:
				forest = ",".join([ k for k,v  in static_db.iteritems() if v[2] == 'NW' ])
			forest = arg
		elif opt in ["-p", "--product"]:
			product = arg
		elif opt in ["-y", "--year"]:
			year = arg
		elif opt in ["-j", "--jsonfilename"]:
			jsonfilename = arg
		elif opt in ["-l", "--layer"]:
			layer = arg
		elif opt in ["-n", "--planyear"]:
			planyear = arg
		elif opt in ["-r"]:
			if reportDetail == "Long":
				reportDetail = "Full"
			else :
				reportDetail = "Long"
		elif opt in ["--reportDetail"]:
			reportDetail = arg
		elif opt in ["-w", "--web-browser"]:
			web_browser = False
			pause_after_running = False
		elif opt in ["-s", "--submission"]:
			subID = arg
		elif opt in ["-e","--errors-only"]:
			errors_only = True
		elif opt in ["--no-spatial-output"]:
			spatial_output = False
		elif opt in ["--geojson-output"]:
			geojson_output = True
		elif opt in ["--raw-json-output"]:
			raw_json_output = True
		elif opt in ["--csv-output"]:
			csv_output = True

	#Load the fi_submission_#####.txt file in case the importer left useful data behind...

	submission_json = json.loads('{"dummy":true}')
	submission_filename_list = glob.glob(os.path.join(os.path.dirname( inputfile ), "fi_submission_*.txt")) + glob.glob(os.path.join(os.path.dirname(os.path.dirname( inputfile )), "fi_submission_*.txt")) +glob.glob(os.path.join(os.path.dirname(os.path.dirname(os.path.dirname( inputfile ))), "fi_submission_*.txt"))
	if submission_filename_list:
		for i in submission_filename_list :
			try:
				submission_json = json.load(open(i,'r'))
				print ("Found submission file {0}".format(submission_filename_list[0]))
				break
			except ValueError as e:
				print e
				print("Tried to open submission_filename_list[i] as a json file for submission metadata stuff, but it didn't work...")
	else :
		pass
		#print ("No Submission_json found")

	if 'year' in submission_json.keys() and year is None: year = submission_json['year']
	if 'product' in submission_json.keys() and product is None: product = submission_json['product']
	if 'fmu' in submission_json.keys() and forest is None: forest = submission_json['fmu']
	if 'fmu_id' in submission_json.keys() and fmuID is None: fmuID = submission_json['fmu_id']
	if 'id' in submission_json.keys() and subID is None: subID = submission_json['id']
	if 'planyear' in submission_json.keys() and planyear is None: planyear = submission_json['planyear']
	if 'submission_type' in submission_json.keys() and submission_type is None: submission_type = submission_json['submission_type']

	try:
		#Write out a batch file that will do this again...
		with  open( os.path.join( os.path.dirname(inputfile) , "Checker_Batch_File_Sample.bat" ) , "w") as bat_file:
			bat_file.write("REM - Sample Batch file for editting. Rename if you want to keep as a record of the proceedings\n")
			if product.upper() == 'AR':
				cmd = get_reg(r"Software\Classes\GeoPackage\Menu\shell\CheckAR2018\command")
			elif product.upper() == 'AWS':
				cmd = get_reg(r"Software\Classes\GeoPackage\Menu\shell\CheckAWS2018\command")
			elif product.upper() == 'FMP':
				cmd = get_reg(r"Software\Classes\GeoPackage\Menu\shell\CheckFMP2018\command")
			if cmd:
				bat_file.write(cmd.replace("%1",inputfile))
			bat_file.write("\n")
			bat_file.write(":: Version{0}\n".format(str(get_reg(r"Software\MNRF_NWR_RIAU_Checker\Version"))))

			bat_file.write("\n:: ")
			bat_file.write( usagemessage().replace("\n","\n::" ) )
			bat_file.write("\n")
	except :
		pass

	#extract year from the filename
	if inputfile:
		basename = os.path.basename( inputfile )

		if year is None:
			if basename[6:10].isdigit() :
				year = basename[6:10]
			elif basename[6:8].isdigit() :
				year = "20" + basename[6:8]
			else:
				year = "2121" #Right....


		if forest is None:
			#extract FMU_code from the filename
			fmuID = basename[2:5]
			#print ("Starting with FMU ID {0}".format(fmuID))
			if pg_db().FMUCodeConverter(fmuID) is not None :
				forest = pg_db().FMUCodeConverter(fmuID)
			elif fmuID == '999':
				forest = "The_Deep_Dark_Spoooky_One..."
			else : #Forest is None and FMUID is invalid - try the parent, grandparent, GGP, GGGP
				if ( pg_db().FMUCodeConverter(os.path.basename(os.path.dirname( inputfile ) ) ) ):
					# parent is a recognizable forest name...
					forest = os.path.basename(os.path.dirname( inputfile ) )
					fmuID = pg_db().FMUCodeConverter(forest)
				elif ( pg_db().FMUCodeConverter(os.path.basename(os.path.dirname( os.path.dirname( inputfile ) ) ) ) ) :
					# grandparent is a recognizable forest name...
					forest = os.path.basename(os.path.dirname( os.path.dirname( inputfile ) ) )
					fmuID = pg_db().FMUCodeConverter(forest)
				elif ( pg_db().FMUCodeConverter(os.path.basename(os.path.dirname( os.path.dirname( os.path.dirname( inputfile ) ) ) ) ) ) :
					# Ggrandparent is a recognizable forest name...
					forest = os.path.basename(os.path.dirname( os.path.dirname( os.path.dirname( inputfile ) ) ) )
					fmuID = pg_db().FMUCodeConverter(forest)
					if year == "2121" or not year.isdigit() :
						if os.path.basename(os.path.dirname( inputfile ) ).isdigit() :
							year = os.path.basename(os.path.dirname( inputfile ) )
				elif ( pg_db().FMUCodeConverter(os.path.basename(os.path.dirname( os.path.dirname( os.path.dirname( os.path.dirname( inputfile ) ) ) ) ) ) ) :
					# GGreat grandparent is a recognizable forest name...
					forest = os.path.basename(os.path.dirname( os.path.dirname( os.path.dirname( os.path.dirname( inputfile ) ) ) ) )
					fmuID = pg_db().FMUCodeConverter(forest)
					if year == "2121" or not year.isdigit() :
						if  os.path.basename(os.path.dirname( os.path.dirname( inputfile ) ) ).isdigit() :
							year = os.path.basename(os.path.dirname( os.path.dirname( inputfile ) ) )
				elif ( pg_db().FMUCodeConverter(os.path.basename(os.path.dirname( os.path.dirname( os.path.dirname( os.path.dirname( os.path.dirname( inputfile ) ) ) ) ) ) ) ) :
					# GGreat Great grandparent is a recognizable forest name...
					forest = os.path.basename(os.path.dirname( os.path.dirname( os.path.dirname( os.path.dirname( os.path.dirname( inputfile ) ) ) ) ) )
					fmuID = pg_db().FMUCodeConverter(forest)
					if year == "2121" or not year.isdigit() :
						if  os.path.basename(os.path.dirname( os.path.dirname( os.path.dirname( inputfile ) ) ) ).isdigit() :
							year = os.path.basename(os.path.dirname( os.path.dirname( os.path.dirname( inputfile ) ) ) )
				else:
					forest = "The Great Cabbage Patch"
					fmuID = '998'
					from stack.fileloader import fileloader
					f = fileloader(inputfile,openTable=False) #Look at the layer names for some ideas...
					for n in f.listLayers():
						if pg_db().FMUCodeConverter(n[2:5]) is not None:
							fmuID = n[2:5]
							forest = pg_db().FMUCodeConverter(fmuID)
							if year == '2121' : # ... The silly flag value
								if n[6:10].isdigit() :
									year = n[6:10]
								elif n[6:8].isdigit() :
									year = "20" + n[6:8]

		if fmuID is None:
			fmuID = pg_db().FMUCodeConverter(forest)
			if fmuID is None: fmuID = "UNK"
		elif isinstance(fmuID, int):
			pass
			#print ("FMU ID is integer==>{0}".format(fmuID ))
		elif not fmuID.isdigit():
			fmuID = pg_db().FMUCodeConverter(forest)
			if fmuID is None: fmuID = "UNK"
		#if fmuID not in [ pg_db().FMUCodeConverter(forest), '999', '998' ] : fmuID = "UNK"
		print ("Working with FMU ID {0}: {1} Forest".format(fmuID,forest))

	# Now run the checker...
	import initialize_checker
	s = initialize_checker.submission_checker(reportDetail=reportDetail,errors_only=errors_only,product=product )
	s.forest = str(forest).replace("_"," ")
	s.fmuID = fmuID

	if submission_type is not None and jsonfilename is None:
		jsonfilename= s.get_jsonfilename(user_spec = submission_type)
	if jsonfilename is None and year is not None and product is not None :
		jsonfilename= s.get_jsonfilename(product,year)

	if product.upper() == 'FMP' and year is not None and planyear is None :
		planyear = year
	elif year is not None and planyear is None:
		try:
			planyear = pg_db().get_plan_year(forest,year.split(',')[0])
		except KeyError:
			planyear = year - 1

	parentFolder = os.path.dirname(inputfile)
	if subID is None :
		subID = "00000"
		if (findSubID(parentFolder)):
			subID = findSubID(parentFolder)
		elif (findSubID(os.path.dirname(parentFolder))):
			parentFolder = os.path.dirname(parentFolder)
			subID = findSubID(parentFolder)
	s.subID = subID

	if outputfile=='clean.html':
		outputfile = os.path.join(parentFolder,"MU%s_%s_%s_%s_%s.html"%(fmuID,product,year,str(subID),datetime.date.today()))

	geojson_outputfile = None
	if geojson_output:
		geojson_outputfile = outputfile.replace('.html','.geojson')

	raw_json_outputfile = None
	if raw_json_output:
		raw_json_outputfile = outputfile.replace('.html','.json')

	csv_outputfile = None
	if csv_output or spatial_output:
		csv_outputfile = outputfile.replace('.html','.csv')

	checker_status = s.go(jsonfilename,tablefilename=inputfile,outputfilename=outputfile,year=year,planyear=planyear,doOnlyTable=tablename,layer=layer,geojson_outputfile=geojson_outputfile, raw_json_outputfile=raw_json_outputfile, csv_outputfile=csv_outputfile)

	try:
		# Tag the version number on the end... for good measure
		with open(os.path.abspath(s.outputFileName),"a") as f:
			a.write("\n<!-- Software version: {0} -->\n".format(str(get_reg(r"Software\MNRF_NWR_RIAU_Checker\Version"))))
	except:
		pass

	if web_browser:
		import webbrowser
		#webbrowser.open_new_tab("file://"+s.outputfile.replace("\\","/"))
		#print ("Opening HTML Output File {0}".format(os.path.abspath(s.outputFileName)))
		webbrowser.open(s.outputFileName)

	if pause_after_running:
		time.sleep(15)

	return checker_status

def usage():
	print usagemessage()

def usagemessage():
	return """
python context_checker.py
	[ --forest=fmu_name ]               { -f= }
	--product=AWS|AR|FMP                { -p= }
	[ --year=4_DIGIT_YEAR ]             { -y= }
	[ --planyear=4_DIGIT_YEAR ]         { -n= }
	--input_data_file=full_data_path_and_filename           { -i= }
	[ --tablename=specific_table_to_process ]               { -t= }
	[ --outputfile=filename ]                               { -o= }
	[ --reportDetail=Context|Short|Long|Full ]
	                                { -r once for Long, twice for Full }
	--jsonfilename=filename_to_the_technical_specification  { -j= }
	[ --layer=Layer_to_examine_inside_file ]                { -l= }
	[ --submission=fi_portal_id ]                           { -s= }
	[ --errors-only ]                                       { -e  }

Most options are optional, but the checker will not run without
	input_data_file
	jsonfilename (Although it might guess...)
	product

forest is the fmu name, with underscores instead of spaces. These are
	as used in the MNRF filesystem and FMPDS. It may be picked up from
	the context of the filename.

product is one of AWS, AR, and FMP.

year is the 4-digit year of the submission, specifying the beginning of
	government fiscal year in which the report is submitted. If the program
	can identify the year from the context of the filenames,
	it will.

planyear is the year of the plan for which this submission is made. For
	FMP's this will be the same as the year. For AR and AWS it will be
	looked up from a list if possible.

input_data_file is the full path to the data container.
	* If it is a multi-featureClass container ( GDB, geopackage ), tables
		will be found within
	* If it is a directory, spatial files ( SHP, Coverages ) will be
		found within
	* If it is a single featureClass file ( SHP ), it will be checked
		directly

outputfile is used to place the output. It may default to the same
	location as the data file, or to the directory you are in.

tablename, if specified, restricts checking to only that table from the json

layer, if specified, will override the name of the layer used for the test

reportDetail, if specified will use a long or full version of the report,
	as requested. Defaults to short.

submission_id is normally found in the file fi_submission_#.txt file in
	the folder, or it's parent.

errors-only reports only Stage 1 Validation Errors.

no-spatial-output does not produce the spatial output report (default CSV)

geojson-output provides spatial output report in GeoJSON output
raw-json-output provides a simplified version of the JSON output
csv-output provides a CSV File with the spatial output

"""

if __name__ == '__main__':
	import sys
	#print sys.argv
	if len(sys.argv) == 1:
		#Let's assume we are running this from the IDE, shall we?
		#testpath = r'\GIS\Black_Sturgeon_FMP_2011\MU815_2011_FMP_P1.gpkg'
		#testpath = r'C:\GIS\FMP\Lake_Nipigon\EFRI\2021\Draft_LakeNipigon_silvatech_submission_07_11_2016.gdb'
		testpath = r'C:\GIS\FMP_New\Ottawa_Valley\AWS\2019\LAYERS\MU780_AWS.gdb'
		if os.path.exists(testpath):
			sys.exit(main(['-i',testpath,'-p','AWS','-o','test.html','-j',r'..\tech_spec\2018\aws.json' ] ))
			#sys.exit(main(['-i',testpath,'-y','2011','-p','FMP','-o','test.html','-j',r'..\tech_spec\2009\fmp.json', '-l' , 'pcm', '-t', 'PlanningComposite' ] ))
#			sys.exit(main(['-i',testpath,'-y','2021','-p','PCI','-o','test.html','-j',r'..\tech_spec\2017\PCI.json', '-l' , 'Forest_Polygons','-t','pci'] ))
	sys.exit(main(sys.argv[1:]))
