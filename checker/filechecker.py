
#filechecker.py
# This forms the basis of rolling through all rows, running a list of
# procedures on each one to test validity.
#
# It ought to work on arcpy or ogr, subject to appropriate data format
#

import sys
import time
import json
import os
from collections import OrderedDict

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from jsonloader import jsonloader
from stack.fileloader import fileloader
#from stack.html_functions import writeHTMLstring, writeHTML, write2File
from stack.log_functions import write2LogFile

from spcomp import test_spcomp, test_SpeciesInList, get_leadspc
from fimdate import fimdate

from stack.pg_db import pg_db
import stack.typeLUT

class tablechecker( jsonloader ):
	json = ''
	tablejson = ''
	rowvalidation = dict()
	shapefile = None
	debug = True
	resultsdetectordictionary = {}
	tablevalidationfailures = []
	status = 0

	shplist = []

	def __init__(self,jsonfilename,tablefilename,tabletype,year=0,planyear=0,debug=False,spatialdataformat=None, shortName = None, SRName = None, layer=None, reportDetail=None ):
		self.json = self.getjson(jsonfilename)
		self.basename = os.path.basename(tablefilename) #This is the name overloaded SHORT version of the filename
		self.SRName = SRName
		self.tablejson = self.findTable(tabletype)
		#print ("\tTablechecker Opening {0}!{1}.{2}=>{3}".format(tablefilename,tabletype,layer,self.tablejson['geometry_type'])) #FIXME Print

		if layer is None:
			self.shapefile = fileloader(tablefilename,layer=tabletype,geometry_type=self.tablejson['geometry_type'])
		else:
			self.shapefile = fileloader(tablefilename,layer=layer,geometry_type=self.tablejson['geometry_type'])

		if self.shapefile is None:
			print("\tTablechecker cannot open data file")
			return None
		#print ("\tOpened shapefile")

		self.basename = os.path.basename(self.shapefile.effective_filename)
		self.shortName =  self.shapefile.layername
		self.year = year
		self.planyear = planyear
		self.debug = debug
		self.LoadRowValidations()
		self.resultsdetectordictionary = OrderedDict()

		if spatialdataformat is not None:
			raise DeprecationWarning("Use of spatialdataformat parameter is deprecated.")

		self.spatialdataformat = self.shapefile.get_spatialdataformat()

		self.SRName = self.shapefile.SRName
		self.SRAuth = self.shapefile.SRAuth
		self.SRCode = self.shapefile.SRCode

		self.shplist = list()

	def __exit__(self, *err):
		self.shapefile.close()

	def validateTable_HTMLfragment(self,logFile=None,debug=False):
		tvs = self.LoadTableValidations()

		try:
			fails = self.TableValidations()
			if not fails:
				msg = '\n'.join( ["<h3>Table Validation</h3>",
					"<pre>",
					#("\t" + "\n\t".join( tvs ) ) if debug else "",
					"<p>No errors found.</p>",
					"</pre>\n"])
			else:
				msg = "<h3>Table Validation</h3>\n<ul>\n"
				msg += "<pre>\n" #+ str(self.tablejson['name'])+"\n"
				if debug : msg += ("\t" + "\n\t".join( tvs ) )
				#msg += "\n".join( self.LoadTableValidations() if self.LoadTableValidations() else list() )
				msg += "\n</pre>"
				iErrorCount = len(fails)
				for f in fails:
					if isinstance(f,str) or isinstance(f,unicode):
						msg+= "\t<li>%s</li>\n"%(f)
					else:
						msg+= "\t<ul>\n"
						for ff in f:
							msg+= "\t\t<li>%s</li>\n"%(ff)
						msg+= "\t</ul>\n"
				msg+= "</ul>\n"
				self.status = 1 # Table fails mean Stage 1 Validation Errors
		except KeyError as e:
			return ''

		return msg

	def LoadTableValidations(self):
		try:
			if isinstance(self.tablejson['Validation:']['python'] , str) or isinstance(self.tablejson['Validation:']['python'] , unicode) :
				valstatements = [ self.tablejson['Validation:']['python'] ]
			else:
				valstatements = self.tablejson['Validation:']['python']
			self.tablevalidation = valstatements
		except KeyError as e:
			self.tablevalidation = list()

		if self.tablevalidation is None: self.tablevalidation = list()

		fields=self.shapefile.fields
		fieldtypes= self.shapefile.fieldtypes
		self.tablevalidation.append("['Field <mark>%s</mark> not found in table'%(f)  for f in self.getMandatoryFieldList() if f not in "+str(fields)+" ] ")

		for s in self.getMandatoryFieldList():
			if s not in fields: continue
			#getFieldType(self, fieldname=None, fieldsjson=None)
			statement = "['Field <mark>{field}</mark> must be type {mandtype}, not {fieldtype}' for f in range(1) if '{mandtype}' != '{fieldtype}' ]".format(field=s, mandtype=self.getFieldType(fieldname=s).lower(), fieldtype=fieldtypes[s].lower())
			#statement = "['Field %s must be type %s'%(field,self.tablejson['fields'][field]) for field in self.getMandatoryFieldList() if f in fields if fieldtypes[field] != self.getFieldType(self.tablejson,field)  ] "
			self.tablevalidation.append(statement)
		return self.tablevalidation

	def TableValidations (self):
		valstatements = self.LoadTableValidations() if self.LoadTableValidations() else list()

		#valstatements.append("[ 'POLYID Field values must be unique' for f in ['POLYID' ] if not self.uniqueValueTest(f) ] ")
		#Add implicit field is mandatory test
		fields=self.shapefile.fields
		fieldtypes= self.shapefile.fieldtypes
		#valstatements.append("[ 'Field %s not found in table'%(f)  for f in self.getMandatoryFieldList() if f not in "+str(fields)+" ] ")

		#statement = "['Field %s must be type %s'%(field,self.tablejson['fields'][field]) for field in self.getMandatoryFieldList() if f in fields if fieldtypes[field] != self.getFieldType(self.tablejson,field)  ] "
		#valstatements.append(statement)


		retlist = list()
		for f in valstatements:
			try:
				#print f
				result = eval(''+f+'\n')
				retlist.extend(result)
			except KeyError as e:
				pass
			except KeyboardInterrupt as e:
				break

		self.tablevalidationfailures = retlist
		return self.tablevalidationfailures

		#return [eval(x) for x in [''+f+'\n' for f in valstatements ] ]

	def LoadRowValidations(self):
		"""Loads Row Validations for the table found in self.tablejson.
		Translates some elements from the variable substitutions in the mindmap
		"""
		self.rowvalidation = dict()

		if self.shapefile:  #FIXME - do we even use this? Really?
			fields=self.shapefile.fields

		try:
			fieldsjson=self.tablejson['fields']
		except KeyError as e:
			#This table has no fields - ignore
			return [ ]

		#DONE - when one field, comes back as the field entry, not a list (AR SGR Update table)
		if not isinstance(fieldsjson,list):
			fieldsjson = [ fieldsjson ]

		valstatements = [ ]
		estatements = [ ]
		for field in fieldsjson:
			fieldname = field['name']

			del valstatements[:]
			try:
				if isinstance(field['Validation:']['python'] , str) or isinstance(field['Validation:']['python'] , unicode) :
					valstatements = [ field['Validation:']['python'] ]
				elif  field['Validation:']['python'] :
					valstatements = field['Validation:']['python']
			except KeyError as e:
				#No Validations worth bothering with
				del valstatements[:]
				pass

			try:
				del estatements[:]
				if isinstance(field['Validation:']['english'], str) or isinstance(field['Validation:']['english'] , unicode) :
					estatements = [ field['Validation:']['english'] ]
				elif isinstance(field['Validation:']['english'],list) :
					estatements = field['Validation:']['english']

				for v in estatements:
					#Check for well-known things
					#[ "ST1: The attribute population must follow the correct coding scheme","ST1: A blank or null value is not a valid code" , "ST1: Blank or null values are not a valid code" ]

					if 'A blank or null value is not a valid code when POLYTYPE is equal to FOR' in v or 'Blank or null values are not a valid code when POLYTYPE is equal to FOR' in v or 'A zero or null value is not a valid code when POLYTYPE is equal to FOR' in v :
						valstatements.append("""['ST1: <mark>${name}</mark> must not be blank or null...FOR...' for row in [row] if row['${name}'] in [ None, '', ' ', 0 ] and row['POLYTYPE'] == 'FOR' ]""")
					elif 'A blank or null value is not a valid code when  AVAIL is equal to A' in v or 'Blank or null values are not a valid code when AVAIL is equal to A' in v or 'A zero or null value is not a valid code when AVAIL is equal to A' in v :
						valstatements.append("""['ST1: <mark>${name}</mark> must not be blank or null...AVAIL...' for row in [row] if row['${name}'] in [ None, '', ' ', 0 ] and row['AVAIL'] == 'A' ]""")
					elif 'A blank or null value is not a valid code' in v or 'Blank or null values are not a valid code' in v  or 'A zero or null value is not a valid code' in v  :
						valstatements.append("""['ST1: <mark>${name}</mark> must not be blank or null...' for row in [row] if row['${name}'] in [ None, '', ' ', 0 ] ]""")


					if 'The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR' in v:
						try:
							vallist = self.getFieldCoding(fieldname=field['name'],fieldsjson=fieldsjson,allow_NULL=True,force_string=( field['type'].upper() in [ 'CHARACTER', 'TEXT', 'STRING' ] ) , force_upper=( field['type'].upper() in [ 'CHARACTER', 'TEXT', 'STRING' ] ) )
							if vallist:
								valstatements.append("""['ST1: <mark>${name}</mark> must follow the correct coding scheme...FOR...' for row in [row] if row['${name}'] not in %s and row['POLYTYPE'] == 'FOR' ]"""%(str(vallist)))
						except KeyError as e:
							#No Validations worth bothering with
							pass
					elif 'The attribute population must follow the correct coding scheme' in v:
						try:
							vallist = self.getFieldCoding(fieldname=field['name'],fieldsjson=fieldsjson,allow_NULL=True,force_string=( field['type'].upper() in [ 'CHARACTER', 'TEXT', 'STRING' ] ), force_upper=( field['type'].upper() in [ 'CHARACTER', 'TEXT', 'STRING' ] ) )
							if vallist:
								valstatements.append("""['ST1: <mark>${name}</mark> must follow the correct coding scheme...' for row in [row] if row['${name}'] not in %s ]"""%(str(vallist)))
						except KeyError as e:
							#No Validations worth bothering with
							pass
						except TypeErrpr as e:
							#Missing 'name'?
							print "Your field is represented as a list???"
							print e


					if 'The attribute must be Null when POLYTYPE is not equal to FOR' in v or 'The attribute must be 0 when POLYTYPE is not equal to FOR' in v or 'The attribute must be zero or null when POLYTYPE is not equal to FOR' in v:
						try:
							valstatements.append("""['ST1: <mark>${name}</mark> must be null if POLYTYPE is not FOR...' for row in [row] if row['${name}'] not in [ None, '', ' ', 0 ] and row['POLYTYPE'] != 'FOR' ]""" )
						except KeyError as e:
							#No Validations worth bothering with
							pass

			except KeyError:
				pass

			self.rowvalidation[fieldname] = [ self.do_variable_substitutions(val,fieldname)  for val in valstatements ]
		return self.rowvalidation

	def do_variable_substitutions(self,inputstring,fieldname,tablename=None,year=None,planyear=None):
		"""
			${name}			str(fieldname)
			${year}			year
			${planyear}		planyear
			${tablename}	tablename
			${list_NULL}	[ None, '', ' ', 0 ]
			${NINN}			row['${name}'] not in ${list_NULL}
			${NIN}			row['${name}'] in ${list_NULL}
			${FOR}			 row['POLYTYPE'] == 'FOR'
			${VERT}			 row['VERT'] in [ 'TO', 'TU', 'MO', 'MU' ]
			${:}			 for row in [row] if
			${:NF}			${:} ${NINN} and ${FOR} and
			${:F}			${:} ${FOR} and
			${:N}			${:} ${NINN} and
			${:V}			${:} ${VERT} and
			${:NV}			${:} ${NINN} and ${VERT} and
			${PCM}			 '${tablename}' in ['PlanningComposite' ]
			${PCI}			 '${tablename}' in ['PlanningCompositeInventory' ]
			${PCI_BMI_OPI}	 '${tablename}' in ['PlanningCompositeInventory','BaseModelInventory','OperationalPlanningInventory' ]
			${PCI_BMI}		 '${tablename}' in ['PlanningCompositeInventory','BaseModelInventory' ]
			${FRI_PCI}		 '${tablename}' in ['ForestResourcesInventory','EnhancedForestResourcesInventory','PlanningCompositeInventory' ]
			${FRI}			 '${tablename}' in ['ForestResourcesInventory','EnhancedForestResourcesInventory' ]
			${BMI}			 '${tablename}' in ['BaseModelInventory' ]
			${BMI_OPI}		 '${tablename}' in ['BaseModelInventory','OperationalPlanningInventory' ]
		"""
		tablename=str(self.tablejson['name']) if tablename is None else str(tablename)
		year = str(self.year)                 if year is None      else str(year)
		planyear = str(self.planyear)         if planyear is None  else str(planyear)
		if inputstring is None: return '' # So that we can handle None tests properly...

		return inputstring.replace("${PCM}"," '${tablename}' in ['PlanningComposite' ] ")\
			.replace("${FRI}"," '${tablename}' in ['ForestResourcesInventory','EnhancedForestResourcesInventory' ] ")\
			.replace("${PCI}"," '${tablename}' in [ 'PlanningCompositeInventory' ] ")\
			.replace("${BMI}"," '${tablename}' in ['BaseModelInventory' ] ")\
			.replace("${FRI_PCI}"," '${tablename}' in ['ForestResourcesInventory','EnhancedForestResourcesInventory','PlanningCompositeInventory' ] ")\
			.replace("${PCI_BMI_OPI}"," '${tablename}' in ['PlanningCompositeInventory','BaseModelInventory','OperationalPlanningInventory' ] ")\
			.replace("${PCI_BMI}"," '${tablename}' in ['PlanningCompositeInventory','BaseModelInventory' ] ")\
			.replace("${BMI_OPI}"," '${tablename}' in ['BaseModelInventory','OperationalPlanningInventory' ] ")\
			.replace("${:N}","${:} ${NINN} and ")\
			.replace("${:F}","${:} ${FOR} and ")\
			.replace("${:V}","${:} ${VERT} and ")\
			.replace("${:NF}","${:} ${NINN} and ${FOR} and ")\
			.replace("${:NV}","${:} ${NINN} and ${VERT} and ")\
			.replace("${:}"," for row in [row] if ")\
			.replace("${NINN}"," row['${name}'] not in ${list_NULL} ")\
			.replace("${NIN}"," row['${name}'] in ${list_NULL} ")\
			.replace('${name}',str(fieldname))\
			.replace('${year}',year)\
			.replace('${planyear}',planyear)\
			.replace('${tablename}',tablename)\
			.replace("${list_NULL}"," [ None, '', ' ', 0 ] ")\
			.replace("${FOR}"," row['POLYTYPE'] == 'FOR' ")\
			.replace("${VERT}"," row['VERT'] in [ 'TO', 'TU', 'MO', 'MU' ] ")

	def validateRows_HTMLruleSummary(self,limit=None,debug=False):
		if debug: self.debug=debug
		msg = [ ]
		msg.append("<details><summary>Row-Level Rules in %s</summary><pre>%s\n%s\n</pre></details>"%(str(self.tablejson['name']),str(self.tablejson['name']),
			"\n".join([ "%s:\n%s"%(k, "\n".join([ "\t%s"%vv for vv in v ]) ) for k,v in self.rowvalidation.iteritems() ])
			)
			)
		return msg


	def validateRows_HTMLfragment(self,limit=None,debug=False, errors_only = False, save_spatial=False ):
		if debug: self.debug=debug
		msg = [ ]

		i=0
		errorcount = 0

		nulldetectorlist = set(self.rowvalidation.keys()).intersection(set(self.shapefile.fields))
		nulldetectorstatement = ' and '.join( [ "(str(row['%s']).strip() in ['0','','None','0.0','0.00'])"%(f)  for f in nulldetectorlist ])

		while True:
			try:
				row = self.shapefile.row(do_upper=True)
			except StopIteration:
				break
			except KeyError as e:
				pass
			#if (row['POLYTYPE'] == 'FOR'): 	print row

			#ignore rows with all None or strip() == '' or 0 values
			try:
				if self.shapefile.knows_about_null :
					pass
				elif  eval( nulldetectorstatement ) :
					continue
			except KeyError as e:
				pass
			except SyntaxError as e:
				pass

			i+=1
			if 'POLYID' in self.shapefile.fields :
				polyid = row['POLYID']
			elif 'OBJECTID' in self.shapefile.fields :
				polyid = row['OBJECTID']
			else:
				polyid = str(i)

			#print polyid
			row = self.make_row_KeyError_safe(row)

			#fails =  [eval(x) for x in [''+f+'\n' for f in self.rowvalidation ] ]
			try:
				for fieldname,listOfValidations in self.rowvalidation.iteritems()	:
					#print fieldname,listOfValidations
					#if fieldname.upper() in row.keys() :
						#Only testing fields that exist, mostly...
						#fails = [eval(x) for x in listOfValidations ]
					if fieldname not in self.shapefile.fields:
						#print ("Not doing tests on missing field {0}".format(fieldname))
						continue

					for test in listOfValidations :
						if test in [u'python\n'] : continue #Ignore blanks - this is how they look...

						for f in self.doASingleRowValidation(test,row,polyid="") :
							#if self.debug: pyprint( "<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>\n"%(polyid,str(row[fieldname]),f,test) )

							try:
								if   "ST1" in f[0:3]:
									msg.append( "<tr><td>%s</td><td>%s</td><td>Error - %s</td></tr>\n"%(polyid,str(row[fieldname]).replace(' ','&nbsp;'),f) )
									self.status = 1
									error_class = 1
								elif "ST2" in f[0:3]:
									if not errors_only: msg.append( "<tr><td>%s</td><td>%s</td><td>Warning - %s</td></tr>\n"%(polyid,str(row[fieldname]).replace(' ','&nbsp;'),f) )
									if self.status == 0: self.status = 2
									error_class = 2
								elif "RODFNST1" in f[0:8]:
									msg.append( "<tr><td>%s</td><td>%s</td><td>Error (ROD Footnote)- %s</td></tr>\n"%(polyid,str(row[fieldname]).replace(' ','&nbsp;'),f) )
									error_class = 3
								elif "RODFNST2" in f[0:8]:
									msg.append( "<tr><td>%s</td><td>%s</td><td>Warning (ROD Footnote)- %s</td></tr>\n"%(polyid,str(row[fieldname]).replace(' ','&nbsp;'),f) )
									error_class = 4
								else:
									msg.append( "<tr><td>%s</td><td>%s</td><td>%s</td></tr>\n"%(polyid,str(row[fieldname]).replace(' ','&nbsp;'),f) )
									error_class = 0
								if save_spatial:
									try:
										pnt = row['__centroid__']
										self.shplist.append((pnt,error_class,msg[-1]))
									except IndexError :
										#When there are no errors reported....
										pass
							except KeyError as e:
								#msg.append(  "<tr><td>%s</td><td>%s</td><td>Missing Field - %s - %s</td></tr>\n"%(polyid,str(fieldname),f, str(e)) )
								pass
							errorcount += 1

							try:
								self.resultsdetectordictionary[str(f)] = self.resultsdetectordictionary[str(f)] + 1
							except KeyError as e:
								self.resultsdetectordictionary[str(f)] = 1

				if limit is None:
					pass
				elif i > limit :
					break
				elif errorcount > 100  and limit == "Short":
					break
				elif errorcount > 1000 and limit == "Long":
					break

			except KeyboardInterrupt:
				break

		self.totalNumRecords = i

		msgheader = "<h3>Row Validations</h3>\n"
		if errorcount > 0:
			if limit == "Full":
				msgheader+="<details open>\n<summary>Row-Level Errors in %s</summary>\n"%(str(self.tablejson['name']) )
			else:
				msgheader+="<details>\n<summary>Row-Level Errors in %s</summary>\n"%(str(self.tablejson['name']) )
			msgheader += "<table>\n<tr>"
			if 'POLYID' in self.shapefile.fields :
				msgheader+= "<th>POLYID</th>"
			elif 'OBJECTID' in self.shapefile.fields :
				msgheader+= "<th>OBJECTID</th>"
			else:
				msgheader+= "<th>Row #</th>"
			msgheader+= "<th>Value</th>"
			msgheader+= "<th>Failure Statement</th></tr>\n"
			msg.insert(0, msgheader)
			msg.append("</table>\n</details>\n")
		else:
			msg.insert(0, msgheader)
		msg.append( "<p>Count of Rows Checked: %d</p>\n"%(i) )
		msg.append( "<p>Count of Row-level Errors: %d</p>\n"%(errorcount) )
		return msg

	def doASingleRowValidation(self, test, row, polyid=None, debug=False ):
		#print test
		#row = self.make_row_KeyError_safe(row)
		try:
			results = eval(test)
		except TypeError as e:
			#print "POLYID %s has TypeError: %s in %s \n\twith value\t %s"%(polyid,e,test,str(row))
			#print "POLYID %s has TypeError: %s in %s"%(polyid,e,test)
			if str(e).strip() in [ "argument of type 'NoneType' is not iterable" ,
					"'NoneType' object has no attribute '__getitem__'"
					] :
				results = [ ]
			else:
				results = [ "POLYID %s has TypeError: %s "%(polyid,e) ]
		except ZeroDivisionError as e:
			print "POLYID %s has ZeroDivisionError: %s in %s "%(polyid,e,test)
			results = [ "POLYID %s has ZeroDivisionError: %s "%(polyid,e) ]
		except NameError as e:
			print "POLYID %s has NameError: %s in %s "%(polyid,e,test)
			results = [ "POLYID %s has NameError: %s "%(polyid,e) ]
		except AttributeError as e:
			print "POLYID %s has AttributeError: %s in %s "%(polyid,e,test)
			results = [ "POLYID %s has AttributeError: %s "%(polyid,e) ]
		except KeyError as e:
			if debug: print "POLYID %s has KeyError- Missing Field: %s in %s "%(polyid,e,test)
			#results = [ "KeyError- Missing Field: %s in %s "%(e,test) ]
			results = [ ]
		except SyntaxError as e:
			print "ID  %s has SyntaxError: %s in %s "%(polyid,e,test)
			results = [ str(e) ]
		except ValueError as e:
			print "ID  %s has ValueError: %s in %s "%(polyid,e,test)
			results = [ str(e) ]

		#for r in results:
		#	if r == "'o'":
		#		print("Test {test} produced These Results:\n{res}".format(test=test,res=results))
		return results

	def make_row_KeyError_safe( self,row ):
		for r in list(set(self.rowvalidation.keys()) - set(row.keys())):
			row[r] = None
		return row

	def uniqueValueTest	(self,fieldname):
		return self.shapefile.uniqueValueTest(fieldname)

	def getFieldType(self, fieldname=None, fieldsjson=None):
		"""
		Returns "type" tag in a field
		"""
		if not fieldsjson :
			try:
				fieldsjson=self.tablejson['fields']
			except KeyError as e:
				#This table has no fields - ignore
				return None

		#DONE - when one field, comes back as the field entry, not a list (AR SGR Update table)
		if not isinstance(fieldsjson,list):
			fieldsjson = [ fieldsjson ]

		if fieldname not in  [ f['name'] for f in fieldsjson ] : return None

		field =  [ f for f in fieldsjson if fieldname == f['name'] ][0]

		if 'TYPE' in [ k.upper() for k in field.keys() ]:
			fieldtype =  [ v for k,v in field.iteritems() if k.upper() == 'TYPE' ][0]
			return stack.typeLUT.type_match(fieldtype)
		else: return None


	def getFieldCoding(self,fieldname=None,fieldsjson=None,allow_NULL=True,force_string=False,force_upper=False):
		"""
		Detects "Values" tag in a field, and returns field-->Values-->name
		"""
		if not fieldsjson :
			try:
				fieldsjson=self.tablejson['fields']
			except KeyError as e:
				#This table has no fields - ignore
				return []

		#DONE - when one field, comes back as the field entry, not a list (AR SGR Update table)
		if not isinstance(fieldsjson,list):
			fieldsjson = [ fieldsjson ]

		if fieldname not in  [ f['name'] for f in fieldsjson ] : return list()

		field =  [ f for f in fieldsjson if fieldname == f['name'] ][0]

		if 'Values' in field.keys():
			vList = field['Values']
		elif 'values' in field:
			vList = field['values']
		else: return []

		if force_string and force_upper:
			fList =  [ str(v['name']).upper() for v in vList ]
		elif force_string and not force_upper:
			fList =  [ str(v['name']) for v in vList ]
		else:
			fList =  [ v['name'] for v in vList ]

		if allow_NULL:
			fList.extend([None,'', ' ', 0])
		return fList


	def contextTest(self):
		"""This function runs SQL test from the sql validation section
			Use the ${WKT} variable in the query to include the shape in the query as WKT.
			Use the ${PLAN_ID} variable to indicate the most recent FMP to which this AR/AWS relates.
			Use the ${PLAN_AWS_ID} variable to indicate a list of AWS and Plan submission id's.

			We should sanitize the inputs, but for now... #FIXME Bobby Tables
		"""
		pass

	def validate_summary(self):
		'''returns a dictionary  like this one: {"MU615_17AGP00":  { 'prj':3161, 'longname':"Existing Aggregate Pits", ...}}'''
		#Starts as an assumed valid summary report
		summaryDictRow =\
			{
				'name':os.path.basename(self.shapefile.effective_filename),
				'lyrName':self.shortName,
				'prj': self.shapefile.SRName,
				'fieldValidation': 'Valid',
				'fieldDefinitionCom': 'All mandatory fields exist.',
				'recordValidation':  'Valid',
				'recordValidationCom': None ,
				'reference': ''
			}

		if len(self.tablevalidationfailures) > 0:
			summaryDictRow['fieldValidation']    = 'Invalid'
			summaryDictRow['fieldDefinitionCom'] = '<ul>\n'+'\n<li>'.join( self.tablevalidationfailures  ) + '\n</ul>'

		Error_Statements = [ '<li>Error on %s record(s): %s</li>'%(v,k) for k,v in self.resultsdetectordictionary.iteritems() if "ST1"     in k ]
		Warn_Statements  = [ '<li>Warning on %s record(s): %s</li>'%(v,k)      for k,v in self.resultsdetectordictionary.iteritems() if "ST1" not in k ]

		if len(Error_Statements) > 0 :
			summaryDictRow['recordValidation'] = 'Invalid - Stage 1'
		elif len(Warn_Statements) > 0 :
			summaryDictRow['recordValidation'] = 'Valid with Concerns - Stage 2'

		if len(self.resultsdetectordictionary) > 0:
			summaryDictRow['recordValidationCom'] = '%s\n\t<ul>\n\t%s\n\t%s</ul>'%(
				'Total number of records: %s'%(str(self.totalNumRecords)) ,
				"\n\t".join(Error_Statements ) ,
				"\n\t".join(Warn_Statements  )
				)
			summaryDictRow['recordValidation'] = 'Invalid'
		else:
			summaryDictRow['recordValidationCom'] = 'Total number of records: %s'%(str(self.totalNumRecords))
		return summaryDictRow

	def validate_summary_HTML_Header(self):
		return '''
	<table id="t02" style="font-family:arial;">
	  <tr>
		<th>Layer Short Name</th>
		<th>Field Validation</th>
		<th>Field Validation Comments</th>
		<th>Record Validation</th>
		<th style="width: 70%;" >Record Validation Comments</th>
	  </tr>
	  '''

	def validate_summary_HTMLfragment(self):
		v = self.validate_summary()
		fclasscode = 'p01' if str(v['fieldValidation']) == 'Valid' else 'p03'
		rclasscode = 'p01' if str(v['recordValidation']) == 'Valid' else 'p03'
		return '\n'.join( ['<tr>',
			'<td>%s</td>'%(v['lyrName']),
			'<td><p id="%s">  %s  </p></td>'%(fclasscode,v['fieldValidation']),
			'<td>%s</td>'%(v['fieldDefinitionCom']),
			'<td><p id="%s">  %s  </p></td>'%(rclasscode,v['recordValidation']),
			'<td style="width: 70%%;">%s</td>'%(v['recordValidationCom']),
			'</tr>'
			] )

	def validate_summary_HTML_Footer(self):
		return '</table>'


if __name__ == "__main__":
#	c = get_iterable_rows(filename=r'Z:\GIS\DonNixon\onaman_addition_04042006.shp')
#	print get_next_row(c)
	verbose = True
	starttime = time.clock()
	if verbose: print "Starttime:",starttime
	###############################################################
	# START OF TESTING CODE
	###############################################################

#	c = fileloader(filename=r'Z:\GIS\DonNixon\onaman_addition_04042006.shp')
#	while True:
#		try:
#			r = c.row()
#			#if r is None: break
#			print r
#		except StopIteration:
#			break
	jsonfilename=r'w:\epcm\roundtrip.json'
	jsonfilename=r'C:\Users\carran\Documents\src\ePCM\roundtrip.json'
	jsonfilename=r'S:\ROD\Northwestregion\RIAU\Data\Extended_Data\Forest\Projects\ePCM\roundtrip.json'

	tablefilename=r'Z:\GIS\EPC\EPCtest.shp'
	tablefilename=r'C:/Users/carran/Documents/GIS/EPCTest.shp'
	tablefilename=r'C:/Users/carran/Documents/GIS/wab_efri.shp'
	tablefilename=r'S:\ROD\Northwestregion\RIAU\Data\Extended_Data\Forest\FMP\Wabigoon\FMP\2018\Wabigoon_PCM2018_shp\mu130_18pcm00.shp'
	tablefilename=r'Z:\GIS\Wabigoon_2018\Wabigoon_PCM2018_shp\mu130_18pcm00.shp'
	tablefilename=r'S:\ROD\Northwestregion\RIAU\Data\Extended_Data\Forest\FMP\Kenora\AR\2015\E00\mu644_15hrv00'
	#tabletype = r'Enhanced Planning Composite'
	tabletype='HRV'
	jsonfilename=r'S:\ROD\Northwestregion\RIAU\Data\Extended_Data\Forest\Projects\ePCM\json_to_mindmap\AR_test.json'
	jsonfilename = os.path.join(os.path.split(os.path.split(os.path.abspath(__file__))[0])[0],'AR.json')
	#print "jsonfilename: %s"%(jsonfilename)

	#tablefilename=r'S:\ROD\Northwestregion\RIAU\Data\Extended_Data\Forest\FMP\Kenora\AR\2015\_data\FMP_Schema.gdb\AnnualReports\SilviculturalGroundRuleUpdate'
	#tabletype='SilviculturalGroundRuleUpdate'


	t = tablechecker(jsonfilename=jsonfilename,
		tablefilename=tablefilename,
		tabletype=tabletype)


	#print "POLYID UNIQUE? %s"%(t.shapefile.uniqueValueTest('POLYID'))

	tablevalidation_output = t.validateTable_HTMLfragment()
	rowvalidation_output = t.validateRows_HTMLfragment(limit=None)

	f = open('tablereport.html','w')
	f.write('<html><head>\n')
	f.write('<link rel="stylesheet" type="text/css" href="FMPstyle.css">')
	f.write('<title>%s test for %s</title>\n'%(tabletype,tablefilename))
	f.write('<META HTTP-EQUIV="refresh" CONTENT="100">\n')
	f.write('</head><body>\n')
	f.write('<h1>%s test for %s</h1>\n'%(tabletype,tablefilename))
	f.write( tablevalidation_output )
	f.write( rowvalidation_output )
	f.write('</body></html>\n')

	###############################################################
	# END OF TESTING CODE
	###############################################################
	endtime = time.clock()
	if verbose: print "Endtime:",endtime
	if verbose: print "Seconds Elapsed:", endtime - starttime
	if verbose: print "Minutes Elapsed:", round(( endtime - starttime ) /60,0)



