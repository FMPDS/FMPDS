""" This file contains some functions that evaluate SPCOMP"""
from collections import OrderedDict
from FMPError import FMPError
from itertools import groupby

SpecListInterp = [ 'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn','CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf', 'OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX',     'OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX', 'Sb','Sw','la','Wb','Wi' ]

SpecListPlot = [ 'AL','Aq','Ap','Ag','Bc','Bp','Gb','Bb','Cat','Cc','Cm', 'Cp','Cs','Ct','Er','Eu','Hk','Ht','Hl','Hb','Hm','Hp', 'Hs','Hc','Kk','Le','Lj','Bl','Ll','Lb','Gt','Mb','Mf',     'Mm','Mt','Mn','Mp','AM','Ema','Mo','Obl','Ob','Och','Op', 'Os','Osw','Pa','Pn','Pp','Pc','Ph','Pe','Red','Ss','Sc','Sk','Sn','Sr','Sy','Tp','Haz']

SpeciesList_Overall = list(set(SpecListPlot + SpecListInterp))

#####################################
# Tests for filechecker
#####################################
def test_spcomp(compstr):
	if compstr is None: return [ "Empty SPComp" ]
	try:
		valid_spcomp(compstr)
	except FMPError as e:
		return [ str(e) ]
	return [ None ]

def test_SpeciesInList(compstr, slist=SpeciesList_Overall):
	"""
	Returns a list of species not in the list (or default list)
	"""
	if compstr is None: return ["Empty SPComp" ]
	try:
		compd = spcomp(compstr.strip(' '))
	except FMPError as e:
		return [] # [ str(e) ]
	#print compstr, compd, str([ s for s in compd if not( s in [spcode.upper().strip() for spcode in slist ] )  ])
	return [ s for s in compd if not( s in [spcode.upper().strip() for spcode in slist ] )  ]

def get_leadspc(compstr):
	if compstr in [ None, '', ' ', 0 ] : return []

	try:
		comp_dictionary = spcomp(compstr)
		increasing_spcomp(comp_dictionary)
		comp_value = comp_dictionary[comp_dictionary.keys()[0]]
		leadspc = [ k.upper() for k,v in comp_dictionary.iteritems() if v == comp_value ]
		return  leadspc
	except FMPError.e.SPCompHasNonNumericComposition :
		return []
	except :
		pass
	try:
		comp_dictionary = spcomp(spcomp_reformat(compstr))
		increasing_spcomp(comp_dictionary)
		comp_value = comp_dictionary[comp_dictionary.keys()[0]]
		leadspc = [ k.upper() for k,v in comp_dictionary.iteritems() if v == comp_value ]
		return  leadsp
	except:
		pass
	return []


#####################################
# END Tests for filechecker
#####################################

def valid_spcomp(compstr):
	"""
	Tests species composition string to see if it is valid.
	Calls other tests in file...
	"""
	#Do pre-interp tests
	# Is it too long (more than 60 chars ==> 10 species? )
	max_spcomp(compstr)

	#interp it
	s = spcomp(compstr)

	#Do post interp tests
	# Are there duplicate species?
	dup_spcomp(compstr)

	# Are there different than 100 % coverage?
	sum_spcomp_is_100(compstr)

	# Are the species in increasing order?
	increasing_spcomp(s)
#    FIMspecies_only(s)
	return s

def spcomp(compstr):
	"""
	Interpret SPCOMP
	Report an error if it is not a multiple of 6 characters.
	Report an error if the designated numeric parts of the string are non-numeric
	"""
	if len(compstr) not in range (6,66,6):
		raise FMPError(FMPError.e.SPCompNot6, '' ) #compstr)
	elif len(compstr.strip()) not in range (6,66,6):
		raise FMPError(FMPError.e.SPCompNot6, '' ) #compstr)
	try:
		testValue = [ int(compstr[i+3:i+6]) for i in range(0,len(compstr),6) ]
	except ValueError:
		raise FMPError(FMPError.e.SPCompHasNonNumericComposition,'' ) #compstr)
	spcompd  = OrderedDict([ (compstr[i:i+3].strip().upper(),int(compstr[i+3:i+6]) ) for i in range(0,len(compstr),6) ])
	return spcompd


def spcomp_reformat(compstr):
	"""
	Reformats compstr to SPCOMP standards. Removes whitespace to do it...
	Groups string by alpha or numeric, pads them to standard, then returns the string
	"""
	#comp_dictionary = spcomp(''.join(  [ {True:str.ljust, False:str.rjust}[k](''.join(list(g)),3) for k,g in groupby(str(compstr.replace(' ','')),str.isalpha) ] ))
	c = [ {True:str.ljust, False:str.rjust}[k](''.join(list(g)),3) for k,g in groupby(str(compstr.replace(' ','')),str.isalpha) ]
	return ''.join(  [ (s[0],s[1]) for s in sorted([ ( c[r+1],c[r] ) for r in range(0,len(c),2) ],reverse=True) ]   )

def spcomp_ignore_whitespace(compstr):
	"""
	Interpret SPCOMP, ignoring whitespace (calls reformat first)
	Ignores whitespace (assumes the string is in any spacing of species (alpha) and comp (numeric)

	"""
	return spcomp(spcomp_reformat(compstr))

def spcomp_as_text(comp_dictionary):
	return ''.join( [ "%s%s"%(k.ljust(3),str(v).rjust(3)) for k,v in comp_dictionary.iteritems() ] )

def max_spcomp(compstr):
	"""
	Interpret SPCOMP. FIM Resource Inventory Tech Spec 2009 A1.15
	Maximum of 10 species and proportions pairs in the string
	"""
	if len(compstr) > 60:
		raise FMPError(FMPError.e.SPCompGT10SPC,'' ) #compstr)

def dup_spcomp(compstr):
	"""
	Interpret SPCOMP. FIM Resource Inventory Tech Spec 2009 A1.15
	No duplicate species codes allowed in the string
	"""
	duplist = [ compstr[i:i+3].strip().upper() for i in range(0,len(compstr),6) ]
	if len(set(duplist)) != len(duplist):
		raise FMPError(FMPError.e.SPCompHasDuplicateSPC,'' ) #comp_dictionary)


def sum_spcomp_is_100(compstr):
	def add(x,y): return x+y
	if reduce(add, [ int(compstr[i+3:i+6]) for i in range(0,len(compstr),6) ] , 0) != 100:
		raise FMPError(FMPError.e.SPCompSumNot100,'' ) #comp_dictionary)

def increasing_spcomp(comp_dictionary):
	"""Interpret species comp. SPCOMP
	   Report an error if species are not in decreasing percentage order"""
	if comp_dictionary.values() != sorted(comp_dictionary.values(), reverse=True):
		raise FMPError(FMPError.e.WARN_SPCompValNotAscending,'' ) #comp_dictionary)

def WGTest(comp_dictionary, wg):
	"""
	Interprets SPCOMP and LEADSPC/WG
	Looks to see if LEADSPC/WG is the first species listed in SPCOMP.
	"""
	if wg != comp_dictionary.keys()[0]:
		raise FMPError(FMPError.e.WGNotFirstInSPComp,'' ) #[comp_dictionary,wg])

def MaxPCT(comp_dictionary):
	"""

	"""
	return sorted(comp_dictionary.values())[-1]

def MinPCT(comp_dictionary):
	"""

	"""
	return sorted(comp_dictionary.values())[0]

def combine_two_stories(spcomp1,spcomp2,cover1,cover2):
	"""
	Combines two species comp strings by relative proportion.
	Will refloat so that total comp floats to 100
	"""
	comp_dictionary = dict()
	def accumulate(d,k,v,p):
		try:
			d[k] += float(v) * float(p)
		except KeyError:
			d[k]  = float(v) * float(p)

	for sp,comp in spcomp1.iteritems():
		accumulate(comp_dictionary,sp,comp,cover1)
	for sp,comp in spcomp2.iteritems():
		accumulate(comp_dictionary,sp,comp,cover2)

	#Calculate total cover to float to 100.0
	fudge_factor = 100.0 / reduce(lambda x,y: ( round(x)+round(y) ),  comp_dictionary.values(), 0)
	return OrderedDict([ (key, int(round( value * fudge_factor ))) for key, value in sorted(comp_dictionary.iteritems(), key=lambda (k,v): (v,k), reverse=True ) ] )




	#TODO: Change this to respond to new fim/old fim
	#TODO: Add a function that checks that Interpspecies only / warn if supinfo species in list

if __name__ == "__main__":
	sppcomps = [
		'PT  90SB  10','PT90SB10','PT  20BW  80','PT  90','PT  80SB  10SB  10',
		'SB 100',
		'PT  80SB  10SB  10',
		'PT  10SB  10SW  10PT  10PB  10PW  10PR  10BY  10BW  10AB   5BF   5',
		'SB  50BW  30BF  20',
		'    50PO  40BW  10',
			  ]
	for s in sppcomps:
		try:
			print s
			vs_dict = valid_spcomp(s)
			#increasing_spcomp(vs_dict)
			#FIMspecies_only(vs_dict)
			#Garnet add functions here
			WGTest(vs_dict, 'SB')
			print 'Success: %s'%(vs_dict)
		except FMPError as e:
			print('Error: {0}'.format(e) )
		#print "spcomp_reformat(%s)==> %s"%(s,spcomp_reformat(s))
	print "==========================\n=========================="
	sppcomps = [
		'PT  90SB  10',
		'PT  90SB  10',
		'PT  20BW  80',
		'PT 100',
		'PT  80SB  10SW  10',
		'SB 100',
		'PT  80SB  10SW  10',
		'PT  10SB  10SW  10PT  10PB  10PW  10PR  10BY  10BW  10AB  10',
		'PT 100','PJ  50',
		'SB  50BW  30BF  20','SB  50BW  30BF  20'
		]
	for n in range(0,len(sppcomps),2):
		print combine_two_stories(spcomp_ignore_whitespace(sppcomps[n]),spcomp_ignore_whitespace(sppcomps[n+1]),0.5,0.5)
	print "==========================\n=========================="
	overStory = 'PW 80PR 20'
	underStory = 'SB 30BW 20PW 20Mr 10Bf 10Mh 10'
	occlo = 0.7
	ucclo = 0.3
	print spcomp_as_text( combine_two_stories(spcomp_ignore_whitespace(overStory),spcomp_ignore_whitespace(underStory),occlo,ucclo) )


	print "========================================================"
	print "= LEADSPC Test"
	print "========================================================"
	OSPCOMP = "Bw  30Pt  30Sw  20Bf  10Pj  10"
	OLEADSPC = "Bw"
	print get_leadspc(OSPCOMP)


	print "========================================================"
	print "== Missing species test: # WI, OX, Mn, PX, Bp, Mf"
	print "========================================================"

	sppcomps = [
		'WI 100',
		'OX 100',
		'Mn 100',
		'PX 100',
		'Bp 100',
		'Mf 100',
			  ]
	for s in sppcomps:
		try:
			print s
			vs_dict = valid_spcomp(s)
			print test_SpeciesInList(s) if test_SpeciesInList(s) else "All Found"
			#increasing_spcomp(vs_dict)
			#FIMspecies_only(vs_dict)
			#Garnet add functions here
			#WGTest(vs_dict, 'SB')
			print 'Success: %s'%(vs_dict)
		except FMPError as e:
			print('Error: {0}'.format(e) )
		#print "spcomp_reformat(%s)==> %s"%(s,spcomp_reformat(s))
	print "==========================\n=========================="
