; example2.nsi
;
; This script is based on example1.nsi, but it remember the directory,
; has uninstall support and (optionally) installs start menu shortcuts.
;
; It will install example2.nsi into a directory that the user selects,

;--------------------------------

; Set the version code
!define VERSION "0.9.15"

; The name of the installer
Name "Checker"

; The file to write
OutFile "..\..\FMPDS_pages\content\releases\Checker_${VERSION}.exe"

; The default installation directory
;InstallDir "$PROGRAMFILES\MNRF_NWR_RIAU_Checker"
InstallDir "$PROFILE\MNRF_NWR_RIAU_Checker"

; Registry key to check for directory (so if you install again, it will
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\MNRF_NWR_RIAU_Checker" "Install_Dir"


; Request application privileges for Windows Vista
RequestExecutionLevel user

;--------------------------------

; Pages

Page components
Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

Var ARC_PYTHON
Var INDEPENDENT_PYTHON
Var OSGEO4W_PYTHON
Var PYTHON


;--------------------------------
; Find a copy of

;--------------------------------

; The stuff to install

Section "Checker Installer"

  SectionIn RO

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR

  ; Put file there
  ;File     "checker.nsi"
  ;File /r  "..\MNRF_FI_Checker_User_Guide.pdf"
  File /r  "..\checker"
  File /r  "..\stack"
  File /r  "..\mm_processor"
  CreateDirectory "$INSTDIR\arcpy_support"
  File "/oname=$INSTDIR\arcpy_support\env_arcpy.bat" "..\arcpy_support\env_arcpy.bat"
  CreateDirectory "$INSTDIR\tech_spec"
  CreateDirectory "$INSTDIR\tech_spec\2009"
  CreateDirectory "$INSTDIR\tech_spec\2017"
  CreateDirectory "$INSTDIR\tech_spec\2018"

  File "/oname=$INSTDIR\tech_spec\2009\AR.mm" "..\tech_spec\2009\AR.mm"
  File "/oname=$INSTDIR\tech_spec\2009\AWS.mm" "..\tech_spec\2009\AWS.mm"

  ;File "/oname=$INSTDIR\tech_spec\2017\AR.mm" "..\tech_spec\2017\AR.mm"
  File "/oname=$INSTDIR\tech_spec\2018\AR.mm" "..\tech_spec\2018\AR.mm"
  ;File "/oname=$INSTDIR\tech_spec\2017\AWS.mm" "..\tech_spec\2017\AWS.mm"
  File "/oname=$INSTDIR\tech_spec\2018\AWS.mm" "..\tech_spec\2018\AWS.mm"

  File "/oname=$INSTDIR\tech_spec\2009\FMP.mm" "..\tech_spec\2009\FMP.mm"
  ;File "/oname=$INSTDIR\tech_spec\2017\FMP.mm" "..\tech_spec\2017\FMP.mm"
  File "/oname=$INSTDIR\tech_spec\2018\FMP.mm" "..\tech_spec\2018\FMP.mm"

  ;File "..\mu999_2020_fmpdp.gpkg"

  ; Write the installation path into the registry
  WriteRegStr HKLM "Software\MNRF_NWR_RIAU_Checker" "Install_Dir" "$INSTDIR"
  WriteRegStr HKLM "Software\MNRF_NWR_RIAU_Checker" "Version" "${VERSION}"

  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\MNRF_NWR_RIAU_Checker" "DisplayName" "Checker"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\MNRF_NWR_RIAU_Checker" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\MNRF_NWR_RIAU_Checker" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\MNRF_NWR_RIAU_Checker" "NoRepair" 1
  WriteUninstaller "uninstall.exe"

SectionEnd

Section /o "Support for Arc Python" arc
	;StrCpy $PYTHON $ARC_PYTHON
	StrCpy $PYTHON "$INSTDIR\arcpy_support\env_arcpy.bat"
SectionEnd

Section /o "Support for Independent Python" independent
	StrCpy $PYTHON $INDEPENDENT_PYTHON
SectionEnd

Section /o "Support for OSGeo4w / QGIS Python" osgeo4w
	StrCpy $PYTHON $OSGEO4W_PYTHON
SectionEnd


# defines for newer versions
!include Sections.nsh
# SECTION_OFF is defined in Sections.nsh as 0xFFFFFFFE
!define SECTION_ON ${SF_SELECTED} # 0x1

Section "User Context Menu Entry Interface"
	; GeoPackage
	WriteRegStr HKCU "Software\Classes\.gpkg" "" "GeoPackage"
	WriteRegStr HKCU "Software\Classes\.gpkg" "Content Type" "application/octet-stream"

	;Generate the context entries for geopackages
	;First the menu
	;menu-name,submenu,icon
	WriteRegStr HKCU "Software\Classes\GeoPackage\Shell\Checker" "MUIVerb" "Checker"
	WriteRegStr HKCU "Software\Classes\GeoPackage\Shell\Checker" "ExtendedSubCommandsKey" "GeoPackage\Menu"
	WriteRegStr HKCU "Software\Classes\GeoPackage\Shell\Checker" "Position" "Top"
	WriteRegStr HKCU "Software\Classes\GeoPackage\Shell\Checker" "Icon" "$INSTDIR\checker\Checker.ico"

	#Second, menu entries
	WriteRegStr	HKCU "Software\Classes\GeoPackage\Menu\shell\CheckFMP2018" "MUIVerb" "Check As FMP (2018 spec)"
	WriteRegStr	HKCU "Software\Classes\GeoPackage\Menu\shell\CheckFMP2018\command" "" '"$PYTHON" "$INSTDIR\checker\context_checker.py" -i "%1" -p FMP -j "$INSTDIR\tech_spec\2018\fmp.json" '

	;add separator above the next entry
	WriteRegDWORD	HKCU "Software\Classes\GeoPackage\Menu\shell\CheckFMP2018" "CommandFlags" "32"

	#More menu entries
	WriteRegStr HKCU "Software\Classes\GeoPackage\Menu\shell\CheckFMP2009" "MUIVerb" "Check As FMP (2009 spec)"
	WriteRegStr	HKCU "Software\Classes\GeoPackage\Menu\shell\CheckFMP2009\command" "" '"$PYTHON" "$INSTDIR\checker\context_checker.py" -i "%1" -p FMP -j "$INSTDIR\tech_spec\2009\fmp.json" '

	#AR 2018 menu entries
	WriteRegStr HKCU "Software\Classes\GeoPackage\Menu\shell\CheckAR2018" "MUIVerb" "Check As AR (2018 spec)"
	WriteRegStr	HKCU "Software\Classes\GeoPackage\Menu\shell\CheckAR2018\command" "" '"$PYTHON" "$INSTDIR\checker\context_checker.py" -i "%1" -p AR -j "$INSTDIR\tech_spec\2018\ar.json" '

	#AWS 2018 menu entries
	WriteRegStr HKCU "Software\Classes\GeoPackage\Menu\shell\CheckAWS2018" "MUIVerb" "Check As AWS (2018 spec)"
	WriteRegStr	HKCU "Software\Classes\GeoPackage\Menu\shell\CheckAWS2018\command" "" '"$PYTHON" "$INSTDIR\checker\context_checker.py" -i "%1" -p AWS -j "$INSTDIR\tech_spec\2018\aws.json" '

	#AR 2009 menu entries
	WriteRegStr HKCU "Software\Classes\GeoPackage\Menu\shell\CheckAR2009" "MUIVerb" "Check As AR (2009 spec)"
	WriteRegStr	HKCU "Software\Classes\GeoPackage\Menu\shell\CheckAR2009\command" "" '"$PYTHON" "$INSTDIR\checker\context_checker.py" -i "%1" -p AR -j "$INSTDIR\tech_spec\2009\ar.json" '


	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;Shapefile
	WriteRegStr HKCU "Software\Classes\.shp" "" "Shapefile"
	WriteRegStr HKCU "Software\Classes\.shp" "Content Type" "application/octet-stream"

	;Generate the context entries for geopackages
	;First the menu
	;menu-name,submenu,icon
	WriteRegStr HKCU "Software\Classes\Shapefile\Shell\Checker" "MUIVerb" "Checker"
	WriteRegStr HKCU "Software\Classes\Shapefile\Shell\Checker" "ExtendedSubCommandsKey" "Shapefile\Menu"
	WriteRegStr HKCU "Software\Classes\Shapefile\Shell\Checker" "Position" "Top"
	WriteRegStr HKCU "Software\Classes\Shapefile\Shell\Checker" "Icon" "$INSTDIR\checker\Checker.ico"

	#Second, menu entries
	WriteRegStr	HKCU "Software\Classes\Shapefile\Menu\shell\CheckFMP2018" "MUIVerb" "Check As FMP (2018 spec)"
	WriteRegStr	HKCU "Software\Classes\Shapefile\Menu\shell\CheckFMP2018\command" "" '"$PYTHON" "$INSTDIR\checker\context_checker.py" -i "%1" -p FMP -j "$INSTDIR\tech_spec\2018\fmp.json" '

	;add separator above the next entry
	WriteRegDWORD	HKCU "Software\Classes\Shapefile\Menu\shell\CheckFMP2018" "CommandFlags" "32"

	#More menu entries
	WriteRegStr HKCU "Software\Classes\Shapefile\Menu\shell\CheckFMP2009" "MUIVerb" "Check As FMP (2009 spec)"
	WriteRegStr	HKCU "Software\Classes\Shapefile\Menu\shell\CheckFMP2009\command" "" '"$PYTHON" "$INSTDIR\checker\context_checker.py" -i "%1" -p FMP -j "$INSTDIR\tech_spec\2009\fmp.json" '

	#AR 2018 menu entries
	WriteRegStr HKCU "Software\Classes\Shapefile\Menu\shell\CheckAR2018" "MUIVerb" "Check As AR (2018 spec)"
	WriteRegStr	HKCU "Software\Classes\Shapefile\Menu\shell\CheckAR2018\command" "" '"$PYTHON" "$INSTDIR\checker\context_checker.py" -i "%1" -p AR -j "$INSTDIR\tech_spec\2018\ar.json" '

	#AWS 2018 menu entries
	WriteRegStr HKCU "Software\Classes\Shapefile\Menu\shell\CheckAWS2018" "MUIVerb" "Check As AWS (2018 spec)"
	WriteRegStr	HKCU "Software\Classes\Shapefile\Menu\shell\CheckAWS2018\command" "" '"$PYTHON" "$INSTDIR\checker\context_checker.py" -i "%1" -p AWS -j "$INSTDIR\tech_spec\2018\aws.json" '


	#AR 2009 menu entries
	WriteRegStr HKCU "Software\Classes\Shapefile\Menu\shell\CheckAR2009" "MUIVerb" "Check As AR (2009 spec)"
	WriteRegStr	HKCU "Software\Classes\Shapefile\Menu\shell\CheckAR2009\command" "" '"$PYTHON" "$INSTDIR\checker\context_checker.py" -i "%1" -p AR -j "$INSTDIR\tech_spec\2009\ar.json" '


	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;Directory (GDB, Dir of Shapefiles, Dir of Coverages, Coverage) for right click in right pane of explorer
	;https://stackoverflow.com/questions/20449316/how-add-context-menu-item-to-windows-explorer-for-folders

	; Delete the existing version of myself...
	DeleteRegKey HKCU "Software\Classes\directory\Menu"
	DeleteRegKey HKCU "Software\Classes\directory\Checker"
	DeleteRegKey HKCU "Software\Classes\directory\Shell\Checker"
	DeleteRegKey HKCU "Software\Classes\directory\Background\Menu"
	DeleteRegKey HKCU "Software\Classes\directory\Background\Checker"
	DeleteRegKey HKCU "Software\Classes\directory\Background\Shell\Checker"

	WriteRegStr HKCU "Software\Classes\directory\Shell\Checker" "MUIVerb" "Checker"
	;; Icon from http://www.iconarchive.com/show/100-flat-2-icons-by-graphicloads.html
	WriteRegStr HKCU "Software\Classes\directory\Shell\Checker" "Icon" "$INSTDIR\checker\Checker.ico"
	WriteRegStr HKCU "Software\Classes\directory\Shell\Checker" "ExtendedSubCommandsKey" "directory\Checker"
	WriteRegStr HKCU "Software\Classes\directory\Shell\Checker" "Position" "Top"

	# Add a default entry so we don't run the checker by default...
	; This works... But it shouldn't...
	; This makes the Rightclick on the folder on the toolbar and then the frequently used folder choice work.
	; Yeesh...
	WriteRegStr	HKCU "Software\Classes\directory\Checker\shell\open" "MUIVerb" "Open in New Window"
	WriteRegExpandStr HKCU "Software\Classes\directory\Checker\shell\open\command" "" 'c:\windows\explorer.exe "%1"'
	;add separator above the next entry
	WriteRegDWORD	HKCU "Software\Classes\directory\Checker\shell\open" "CommandFlags" "32"

	#Second, menu entries
	WriteRegStr	HKCU "Software\Classes\directory\Checker\shell\CheckFMP2018" "MUIVerb" "Check As FMP (2018 spec)"
	WriteRegStr	HKCU "Software\Classes\directory\Checker\shell\CheckFMP2018\command" "" '"$PYTHON" "$INSTDIR\checker\context_checker.py" -i "%1" -p FMP -j "$INSTDIR\tech_spec\2018\fmp.json" '


	;add separator above the next entry
	;WriteRegDWORD	HKCU "Software\Classes\directory\Menu\shell\CheckFMP2017" "CommandFlags" "32"

	#More menu entries
	WriteRegStr HKCU "Software\Classes\directory\Checker\shell\CheckFMP2009" "MUIVerb" "Check As FMP (2009 spec)"
	WriteRegStr	HKCU "Software\Classes\directory\Checker\shell\CheckFMP2009\command" "" '"$PYTHON" "$INSTDIR\checker\context_checker.py" -i "%1" -p FMP -j "$INSTDIR\tech_spec\2009\fmp.json" '

	# AR 2018 Entries
	WriteRegStr	HKCU "Software\Classes\directory\Checker\shell\CheckAR2018" "MUIVerb" "Check As AR (2018 spec)"
	WriteRegStr	HKCU "Software\Classes\directory\Checker\shell\CheckAR2018\command" "" '"$PYTHON" "$INSTDIR\checker\context_checker.py" -i "%1" -p AR -j "$INSTDIR\tech_spec\2018\ar.json" '

	# AWS 2018 Entries
	WriteRegStr	HKCU "Software\Classes\directory\Checker\shell\CheckAWS2018" "MUIVerb" "Check As AWS (2018 spec)"
	WriteRegStr	HKCU "Software\Classes\directory\Checker\shell\CheckAWS2018\command" "" '"$PYTHON" "$INSTDIR\checker\context_checker.py" -i "%1" -p AWS -j "$INSTDIR\tech_spec\2018\aws.json" '

	# AR 2009 Entries
	WriteRegStr	HKCU "Software\Classes\directory\Checker\shell\CheckAR2009" "MUIVerb" "Check As AR (2009 spec)"
	WriteRegStr	HKCU "Software\Classes\directory\Checker\shell\CheckAR2009\command" "" '"$PYTHON" "$INSTDIR\checker\context_checker.py" -i "%1" -p AR -j "$INSTDIR\tech_spec\2009\ar.json" '


	;;;;;;;;;
	; And for right click in left pane, or right click in background of right pane

	WriteRegStr HKCU "Software\Classes\directory\Background\Shell\Checker" "MUIVerb" "Checker"
	WriteRegStr HKCU "Software\Classes\directory\Background\Shell\Checker" "Icon" "$INSTDIR\checker\Checker.ico"
	WriteRegStr HKCU "Software\Classes\directory\Background\Shell\Checker" "ExtendedSubCommandsKey" "directory\Checker"
	WriteRegStr HKCU "Software\Classes\directory\Background\Shell\Checker" "Position" "Top"

SectionEnd

; Optional section (can be disabled by the user)
Section "Start Menu Shortcuts"

	CreateDirectory "$SMPROGRAMS\Checker"
	CreateShortcut "$SMPROGRAMS\Checker\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
	;CreateShortcut "$SMPROGRAMS\Checker\Checker.lnk" "$INSTDIR\Checker.nsi" "" "$INSTDIR\Checker.nsi" 0

	; Sample BAT file
	; Write a bat file out to run the fschecker on the mindmap directories
	FileOpen $9 DemonstratetheChecker.bat w ;Opens a Empty File and fills it
	FileWrite $9 "@echo off$\r$\n"
	FileWrite $9 '"$PYTHON" "$INSTDIR\checker\context_checker.py" -i "$INSTDIR\mu999_2020_fmpdp.gpkg" -p FMP -j "$INSTDIR\tech_spec\2018\fmp.json" '
	FileWrite $9 "$\r$\n$\r$\n$\r$\n"
	FileClose $9 ;Closes the filled file

	CreateShortcut "$SMPROGRAMS\Checker\Demonstrate the Checker.lnk" "$INSTDIR\DemonstratetheChecker.bat" "" "$INSTDIR\DemonstratetheChecker.bat" 0

	; Write a bat file out to run the fschecker on the mindmap directories
	FileOpen $9 Compiler.bat w ;Opens a Empty File and fills it
	FileWrite $9 "@echo off$\n"
	FileWrite $9 '"$PYTHON" $INSTDIR\mm_processor\fswatcher_roundtrip.py -i "$INSTDIR\tech_spec" -e ".mm" -t '
	FileWrite $9 "$\r$\n$\r$\n$\r$\n"
	FileClose $9 ;Closes the filled file

	CreateShortcut "$SMPROGRAMS\Checker\Compiler.lnk" "$INSTDIR\Compiler.bat" "" "$INSTDIR\Compiler.bat" 0


	; Generate a Show Installed Folder Shortcut
	CreateShortcut "$SMPROGRAMS\Checker\Installation Folder.lnk" "$INSTDIR" "" "$INSTDIR" 0

	; PDF Files for the Tech Specs
	; PDF Files for the Manual
	;CreateShortcut "$SMPROGRAMS\Checker\Manual.lnk" "$INSTDIR\MNRF_FI_Checker_User_Guide.pdf" "" "$INSTDIR\MNRF_FI_Checker_User_Guide.pdf" 0

SectionEnd

Section /o "Optional Specification Files"
	; French-Severn 2009-2018 Hybrid Spec.mm
	File "/oname=$INSTDIR\tech_spec\2018\French-Severn 2009-2018 Hybrid Spec.mm" "..\tech_spec\2018\French-Severn 2009-2018 Hybrid Spec.mm"

	# menu entries
	WriteRegStr	HKCU "Software\Classes\Shapefile\Menu\shell\CheckFMP_FrenchSevern" "MUIVerb" "Check As FMP (French-Severn 2018 spec)"
	WriteRegStr	HKCU "Software\Classes\Shapefile\Menu\shell\CheckFMP_FrenchSevern\command" "" '"$PYTHON" "$INSTDIR\checker\context_checker.py" -i "%1" -p FMP -j "$INSTDIR\tech_spec\2018\French-Severn 2009-2018 Hybrid Spec.json" '

	WriteRegStr	HKCU "Software\Classes\directory\Checker\shell\CheckFMP_FrenchSevern" "MUIVerb" "Check As FMP (French-Severn 2018 spec)"
	WriteRegStr	HKCU "Software\Classes\directory\Checker\shell\CheckFMP_FrenchSevern\command" "" '"$PYTHON" "$INSTDIR\checker\context_checker.py" -i "%1" -p FMP -j "$INSTDIR\tech_spec\2018\French-Severn 2009-2018 Hybrid Spec.json" '

	WriteRegStr	HKCU "Software\Classes\GeoPackage\Menu\shell\CheckFMP_FrenchSevern" "MUIVerb" "Check As FMP (French-Severn 2018 spec)"
	WriteRegStr	HKCU "Software\Classes\GeoPackage\Menu\shell\CheckFMP_FrenchSevern\command" "" '"$PYTHON" "$INSTDIR\checker\context_checker.py" -i "%1" -p FMP -j "$INSTDIR\tech_spec\2018\French-Severn 2009-2018 Hybrid Spec.json" '

SectionEnd

Section /o "Dataset Build Scripts" buildscripts
	; Generator for sample data
	FileOpen $9 GenerateTestDataset.bat w ;Opens a Empty File and fills it
	FileWrite $9 "@echo off$\r$\n"
	FileWrite $9 '"$PYTHON" "$INSTDIR\mm_processor\build_test_dataset.py" '
	FileWrite $9 "$\r$\n$\r$\n$\r$\n"
	FileClose $9 ;Closes the filled file

	CreateShortcut "$SMPROGRAMS\Checker\Generate Test Dataset.lnk" "$INSTDIR\GenerateTestDataset.bat" "" "$INSTDIR\GenerateTestDataset.bat" 0

	; Generator for 2018 FMP sample data
	FileOpen $9 Generate2018FMPTestDataset.bat w ;Opens a Empty File and fills it
	FileWrite $9 "@echo off$\r$\n"
	FileWrite $9 '"$PYTHON" "$INSTDIR\mm_processor\build_test_dataset.py" "tech_spec\2018\FMP.py" '
	FileWrite $9 "$\r$\n$\r$\n$\r$\n"
	FileClose $9 ;Closes the filled file

	CreateShortcut "$SMPROGRAMS\Checker\Generate 2018 FMP Test Dataset.lnk" "$INSTDIR\Generate2018FMPTestDataset.bat" "" "$INSTDIR\Generate2018FMPTestDataset.bat" 0

	; Generator for the 2018 AR Test Dataset:	Generate2018ARTestDataset
	; Generator for 2018 FMP sample data
	FileOpen $9 Generate2018ARTestDataset.bat w ;Opens a Empty File and fills it
	FileWrite $9 "@echo off$\r$\n"
	FileWrite $9 '"$PYTHON" "$INSTDIR\mm_processor\build_test_dataset.py" "tech_spec\2018\AR.py" '
	FileWrite $9 "$\r$\n$\r$\n$\r$\n"
	FileClose $9 ;Closes the filled file

	CreateShortcut "$SMPROGRAMS\Checker\Generate 2018 AR Test Dataset.lnk" "$INSTDIR\Generate2018ARTestDataset.bat" "" "$INSTDIR\Generate2018ARTestDataset.bat" 0


	; Generator for sample data
	FileOpen $9 GenerateDemonstrationData.bat w ;Opens a Empty File and fills it
	FileWrite $9 "@echo off$\r$\n"
	FileWrite $9 '"$PYTHON" "$INSTDIR\mm_processor\build_PRC_Only_dataset.py" '
	FileWrite $9 "$\r$\n$\r$\n$\r$\n"
	FileClose $9 ;Closes the filled file

	CreateShortcut "$SMPROGRAMS\Checker\Generate Compiler Demonstration Data.lnk" "$INSTDIR\GenerateDemonstrationData.bat" "" "$INSTDIR\GenerateDemonstrationData.bat" 0
SectionEnd

;--------------------------------

; Uninstaller

Section "Uninstall"
  SetRegView 32

  ; Delete ArcToolbox Version
  ; if RegKEY HKLM ...
  ; Delete tbx and directory
  ; delete reg key

  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\MNRF_NWR_RIAU_Checker"
  DeleteRegKey HKLM "Software\MNRF_NWR_RIAU_Checker"

  ; Remove Context Entry Registry Keys
  ;DeleteRegKey HKCU "Software\Classes\Shapefile\Menu\shell\CheckFMP2009\command"
  ;DeleteRegKey HKCU "Software\Classes\Shapefile\Menu\shell\CheckFMP2009"
  ;DeleteRegKey HKCU "Software\Classes\Shapefile\Menu\shell\CheckFMP2017\command"
  ;DeleteRegKey HKCU "Software\Classes\Shapefile\Menu\shell\CheckFMP2017"
  ;DeleteRegKey HKCU "Software\Classes\Shapefile\Shell\Checker"
  DeleteRegKey HKCU "Software\Classes\Shapefile"
  DeleteRegKey HKCU "Software\Classes\.shp"
  DeleteRegKey HKCU "Software\Classes\GeoPackage"
  DeleteRegKey HKCU "Software\Classes\.gpkg"

  DeleteRegKey HKCU "Software\Classes\directory\Shell\Checker"
  DeleteRegKey HKCU "Software\Classes\directory\Shell"
  DeleteRegKey HKCU "Software\Classes\directory\Checker"

  DeleteRegKey HKCU "Software\Classes\directory\background\Shell\Checker"
  DeleteRegKey HKCU "Software\Classes\directory\background\Shell"
  DeleteRegKey HKCU "Software\Classes\directory\background\Menu"

  DeleteRegKey HKCU "Software\Classes\directory\Shell\Open"

  ; Remove files and uninstaller
  ;Delete $INSTDIR\checker.nsi
  RMDIR /r $INSTDIR\checker
  RMDIR /r $INSTDIR\stack
  RMDIR /r $INSTDIR\mm_processor
  RMDIR /r $INSTDIR\tech_spec
  RMDIR /r $INSTDIR\arcpy_support
  Delete $INSTDIR\uninstall.exe
  Delete $INSTDIR\Compiler.bat
  Delete $INSTDIR\mu999_2020_fmpdp.gpkg
  Delete $INSTDIR\DemonstratetheChecker.bat
  Delete $INSTDIR\GenerateTestDataset.bat
  Delete $INSTDIR\Generate2018FMPTestDataset.bat
  Delete $INSTDIR\Generate2018ARTestDataset.bat
  Delete $INSTDIR\GenerateDemonstrationData.bat
  ;Delete $INSTDIR\MNRF_FI_Checker_User_Guide.pdf

  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\Checker\*.*"

  ; Remove directories used
  RMDir "$SMPROGRAMS\Checker"
  RMDir "$INSTDIR"

SectionEnd


Function .onInit
	SetRegView 32

	SectionGetFlags ${arc} $0
	SectionGetFlags ${buildscripts} $1

	IfFileExists "C:\python27\ArcGIS10.7\python.exe"  0 +4
		StrCpy $ARC_PYTHON "C:\python27\ArcGIS10.7\python.exe"
		IntOp $0 $0 | ${SECTION_ON}
		SectionSetFlags ${arc} $0

	IfFileExists "C:\python27\ArcGIS10.6\python.exe"  0 +4
		StrCpy $ARC_PYTHON "C:\python27\ArcGIS10.6\python.exe"
		IntOp $0 $0 | ${SECTION_ON}
		SectionSetFlags ${arc} $0


	IfFileExists "C:\python27\ArcGIS10.5\python.exe"  0 +4
		StrCpy $ARC_PYTHON "C:\python27\ArcGIS10.5\python.exe"
		IntOp $0 $0 | ${SECTION_ON}
		SectionSetFlags ${arc} $0


	IfFileExists "C:\python27\ArcGIS10.4\python.exe" 0 +4
		StrCpy $ARC_PYTHON "C:\python27\ArcGIS10.4\python.exe"
		IntOp $0 $0 | ${SECTION_ON}
		SectionSetFlags ${arc} $0


	IfFileExists "C:\python27\ArcGIS10.3\python.exe"  0 +4
		StrCpy $ARC_PYTHON "C:\python27\ArcGIS10.3\python.exe"
		IntOp $0 $0 | ${SECTION_ON}
		SectionSetFlags ${arc} $0



	SectionGetFlags ${independent} $0
	IfFileExists "C:\Python27\python.exe"  0 +6
		StrCpy $INDEPENDENT_PYTHON "C:\Python27\python.exe"
		IntOp $0 $0 | ${SECTION_ON}
		SectionSetFlags ${independent} $0
		IntOp $1 $1 | ${SECTION_ON}
		SectionSetFlags ${buildscripts} $1


	SectionGetFlags ${osgeo4w} $0
	IfFileExists "C:\Program Files\QGIS 2.18\osgeo4w.bat" 0 +6
		StrCpy $OSGEO4W_PYTHON 'C:\Program Files\QGIS 2.18\osgeo4w.bat" "python.exe'
		IntOp $0 $0 | ${SECTION_ON}
		SectionSetFlags ${osgeo4w} $0
		IntOp $1 $1 | ${SECTION_ON}
		SectionSetFlags ${buildscripts} $1


	IfFileExists "C:\Program Files\QGIS 3.2\osgeo4w.bat" 0 +6
		StrCpy $OSGEO4W_PYTHON 'C:\Program Files\QGIS 3.2\osgeo4w.bat" "python.exe'
		IntOp $0 $0 | ${SECTION_ON}
		SectionSetFlags ${osgeo4w} $0
		IntOp $1 $1 | ${SECTION_ON}
		SectionSetFlags ${buildscripts} $1

	IfFileExists "C:\Program Files\QGIS 3.4\osgeo4w.bat" 0 +6
		StrCpy $OSGEO4W_PYTHON 'C:\Program Files\QGIS 3.4\osgeo4w.bat" "python.exe'
		IntOp $0 $0 | ${SECTION_ON}
		SectionSetFlags ${osgeo4w} $0
		IntOp $1 $1 | ${SECTION_ON}
		SectionSetFlags ${buildscripts} $1

	IfFileExists "C:\Program Files\QGIS 3.6\osgeo4w.bat" 0 +6
		StrCpy $OSGEO4W_PYTHON 'C:\Program Files\QGIS 3.6\osgeo4w.bat" "python.exe'
		IntOp $0 $0 | ${SECTION_ON}
		SectionSetFlags ${osgeo4w} $0
		IntOp $1 $1 | ${SECTION_ON}
		SectionSetFlags ${buildscripts} $1

	IfFileExists "C:\Program Files (x86)\QGIS 2.18\osgeo4w.bat" 0 +6
		StrCpy $OSGEO4W_PYTHON 'C:\Program Files (x86)\QGIS 2.18\osgeo4w.bat" "python.exe'
		IntOp $0 $0 | ${SECTION_ON}
		SectionSetFlags ${osgeo4w} $0
		IntOp $1 $1 | ${SECTION_ON}
		SectionSetFlags ${buildscripts} $1


	IfFileExists "C:\Program Files (x86)\QGIS 3.2\osgeo4w.bat" 0 +6
		StrCpy $OSGEO4W_PYTHON 'C:\Program Files (x86)\QGIS 3.2\osgeo4w.bat" "python.exe'
		IntOp $0 $0 | ${SECTION_ON}
		SectionSetFlags ${osgeo4w} $0
		IntOp $1 $1 | ${SECTION_ON}
		SectionSetFlags ${buildscripts} $1


	IfFileExists "C:\Program Files (x86)\QGIS 3.4\osgeo4w.bat" 0 +6
		StrCpy $OSGEO4W_PYTHON 'C:\Program Files (x86)\QGIS 3.4\osgeo4w.bat" "python.exe'
		IntOp $0 $0 | ${SECTION_ON}
		SectionSetFlags ${osgeo4w} $0
		IntOp $1 $1 | ${SECTION_ON}
		SectionSetFlags ${buildscripts} $1


	IfFileExists "C:\Program Files (x86)\QGIS 3.6\osgeo4w.bat" 0 +6
		StrCpy $OSGEO4W_PYTHON 'C:\Program Files (x86)\QGIS 3.6\osgeo4w.bat" "python.exe'
		IntOp $0 $0 | ${SECTION_ON}
		SectionSetFlags ${osgeo4w} $0
		IntOp $1 $1 | ${SECTION_ON}
		SectionSetFlags ${buildscripts} $1


	IfFileExists "C:\Program Files\QGIS 3.0\osgeo4w.bat" 0 +6
		StrCpy $OSGEO4W_PYTHON 'C:\Program Files\QGIS 3.0\osgeo4w.bat" "python.exe'
		IntOp $0 $0 | ${SECTION_ON}
		SectionSetFlags ${osgeo4w} $0
		IntOp $1 $1 | ${SECTION_ON}
		SectionSetFlags ${buildscripts} $1


	IfFileExists "C:\Program Files (x86)\QGIS 3.0\osgeo4w.bat" 0 +6
		StrCpy $OSGEO4W_PYTHON 'C:\Program Files (x86)\QGIS 3.0\osgeo4w.bat" "python.exe'
		IntOp $0 $0 | ${SECTION_ON}
		SectionSetFlags ${osgeo4w} $0
		IntOp $1 $1 | ${SECTION_ON}
		SectionSetFlags ${buildscripts} $1


	IfFileExists "C:\OSGeo4W\osgeo4w.bat" 0 +6
		StrCpy $OSGEO4W_PYTHON 'C:\OSGeo4W\osgeo4w.bat" "python.exe'
		IntOp $0 $0 | ${SECTION_ON}
		SectionSetFlags ${osgeo4w} $0
		IntOp $1 $1 | ${SECTION_ON}
		SectionSetFlags ${buildscripts} $1


	IfFileExists "C:\OSGeo4W64\osgeo4w.bat" 0 +6
		StrCpy $OSGEO4W_PYTHON 'C:\OSGeo4W64\osgeo4w.bat" "python.exe'
		IntOp $0 $0 | ${SECTION_ON}
		SectionSetFlags ${osgeo4w} $0
		IntOp $1 $1 | ${SECTION_ON}
		SectionSetFlags ${buildscripts} $1

FunctionEnd

