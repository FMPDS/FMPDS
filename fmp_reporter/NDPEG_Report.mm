    <map version="1.0.1">
        <node TEXT="NDPEG Report" ID="ID_1982568238">
            <node TEXT="Title" FOLDED="true" POSITION="right" ID="ID_310932818">
                <node TEXT="NDPEG Report for `\qecho ':fmu, :year '` " FOLDED="true" ID="ID_1129465386"/>
            </node>
            <node TEXT="Description" FOLDED="true" POSITION="right" ID="ID_394589739">
                <node TEXT="NDPEG Analysis is required by the NDPEG Guide, 2001. It is a requirement in all plans until made obsolete by the Landscape Guide." FOLDED="true" ID="ID_1578187663"/>
                <node TEXT="The Analysis consists of the comparison of the plan-start disturbance template with the plan-end disturbance template (Template Analysis) and the Planned Clearcut Analysis." FOLDED="true" ID="ID_1915051111"/>
            </node>
            <node TEXT="Section" FOLDED="true" POSITION="right" ID="ID_639599432">
                <node TEXT="Heading" FOLDED="true" ID="ID_645184821">
                    <node TEXT="Template Analysis" FOLDED="true" ID="ID_11927360"/>
                </node>
                <node TEXT="Description" FOLDED="true" ID="ID_1264580136">
                    <node TEXT="The template describes the arrangement of young forest. It is required by NDPEG, but was replaced in the Landscape Guide." FOLDED="true" ID="ID_1809426281"/>
                </node>
                <node TEXT="Table" FOLDED="true" ID="ID_1567892831">
                    <node TEXT="Description" FOLDED="true" ID="ID_1083486708">
                        <node TEXT="Template Analysis For `\qecho :fmu, :planyear` FMP, Phase `\qecho :phase`." FOLDED="true" ID="ID_1737101533"/>
                    </node>
                    <node TEXT="SQL" FOLDED="true" ID="ID_17603888">
                        <node TEXT="SELECT 
	CASE WHEN b.sizeclass is NULL 
		THEN e.sizeclass
	ELSE b.sizeclass
	END as &quot;Size Class&quot;, 
	b.count as &quot;Count in Year 0&quot;, 
	round(b.area) as &quot;Area in Year 0 (ha)&quot;, 
	e.count as &quot;Count in Year 10&quot;, 
	round(e.area) as &quot;Area in Year 10 (ha)&quot;
FROM troutlake_2017_ndpeg.template_yr0 b FULL OUTER JOIN troutlake_2017_ndpeg.template_yr10 e ON b.sizeclass=e.sizeclass
ORDER BY 
CASE 
	WHEN b.sizeclass = '0-130' THEN 1
	WHEN b.sizeclass = '130-260' THEN 2
	WHEN b.sizeclass = '260-500' THEN 3
	WHEN b.sizeclass = '500-1000' THEN 4
	WHEN b.sizeclass = '1000-5000' THEN 5
	WHEN b.sizeclass = '5000-10000' THEN 6
	WHEN b.sizeclass = '10000+' THEN 7
	WHEN e.sizeclass = '0-130' THEN 1
	WHEN e.sizeclass = '130-260' THEN 2
	WHEN e.sizeclass = '260-500' THEN 3
	WHEN e.sizeclass = '500-1000' THEN 4
	WHEN e.sizeclass = '1000-5000' THEN 5
	WHEN e.sizeclass = '5000-10000' THEN 6
	WHEN e.sizeclass = '10000\+' THEN 7
	ELSE 8
	END 
;" FOLDED="true" ID="ID_1006867234"/>
                    </node>
                </node>
            </node>
            <node TEXT="Section" FOLDED="true" POSITION="right" ID="ID_1556464586">
                <node TEXT="Heading" FOLDED="true" ID="ID_1734216936">
                    <node TEXT="Planned Clearcut Analysis" FOLDED="true" ID="ID_33400449"/>
                </node>
                <node TEXT="Description" FOLDED="true" ID="ID_704527345">
                    <node TEXT="The Planned Clearcut Analysis identifies areas of harvest which are greater in area than 1 square mile (260 ha)." FOLDED="true" ID="ID_1926627591"/>
                </node>
                <node TEXT="Section" FOLDED="true" ID="ID_506867938">
                    <node TEXT="Heading" FOLDED="true" ID="ID_990169635">
                        <node TEXT="FMP Table 12" FOLDED="true" ID="ID_836892725"/>
                    </node>
                    <node TEXT="Description" FOLDED="true" ID="ID_206767183">
                        <node TEXT="Table FMP-12 identifies by a unique identifier (Location ID) which areas are greater than 260 ha. Each location is described by total area and area harvested in this plan." FOLDED="true" ID="ID_818839046"/>
                    </node>
                    <node TEXT="Table" FOLDED="true" ID="ID_516658150">
                        <node TEXT="Description" FOLDED="true" ID="ID_1663911649">
                            <node TEXT="FMP-12 for `\qecho :fmu, :planyear` FMP, Phase `\qecho :phase`." FOLDED="true" ID="ID_1122038837"/>
                        </node>
                        <node TEXT="SQL" FOLDED="true" ID="ID_1670066382">
                            <node TEXT="SELECT * FROM troutlake_2017_ndpeg.fmp12 ;
" FOLDED="true" ID="ID_995306107"/>
                        </node>
                    </node>
                </node>
                <node TEXT="Section" FOLDED="true" ID="ID_321679071">
                    <node TEXT="Heading" FOLDED="true" ID="ID_180958085">
                        <node TEXT="80/20 Analysis" FOLDED="true" ID="ID_1978406354"/>
                    </node>
                    <node TEXT="Description" FOLDED="true" ID="ID_1057218807">
                        <node TEXT="The 80/20 analysis identifies the percentage of clearcuts that are greater than 260 ha." FOLDED="true" ID="ID_1175513036"/>
                    </node>
                    <node TEXT="Table" FOLDED="true" ID="ID_1781269005">
                        <node TEXT="Description" FOLDED="true" ID="ID_1564713765">
                            <node TEXT="NDPEG 80/20 Rule Report" FOLDED="true" ID="ID_1363332913"/>
                        </node>
                        <node TEXT="SQL" FOLDED="true" ID="ID_1949362137">
                            <node TEXT="SELECT 
	label as &quot;Size Class&quot;, 
	sum_ha as &quot;Sum of Area (ha)&quot;, 
	round((sum_ha / sum_sum_ha*100)::numeric,0) as &quot;Percentage Area&quot;, 
	count as &quot;Count of Individual Polygons&quot;, 
	round((count / sum_count*100)::numeric,0) as &quot;Percentage Count&quot;
  FROM troutlake_2017_ndpeg.ndpeg_8020 n , 
  (SELECT sum(count) as sum_count, sum(sum_ha) as sum_sum_ha FROM troutlake_2017_ndpeg.ndpeg_8020 ) s
ORDER BY
CASE WHEN label = '&lt;260 ha' THEN 1
	WHEN label = '&gt;=260 ha' THEN 2
END
  ;" FOLDED="true" ID="ID_914715936"/>
                        </node>
                    </node>
                </node>
                <node TEXT="Image" FOLDED="true" ID="ID_197034865">
                    <node TEXT="Type" FOLDED="true" ID="ID_1076250014">
                        <node TEXT="Map" FOLDED="true" ID="ID_1100472841">
                            <node TEXT="Extent" FOLDED="true" ID="ID_1956850004">
                                <node TEXT="SQL" FOLDED="true" ID="ID_1817113772">
                                    <node TEXT='SELECT ST_Envelope(geom) FROM admin."FOREST_MANAGEMENT_UNIT"' FOLDED="true" ID="ID_1889572471"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="Description" FOLDED="true" ID="ID_175670883">
                        <node TEXT="Map of Planned Clearcuts for `\qecho :fmu, :planyear` FMP, Phase `\qecho :phase`. " FOLDED="true" ID="ID_1223797068"/>
                    </node>
                    <node TEXT="Layer" FOLDED="true" ID="ID_673273774">
                        <node TEXT="class" FOLDED="true" ID="ID_1285185363">
                            <node TEXT="fmu_background" FOLDED="true" ID="ID_275843301"/>
                        </node>
                        <node TEXT="SQL" FOLDED="true" ID="ID_1009931977">
                            <node TEXT="SELECT ST_AsSVG( FROM admin.&quot;FOREST_MANAGEMENT_UNIT&quot; WHERE &quot;FMU_NAME&quot; = 'Trout Lake'" FOLDED="true" ID="ID_19567682"/>
                        </node>
                    </node>
                </node>
            </node>
        </node>
    </map>
