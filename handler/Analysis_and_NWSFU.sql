
-- Break out the species used in the Standard Forest Units, and some others found in the NW
DROP TABLE IF EXISTS spc_breakout ;
CREATE TABLE spc_breakout AS
SELECT
    fid,
    CASE WHEN instr(sspcomp,'PJ') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'PJ') + 3, 3 )) ELSE 0 END as PJ,
    CASE WHEN instr(sspcomp,'SB') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'SB') + 3, 3 )) ELSE 0 END as SB,
    CASE WHEN instr(sspcomp,'SX') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'SX') + 3, 3 )) ELSE 0 END as SX,
    CASE WHEN instr(sspcomp,'SW') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'SW') + 3, 3 )) ELSE 0 END as SW,
    CASE WHEN instr(sspcomp,'SR') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'SR') + 3, 3 )) ELSE 0 END as SR,
    CASE WHEN instr(sspcomp,'SN') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'SN') + 3, 3 )) ELSE 0 END as SN,
    CASE WHEN instr(sspcomp,'LA') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'LA') + 3, 3 )) ELSE 0 END as LA,
    CASE WHEN instr(sspcomp,'PO') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'PO') + 3, 3 )) ELSE 0 END as PO,
    CASE WHEN instr(sspcomp,'PT') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'PT') + 3, 3 )) ELSE 0 END as PT,
    CASE WHEN instr(sspcomp,'PL') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'PL') + 3, 3 )) ELSE 0 END as PL,
    CASE WHEN instr(sspcomp,'PR') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'PR') + 3, 3 )) ELSE 0 END as PR,
    CASE WHEN instr(sspcomp,'PS') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'PS') + 3, 3 )) ELSE 0 END as PS,
    CASE WHEN instr(sspcomp,'PW') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'PW') + 3, 3 )) ELSE 0 END as PW,
    CASE WHEN instr(sspcomp,'CW') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'CW') + 3, 3 )) ELSE 0 END as CW,
    CASE WHEN instr(sspcomp,'CE') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'CE') + 3, 3 )) ELSE 0 END as CE,
    CASE WHEN instr(sspcomp,'BF') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'BF') + 3, 3 )) ELSE 0 END as BF,
    CASE WHEN instr(sspcomp,'BW') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'BW') + 3, 3 )) ELSE 0 END as BW,
    CASE WHEN instr(sspcomp,'MX') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'MX') + 3, 3 )) ELSE 0 END as MX,
    CASE WHEN instr(sspcomp,'MH') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'MH') + 3, 3 )) ELSE 0 END as MH,
    CASE WHEN instr(sspcomp,'OX') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'OX') + 3, 3 )) ELSE 0 END as OX,
    CASE WHEN instr(sspcomp,'OB') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'OB') + 3, 3 )) ELSE 0 END as OB,
    CASE WHEN instr(sspcomp,'MR') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'MR') + 3, 3 )) ELSE 0 END as MR,
    CASE WHEN instr(sspcomp,'OR') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'OR') + 3, 3 )) ELSE 0 END as Ored, -- ORed since OR is an SQL Keyword
    CASE WHEN instr(sspcomp,'MS') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'MS') + 3, 3 )) ELSE 0 END as MS,
    CASE WHEN instr(sspcomp,'AX') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'AX') + 3, 3 )) ELSE 0 END as AX,
    CASE WHEN instr(sspcomp,'AB') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'AB') + 3, 3 )) ELSE 0 END as AB,
    CASE WHEN instr(sspcomp,'PB') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'PB') + 3, 3 )) ELSE 0 END as PB,
    CASE WHEN instr(sspcomp,'EX') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'EX') + 3, 3 )) ELSE 0 END as EX,
    CASE WHEN instr(sspcomp,'EW') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'EW') + 3, 3 )) ELSE 0 END as EW,
    CASE WHEN instr(sspcomp,'HE') > 0 THEN trim(substr(sspcomp, instr(sspcomp,'HE') + 3, 3 )) ELSE 0 END as HE
FROM baseclass ;


-- Species Groups provide functional grouping of species for use in the Analysis Unit code
DROP TABLE IF EXISTS spc_group ;
CREATE TABLE spc_group AS
SELECT
*,
PJ+PR+PW+SB+SW+LA+CW+BF+OC AS AllCon,
PO+BW+UH+LH AS AllHrd,
PJ+PR+PW+SW+BF+PO AS UpLnd
FROM
    (
    SELECT
        fid,
        PJ,
        SB + SX AS SB,
        SW + SR + SN AS SW,
        LA,
        PO + PT + PL AS PO,
        PR + PS AS PR,
        PW,
        CW + CE as CW,
        BF,
        BW,
        MX+MH+OX+OB+MR+ORed+MS as UH,
        AX+AB+PB+EX+EW AS LH,
        HE AS OC,
        AB as AB, -- Already included in LH, but used in the analysis units directly as well to distinguish rich OCLow
        PB as PB  -- Already included in LH, but used in the analysis units directly as well to distinguish rich OthHd
    FROM spc_breakout ) i ;


DROP TABLE IF EXISTS nwsfu ;
CREATE TABLE nwsfu AS
SELECT s.fid,
       CASE
       WHEN PW >= 40 THEN --'PwDom0'
           CASE
               -- Boarderline white pine that transitions late to ConMx
               WHEN PW = 40 AND PR < 10 AND SB + BF >= 30 AND ecogrp = 'shallow' THEN  'PwDom-pwlimitsha'
               WHEN PW = 40 AND PR < 10 AND SB + BF >= 30 AND (ecogrp <> 'shallow' OR ecogrp IS NULL) THEN 'PwDom-pwlimitdee'
               -- White pine >40% that takes a long time to transition to PwDom
               ELSE 'PwDom-pw'
           END
       WHEN PR >= 70 THEN 'PrDom-all'
       WHEN PW + PR >= 40 THEN -- 'PrwMx0'
           CASE -- White and Red pine that transitions later to ConMx
               WHEN PW + PR = 40 AND SB + BF >= 30  AND ecogrp = 'shallow' THEN 'PrwMx-prwlimitsha'
               WHEN PW + PR = 40 AND SB + BF >= 30  AND (ecogrp <> 'shallow' OR ecogrp IS NULL) THEN 'PrwMx-prwlimitdee'
               -- White & Red pine PrwMx that takes a long time to transfer back to itself
               ELSE 'PrwMx-prw'
           END
       WHEN CW >= 20 AND ecogrp = 'uplce' THEN 'UplCe-all'
       WHEN CW >= 40 AND BF <= 10 AND UpLnd >= 20 AND AllCon >= 70 AND (ecogrp = 'shallow' OR ecogrp IS NULL) THEN 'UplCe-all'

       WHEN ( CW + LA >= 50  OR sleadspc = 'CW' OR sleadspc = 'CW' OR sleadspc = 'LA') AND ecogrp = 'clowland' THEN  --'OCLow'
           CASE
               -- Cedar leading OCLow stays OCLow
               -- ** Consider adding and OR leadspc = 'Cw' to grap the Cw = 40, 30 but with leadspc Cw
               WHEN CW >= 50 OR sleadspc = 'Cw' THEN 'OCLow-cw'
               -- These are the sbla50 stands that will quickly move to SbLow
               WHEN SB = 50 AND LA = 50 THEN 'OCLow-sb50la50'
               -- Sb overtaking La, curve   with late transition to SbLow
               WHEN SB + LA >= 50 AND SB > 0 THEN 'OCLow-sbla'
               --** La with some Cw, OR pure La OCLow transition to OCLow
               WHEN CW >= 10 OR LA = 100 THEN 'OCLow-oclate'
               --** La stands with Ab occurance
               WHEN AB > 0 THEN 'OCLow-ab'
               -- Suspect LA with some mix of species. Anything else?
               ELSE 'OCLow-misc'
           END
        -- SBLow transitions to SBLow
       WHEN ecogrp = 'clowland' OR (ecogrp = 'hlowland' AND (AllCon > AllHrd) ) THEN 'SbLow-all'

       WHEN SB >= 70 AND PO + BW <= 20 AND ecogrp = 'shallow' THEN --'SbSha0'
           CASE
               -- Vary pure spruce staying Sb for a long time
               WHEN SB >= 80 THEN 'SbSha-pure'
               -- Boarderline hardwood limit with no Bf transitions to ConMx
               WHEN SB = 70 AND AllHrd >= 20 AND BF <= 10 THEN 'SbSha-hwd'
               -- Noteable component of Sb, Bf present
               WHEN SB = 70 AND BF > 0 AND BF + SW >= 20 THEN 'SbSha-bf'
               -- Noteable component of Sb, little Bf and Hrdwd transitions to SbMx1
               ELSE 'SbSha-conif'
           END
       WHEN SB >= 70 AND PO + BW <= 20 THEN --'SbDee0'
           CASE
               -- Vary pure spruce staying Sb for a long time
               WHEN SB >= 80 THEN 'SbDee-pure'
               -- Boarderline hardwood limit with no Bf transitions to ConMx
               WHEN SB = 70 AND AllHrd >= 20 AND BF <= 10 THEN 'SbDee-hwd'
               -- Noteable component of Sb, Bf present
               WHEN SB = 70 AND BF > 0 AND BF + SW >= 20 THEN 'SbDee-bf'
               -- Noteable component of Sb, little Bf and Hrdwd transitions to SbMx1
               ELSE 'SbDee-conif'
           END

       WHEN (PJ >= 70 AND PO + BW <= 20)  OR (PJ >= 50 AND PO + BW <= 20 AND sage >= 120)  OR (PJ >= 70 AND ecopri IN ('B034', 'B035') ) THEN -- 'PjSha1'
           'Pj' ||
           CASE WHEN ecogrp = 'shallow' THEN 'Sha'
                ELSE 'Dee'
            END ||
           --WHEN (PJ >= 50 AND PO + BW <= 20 AND sage >= 120) AND ecogrp = 'shallow' THEN 'PjSha2'
           -- ? What about PjSha and PjDee with the Pj 50 age requirement? Would it be on the '-CM' path?
           CASE
           -- Vary pure jack pine staying Pj for a long time transitioning back to self but poorer quality
               WHEN PJ >= 80 THEN '-pure'
               -- Boarderline hardwood limit with no Bf curve with late transition to ConMx
               -- Limited to deep eco's
               WHEN (ecogrp <> 'shallow' OR ecogrp IS NULL) AND PJ = 70 AND AllHrd >= 20 AND BF = 0 THEN '-hwdlimit'
               -- Noteable component of Pj, Bf present transition to BfMx1
               WHEN PJ >= 70 AND BF > 0 AND BF + SW >= 20 THEN '-bf'
               -- Little Bf or hwd, Curve with long tail transitioning to PjMx1
               WHEN PJ < 70 AND SB >= 40 THEN '-pjlt70sb'
               -- Pj less than 70 with 20 or less Sb
               WHEN PJ < 70 THEN '-pjlt70'
               WHEN SB + BF <= 10 THEN '-pjmix'
               WHEN SB + BF > 10 AND BF >= SB THEN '-bfmix'
               WHEN SB + BF > 10 AND SB > BF THEN '-sbmix'
               -- error trap
               ELSE '-err'
           END
       WHEN PO >= 70 AND ecogrp = 'shallow' THEN --'PoSha0'
           CASE
               -- Very pure Po stands with late transition to HrdMw only for eco's not moist
               -- Concern about moist sites not hanging on as long (AND NOT 'moist').  AND (RIGHT(ecosite,3)::Int) > 108
               WHEN PO >= 80 THEN 'PoSha-pure' -- Less than moist because shallow
               -- Rest no moisture considerations
               -- Conifer present with little Bw early transition to HrdMw
               -- Some concern about non 10's in the spcomp. Do we need to adjust
               WHEN AllCon = 30 OR (AllCon = 20 AND PB = 10) THEN 'PoSha-conif'
               -- Stands that heve enough UH, LH to carry the othhd FU when PO slips bellow 70
               ELSE 'PoSha-hwd'
           END
       WHEN PO >= 70 THEN --'PoDee0'
           CASE
               -- Very pure Po stands with late transition to HrdMw only for eco's not moist
               -- Concern about moist sites not hanging on as long (AND NOT 'moist').  AND (RIGHT(ecosite,3)::Int) > 108
               WHEN PO >= 80 AND econum <= 108 THEN 'PoDee-purenmst' -- Less than moist
               -- Rest no moisture considerations
               -- Conifer present with little Bw early transition to HrdMw
               -- Some concern about non 10's in the spcomp. Do we need to adjust
               WHEN AllCon = 30 OR (AllCon = 20 AND PB = 10) THEN 'PoDee-conif'
               -- Stands that heve enough UH, LH to carry the othhd FU when PO slips bellow 70
               WHEN UH + LH = 30 AND AB > 0 THEN 'PoDee-abothhd'
               -- Really pure hardwood stand late transition to HrDom
               ELSE 'PoDee-hwd'
           END

       WHEN BW >= 60 AND BW + PO >= 70  THEN --'BwSha0'/'BwDee'
           'Bw' || CASE WHEN ecogrp='shallow' THEN 'Sha' ELSE 'Dee' END ||
           CASE
               -- Really pure hardwood stand transition to HrDom
               --** Issue with other hardwoods not being accounted for
               WHEN BW + PO + UH + LH >= 80 AND UH + LH < 30 AND BW = 60 AND SB + BF <= 10 THEN '-hwdpure'
               -- Very pure Bw + Po stands with late transition to self (OR HrdMx)
               --** Issue with other hardwoods not being accounted for
               WHEN BW + PO + UH + LH >= 80 AND UH + LH < 30 THEN '-bwpure'
               -- Conifer present with little Bw early transition to HrdMw
               WHEN AllCon >= 20 AND BW + PO = 70 THEN '-conif'
               -- Stands that heve enough UH, LH to carry the othhd FU when PO slips bellow 70
               WHEN UH + LH = 30 THEN '-othhd'
               -- Error trap is there anything falling through?
               ELSE '-err'
           END

       WHEN UH + LH >= 30 THEN
          CASE
              WHEN sleadspc in ('AB', 'AX') THEN 'OthHd-ab'
              WHEN sleadspc = 'Pb' THEN 'OthHd-pb'
              ELSE 'OthHd-other'
          END

       WHEN AllCon >= 70 AND BF <= 10 AND PO + BW <= 20 AND (SB + SW) > PJ AND PJ + SB + SW >= 40 THEN --'SbMx10'
           'SbMx1' ||
           CASE
               -- SbMx1 stands that would transition to SbDom shallow or deep
               WHEN SB + PJ >= 90 AND ecogrp = 'shallow' THEN '-sbsha'
               WHEN SB + PJ >= 90 AND (ecogrp <> 'shallow' OR ecogrp IS NULL) THEN '-sbdee'
               -- Noteable component of Pj, Bf present that transition to BfMx1
               WHEN BF = 10 AND BF + SW >= 20 AND ecogrp = 'shallow' THEN '-bfsha'
               WHEN BF = 10 AND BF + SW >= 20 AND (ecogrp <> 'shallow' OR ecogrp IS NULL) THEN '-bfdee'
               -- These are later transitions to SbDom shallow or deep
               WHEN ecogrp = 'shallow' THEN '-mixsha'
               WHEN (ecogrp <> 'shallow' OR ecogrp IS NULL) THEN '-mixdee'
               ELSE '-err'
           END
       WHEN AllCon >= 70 AND BF <= 10 AND PO + BW <= 20 AND (SB + SW) <= PJ AND PJ + SB + SW >= 40 THEN --'PjMx10'
           'PjMx1' ||
           CASE
               WHEN SB < 40 AND PO + BW = 20 AND BF >= 10 AND ecogrp = 'shallow' THEN '-bfsha'
               WHEN SB < 40 AND PO + BW = 20 AND BF >= 10 AND (ecogrp <> 'shallow' OR ecogrp IS NULL) THEN '-bfdee'
               -- Little Bf present but PO + BW that transition to ConMx, Bf requirement is redundant
               WHEN SB < 40 AND PO + BW = 20 AND BF < 10 AND ecogrp = 'shallow' THEN '-conifmixsha'
               WHEN SB < 40 AND PO + BW = 20 AND BF < 10 AND (ecogrp <> 'shallow' OR ecogrp IS NULL) THEN '-conifmixdee'
               -- PjMx1 stands that would transition to SbMx1 stands later
               WHEN ecogrp = 'shallow' THEN '-sbmixsha'
               WHEN (ecogrp <> 'shallow' OR ecogrp IS NULL) THEN '-sbmixdee'
               -- Error trap
               ELSE '-err'
           END
       WHEN BF >= 70 THEN 'BfPur-all'
       WHEN AllCon >= 70 AND BF > 10 AND BF + SW >= 30 THEN 'BfMx1' ||
           CASE
               -- Notable Sb component shallow or deep
               WHEN SB = 60 AND AllHrd <= 10 AND econum > 108 THEN '-sbdeepmoist'  -- no shallow option
               WHEN BF = 60 OR (BF >= 50 AND BW + PO + Pj >= 20) THEN '-bf'  -- Transition to BfPur
               WHEN ecogrp = 'shallow' THEN '-sha'
               WHEN (ecogrp <> 'shallow' OR ecogrp IS NULL) THEN '-dee'
               ELSE '-err'
           END
       WHEN AllHrd >= 70 THEN 'HrDom' ||
           CASE
               -- Bf hrdom au's added in hrdom to reflect the longer trans to Bfmx1. Need discussion.
               -- Rather hold here even though may truly be hrdmw (treat as ephemeral). Needed to avoid the quick trans post succession.
               -- Not spot in hwdmw to put these stands as would take longer to build 70 conifer
               -- So better error is to leave here even though doen not fit hrdom.
               WHEN BF >= 20 AND ecogrp = 'shallow' THEN '-bfsha'  -- Not many hrdom's on shallows
               WHEN BF >= 20 AND (ecogrp <> 'shallow' OR ecogrp IS NULL) THEN '-bfdee'
               WHEN ecogrp = 'shallow' THEN '-hwdsha'
               WHEN (ecogrp <> 'shallow' OR ecogrp IS NULL) THEN '-hwddee'
               ELSE '-err'
           END
       WHEN AllHrd >= 50 THEN 'HrdMw' ||
           CASE
               WHEN BF >= 10 AND BF + SW >= 20 AND ecogrp = 'shallow' THEN '-bfsha'
               WHEN BF >= 10 AND BF + SW >= 20 AND (ecogrp <> 'shallow' OR ecogrp IS NULL) THEN '-bfdee'
               WHEN SB >= 40 AND ecogrp = 'shallow' THEN '-sbsha'
               WHEN SB >= 40 AND (ecogrp <> 'shallow' OR ecogrp IS NULL) THEN '-sbdee'
               -- elseshe and dee should be 60/40 50/50 hwd/con with little bf or sb<=30
               -- Noteable PO+BW component transitions later to ConMx
               WHEN ecogrp = 'shallow' THEN '-mixsha'
               WHEN (ecogrp <> 'shallow' OR ecogrp IS NULL) THEN '-mixdee'
               ELSE '-err'
           END
       WHEN AllCon >= 50 THEN 'ConMx' ||
           CASE
               -- Where significant PJ with only other species PO + BW transitions to PjDom for a long time shallow or deep
               WHEN PJ = 70 AND PO + BW = 30 AND ecogrp = 'shallow' THEN '-pjsha'
               WHEN PJ = 70 AND PO + BW = 30 AND (ecogrp <> 'shallow' OR ecogrp IS NULL) THEN '-pjdee'
               -- Where significant SB  with only other species PO + BW transitions to SbDom shallow or deep
               WHEN SB >= 60 AND PO + BW >= 30 AND ecogrp = 'shallow' THEN '-sbsha'
               WHEN SB >= 60 AND PO + BW >= 30 AND (ecogrp <> 'shallow' OR ecogrp IS NULL) THEN '-sbdee'
               -- Shallow and deep added due to from AU needing the shallow deep break
               -- Where significant PJ, SB  with only other species PO + BW transitions to PjMx1
               WHEN PJ + SB >= 60 AND PJ > SB AND PO + BW >= 30 AND ecogrp = 'shallow' THEN '-pjmixsha'
               WHEN PJ + SB >= 60 AND PJ > SB AND PO + BW >= 30 AND (ecogrp <> 'shallow' OR ecogrp IS NULL) THEN '-pjmixdee'
               -- Where significant SB, SW, PJ  with only other species PO + BW transitions to SbMx1
               WHEN PJ + SB + SW >= 60 AND PO + BW >= 30 AND ecogrp = 'shallow' THEN '-sbmixsha'
               WHEN PJ + SB + SW >= 60 AND PO + BW >= 30 AND (ecogrp <> 'shallow' OR ecogrp IS NULL) THEN '-sbmixdee'
               -- Where significant BF transitions to BfPur
               WHEN BF >= 50 THEN '-bfpure'
               -- Where BF transitions to BfMx1
               WHEN BF >= 20 AND ecogrp = 'shallow' THEN '-bfmixsha'
               WHEN BF >= 20 AND (ecogrp <> 'shallow' OR ecogrp IS NULL) THEN '-bfmixdee'
               -- Remainder broken by shallow and deep
               WHEN ecogrp = 'shallow' THEN '-mixsha'
               WHEN (ecogrp <> 'shallow' OR ecogrp IS NULL) THEN '-mixdee'
               ELSE '-err'
           END
       ELSE NULL END AS nwau,
       '     ' as nwsfu,
       '     ' as lgfu
  FROM spc_group s
       JOIN
       baseclass f ON f.fid = s.fid
;

-- Standard Forest Units are a grouping of Analysis Units
UPDATE nwsfu
   SET nwsfu = substr(nwau, 1, 5)
WHERE nwau IS NOT NULL;

-- Landscape Guide Forest Units are a grouping of Standard Forest Units
UPDATE nwsfu
    SET lgfu = CASE
        WHEN nwsfu in ('BfMx1', 'BfDom' ) THEN 'BfDom'
        WHEN nwsfu in ('BwDee', 'BwSha' ) THEN 'BwDom'
        WHEN nwsfu in ('UplCe'          ) THEN 'OCLow'
        WHEN nwsfu in ('PjDee', 'PjSha' ) THEN 'PjDom'
        WHEN nwsfu in ('PoDee', 'PoSha' ) THEN 'PoDom'
        WHEN nwsfu in ('PwDom', 'PrDom' ) THEN 'PrwMx'
        WHEN nwsfu in ('SbDee', 'SbSha' ) THEN 'SbDom'
        ELSE nwsfu  END
        WHERE nwau IS NOT NULL;


DROP VIEW IF EXISTS nwclass ;
CREATE VIEW nwclass AS
SELECT
    b.fid,
    CASE
        WHEN b.plan_start_age < s.imm THEN 'PSap'
        WHEN s.lgfu in ( 'OCLow', 'SbLow', 'PrwMx', 'SbMx1', 'SbDom', 'PjDom', 'PjMx1', 'BfDom', 'ConMx' ) and b.plan_start_age >= s.imm and b.plan_start_age < s.mat THEN 'ICon'
        WHEN s.lgfu in ( 'PoDom', 'OthHd','BwDom', 'HrDom','HrdMw'  ) and b.plan_start_age >= s.imm and b.plan_start_age < s.mat THEN 'IHwd'
        WHEN s.lgfu in ( 'OCLow', 'SbLow' ) THEN 'MLLCon'
        WHEN s.lgfu in ( 'PrwMx', 'SbMx1', 'SbDom', 'PjDom', 'PjMx1', 'ConMx' ) THEN 'MLUCon'
        WHEN s.lgfu in ( 'BfDom' ) THEN 'MLBf' WHEN s.lgfu in ( 'PoDom', 'OthHd', 'BwDom', 'HrDom', 'HrdMw'  ) THEN 'MLHwd'
        WHEN s.lgfu in ( 'BfDom' ) THEN 'MLBf' ELSE NULL END as lgcls,
    CASE WHEN s.cwu IS NULL THEN 0 ELSE b.plan_start_age >= s.cwu END as lgcwu,
    CASE WHEN s.cwp IS NULL THEN 0 ELSE b.plan_start_age >= s.cwp END as lgcwp,
    CASE WHEN s.cw  IS NULL THEN 0  ELSE b.plan_start_age >= s.cw  END as lgcw,
    CASE WHEN s.cr  IS NULL THEN 0 ELSE b.plan_start_age >= s.cr  END as lgcr,
    s.ogfu as lg_og_fu,
    CASE WHEN b.plan_start_age >= s.ogonset AND b.plan_start_age < s.ogonset + s.ogdurat THEN 1 ELSE 0 END as lgog
    FROM
        baseclass b
        LEFT JOIN nwsfu n
            ON b.fid = n.fid
        LEFT JOIN lut_sfu s ON s.sfu = n.nwsfu ;
