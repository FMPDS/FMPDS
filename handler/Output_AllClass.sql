CREATE VIEW all_class AS
 SELECT b.fid,
    b.ecopri,
    b.econum,
    b.ecogrp,
    b.story,
    b.sspcomp,
    b.syrorg,
    b.sleadspc,
    b.sage,
    b.sht,
    b.ssc,
    b.scclo,
    b.sstkg,
    g.si,
    g.plan_start_age,
    g.plan_start_ht,
    s.nwau,
    s.nwsfu,
    s.lgfu,
    n.lgcls,
    n.lgcwu,
    n.lgcwp,
    n.lgcw,
    n.lgcr,
    n.lg_og_fu,
    n.lgog,
    bc.capwint,
    bc.capref,
    bc.wintsuit,
    bc.refuge,
    bc.mooprod,
    bc.moopst,
    bc.moopend,
    r.PO,
    r.PJ,
    r.PR,
    r.PW,
    r.SB,
    r.SW,
    r.LA,
    r.CW,
    r.BF,
    r.BW,
    r.UH,
    r.LH,
    r.OC
FROM baseclass b
   LEFT JOIN BioClass bc ON bc.fid= b.fid
   LEFT JOIN HT_Grow g   ON g.fid = b.fid
   LEFT JOIN spc_group r ON r.fid = b.fid
   LEFT JOIN nwsfu   s   ON s.fid = b.fid
   LEFT JOIN nwclass n   ON n.fid = b.fid
   ;

BEGIN ;
DROP VIEW IF EXISTS pci_allclass ;
CREATE VIEW pci_allclass AS
SELECT f.fid, f.geom, f.POLYID, f.POLYTYPE,
    CASE WHEN f.DEVSTAGE in (' ', '', 0 ) THEN NULL ELSE f.DEVSTAGE END AS DEVSTAGE,
    CASE WHEN f.YRDEP    in (' ', '', 0 ) THEN NULL ELSE f.YRDEP    END AS YRDEP,
    CASE WHEN f.DEPTYPE  in (' ', '', 0 ) THEN NULL ELSE f.DEPTYPE  END AS DEPTYPE,
    b.ecopri,
    b.econum,
    b.ecogrp,
    b.story,
    b.sspcomp,
    b.syrorg,
    b.sleadspc,
    b.sage,
    b.sht,
    b.ssc,
    b.scclo,
    b.sstkg,
    h.si,
    h.plan_start_age,
    h.plan_start_ht,
    s.nwau,
    s.nwsfu,
    s.lgfu,
    n.lgcls,
    n.lgcwu,
    n.lgcwp,
    n.lgcw,
    n.lgcr,
    n.lg_og_fu,
    n.lgog,
    bc.capwint,
    bc.capref,
    bc.wintsuit,
    bc.refuge,
    bc.mooprod,
    bc.moopst,
    bc.moopend,
    r.PO,
    r.PJ,
    r.PR,
    r.PW,
    r.SB,
    r.SW,
    r.LA,
    r.CW,
    r.BF,
    r.BW,
    r.UH,
    r.LH,
    r.OC
FROM planningcompositeinventory  f LEFT JOIN
   LEFT JOIN baseclass b ON b.fid = f.fid
   LEFT JOIN BioClass bc ON bc.fid= b.fid
   LEFT JOIN HT_Grow h   ON h.fid = b.fid
   LEFT JOIN spc_group r ON r.fid = b.fid
   LEFT JOIN nwsfu   s   ON s.fid = b.fid
   LEFT JOIN nwclass n   ON n.fid = b.fid
;

DELETE FROM gpkg_contents WHERE table_name = 'pci_allclass' ;

INSERT INTO gpkg_contents (table_name, data_type, identifier, min_x, min_y, max_x, max_y, srs_id)
SELECT 'pci_allclass' as table_name, data_type, 'pci_allclass' as identifier, min_x, min_y, max_x, max_y, srs_id
FROM gpkg_contents WHERE table_name = 'planningcompositeinventory' ;


DELETE FROM gpkg_geometry_columns WHERE table_name = 'pci_allclass' ;

INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m )
SELECT 'pci_allclass' as table_name, column_name, geometry_type_name, srs_id, z, m
FROM gpkg_geometry_columns WHERE table_name = 'planningcompositeinventory' ;

DELETE FROM gpkg_ogr_contents WHERE table_name = 'pci_allclass' ;

INSERT INTO gpkg_ogr_contents (table_name, feature_count )
SELECT 'pci_allclass' as table_name, feature_count
FROM gpkg_ogr_contents WHERE table_name = 'planningcompositeinventory' ;

COMMIT ;


