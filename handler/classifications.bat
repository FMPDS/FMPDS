@echo off
::
:: BAT FILE example of classifications processing for PCI
::

SETLOCAL

::echo %~1
IF "%~1"=="" GOTO NO_GPKG
SET GPKG=%~1

GOTO GOT_GPKG

:NO_GPKG
echo Can't find Geopackage...
GOTO END
SET GPKG=F:\Whitefeather\PCI\2019\MU994_2022PCI.gpkg

:GOT_GPKG
FOR %%f in ( %GPKG% ) do SET FOLDER=%%~dpf
echo Output CSV Files to %FOLDER%
echo Input SQL Files in %~dp0


::echo %~2
IF "%~2"=="--CLEANUP" GOTO NO_YEAR
IF "%~2"=="--Step1" GOTO NO_YEAR
IF "%~2"=="--Step2" GOTO NO_YEAR
IF "%~2"=="--Step3" GOTO NO_YEAR
IF "%~2"=="--Step4" GOTO NO_YEAR
IF "%~2"=="--Step5" GOTO NO_YEAR
IF "%~2"=="--Step6" GOTO NO_YEAR
IF "%~2"=="--Step7" GOTO NO_YEAR
IF "%~2"=="" GOTO NO_YEAR

SET YEAR=%~2
GOTO GOT_YEAR

:NO_YEAR
FOR %%f in ( %GPKG%\.. ) do SET YEAR=%%~nf

:GOT_YEAR
echo Using Year %YEAR%


::echo %~3
IF "%~9"=="--CLEANUP" SET DO_CLEANUP=1
IF "%~8"=="--CLEANUP" SET DO_CLEANUP=1
IF "%~7"=="--CLEANUP" SET DO_CLEANUP=1
IF "%~6"=="--CLEANUP" SET DO_CLEANUP=1
IF "%~5"=="--CLEANUP" SET DO_CLEANUP=1
IF "%~4"=="--CLEANUP" SET DO_CLEANUP=1
IF "%~3"=="--CLEANUP" SET DO_CLEANUP=1
IF "%~2"=="--CLEANUP" SET DO_CLEANUP=1

IF "%~1"=="--Step1" SET DO_STEP1=1
IF "%~2"=="--Step1" SET DO_STEP1=1
IF "%~3"=="--Step1" SET DO_STEP1=1
IF "%~4"=="--Step1" SET DO_STEP1=1
IF "%~5"=="--Step1" SET DO_STEP1=1
IF "%~6"=="--Step1" SET DO_STEP1=1
IF "%~7"=="--Step1" SET DO_STEP1=1
IF "%~8"=="--Step1" SET DO_STEP1=1
IF "%~9"=="--Step1" SET DO_STEP1=1

IF "%~1"=="--Step2" SET DO_STEP2=2
IF "%~2"=="--Step2" SET DO_STEP2=2
IF "%~3"=="--Step2" SET DO_STEP2=2
IF "%~4"=="--Step2" SET DO_STEP2=2
IF "%~5"=="--Step2" SET DO_STEP2=2
IF "%~6"=="--Step2" SET DO_STEP2=2
IF "%~7"=="--Step2" SET DO_STEP2=2
IF "%~8"=="--Step2" SET DO_STEP2=2
IF "%~9"=="--Step2" SET DO_STEP2=2

IF "%~1"=="--Step3" SET DO_STEP3=3
IF "%~2"=="--Step3" SET DO_STEP3=3
IF "%~3"=="--Step3" SET DO_STEP3=3
IF "%~4"=="--Step3" SET DO_STEP3=3
IF "%~5"=="--Step3" SET DO_STEP3=3
IF "%~6"=="--Step3" SET DO_STEP3=3
IF "%~7"=="--Step3" SET DO_STEP3=3
IF "%~8"=="--Step3" SET DO_STEP3=3
IF "%~9"=="--Step3" SET DO_STEP3=3

IF "%~1"=="--Step4" SET DO_STEP4=4
IF "%~2"=="--Step4" SET DO_STEP4=4
IF "%~3"=="--Step4" SET DO_STEP4=4
IF "%~4"=="--Step4" SET DO_STEP4=4
IF "%~5"=="--Step4" SET DO_STEP4=4
IF "%~6"=="--Step4" SET DO_STEP4=4
IF "%~7"=="--Step4" SET DO_STEP4=4
IF "%~8"=="--Step4" SET DO_STEP4=4
IF "%~9"=="--Step4" SET DO_STEP4=4

IF "%~1"=="--Step5" SET DO_STEP5=5
IF "%~2"=="--Step5" SET DO_STEP5=5
IF "%~3"=="--Step5" SET DO_STEP5=5
IF "%~4"=="--Step5" SET DO_STEP5=5
IF "%~5"=="--Step5" SET DO_STEP5=5
IF "%~6"=="--Step5" SET DO_STEP5=5
IF "%~7"=="--Step5" SET DO_STEP5=5
IF "%~8"=="--Step5" SET DO_STEP5=5
IF "%~9"=="--Step5" SET DO_STEP5=5

IF "%~1"=="--Step6" SET DO_STEP6=6
IF "%~2"=="--Step6" SET DO_STEP6=6
IF "%~3"=="--Step6" SET DO_STEP6=6
IF "%~4"=="--Step6" SET DO_STEP6=6
IF "%~5"=="--Step6" SET DO_STEP6=6
IF "%~6"=="--Step6" SET DO_STEP6=6
IF "%~7"=="--Step6" SET DO_STEP6=6
IF "%~8"=="--Step6" SET DO_STEP6=6
IF "%~9"=="--Step6" SET DO_STEP6=6

IF "%~1"=="--Step7" SET DO_STEP7=7
IF "%~2"=="--Step7" SET DO_STEP7=7
IF "%~3"=="--Step7" SET DO_STEP7=7
IF "%~4"=="--Step7" SET DO_STEP7=7
IF "%~5"=="--Step7" SET DO_STEP7=7
IF "%~6"=="--Step7" SET DO_STEP7=7
IF "%~7"=="--Step7" SET DO_STEP7=7
IF "%~8"=="--Step7" SET DO_STEP7=7
IF "%~9"=="--Step7" SET DO_STEP7=7

SET DO_ALLSTEP=bananas
FOR %%f IN  ( %DO_STEP1%  %DO_STEP2%  %DO_STEP3%  %DO_STEP4%  %DO_STEP5% %DO_STEP6% %DO_STEP7% ) do SET DO_ALLSTEP=
IF defined DO_ALLSTEP SET DO_STEP1=1
IF defined DO_ALLSTEP SET DO_STEP2=1
IF defined DO_ALLSTEP SET DO_STEP3=1
IF defined DO_ALLSTEP SET DO_STEP4=1
IF defined DO_ALLSTEP SET DO_STEP5=1
IF defined DO_ALLSTEP SET DO_STEP6=1
IF defined DO_ALLSTEP SET DO_STEP7=1


:: Generate all the variables first....
::GOTO GenVariables

GOTO DISPATCH


:DISPATCH
::CLEANUP First
IF defined DO_CLEANUP GOTO CLEANUP
:CLEANUPDone


::Step1 Generates ogrinfo to a text file
echo Step1 Generates ogrinfo to a text file
IF defined DO_STEP1 GOTO Step1
::GOTO Step1
:Step1Done

::Step2 Builds the LUT for ecogrp
echo Step2 Builds the LUT for ecogrp
IF defined DO_STEP2 GOTO Step2
::GOTO Step2
:Step2Done

::Step3 Builds baseclass (chooses canopy from VERT)
echo Step3 Builds baseclass (chooses canopy from VERT)
IF defined DO_STEP3 GOTO Step3
::GOTO Step3
:Step3Done

::Step4 Creates Grown Height
echo Step4 Creates Grown Height
IF defined DO_STEP4 GOTO Step4
::GOTO Step4
:Step4Done

::Step5 Species Breakout and grouping, AU's, SFU's, LGFU's, NWClass
echo Step5 Species Breakout and grouping, AU's, SFU's, LGFU's, NWClass
IF defined DO_STEP5 GOTO Step5
::GOTO Step5
:Step5Done

::Step6 Builds out the "Bio Classes"
echo Step6 Builds out the "Bio Classes"
IF defined DO_STEP6 GOTO Step6
::GOTO Step6
:Step6Done

::Step7 All Class and generate CSV files
echo Step7 All Class and generate CSV files
IF defined DO_STEP7 GOTO Step7
::GOTO Step7
:Step7Done


GOTO END

:CLEANUP
:: Cleanup commands
echo Cleanup
sqlite3 -echo %GPKG% "DROP TABLE if exists lut_ecogrp ; "
sqlite3 -echo %GPKG% "DROP TABLE if exists lut_sfu ;"
sqlite3 -echo %GPKG% "DROP TABLE if exists baseclass ; "
sqlite3 -echo %GPKG% "DROP TABLE if exists HT_Grow ; "
sqlite3 -echo %GPKG% "DROP TABLE if exists spc_breakout ; "
sqlite3 -echo %GPKG% "DROP TABLE if exists spc_group ; "
sqlite3 -echo %GPKG% "DROP TABLE if exists nwsfu ; "
sqlite3 -echo %GPKG% "DROP VIEW  if exists nwclass ; "
sqlite3 -echo %GPKG% "DROP VIEW  if exists BioClass ; "
sqlite3 -echo %GPKG% "DROP TABLE if exists lut_elceco_caribou ; "
sqlite3 -echo %GPKG% "DROP VIEW  if exists lut_elceco_caribou2 ; "
sqlite3 -echo %GPKG% "DROP VIEW  if exists all_class ; "
sqlite3 -echo %GPKG% "DROP VIEW  if exists pci_allclass ; DELETE FROM gpkg_geometry_columns WHERE table_name = 'pci_allclass' ; DELETE FROM gpkg_ogr_contents WHERE table_name = 'pci_allclass' ; DELETE FROM gpkg_contents WHERE table_name = 'pci_allclass' ; "

GOTO CLEANUPDone

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:Step1
::Step1 Generates ogrinfo to a text file

FOR %%f in ( %GPKG% ) do SET BASENAME=%%~dpnxf
::  %%~dpf is FOLDER
:: ECHO %FOLDER%

::ECHO %BASENAME%
SET OGR_OUTPUT=%BASENAME%_ogrinfo.txt

:: OGRINFO to a file for info's sake
ogrinfo %GPKG% -listmdd -al -fid 1111 -fields=yes -geom=summary >> %OGR_OUTPUT%
START notepad %OGR_OUTPUT%

GOTO Step1Done

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:Step2
:: Generate Lookup Tables for ecogrp, sfu / lgfu, caribou from ecosite
:: d:\PostgreSQL\10\bin\pg_dump.exe -h localhost -U carran -d fmp -p 5432  -t  code.lut_ecogrp  --column-inserts | sed "s/code.//g" | ${Do_Some_Manual_editting} > lut_sfu.sql    :-)
:: d:\PostgreSQL\10\bin\pg_dump.exe -h localhost -U carran -d fmp -p 5432  -t  beemerga.fk_sfu_lgfu  --column-inserts
:: d:\PostgreSQL\10\bin\pg_dump.exe -h localhost -U carran -d fmp -p 5432  -t  code.lut_elceo_caribou  --column-inserts
sqlite3 %GPKG% -init %~dp0lut_sfu.sql ".exit"

GOTO Step2Done


:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:Step3
:: Generate the baseclass view - this one contains the VERT-chosen canopy
::   as the "S" Canopy
sqlite3 %GPKG% "CREATE TABLE baseclass AS SELECT f.fid, substr(f.PRI_ECO,1,4) AS ecopri, CAST(substr(f.pri_eco, 2, 3) AS integer) AS econum, e.ecogrp, canopy.story as story, upper(canopy.sspcomp) as sspcomp, canopy.syrorg, %YEAR% - canopy.syrorg as plan_start_age, upper(canopy.sleadspc) as sleadspc, canopy.sage, canopy.sht, canopy.scclo, canopy.sstkg, canopy.ssc FROM planningcompositeinventory f LEFT JOIN ( SELECT fid, 'Under' as story, uspcomp as sspcomp, uyrorg as syrorg, uleadspc as sleadspc, uage as sage, CAST( uht AS FLOAT) as sht, ucclo as scclo, ustkg as sstkg, usc as ssc FROM planningcompositeinventory WHERE upper(VERT) IN ('TU', 'MU') AND POLYTYPE = 'FOR'   UNION  SELECT fid, 'Over'  as story, ospcomp as sspcomp, oyrorg as syrorg, oleadspc as sleadspc, oage as sage, CAST(oht AS FLOAT) as sht, occlo as scclo, ostkg as sstkg, osc as ssc FROM planningcompositeinventory WHERE upper(VERT) NOT IN ('TU', 'MU') AND POLYTYPE = 'FOR' ) canopy ON canopy.fid = f.fid  LEFT JOIN lut_ecogrp e ON e.ecosite = substr(f.PRI_ECO,1,4) WHERE f.POLYTYPE = 'FOR' ;"
GOTO Step3Done

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:Step4
:: Attach the SIHI database to do height growth as a new table
SET SIHI=%~dp0si_ht_lookup.db


IF EXIST "%SIHI%" GOTO SIHI_BUILT

echo Generating Site Index and Height Index Lookup Table %SIHI%
SET MISTFOLDER=%~dp0..\stack\mist\
if EXIST %~dp0ht_equations_and_LUT.py SET MISTFOLDER=%~dp0

python %MISTFOLDER%ht_equations_and_LUT.py --sqlite | sqlite3 %SIHI%
python %MISTFOLDER%si_equations_and_LUT.py --sqlite | sqlite3  %SIHI%
python %MISTFOLDER%si_parameters.py --sqlite | sqlite3  %SIHI%

:: call %~dp0..\osgeo_env.bat %~dp0..\stack\mist\build_sqliteFile.bat %SIHI%

:SIHI_BUILT

sqlite3 %GPKG% "ATTACH DATABASE '%SIHI%' as SIHI ; CREATE TABLE HT_Grow AS SELECT fid, i.sleadspc, i.sht as height, i.sage as age, plan_start_age, si_lut.si as si, ht_lut.ht as plan_start_ht FROM baseclass i JOIN SIHI.si_lut ON si_lut.sp=upper(sleadspc) and si_lut.ht = round(i.sht,1) and si_lut.age = i.sage JOIN SIHI.ht_lut ON si_lut.sp = ht_lut.sp	and round(si_lut.si,1) = ht_lut.si and i.plan_start_age = ht_lut.age ; DETACH DATABASE SIHI ;"
GOTO Step4Done

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:Step5
:: Work the nwcalcs...
:: Species Breakout Join Table
:: Species Group Join Table
:: NW AU and SFU SQL
:: Aggregation of NW SFU into Landscape Guide FU's
:: NW Classification - LGFU and Age
sqlite3 %GPKG% -init %~dp0Analysis_and_NWSFU.sql ".exit"

GOTO Step5Done

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:Step6
sqlite3 %GPKG% -init %~dp0BIO_Classifications.sql ".exit"
GOTO Step6Done

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:Step7
sqlite3 -csv %GPKG% "SELECT * FROM BioClass ; " > %FOLDER%BioClass.csv
sqlite3 -csv %GPKG% "SELECT * FROM nwclass  ; " > %FOLDER%NWClass.csv
sqlite3 -csv %GPKG% "SELECT * FROM HT_Grow  ; " > %FOLDER%HT_Grow.csv
sqlite3 -csv %GPKG% -init %~dp0Output_AllClass.sql "SELECT * FROM all_class ; " > %FOLDER%all_class.csv

GOTO Step7Done



:END
ENDLOCAL
TIMEOUT /T 90
