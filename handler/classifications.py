#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  classifications.py
#
#  Copyright 2019 CARRAN <CARRAN@ON34C02585177>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import os
import sys
import ogr

import subprocess


sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
#Load from eith DEV location or DEPLOYMENT location
try:
	#print (os.path.dirname(os.path.dirname(os.path.abspath(__file__))) )
	from stack.mist.ht_equations_and_LUT import main as ht_equations_and_LUT
	from stack.mist.si_equations_and_LUT import main as si_equations_and_LUT
	from stack.mist.si_parameters import main as si_parameters
except ModuleNotFoundError:
	from ht_equations_and_LUT import main as ht_equations_and_LUT
	from si_equations_and_LUT import main as si_equations_and_LUT
	from si_parameters import main as si_parameters


class classifier():
	DS = None

	lut_sfu_location = os.path.join(os.path.dirname(os.path.abspath(__file__)),"lut_sfu.sql")
	Analysis_and_NWSFU_location  = os.path.join(os.path.dirname(os.path.abspath(__file__)),"Analysis_and_NWSFU.sql")
	BIO_Classifications_location  = os.path.join(os.path.dirname(os.path.abspath(__file__)),"BIO_Classifications.sql")
	Output_AllClass_location  = os.path.join(os.path.dirname(os.path.abspath(__file__)),"Output_AllClass.sql")

	def __init__(self,*args):
		self.parm = {}
		print (args)
		for a in [ u.upper() for u in args[0] ] :
			if os.path.exists(a):
				self.parm['FILENAME'] = a
			elif a.isdigit() :
				self.parm['YEAR'] = int(a)
			elif a[:2] == '--':
				self.parm[a[2:].upper()] = True

		#Find a year in the filename or parent directory...
		if 'YEAR' not in self.parm.keys():
			# Check the filename
			# If the filename is FIM Standard, it will be MU###_####PCI.gpkg or something similar.
			try:
				self.parm['YEAR'] = int((os.path.basename(self.parm['FILENAME']).split('_')[1])[0:4])
			except IndexError:
				# Check the parent directory
				self.parm['YEAR'] = int(os.path.basename(os.path.dirname(self.parm['FILENAME'])))

		print (self.parm)
		self.open_file()

	def go(self):
		steps={'CLEANUP':self.do_cleanup,
			'STEP1':self.do_step1,
			'STEP2':self.do_step2,
			'STEP3':self.do_step3,
			'STEP4':self.do_step4,
			'STEP5':self.do_step5,
			'STEP6':self.do_step6,
			'STEP7':self.do_step7,
			}

		if len([ k for k in steps.keys() if k in self.parm ]) == 0 :
			print ("No steps - assume all")
			for k,v in steps.items() :
				self.parm[k]=True

		print ( "Steps to be performed: \n\t{0}".format("\n\t".join( [ k for k,v in steps.items() if k in self.parm ] ) ) )

		for k,v in steps.items() :
			if k in self.parm:
				#print("Trying {0}".format(k))
				v()

	def open_file(self):
		self.DS = ogr.GetDriverByName('GPKG').Open(self.parm['FILENAME'],1)


	def call(self,*args):
		return subprocess.run(capture_output=True, *args)

	def do_step1(self):
		"""
		Run OGRINFO and dump to a file
		"""
		#os.path.join(os.path.join(os.environ['OSGEO4W_ROOT'],'bin'),'ogrinfo')
		res = self.call(["ogrinfo",self.parm['FILENAME'],"-listmdd","-al","-fid","1111","-fields=yes","-geom=summary"])

		with open(self.parm['FILENAME'].replace(".GPKG","_ogrinfo.txt"),"w") as f:
			f.write(res.stdout.decode("utf-8"))

		print (" OGRInfo Succeeded")

	def do_step2(self):
		"""
		Run SQLITE3 with an SQL File and dump to a file
		"""
		res = self.call(["sqlite3", self.parm['FILENAME'], "-init", self.lut_sfu_location, '.exit' ] )
		print (res.stdout.decode("utf-8") )
		print (" Step2 Succeeded ")

	def do_step3(self):
		"""
		Generate baseclass table
		"""
		sql= """CREATE TABLE baseclass AS
			SELECT
				f.fid, substr(f.PRI_ECO,1,4) AS ecopri,
				CAST(substr(f.pri_eco, 2, 3) AS integer) AS econum, e.ecogrp,
				canopy.story as story, upper(canopy.sspcomp) as sspcomp,
				canopy.syrorg, {year} - canopy.syrorg as plan_start_age,
				upper(canopy.sleadspc) as sleadspc, canopy.sage, canopy.sht,
				canopy.scclo, canopy.sstkg, canopy.ssc
			FROM planningcompositeinventory f LEFT JOIN (
				SELECT fid, 'Under' as story,
					uspcomp as sspcomp, uyrorg as syrorg, uleadspc as sleadspc,
					uage as sage, CAST( uht AS FLOAT) as sht, ucclo as scclo,
					ustkg as sstkg, usc as ssc
				FROM planningcompositeinventory WHERE upper(VERT) IN ('TU', 'MU') AND POLYTYPE = 'FOR'
				UNION  SELECT fid, 'Over'  as story, ospcomp as sspcomp,
					oyrorg as syrorg, oleadspc as sleadspc, oage as sage,
					CAST(oht AS FLOAT) as sht, occlo as scclo, ostkg as sstkg,
					osc as ssc
				FROM planningcompositeinventory
				WHERE upper(VERT) NOT IN ('TU', 'MU') AND POLYTYPE = 'FOR' ) canopy
				ON canopy.fid = f.fid
				LEFT JOIN lut_ecogrp e ON e.ecosite = substr(f.PRI_ECO,1,4)
			WHERE f.POLYTYPE = 'FOR' ;""".format(year=self.parm['YEAR'])
		self.DS.ExecuteSQL(sql)

		print (" Step3 Complete ")

	def do_step4(self):
		"""
		Site Index / Height Index Aging
		"""
		SIHI=os.path.join(os.path.dirname(os.path.abspath(__file__)),'si_ht_lookup.db')

		if not os.path.exists(SIHI):
			SIHIDS =ogr.GetDriverByName('SQLite').CreateDataSource(SIHI)

			print('Generating Site Index and Height Index Lookup Table {0}'.format(SIHI))
			for sql in ht_equations_and_LUT('--sqlite') :
				if sql in [ 'BEGIN;' , 'COMMIT;']: continue
				SIHIDS.ExecuteSQL(sql)

			msg = si_equations_and_LUT('--sqlite')
			if msg:
				for sql in msg :
					if sql in [ 'BEGIN;' , 'COMMIT;']: continue
					SIHIDS.ExecuteSQL(sql)

			for sql in si_parameters('--sqlite') :
				if sql in [ 'BEGIN;' , 'COMMIT;']: continue
				SIHIDS.ExecuteSQL(sql)
			#SIHIDS.close()
			#del(SIHIDS)

		sql="""
			ATTACH DATABASE '{SIHI}' as SIHI ;
			CREATE TABLE HT_Grow AS
			SELECT fid, i.sleadspc, i.sht as height, i.sage as age,
				plan_start_age, si_lut.si as si, round(ht_lut.ht,1) as plan_start_ht
				FROM baseclass i
				JOIN SIHI.si_lut ON si_lut.sp=upper(sleadspc) and
					si_lut.ht = round(i.sht,1) and si_lut.age = i.sage
				JOIN SIHI.ht_lut ON si_lut.sp = ht_lut.sp and
					round(si_lut.si,1) = ht_lut.si and i.plan_start_age = ht_lut.age ;
			DETACH DATABASE SIHI ;""".format(SIHI=SIHI)
		#print (sql)
		res = self.call(["sqlite3", self.parm['FILENAME'], sql ] )
		print (res.stdout.decode("utf-8") )
		print (res.stderr.decode("utf-8") )

		#print ( self.DS.ExecuteSQL(sql) )
		print (" Step 4 Done ")


	def do_step5(self):
		"""
		:: Work the nwcalcs...
		:: Species Breakout Join Table
		:: Species Group Join Table
		:: NW AU and SFU SQL
		:: Aggregation of NW SFU into Landscape Guide FU's
		:: NW Classification - LGFU and Age
		sqlite3 %GPKG% -init %~dp0Analysis_and_NWSFU.sql ".exit"
		"""
		res = self.call(["sqlite3", self.parm['FILENAME'], "-init", self.Analysis_and_NWSFU_location, '.exit' ] )
		print (res.stdout.decode("utf-8") )
		print (res.stderr.decode("utf-8") )
		print (" Step5 Done ")

	def do_step6(self):
		"""
		BIO Classifications
		sqlite3 %GPKG% -init %~dp0BIO_Classifications.sql ".exit"
		GOTO Step6Done
		"""
		res = self.call(["sqlite3", self.parm['FILENAME'], "-init", self.BIO_Classifications_location, '.exit' ] )
		print (res.stdout.decode("utf-8") )
		print (res.stderr.decode("utf-8") )
		print (" Step6Done ")

	def do_step7(self):
		"""
		:Step7
		sqlite3 -csv %GPKG% "SELECT * FROM BioClass ; " > %FOLDER%BioClass.csv
		sqlite3 -csv %GPKG% "SELECT * FROM nwclass  ; " > %FOLDER%NWClass.csv
		sqlite3 -csv %GPKG% "SELECT * FROM HT_Grow  ; " > %FOLDER%HT_Grow.csv
		sqlite3 -csv %GPKG% -init %~dp0Output_AllClass.sql "SELECT * FROM all_class ; " > %FOLDER%all_class.csv

		GOTO Step7Done

		"""
		res = self.call(["sqlite3", self.parm['FILENAME'], "-init", self.Output_AllClass_location, '.exit' ] )
		print (res.stdout.decode("utf-8") )
		print (res.stderr.decode("utf-8") )

		for t in [ 'BioClass','nwclass', 'HT_Grow', 'all_class' ]:
			res = self.call(["sqlite3", self.parm['FILENAME'], 'SELECT * FROM {0} ;'.format(t) ] )
			with open(os.path.join(os.path.dirname(self.parm['FILENAME']),'{0}.csv'.format(t)),"w") as f:
				f.write(res.stdout.decode("utf-8"))
			print (res.stderr.decode("utf-8") )


		print (" Step7Done ")


	def do_cleanup(self):
		print ("Doing Cleanup")
		self.DS.ExecuteSQL("DROP TABLE if exists lut_ecogrp ; ")
		self.DS.ExecuteSQL("DROP TABLE if exists lut_sfu ;")
		self.DS.ExecuteSQL("DROP TABLE if exists baseclass ; ")
		self.DS.ExecuteSQL("DROP TABLE if exists HT_Grow ; ")
		self.DS.ExecuteSQL("DROP TABLE if exists spc_breakout ; ")
		self.DS.ExecuteSQL("DROP TABLE if exists spc_group ; ")
		self.DS.ExecuteSQL("DROP TABLE if exists nwsfu ; ")
		self.DS.ExecuteSQL("DROP VIEW  if exists nwclass ; ")
		self.DS.ExecuteSQL("DROP VIEW  if exists BioClass ; ")
		self.DS.ExecuteSQL("DROP TABLE if exists lut_elceco_caribou ; ")
		self.DS.ExecuteSQL("DROP VIEW  if exists lut_elceco_caribou2 ; ")
		self.DS.ExecuteSQL("DROP VIEW  if exists all_class ; ")
		self.DS.ExecuteSQL("DROP VIEW  if exists pci_allclass ;")
		self.DS.ExecuteSQL("DELETE FROM gpkg_geometry_columns WHERE table_name = 'pci_allclass' ; ")
		self.DS.ExecuteSQL("DELETE FROM gpkg_ogr_contents WHERE table_name = 'pci_allclass' ; ")
		self.DS.ExecuteSQL("DELETE FROM gpkg_contents WHERE table_name = 'pci_allclass' ; ")








def main(*args):
	classifier(*args).go()
	return 0

if __name__ == '__main__':
	import sys
	#print ( os.path.join(os.path.join(os.environ['OSGEO4W_ROOT'],'bin'),'ogrinfo') )
	sys.exit(main(sys.argv[1:]))
	#main(r'C:\GIS\FMP\Whitefeather\PCI\2022\MU994_2022PCI.gpkg','2022','--CLEANUP','--Step1','--Step2', '--Step3')
	#main(r'C:\GIS\FMP\Whitefeather\PCI\2022\MU994_2022PCI.gpkg','2022', '--Step4')
	#main(r'C:\GIS\FMP\Whitefeather\PCI\2022\MU994_2022PCI.gpkg','2022', '--Step5')
	#main(r'C:\GIS\FMP\Whitefeather\PCI\2022\MU994_2022PCI.gpkg','2022', '--Step6')
	#main(r'C:\GIS\FMP\Whitefeather\PCI\2022\MU994_2022PCI.gpkg','2022', '--Step7')
	#main(r'C:\GIS\FMP\Whitefeather\PCI\2022\MU994_2022PCI.gpkg','2022' )
