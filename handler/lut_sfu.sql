DROP TABLE IF EXISTS lut_ecogrp ;
CREATE TABLE lut_ecogrp (
    ecosite character varying(4) NOT NULL PRIMARY KEY ,
    ecogrp character varying(8),
    mooseprod text
);

--
-- Data for Name: lut_ecogrp; Type: TABLE DATA; Schema:  Owner: angus.carr
--

INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B023', 'shallow', NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B025', 'shallow', NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B026', 'shallow', NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B027', 'shallow', NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B028', 'shallow', NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B132', 'hlowland', NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B001', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B003', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B004', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B005', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B006', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B007', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B008', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B009', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B010', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B032', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B038', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B044', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B046', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B047', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B063', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B075', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B079', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B093', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B096', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B111', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B112', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B123', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B145', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B158', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B159', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B161', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B162', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B164', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B165', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B167', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B168', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B171', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B172', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B189', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B190', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B191', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B193', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B195', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B197', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B198', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B200', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B011', 'shallow', 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B012', 'shallow', 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B013', 'uplce', 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B014', 'shallow', 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B015', 'shallow', 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B016', 'shallow', 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B017', 'shallow', 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B018', 'shallow', 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B019', 'shallow', 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B024', 'shallow', 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B033', NULL, 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B034', NULL, 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B039', NULL, 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B054', NULL, 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B062', NULL, 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B064', NULL, 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B065', NULL, 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B043', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B113', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B067', NULL, 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B068', NULL, 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B095', NULL, 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B126', 'clowland', 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B127', 'clowland', 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B128', 'clowland', 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B136', 'clowland', 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B137', 'clowland', 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B138', NULL, 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B139', NULL, 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B140', NULL, 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B146', NULL, 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B147', NULL, 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B148', NULL, 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B223', 'clowland', 'poor');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B035', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B036', 'uplce', 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B037', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B040', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B041', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B042', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B048', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B049', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B050', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B051', 'uplce', 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B052', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B053', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B055', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B056', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B057', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B058', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B059', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B066', 'uplce', 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B069', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B070', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B071', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B074', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B073', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B076', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B081', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B082', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B083', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B084', 'uplce', 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B087', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B085', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B097', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B098', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B099', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B100', 'uplce', 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B101', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B102', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B103', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B110', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B114', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B115', 'uplce', 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B116', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B117', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B129', 'clowland', 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B141', NULL, 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B222', 'clowland', 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B224', 'clowland', 'mod');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B088', NULL, 'rich');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B089', NULL, 'rich');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B092', NULL, 'rich');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B091', NULL, 'rich');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B104', NULL, 'rich');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B105', NULL, 'rich');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B106', NULL, 'rich');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B107', NULL, 'rich');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B108', NULL, 'rich');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B120', NULL, 'rich');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B109', NULL, NULL);
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B118', NULL, 'rich');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B119', NULL, 'rich');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B122', NULL, 'rich');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B125', NULL, 'rich');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B130', 'hlowland', 'rich');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B131', 'hlowland', 'rich');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B133', 'hlowland', 'rich');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B134', 'clowland', 'rich');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B135', 'clowland', 'rich');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B142', NULL, 'rich');
INSERT INTO lut_ecogrp (ecosite, ecogrp, mooseprod) VALUES ('B144', NULL, 'rich');




-- Table: beemerga.fk_sfu_lgfu
-- DROP TABLE beemerga.fk_sfu_lgfu;
DROP TABLE IF EXISTS lut_sfu ;
CREATE TABLE lut_sfu
(
  sfu character(5) NOT NULL PRIMARY KEY, lgfu character(5) NOT NULL,
  psap integer,  imm integer,  mat integer,  late integer,
  cwu integer,  cwp integer,  cw integer,  cr integer,
  pft character(5),  grp1 character(5),  grp2 character(5),
  ogonset integer,  ogdurat_org integer,  ogfu character(5),
  ogdurat integer,  ogrpton integer,  ogrptdur integer
) ;

INSERT INTO lut_sfu (sfu,lgfu, psap, imm, mat, late, cwu, cwp, cw, cr, pft, grp1, grp2, ogonset, ogdurat_org, ogfu, ogdurat, ogrpton, ogrptdur) VALUES  ( 'BwDee' , 'BwDom' , 5, 10, 50, 110, NULL, NULL, NULL, NULL, 'bwt' , 'BwDom' , 'BwDom', 110, 150, 'Mixed' , 40, 110, NULL);
INSERT INTO lut_sfu (sfu,lgfu, psap, imm, mat, late, cwu, cwp, cw, cr, pft, grp1, grp2, ogonset, ogdurat_org, ogfu, ogdurat, ogrpton, ogrptdur) VALUES  ( 'BwSha' , 'BwDom' , 5, 10, 50, 110, NULL, NULL, NULL, NULL, 'bwt' , 'BwDom' , 'BwDom', 110, 150, 'Mixed' , 40, 110, NULL);
INSERT INTO lut_sfu (sfu,lgfu, psap, imm, mat, late, cwu, cwp, cw, cr, pft, grp1, grp2, ogonset, ogdurat_org, ogfu, ogdurat, ogrpton, ogrptdur) VALUES  ( 'ConMx' , 'ConMx' , 10, 30, 70, 110, NULL, NULL, NULL, 70, 'mix' , 'ConMx' , 'ConMx', 100, 170, 'Mixed' , 70, 110, NULL);
INSERT INTO lut_sfu (sfu,lgfu, psap, imm, mat, late, cwu, cwp, cw, cr, pft, grp1, grp2, ogonset, ogdurat_org, ogfu, ogdurat, ogrpton, ogrptdur) VALUES  ( 'HrdMw' , 'HrdMw' , 5, 10, 60, 110, NULL, NULL, NULL, NULL, 'mix' , 'HrdMw' , 'HrdMw', 100, 150, 'Mixed' , 50, 100, NULL);
INSERT INTO lut_sfu (sfu,lgfu, psap, imm, mat, late, cwu, cwp, cw, cr, pft, grp1, grp2, ogonset, ogdurat_org, ogfu, ogdurat, ogrpton, ogrptdur) VALUES  ( 'HrDom' , 'HrDom' , 5, 10, 60, 100, NULL, NULL, NULL, NULL, 'tol' , 'HrDom' , 'HrDom', 100, 120, 'Mixed' , 20, 100, NULL);
INSERT INTO lut_sfu (sfu,lgfu, psap, imm, mat, late, cwu, cwp, cw, cr, pft, grp1, grp2, ogonset, ogdurat_org, ogfu, ogdurat, ogrpton, ogrptdur) VALUES  ( 'OCLow' , 'OCLow' , 10, 30, 70, 120, 50, NULL, 50, 0, 'mcl' , 'OCLow' , 'OCLow', 100, 310, 'LowCo' , 210, 100, NULL);
INSERT INTO lut_sfu (sfu,lgfu, psap, imm, mat, late, cwu, cwp, cw, cr, pft, grp1, grp2, ogonset, ogdurat_org, ogfu, ogdurat, ogrpton, ogrptdur) VALUES  ( 'OthHd' , 'OthHd' , 5, 10, 60, 100, NULL, NULL, NULL, NULL, 'tol' , 'OthHd' , 'OthHd', 90, 160, 'Mixed' , 70, 90, NULL);
INSERT INTO lut_sfu (sfu,lgfu, psap, imm, mat, late, cwu, cwp, cw, cr, pft, grp1, grp2, ogonset, ogdurat_org, ogfu, ogdurat, ogrpton, ogrptdur) VALUES  ( 'PjDee' , 'PjDom' , 10, 30, 70, 100, 40, 60, 40, 0, 'pjk' , 'PjDee' , 'PjDom', 100, 180, 'UplCo' , 80, 100, NULL);
INSERT INTO lut_sfu (sfu,lgfu, psap, imm, mat, late, cwu, cwp, cw, cr, pft, grp1, grp2, ogonset, ogdurat_org, ogfu, ogdurat, ogrpton, ogrptdur) VALUES  ( 'PjMx1' , 'PjMx1' , 10, 30, 70, 100, 40, 60, 40, 40, 'mcu' , 'PjMx1' , 'PjMx1', 100, 170, 'UplCo' , 70, 100, NULL);
INSERT INTO lut_sfu (sfu,lgfu, psap, imm, mat, late, cwu, cwp, cw, cr, pft, grp1, grp2, ogonset, ogdurat_org, ogfu, ogdurat, ogrpton, ogrptdur) VALUES  ( 'PjSha' , 'PjDom' , 10, 30, 70, 100, 40, 60, 40, 0, 'pjk' , 'PjSha' , 'PjDom', 100, 180, 'UplCo' , 80, 100, NULL);
INSERT INTO lut_sfu (sfu,lgfu, psap, imm, mat, late, cwu, cwp, cw, cr, pft, grp1, grp2, ogonset, ogdurat_org, ogfu, ogdurat, ogrpton, ogrptdur) VALUES  ( 'PoDee' , 'PoDom' , 5, 10, 60, 100, NULL, NULL, NULL, NULL, 'pop' , 'PoDom' , 'PoDom', 100, 150, 'Mixed' , 50, 100, NULL);
INSERT INTO lut_sfu (sfu,lgfu, psap, imm, mat, late, cwu, cwp, cw, cr, pft, grp1, grp2, ogonset, ogdurat_org, ogfu, ogdurat, ogrpton, ogrptdur) VALUES  ( 'PoSha' , 'PoDom' , 5, 10, 60, 100, NULL, NULL, NULL, NULL, 'pop' , 'PoDom' , 'PoDom', 100, 150, 'Mixed' , 50, 100, NULL);
INSERT INTO lut_sfu (sfu,lgfu, psap, imm, mat, late, cwu, cwp, cw, cr, pft, grp1, grp2, ogonset, ogdurat_org, ogfu, ogdurat, ogrpton, ogrptdur) VALUES  ( 'PrDom' , 'PrwMx' , 10, 20, 80, 140, NULL, NULL, NULL, NULL, 'pwr' , 'PrDom' , 'PrDom', 130, 240, 'Mixed' , 110, 130, NULL);
INSERT INTO lut_sfu (sfu,lgfu, psap, imm, mat, late, cwu, cwp, cw, cr, pft, grp1, grp2, ogonset, ogdurat_org, ogfu, ogdurat, ogrpton, ogrptdur) VALUES  ( 'PrwMx' , 'PrwMx' , 10, 20, 80, 140, NULL, NULL, NULL, NULL, 'pwr' , 'PrwMx' , 'PrwMx', 140, 275, 'Mixed' , 145, 150, NULL);
INSERT INTO lut_sfu (sfu,lgfu, psap, imm, mat, late, cwu, cwp, cw, cr, pft, grp1, grp2, ogonset, ogdurat_org, ogfu, ogdurat, ogrpton, ogrptdur) VALUES  ( 'PwDom' , 'PrwMx' , 10, 20, 80, 140, NULL, NULL, NULL, NULL, 'pwr' , 'PwDom' , 'PwDom', 150, 300, 'Mixed' , 150, 150, NULL);
INSERT INTO lut_sfu (sfu,lgfu, psap, imm, mat, late, cwu, cwp, cw, cr, pft, grp1, grp2, ogonset, ogdurat_org, ogfu, ogdurat, ogrpton, ogrptdur) VALUES  ( 'SbDee' , 'SbDom' , 10, 30, 70, 120, 60, NULL, 50, 0, 'mcu' , 'SbDom' , 'SbDom', 120, 190, 'UplCo' , 70, 120, NULL);
INSERT INTO lut_sfu (sfu,lgfu, psap, imm, mat, late, cwu, cwp, cw, cr, pft, grp1, grp2, ogonset, ogdurat_org, ogfu, ogdurat, ogrpton, ogrptdur) VALUES  ( 'SbLow' , 'SbLow' , 10, 30, 70, 160, 40, 100, 40, 0, 'mcl' , 'SbLow' , 'SbLow', 160, 350, 'LowCo' , 190, 160, NULL);
INSERT INTO lut_sfu (sfu,lgfu, psap, imm, mat, late, cwu, cwp, cw, cr, pft, grp1, grp2, ogonset, ogdurat_org, ogfu, ogdurat, ogrpton, ogrptdur) VALUES  ( 'SbMx1' , 'SbMx1' , 10, 30, 70, 110, 60, NULL, 50, 0, 'mcu' , 'SbMx1' , 'SbMx1', 110, 190, 'UplCo' , 80, 110, NULL);
INSERT INTO lut_sfu (sfu,lgfu, psap, imm, mat, late, cwu, cwp, cw, cr, pft, grp1, grp2, ogonset, ogdurat_org, ogfu, ogdurat, ogrpton, ogrptdur) VALUES  ( 'SbSha' , 'SbDom' , 10, 30, 70, 120, 60, NULL, 50, 0, 'mcu' , 'SbDom' , 'SbDom', 120, 190, 'UplCo' , 70, 120, NULL);
INSERT INTO lut_sfu (sfu,lgfu, psap, imm, mat, late, cwu, cwp, cw, cr, pft, grp1, grp2, ogonset, ogdurat_org, ogfu, ogdurat, ogrpton, ogrptdur) VALUES  ( 'UplCe' , 'OCLow' , 10, 30, 70, 120, 50, NULL, 50, 0, 'mcu' , 'UplCe' , 'UplCe', 100, 170, 'Mixed' , 70, 140, NULL);
INSERT INTO lut_sfu (sfu,lgfu, psap, imm, mat, late, cwu, cwp, cw, cr, pft, grp1, grp2, ogonset, ogdurat_org, ogfu, ogdurat, ogrpton, ogrptdur) VALUES  ( 'BfMx1' , 'BfDom' , 5, 10, 60, 80, NULL, NULL, NULL, 60, 'mcu' , 'BfDom' , 'BfDom', 80, 150, 'Mixed' , 70, 80, 70);
INSERT INTO lut_sfu (sfu,lgfu, psap, imm, mat, late, cwu, cwp, cw, cr, pft, grp1, grp2, ogonset, ogdurat_org, ogfu, ogdurat, ogrpton, ogrptdur) VALUES  ( 'BfPur' , 'BfDom' , 5, 10, 60, 80, NULL, NULL, NULL, 60, 'mcu' , 'BfDom' , 'BfDom', 80, 150, 'Mixed' , 70, 80, 70);



DROP TABLE IF EXISTS lut_elceco_caribou ;
CREATE TABLE lut_elceco_caribou (
    elceco character varying NOT NULL PRIMARY KEY,
    cw_0 integer,
    cw_21 integer,
    cw_41 integer,
    cw_61 integer,
    cw_101 integer,
    cr_0 integer,
    cr_21 integer,
    cr_41 integer,
    cr_61 integer,
    cr_101 integer,
    comment character varying
);


--
-- Data for Name: lut_elceco_caribou; Type: TABLE DATA; Schema:  Owner: beemerga
--

INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B012', NULL, 1, 2, 2, 2, 1, 2, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B024', NULL, 1, 2, 2, 2, 1, 2, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B026', NULL, 1, 2, 2, 2, 1, 2, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B034', NULL, NULL, 1, 2, 2, 1, 2, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B035', NULL, NULL, 1, 2, 2, NULL, 1, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B036', NULL, NULL, NULL, NULL, NULL, NULL, 1, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B037', NULL, NULL, 1, 1, 1, NULL, 1, 2, 1, 1, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B038', NULL, NULL, 1, 2, 2, NULL, 1, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B050', NULL, NULL, NULL, 1, 1, NULL, 1, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B052', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B053', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B064', NULL, NULL, NULL, 1, 1, NULL, NULL, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B065', NULL, NULL, NULL, 1, 1, NULL, NULL, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B067', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B068', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B082', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B083', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B097', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B098', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B099', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B114', NULL, NULL, NULL, 1, 1, NULL, NULL, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B126', 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B127', NULL, NULL, 1, 1, 2, 2, 2, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B128', NULL, NULL, NULL, 1, 1, 1, 1, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B136', 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B137', 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B138', 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B139', NULL, NULL, NULL, NULL, NULL, 2, 2, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B140', NULL, NULL, NULL, NULL, NULL, 2, 2, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B141', NULL, NULL, NULL, NULL, NULL, 2, 2, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B163', NULL, 1, 2, 2, 2, 2, 2, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B164', NULL, 1, 2, 2, 2, 2, 2, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B165', NULL, 1, 2, 2, 2, 2, 2, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B179', NULL, 1, 2, 2, 2, 2, 2, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B180', NULL, 1, 2, 2, 2, 2, 2, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B181', NULL, 1, 2, 2, 2, 2, 2, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B222', NULL, NULL, 1, 1, 2, 2, 2, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B223', NULL, NULL, NULL, 1, 1, 1, 1, 2, 2, 2, NULL);
INSERT INTO lut_elceco_caribou (elceco, cw_0, cw_21, cw_41, cw_61, cw_101, cr_0, cr_21, cr_41, cr_61, cr_101, comment) VALUES ('B049', NULL, NULL, NULL, 1, 2, NULL, 1, 2, 2, 2, 'May 2, 2017 Glen changed cw_101 to a 2... Preferred');

DROP VIEW IF EXISTS lut_elceco_caribou2 ;
CREATE VIEW lut_elceco_caribou2 AS
 SELECT lut_elceco_caribou.elceco,
    lut_elceco_caribou.cw_0,
    lut_elceco_caribou.cw_21,
    lut_elceco_caribou.cw_41,
    lut_elceco_caribou.cw_61,
    lut_elceco_caribou.cw_101,
    lut_elceco_caribou.cr_0,
    lut_elceco_caribou.cr_21,
    lut_elceco_caribou.cr_41,
    lut_elceco_caribou.cr_61,
    lut_elceco_caribou.cr_101,
        CASE
            WHEN lut_elceco_caribou.cw_0 = 1 THEN 0
            WHEN lut_elceco_caribou.cw_21 = 1 THEN 21
            WHEN lut_elceco_caribou.cw_41 = 1 THEN 41
            WHEN lut_elceco_caribou.cw_61 = 1 THEN 61
            WHEN lut_elceco_caribou.cw_101 = 1 THEN 101
            ELSE NULL
        END AS cwu_on,
        CASE
            WHEN lut_elceco_caribou.cw_101 = 1 THEN 1000
            WHEN lut_elceco_caribou.cw_61 = 1 THEN 100
            WHEN lut_elceco_caribou.cw_41 = 1 THEN 60
            WHEN lut_elceco_caribou.cw_21 = 1 THEN 40
            WHEN lut_elceco_caribou.cw_0 = 1 THEN 20
            ELSE NULL
        END AS cwu_off,
        CASE
            WHEN lut_elceco_caribou.cw_0 = 2 THEN 0
            WHEN lut_elceco_caribou.cw_21 = 2 THEN 21
            WHEN lut_elceco_caribou.cw_41 = 2 THEN 41
            WHEN lut_elceco_caribou.cw_61 = 2 THEN 61
            WHEN lut_elceco_caribou.cw_101 = 2 THEN 101
            ELSE NULL
        END AS cwp_on,
        CASE
            WHEN lut_elceco_caribou.cw_101 = 2 THEN 1000
            WHEN lut_elceco_caribou.cw_61 = 2 THEN 100
            WHEN lut_elceco_caribou.cw_41 = 2 THEN 60
            WHEN lut_elceco_caribou.cw_21 = 2 THEN 40
            WHEN lut_elceco_caribou.cw_0 = 2 THEN 20
            ELSE NULL
        END AS cwp_off,
        CASE
            WHEN lut_elceco_caribou.cr_0 = 1 THEN 0
            WHEN lut_elceco_caribou.cr_21 = 1 THEN 21
            WHEN lut_elceco_caribou.cr_41 = 1 THEN 41
            WHEN lut_elceco_caribou.cr_61 = 1 THEN 61
            WHEN lut_elceco_caribou.cr_101 = 1 THEN 101
            ELSE NULL
        END AS cru_on,
        CASE
            WHEN lut_elceco_caribou.cr_101 = 1 THEN 1000
            WHEN lut_elceco_caribou.cr_61 = 1 THEN 100
            WHEN lut_elceco_caribou.cr_41 = 1 THEN 60
            WHEN lut_elceco_caribou.cr_21 = 1 THEN 40
            WHEN lut_elceco_caribou.cr_0 = 1 THEN 20
            ELSE NULL
        END AS cru_off,
        CASE
            WHEN lut_elceco_caribou.cr_0 = 2 THEN 0
            WHEN lut_elceco_caribou.cr_21 = 2 THEN 21
            WHEN lut_elceco_caribou.cr_41 = 2 THEN 41
            WHEN lut_elceco_caribou.cr_61 = 2 THEN 61
            WHEN lut_elceco_caribou.cr_101 = 2 THEN 101
            ELSE NULL
        END AS crp_on,
        CASE
            WHEN lut_elceco_caribou.cr_101 = 2 THEN 1000
            WHEN lut_elceco_caribou.cr_61 = 2 THEN 100
            WHEN lut_elceco_caribou.cr_41 = 2 THEN 60
            WHEN lut_elceco_caribou.cr_21 = 2 THEN 40
            WHEN lut_elceco_caribou.cr_0 = 2 THEN 20
            ELSE NULL
        END AS crp_off,
    lut_elceco_caribou.comment
   FROM lut_elceco_caribou;
