REM Drag_N_DROP_Here_to_register_a_download
::ECHO ON
::ECHO "%~1"
if exist "D:\OSGeo4w\OSGeo4W.bat" (
set OSGEO_BAT=D:\OSGeo4w\OSGeo4W.bat
) else if exist "C:\OSGeo4W\OSGeo4W.bat" (
set OSGEO_BAT=C:\OSGeo4W\OSGeo4W.bat
) else (
echo "OSGEO Not Found. Exitting."
EXIT /B 2
)

call %OSGEO_BAT% python %~dp0\Drag_n_drop_submission_file.py "%~1"
timeout /T 15
