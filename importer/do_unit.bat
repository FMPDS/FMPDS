:: Do_Kenora.bat

:: AR's
::for %%n in ( 6214 7604 12902 13867 15608 17187 19183 21207 23240 ) DO python  run_full_process_for_a_fisubmission.py --complete --delete_sink_dir %%n

:: AWS's
::for %%n in ( 8472 11406 13252 15497 17312 19330 21379 23421 24967 ) DO python  run_full_process_for_a_fisubmission.py --complete --delete_sink_dir %%n

::FMP Final Plan
:: Phase 1
::for %%n in ( 10904 ) DO python  run_full_process_for_a_fisubmission.py --complete --delete_sink_dir %%n
:: Phase 2
::for %%n in ( 20670 ) DO python  run_full_process_for_a_fisubmission.py --complete --delete_sink_dir %%n


::: Get the lowercase username... <sigh/>
setlocal EnableExtensions EnableDelayedExpansion
  set var_=%USERNAME%
  for %%c in (a b c d e f g h i j k l m n o p q r s t u v w x y z ) do (
    set var_=!var_:%%c=%%c!
  )
endlocal & set user=%var_%
:: echo %user%




:set "param1=%~1"
::setlocal EnableDelayedExpansion
::if "!param1!"=="" ( echo FMU not defined ; GOTO NoUnit )
rem ... or use the DEFINED keyword now
::if defined param1 echo Completely Rebuilding !param1!

::SET fmu=!param1!
IF "%~1" == "" GOTO NoUnit
:: Get a Management Unit from the first parameter
SET fmu=%~1
echo Doing %fmu%


set sql=psql -U %user% -h lrcgikdcwhiis06 -p 5433 -d FI_Portal_Scrape -t -A -c "SELECT submissionid::text FROM  fi_portal_scraper.latest_submissions WHERE lower(managementunitdistrict) LIKE lower('%fmu%%%') ORDER BY product, submissionid::int "
:: Could specify products in the where clause - product in ('AR','AWS') ...
:: echo %sql%



:: Get the list of fi_submissions...
for /F "usebackq" %%n in ( `%sql%` ) DO (
	echo Go Go Gadget %%n
	python  run_full_process_for_a_fisubmission.py  --run_delivery %%n
	:: --complete --delete_sink_dir
	:: python  run_full_process_for_a_fisubmission.py --drop_claims_nw_checker %%n
)



:NoUnit

TIMEOUT /t 33

