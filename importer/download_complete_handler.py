#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  download_complete_handler.py
#
#  Copyright 2018 CARRAN <CARRAN@ON34C02585177>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#


from tape_handler import tape_handler
import os
import sys
import datetime
import glob
import json

import zipfile

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
#from importer import importer_conductor
from stack.choose_a_tech_spec  import choose_a_tech_spec

debug = True

class download_complete_handler( tape_handler ):
	"""
	This is a TAPE Handler

	T: When a download is completed ('fi_portal_download_completed')
	A: Run the Importer if
		the forest-product-year has never been run, or
		the fi_portal_id of this event is higher (later) than the
			completed import on the drive
	P: Imported files
	E: fi_portal_download_imported ( sink_dir name, fi_portal_id, filename )

	"""

	eType = 'fi_portal_download_completed'
	successEvent = "fi_portal_imported"
	channel = eType # event Type is used for the channel name... :-)
	jobType = 'fi_portal_importer'
	exclusive_job = True

	staletime = '15 minutes'
	#staletime = '2 minutes'
	#backlog_frequency = 60
	backlog_frequency = 2
	timeout = 600 # seconds
	#timeout = 1 # seconds
	#This makes the default to be run a backlog every hour, and handle
	# interstitial events every minute.
	failure_count = 2

	def go( self, event=None):
		"""
		Run the Importer

		"""
		print ("Running the importer")
		#print event.keys()
		#print event['payload']
		#print eval(event['payload'])['fi_portal_id']
		importer_call = [ 'python.exe'  ]
		importer_call.append(os.path.join(os.path.abspath(os.path.dirname(__file__)),'importer.py'))

		try:
			submission_id = event['payload']['fi_portal_id']
			filename = event['payload']['filename']
		except TypeError:
			submission_id = eval(event['payload'])['fi_portal_id']
			filename = eval(event['payload'])['filename']

		# Test only AR AWS FMP FMPDP
		# In other words, these are the products we can handle
		d = self.getforestproductyear( submission_id )
		if not d['latest_submission']:
			return self.status("Success")

		try:
			#importer_call.append('-j')
			choose_a_tech_spec( d['product'],d['year'] )
		except RuntimeError as e:
			print e
			return self.status("No Tech Spec")

		#############################################
		# Do the work (import file, if applicable ) #
		#############################################

		for k,v in { '-i':filename,'-d':r'f:\\','-w':"True"}.iteritems() :
			importer_call.append(k)
			importer_call.append(v)

		sink_dir = "f:\\{forest}\\{product}\\{year}".format(**d)

		print ("Importing {forest}\{product}\{year}".format(**d))

		status = self.call(importer_call)
		print status

		if status == self.status("Success"):
			importer_results = json.load(open(os.path.join(sink_dir,'importer_results.txt')) )

			sink_gpkg = importer_results['sink_gpkg']
			if not os.path.exists(sink_gpkg) :
				found_gpkg = None
				for n in glob.glob(sink_dir+"\\mu{fmuid}_{year}*{product}.gpkg".format(**d)):
					if os.path.exists(n):
						found_gpkg = n
						break

				if found_gpkg is not None:
					sink_gpkg=found_gpkg
				else :
					for n in glob.glob(sink_dir+"\\mu{fmuid}*.gpkg".format(**d)):
						if os.path.exists(n):
							found_gpkg = n
							break
					if found_gpkg is not None:
						sink_gpkg=found_gpkg

			####################################
			# Status Report and event creation #
			####################################
			# if we get here, there is no problem... So the import was good
			print "Creating new event.."
			#     'eType':"fi_portal_imported",
			new_event_payload = dict( event['payload'], **{ 'sink_dir':sink_dir, 'sink_gpkg':sink_gpkg } )
			#print new_event_payload
			self.generateEvent(new_event_payload)
		return self.status(status)


if __name__ == '__main__':
	t = download_complete_handler({})
	t.interpret_command_line(sys.argv)
