#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  build_empty_dataset.py
#
#  Copyright 2018 CARRAN <CARRAN@ON34C02585177>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import os
import sys
from osgeo import gdal
from osgeo import ogr
from osgeo import osr

import zipfile

from io import BytesIO

from itertools import chain

import win32api

import shutil
import fnmatch
import tempfile

import subprocess

import json

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from checker.filechecker import jsonloader
from mm_processor.validator import validator


from stack.pg_db import pg_db

from stack.typeLUT import fieldTypeLUT, geom_type_LUT, e00_type_LUT
import stack.choose_a_tech_spec


class importer_conductor( jsonloader ):
	"""OK, so here goes"""
	sink_dir = r"c:\GIS\FMP_New"
	submission_metadata = None
	overwrite = False

	#OGR Stuff
	ds = None
	fmpds = None
	srs = None # SRS used for data. Assuming we are always using 1 SRS in this system
	layers = dict() # List of layers in out submission
	deferred_layers = list()
	results = dict()
	temp_file_list = []

	sql_to_get_metadata = 'SELECT * FROM fi_portal_scraper.importer_metadata '


	def __init__(self,jsonfilename=None, datafilename=None, filemetadata=None, overwrite=False, submission_id=None, sink_dir=None ):
		"""
		This imports a dataset which is formatted according to the
		mindmap.
		"""
		self.datafilename = datafilename
		if sink_dir : self.sink_dir = sink_dir

		if submission_id is not None:
			j,fm = self.get_metadata_from_database(submission_id)
			print ('Database says json is {0}'.format(j))
			if jsonfilename is None:
				jsonfilename = j
		else:
			j,fm = self.get_metadata_from_database(self.get_fi_portal_id_from_zipfilename())
			print ('Database says json is {0}'.format(j))
			if jsonfilename is None:
				jsonfilename = j

		if filemetadata is None: filemetadata = fm

		jsonloader.__init__(self, jsonfilename=jsonfilename, ignore_non_tables=False )

		submission_metadata = dict()
		for i in self.json:
			if 'metadata' == i['name']:
				#print "Metadata Found: %s"%( i.keys() )
				submission_metadata = i
				break

		self.submission_metadata = dict(chain(submission_metadata.items(), filemetadata.items() ) )
		#print "====================================="
		#print self.submission_metadata
		#print "====================================="

		#Assume no overwrite unless explicitly set on...
		self.overwrite = overwrite

	def __del__(self):
		self.clean_up_temp_extracted_zipfiles(self.temp_file_list)

	def go(self):
		""" This is the function that actually causes something to happen."""
		d = self.create_sink_directory()
		if d: self.results['sink_directory']=d
		d = self.generate_sink_geopackage()
		if d: self.results['sink_gpkg']=d
		d = self.process_zip_file()
		if d: self.results['files_processed']=d
		if 'BadZip' in json.dumps(d):
			return 91
		self.process_deferred_layers()
		if d: self.results['layers_processed']=d
		self.write_results_file()
		# If any errors in the zip file, how do we identify them?
		# If 'Failure' in self.results['layers_processed']
		# otherwise 0 for OK
		return 0 # self.results

	def process_deferred_layers(self):
		r = dict()
		for f in self.deferred_layers:
			r[f]=self.copy_layers(f)
		return r


#######################################
## Arrange metadata and the like
#######################################

	def get_fi_portal_id_from_zipfilename(self):
		if self.datafilename[-4:] == '.zip':
			# print ("It's a zip File Name! {0}".format(str(self.datafilename)))
			pass
		else :
			return None

		#archive = zipfile.ZipFile(self.datafilename,'r')

		#for n in archive.infolist():
		#	print( "{filename}: {file_size} ".format( filename=n.filename, file_size=n.file_size ) )

		#fi_portal_id is at the end of the filename before the dot...
		for i in range( -9, -4) :
			#print ("Testing zip name has number in {0}".format((self.datafilename[i:]).replace('.zip','') ) )
			if (self.datafilename[i:]).replace('.zip','').isdigit():
				return (self.datafilename[i:]).replace('.zip','')

		return None

	def get_metadata_from_database(self,fi_portal_id=42 ):
		"""
		Deprecated for now, since this is being fed from the database anyway.
		#FIXME
		Should not be needed by auto importer, since the metadata should
		be passed in, but isn't...
		"""
		sql = self.sql_to_get_metadata +""" WHERE submissionid = {0}""".format(fi_portal_id)

		results = pg_db(port=5433, dbname='FI_Portal_Scrape' ).run_select_get_value_or_rowlist(sql)
		#print sql
		#print ("=========================== Submission Row from DB =====")
		#print (results)
		#print ("--------------------------- Submission Row from DB -----")
		if results :
			for submission_row in results :
				jsonfilename = stack.choose_a_tech_spec.choose_a_tech_spec(submission_row['product'],submission_row['year'])

				datafilename = submission_row['filename']

				filemetadata=dict()
				filemetadata['fmu']=submission_row['forest']
				filemetadata['fmu_id']=submission_row['fmuid']
				filemetadata['product']=submission_row['product']
				filemetadata['year']=submission_row['year']
				filemetadata['planyear'] = submission_row['planyear']
				filemetadata['id'] = submission_row['submissionid']
				filemetadata['phase'] = submission_row['planterm']

				#print filemetadata

				return (jsonfilename,filemetadata)

	def get_sink_directory(self,path=None):
		if path is None :
			path = self.submission_metadata['path'].format(**self.submission_metadata )
		elif path == '' :
			path = self.submission_metadata['path'].format(**self.submission_metadata )
		else :
			path = os.path.join(self.submission_metadata['path'].format(**self.submission_metadata ),path )

		#Clear "-" to "_"
		#path=path.replace('-','_')

		return os.path.join(self.sink_dir,path)

	def create_sink_directory(self, dirname=None):
		if dirname is None:
			dirname = self.get_sink_directory()

		self.fi_submission_txt_file = os.path.join(dirname,'fi_submission_'+str( self.submission_metadata['id'] )+'.txt')
		if os.path.exists(dirname):
			if os.path.exists(self.fi_submission_txt_file ):
				#This suggests that we are repeating the task... That's probably an error.
				print ("=======================")
				print ("Import directory and fi submission folder exists! ==> {0}".format(self.fi_submission_txt_file))
				#raise(RuntimeError("Import directory and fi submission folder exists! ==> {0}".format(self.fi_submission_txt_file)))
				print ("=======================")
		if os.path.exists(dirname) and self.overwrite :
			#Delete and Replace! --> rm -rf self.get_sink_directory() && mkdir self.get_sink_directory()
			self.remove_sink_directory(dirname=dirname,ignore_errors=True)

		print ("Creating Sink Dir! {0}".format(dirname))
		try:
			os.makedirs(dirname)
		except WindowsError as e:
			#print e
			if os.path.exists(dirname):
				print("Re-using existing directory")
			#raise e
			# sys.exit(1)

		# Create fi_submission_#.txt
		with open(self.fi_submission_txt_file,'w' ) as t:
			t.write(json.dumps(self.submission_metadata, indent=4, sort_keys=True))

		return dirname


	def remove_sink_directory(self, dirname=None, ignore_errors = False):
		if dirname is None:
			dirname = self.get_sink_directory()

		if os.path.exists(dirname) :
			print "Deleting {0}.".format(dirname)
			try:
				os.removedirs(dirname)
			except WindowsError as e:
				#print ("\tCannot remove {1}:\n\t\t{0}".format(e,dirname))
				# YIKES!
				if ignore_errors :
					#print ("\tTrying again with shutil.rmtree")
					shutil.rmtree(dirname,ignore_errors=True)

########################################
## File looping
########################################

	def generate_sink_geopackage(self):
		path = self.submission_metadata['Layer_Filename'].format(**self.submission_metadata ) + '.gpkg'
		self.gpkg_name = os.path.join(self.get_sink_directory(),path)

		self.ds = ogr.GetDriverByName('GPKG').CreateDataSource(self.gpkg_name)

		#tables_to_create = [ t['name'] for t in self.json ]

		#for t in self.json:
		#	try:
		#		if t['name'] in tables_to_create and 'fields' in t.keys():
		#			self.create_layer(layerdesc=t,lower_case=True)
		#	except RuntimeError as e:
		#		print e
		#		print "moving on..."
		return self.gpkg_name

	def use_metadata_to_handle_file(self, filename, file_handle=None, zipfileOBJ=None):
		#for k,v in self.submission_metadata.iteritems():
		for k in sorted(self.submission_metadata):
			v = self.submission_metadata[k]
			# First, handle the directories in the metadata
			# Directories are found by having a similar metadata item that is the same
			# 	plus the word "_Directory"
			# The file type is the metadata item, which contains the format
			# 	of the filename, and then there is a directory for it...
			# Or it is a layer, which goes in the gpkg...
			if isinstance(v,str) or isinstance(v,unicode):
				v = [ v ]
			#print k,v
			if not ( (str(k) + '_Directory' in self.submission_metadata.keys() ) or k[0:5] == 'Layer' ) :
				continue

			#print 'Testing for {0} type in {1}'.format(k, filename)
			for vv in v:
				pattern = vv.format(**self.submission_metadata )
				if pattern == '' : continue

				#print "\tTesting {0} with pattern {1}".format(filename,pattern)

				if fnmatch.fnmatch(filename.upper() , pattern.upper() ) :
					#print "\t\tHandling {1} with pattern {0}".format(pattern,filename)
					if k == 'Internal_zipfile':
						#print ("Found a {k}".format(k=k))
						return self.process_zip_file( file_handle, zipfileOBJ )
					elif k[0:5]  == 'Layer':
						print ("Found a {k}".format(k=k))
						#try:
						return self.copy_layer(filename, file_handle, zipfileOBJ )
						#except RuntimeError as e:
						#	return ('Failure',filename)
					elif k[0:13] == 'DeferredLayer':
						#print ("Found a {k}".format(k=k))
						sink_dirname=str(self.submission_metadata[str(k) + '_Directory']).format(**self.submission_metadata )
						file_copy_results = self.copy_file(filename,file_handle, sink_dirname = sink_dirname, zipfileOBJ=zipfileOBJ,remove_subdirectories=True)
						if os.path.dirname(file_copy_results) not in self.deferred_layers:
							self.deferred_layers.append(os.path.dirname(file_copy_results))
						return (k,file_copy_results)
					elif k == 'Trash':
						#print ("Found a {k}".format(k=k))
						# These files should be ignored silently... Thumbs.db
						return('Trash',filename)
					else :
						#print ("Found a {k}".format(k=k))
						sink_dirname=str(self.submission_metadata[str(k) + '_Directory']).format(**self.submission_metadata )
						try:
							file_copy_results = self.copy_file(filename,file_handle, sink_dirname = sink_dirname, zipfileOBJ=zipfileOBJ)
						except zipfile.BadZipfile as e:
							return ('Failure',filename)

						if k == "Model" :
							r = self.process_model_file(filename,file_handle, zipfileOBJ = zipfileOBJ )
							return (k,file_copy_results,r)
						elif k == "Document":
							r = self.process_plan_textandtables(filename,file_handle, zipfileOBJ = zipfileOBJ )
							return (k,file_copy_results,r)

						return (k,file_copy_results)


		# Tried all the patterns, so now what?
		# Catchall is everything not handled, in the root...
		try:
			sink_dirname=str(self.submission_metadata['Catchall_Directory']).format(**self.submission_metadata )
		except KeyError as e:
			sink_dirname=""
		try:
			file_copy_results = self.copy_file(filename,file_handle, sink_dirname = sink_dirname, zipfileOBJ=zipfileOBJ)
		except zipfile.BadZipfile as e:
			return ('Failure',filename)
		return ("Caught in Catchall",file_copy_results)

		# If We get HERE, the file has not been handled by a file handler... Do we care?
		#print ("{0} not handled - Does it matter?".format(filename))
		#return ('Failure',filename)




#########################################
## Specific File Handling
#########################################

	def process_zip_file(self, file_handle=None, zipfileOBJ=None):
		results = dict()
		if file_handle is None:
			archive = zipfile.ZipFile(self.datafilename,'r')
		else :
			#This causes a memory error:
			try:
				zfiledata = BytesIO(zipfileOBJ.read(file_handle))
				archive = zipfile.ZipFile( zfiledata, 'r' )
			except MemoryError as e:
				print e
				# extract_as_tempfile(self, filename = None, file_handle = None, zipfile = None , temp_dir = None, suffix=None)
				zfiledata = self.extract_as_tempfile(file_handle = file_handle, zipfile=zipfileOBJ)
				archive = zipfile.ZipFile( zfiledata, 'r' )
			except zipfile.BadZipfile as e:
				print e
				results['BadZip'] = file_handle.filename
				#n.filename for n in zipfileOBJ.infolist() if os.path.split(n.filename)[0][-4:].lower() == '.gdb' ] :
				return results
			except RuntimeError as e:
				print( "{filename}: {file_size} \n\t{e}".format( filename=n.filename, file_size=n.file_size, e=str(e) ) )
				results['EncryptedZip'] = self.copy_file(self,filename=n.filename,file_handle=file_handle,sink_dirname=None,zipfileOBJ=archive )
				return results

		#test if we can extract one file from it... if so, continue...
		try:
			self.extract_as_tempfile(file_handle=archive.infolist()[0])
		except RuntimeError as e:
			print( "{filename}: {file_size} \n\t{e}".format( filename=archive.infolist()[0].filename, file_size=archive.infolist()[0].file_size, e=str(e) ) )
			results['EncryptedZip'] = self.copy_file(self,filename=zipfileOBJ.filename,file_handle=file_handle,sink_dirname=none,zipfileOBJ=zipFileOBJ )
			return results

		#	Test if files inside represent a shapefile directory, a geopackage,
		#	a file geodatabase, or any other obvious shape-y stuff
		#	print [ os.path.split(n.filename)[0][-4:] for n in archive.infolist()  ]

		if [ os.path.split(n.filename)[0][-4:] for n in archive.infolist() if os.path.split(n.filename)[0][-4:].lower() == '.gdb' ] :
			#print "File GDB in a zip file!!!!"
			#print [ os.path.split(n.filename)[0] for n in archive.infolist() if os.path.split(n.filename)[0][-4:] == '.gdb' ][0]
			GDB_Name = list(frozenset([ os.path.split(n.filename)[0] for n in archive.infolist() if os.path.split(n.filename)[0][-4:] == '.gdb' ]))[0]
			#print GDB_Name
			archive.extractall(path=self.get_sink_directory(path="LAYERS"))
			results['GDB'] = self.get_sink_directory(path=os.path.join("LAYERS", GDB_Name ))
			self.deferred_layers.append(os.path.join(self.get_sink_directory(path="LAYERS"),GDB_Name))
		else:
			for n in archive.infolist():
				#print( "{filename}: {file_size} ".format( filename=n.filename, file_size=n.file_size ) )
				r = self.use_metadata_to_handle_file(filename=n.filename,file_handle=n,zipfileOBJ=archive)
				if r:
					if 'files' not in results.keys():
						results['files'] = list()
					results['files'].append(  r  )

		return results


	#find data files in the zip file and process them...
	def process_e00_file(self):
		pass

	def extract_as_tempfile(self, **kwargs):
		temp_dir    = None if 'temp_dir' not in kwargs.keys() else kwargs['temp_dir']
		file_handle = None if 'file_handle' not in kwargs.keys() else kwargs['file_handle']
		zipfile     = None if 'zipfile' not in kwargs.keys() else kwargs['zipfile']
		if temp_dir is None:
			temp_dir = tempfile.mkdtemp()
		#print "\t\t\textract_as_tempfile: {0}".format(file_handle.filename)
		#print "\t\t\textract_as_tempfile: {0}".format(temp_dir)
		temp_file = zipfile.extract(file_handle, temp_dir )
		#print "\t\t\textract_as_tempfile: {0}".format(temp_file)
		self.temp_file_list.append(temp_file)
		return temp_file

	def process_plan_textandtables(self,filename,file_handle, zipfileOBJ=None):
		"""
		If in the document structure, determine if we should extract anything...
		"""
		pass

	def process_model_file(self,filename,file_handle, zipfileOBJ=None):
		"""
		If in the model structure, determine if we should extract anything...
		"""
		pass

	def copy_file(self,filename,file_handle,sink_dirname,zipfileOBJ=None,remove_subdirectories=False):
		"""
		Copy file to sink_dirname, preserving paths within the filename, except for the top one
		The idea here is that if it is packed in the MODEL folder with subs (for example) we
		should keep the sub...
		"""
		#print "\t\t\tcp {0} {1}".format(filename, self.get_sink_directory(path=sink_dirname))
		try:
			fn = zipfileOBJ.extract(file_handle, self.get_sink_directory(path=sink_dirname) )
		except IOError:
			print ("Cannot extract file - presumably in use...")
			return None
		except RuntimeError as e:
			print (e)
			raise RuntimeError(e)

		if remove_subdirectories:
			#mv fn os.path.join(self.get_sink_directory(path=sink_dirname),os.path.basename(fn))
			new_fn = os.path.join(self.get_sink_directory(path=sink_dirname),os.path.basename(fn))
			shutil.move(fn,new_fn)
			return new_fn
		else :
			return fn


	def copy_layers(self,filename):
		results = dict()
		try:
			src_ds = ogr.Open(filename)
		except RuntimeError as e:
			print e
			return ("Could not open Deferred Layer files {0}".format(filename))
		if not src_ds:
			print ("Could not open Deferred Layer files {0}".format(filename))
			return ("Could not open Deferred Layer files {0}".format(filename))
		for src_layer in src_ds:
			#src_layer = src_ds.GetLayer(layername)
			self.get_srs_from_file(src_layer)
			#print self.layerdescription(src_layer)

			layerDefinition = src_layer.GetLayerDefn()
			try:
				print ("Processing {0} Layer as {1}".format(layerDefinition.GetName(), self.getTableListMatchingString(layerDefinition.GetName())[0] ))
			except IndexError as e:
				print ("Processing {0} Layer as unspecified table type")

			try:
				t = self.findTable(self.getTableListMatchingString(layerDefinition.GetName())[0])
				dst_fieldNames = [f['name'].lower() for f in  t['fields']]
			except IndexError:
				t = { 'name':layerDefinition.GetName() , 'fields':[] }
				dst_fieldNames = [ ]

			geom_type = src_layer.GetGeomType()

			# if the data file is open (self.ds) then we have a geopackage
			if self.ds is not None:
				# Create Layer (Fields from mindmap spec)
				dst_geom = src_layer.GetGeomType()
				#Promote to multi types for Shapefiles...
				promote_to_multi = False
				#print ("src_driver is of type {0}".format(src_ds.GetDriver().GetName()))
				if src_ds.GetDriver().GetName() == "ESRI Shapefile":
					if dst_geom in [ 1,2,3 ]:   # wkbPoint = 1,wkbLineString = 2,wkbPolygon = 3
						dst_geom = dst_geom + 3 # wkbMultiPoint = 4,wkbMultiLineString = 5,wkbMultiPolygon = 6,
						promote_to_multi = True
				#print ("dst_geom is of type {0}".format(dst_geom))
				dst_layer = self.create_layer(layerdesc=t, geom_type=dst_geom)

				# Add Fields from datafile
				for i in range(layerDefinition.GetFieldCount()):
					if layerDefinition.GetFieldDefn(i).GetName().lower() not in dst_fieldNames:
						# Add this field to the gpkg
						try:
							dst_layer.CreateField(layerDefinition.GetFieldDefn(i))
						except RuntimeError:
							pass # This happens when the field already exists

				dst_LayerDefn = dst_layer.GetLayerDefn()
				results['srcLayer_'+layerDefinition.GetName()] = [ 'GPKG_' + dst_LayerDefn.GetName() ]


			#
			# Append the datafile rows
			#
			i=0
			try:
				for infeature in src_layer:
					i=i+1
					#Build Row
					row = dict([ (layerDefinition.GetFieldDefn(i).GetName(),infeature.GetField(i) ) for i in range(layerDefinition.GetFieldCount()) ])
					geom = infeature.GetGeometryRef()

					#If we are doing the geopackage
					if self.ds is not None:
						outFeature = ogr.Feature(dst_LayerDefn)

						for fname,fvalue in row.iteritems():
							try:
								outFeature.SetField(fname, fvalue )
							except NotImplementedError:
								# This happens with the ArcIDs field... it's a list of id's...
								if isinstance(fvalue,list):
									outFeature.SetField(fname, str(fvalue) )
						if geom:
							if promote_to_multi:
								outFeature.SetGeometry(ogr.ForceTo(geom.GetLinearGeometry(), dst_geom ))
							else :
								outFeature.SetGeometry(geom.GetLinearGeometry())

						# Add new feature to output Layer
						try:
							dst_layer.CreateFeature(outFeature)
						except RuntimeError as e:
							print e
							print ( "Failed row is {0}".format(row))
							print ( "src_geom is {0}".format(str(geom)))

						outFeature = None

					results['srcLayer_'+layerDefinition.GetName()+'_rowcount'] = i

					# if we are supposed to put the data into the database...
					if self.fmpds is not None:
						pass
			except RuntimeError as e:
				results['srcLayer_'+layerDefinition.GetName()+'_rowcount']=i
				results['srcLayer_'+layerDefinition.GetName()+'failure'] =str(e)

		src_layer = None
		src_ds = None

		if dst_layer:
			dst_layer.SyncToDisk()

		return results













































	def copy_layer(self,filename, file_handle=None, zipfileOBJ = None):
		#print "\togr2ogr {0} {1}".format(filename, self.gpkg_name)
		# Do we need to extract and copy rows?
		file_type = None

		# Identify the layer name for the sink layer (from mindmap spec)
		tablenames = self.getTableListMatchingString( os.path.splitext(filename.upper())[0] )
		#print tablenames
		try:
			t = self.findTable(tablenames[0])
		except IndexError as e:
			t = None
		if not t:
			t = { 'fields': [] , 'name':os.path.splitext(os.path.basename(filename))[0]}

		dst_layer = None

		#	E00
		if os.path.splitext(filename.upper())[1] in [ '.E00' ]:
			layer_filename = os.path.splitext(os.path.basename(filename))[0]
			#print ("\tE00 Filename: {0}".format(filename))

			temp_dir = self.get_sink_directory(path='LAYERS') #tempfile.mkdtemp(suffix='_E00_'+layer_filename[8:11])

			coverage_name = os.path.join(temp_dir,layer_filename)
			#print ("\tcoverage_name: {0}".format(os.path.abspath(coverage_name)))

			#print "\tExtract_as_tempfile"
			#print "\t\t{filename} {file_handle} {zipfile} {temp_dir} ".format(filename = filename, file_handle = file_handle, zipfile = zipfileOBJ, temp_dir = temp_dir)
			try:
				filepath = self.extract_as_tempfile(filename = filename, file_handle = file_handle, zipfile = zipfileOBJ, temp_dir = temp_dir )
			except RuntimeError as e:
				print("Extract failed {0}".format(e))
				return ('Failure',filename)
			#print ("\tE00 Filepath: {0}".format(os.path.abspath(filepath)))

			try:
				subprocess.call([os.path.join(os.path.split(__file__)[0],r"import71\import71.exe"), filepath, coverage_name , "/T"])
			except subprocess.CalledProcessError :
				#clear out the garbage and try again
				try:
					shutil.rmtree(coverage_name)
				except:
					pass
				result = subprocess.check_call(["avcimport" , filepath, os.path.join(temp_dir,layer_filename) ])

			filename = coverage_name
			#zipfile  = None
			#file_handle = None
			file_type = 'e00'
		elif os.path.splitext(filename.upper())[1] in [ '.SHP' ]:
			file_type = 'shp'
			# We need to defer this so all the files can be extracted...
			zipfileOBJ.extract(file_handle, self.get_sink_directory(path="LAYERS") )
			self.deferred_layers.append(os.path.join(self.get_sink_directory(path="LAYERS"),filename  ))
			return None
		elif os.path.splitext(filename.upper())[1] in [ '.DBF', '.SHX', '.PRJ' ,'.XML']:
			file_type = 'shp'
			# We need to defer this so all the files can be extracted...
			zipfileOBJ.extract(file_handle, self.get_sink_directory(path="LAYERS") )
			#self.deferred_layers.append(os.path.join(self.get_sink_directory(path="LAYERS"),filename  ))
			return None
		else :
			temp_dir = None

		#Get ready for putting the data into respective containers

		#Open the layer
		# This will not work on files not already extracted...
		src_ds = ogr.Open(filename)
		if src_ds is None:
			print("\t{0} is not openable by ogr.Open()".format(filename))
			return None

		layername = 0
		if file_type == 'e00' :
			if 'geometry_type' in t.keys():
				layername = e00_type_LUT[ t['geometry_type'] ]
			elif 'PAL' in  [ l.GetName() for l in src_ds ]:
				layername = 'PAL'
				t['geometry_type'] = 'Polygon'
			elif 'ARC' in  [ l.GetName() for l in src_ds ]:
				layername = 'ARC'
				t['geometry_type'] = 'Polyline'
			elif 'LAB' in  [ l.GetName() for l in src_ds ]:
				layername = 'LAB'
				t['geometry_type'] = 'Point'
		#print ("Opened {0} Layer {1}".format(filename,layername))
		src_layer = src_ds.GetLayer(layername)

		if not src_layer:
			self.log_an_error ("Opened {0} Layer {1}".format(filename,layername)+"\n\tCannot open... Skipping. Layers Available: {0}".format(str([ l.GetName() for l in src_ds ]).replace("'",'')))
			return None

		self.get_srs_from_file(src_layer)

		#print self.layerdescription(src_layer)

		layerDefinition = src_layer.GetLayerDefn()

		dst_fieldNames = [f['name'].lower() for f in  t['fields']]

		# if the data file is open (self.ds) then we have a geopackage
		if self.ds is not None:
			# Create Layer (Fields from mindmap spec)
			dst_layer = self.create_layer(layerdesc=t)

			# Add Fields from datafile
			for i in range(layerDefinition.GetFieldCount()):
				if layerDefinition.GetFieldDefn(i).GetName().lower() not in dst_fieldNames:
					# Add this field to the gpkg
					try:
						dst_layer.CreateField(layerDefinition.GetFieldDefn(i))
					except RuntimeError:
						pass # This happens when the field already exists

			dst_LayerDefn = dst_layer.GetLayerDefn()


		#
		# Append the datafile rows
		#
		mf = self.getMandatoryFieldList(t)
		do_null_check = ( file_type == 'e00' and t['geometry_type'] == 'Polygon' )
		for infeature in src_layer:
			#Build Row
			row = dict([ (layerDefinition.GetFieldDefn(i).GetName(),infeature.GetField(i) ) for i in range(layerDefinition.GetFieldCount()) ])
			geom = infeature.GetGeometryRef().GetLinearGeometry()

			#If we are doing the geopackage
			if self.ds is not None:

				outFeature = ogr.Feature(dst_LayerDefn)
				if do_null_check:
					if not [ fname for fname,fvalue in row.iteritems() if fname in mf and fvalue  not in [None, '',' ',0] ] :
						continue # when there are no non-blank rows...

				for fname,fvalue in row.iteritems():
					if fname not in ['fid']:
						try:
							outFeature.SetField(fname, fvalue )
						except NotImplementedError:
							# This happens with the ArcIDs field... it's a list of id's...
							if isinstance(fvalue,list):
								outFeature.SetField(fname, str(fvalue) )

				#NULL Geometry is invalid, so if we have no geometry, drop the feature
				if geom:
					try:
						if geom.IsValid():
							outFeature.SetGeometry(geom)
						else:
							outFeature = None
							continue
					except RuntimeError as e:
							outFeature = None
							continue
				else:
					outFeature = None
					continue

				# Add new feature to output Layer
				try:
					dst_layer.CreateFeature(outFeature)
				except RuntimeError as e:
					#print str(e)
					if str(e)[:66] == 'RuntimeError: failed to execute insert : UNIQUE constraint failed: slashchiptreatment.fid'[:66] :
						#remove the field from the feature and try again....
						fname = str(e).split('.')[1]
						outFeature.SetField(fname,None)
						dst_layer.CreateFeature(outFeature)
				# And clear the feature
				outFeature = None


		src_layer = None
		src_ds = None

		if dst_layer:
			try:
				dst_layer.SyncToDisk()
			except RuntimeError as e:
				print e
				pass


	def clean_up_temp_extracted_zipfiles(self,temp_dir_list=[]):
		"""Remove the temp directory and associated files"""
		if temp_dir_list :
			for temp_dir in temp_dir_list:
				#print "shutil.rmtree(temp_dir)"
				try:
					shutil.rmtree(temp_dir)
				except WindowsError as e:
					print e




	def log_an_error(self,msg,type='Importer Error'):
		print msg
		if type not in self.results.keys() :
			self.results[type] = list()
		(self.results[type]).append(msg)






















	def copy_deferredlayer(self,filename, file_handle=None, zipfileOBJ = None):
		#print "\togr2ogr {0} {1}".format(filename, self.gpkg_name)
		# Do we need to extract and copy rows?
		filetype = None

		# Identify the layer name for the sink layer (from mindmap spec)
		tablenames = self.getTableListMatchingString( os.path.splitext(filename.upper())[0] )
		#print tablenames
		try:
			t = self.findTable(tablenames[0])
		except IndexError as e:
			t = None
		if not t:
			t = None

		dst_layer = None

		if os.path.splitext(filename.upper())[1] in [ '.GDBTABLE', '.GDBTABLX', '.GDBINDEXES', '.ATX','.SPX','.FREELIST']:
			file_type = 'gdb'
			os.path.dirname(filename.upper())
			# We need to defer this so all the files can be extracted...
			zipfileOBJ.extract(file_handle, self.get_sink_directory(path="LAYERS\\") )
			#self.deferred_layers.append(os.path.join(self.get_sink_directory(path="LAYERS"),filename  ))
			return None
















	def layerdescription(self,layer):
		lyrDefn = layer.GetLayerDefn()
		msg = ''
		msg += lyrDefn.GetName() +"\n"
		for i in range( lyrDefn.GetFieldCount() ):
			fieldName =  lyrDefn.GetFieldDefn(i).GetName()
			fieldTypeCode = lyrDefn.GetFieldDefn(i).GetType()
			fieldType = lyrDefn.GetFieldDefn(i).GetFieldTypeName(fieldTypeCode)
			fieldWidth = lyrDefn.GetFieldDefn(i).GetWidth()
			GetPrecision = lyrDefn.GetFieldDefn(i).GetPrecision()

			msg += "\t"+ fieldName + " - " + fieldType+ " " + str(fieldWidth) + " " + str(GetPrecision)+"\n"
		return msg



########################################
## After the file movement, handle the visualization
########################################
	def generate_sink_qgis_layer_file(self):
		pass

	def generate_sink_html_browse(self):
		pass

	def generate_sink_arcpy_files(self):
		pass

###############################
## Some worker functions for adding layers and fields...
###############################

	def get_srs_from_file(self,srs):
		if isinstance(srs,osr.SpatialReference):
			self.srs =  srs
		elif isinstance(srs,int):  # e.g. 26916
			self.srs.ImportFromEPSG(srs)
		elif isinstance(srs,ogr.Layer): # Source from a filename...
			self.srs = srs.GetSpatialRef()
		elif isinstance(srs, str):    # e.g. "WGS84"
			self.srs.SetFromUserInput(srs)

	def create_layer(self,layerdesc,lower_case=True,geom_type=None):
		"""
		"""
		try:
			layername = ( layerdesc['name']).lower() if lower_case else (layerdesc['name']).upper()
		except KeyError as e:
			print e


		#First, safely return from a second run of this darn thing...
		try:
			return self.layers[layername]
		except KeyError :
			pass


		# Create Data Layer
		try:
			if geom_type is None:
				geom_type = geom_type_LUT[layerdesc['geometry_type']  ]
		except KeyError as e:
			print ("Error creating layer {0} in {1} as {2}".format(layerdesc['name'], self.gpkg_name,"layerdesc['geometry_type']"))
			print e
			#raise(e)
			return None

		#print "Doing layer"
		#CreateLayer(DataSource self, char const * name, SpatialReference srs=None, OGRwkbGeometryType geom_type, char ** options=None)
		#Original layername was FIM compatible name
		#layername = ('mu999_20'+layerdesc['shortname']+'00').lower() if lower_case else ('mu999_20'+layerdesc['shortname']+'00').upper()
		lyr = self.ds.CreateLayer(layername , srs=self.srs, geom_type = geom_type, options=['OVERWRITE=YES'])

		#print "Doing Fields"
		for f in layerdesc['fields'] :
			try:
				#field_type = fieldTypeLUT[f['type'].lower() ]
				field_type = self.getOGRFieldType(f['type'])
			except KeyError as e:
				print ("Error creating layer {0} in {1} as {2}".format(f['name'], layername, f['type']))
				print e

			lyr.CreateField( ogr.FieldDefn(f['name'], field_type ) )
			#print (f['name'] + " created")
		#print "Fields Done"
		self.layers[layername ] = lyr

		return self.layers[layername]


	def getOGRFieldType(self,field_type = None):
		if field_type in fieldTypeLUT.values():
			pass
		elif field_type.lower() in fieldTypeLUT.keys():
			field_type = fieldTypeLUT[field_type.lower() ]
		else :
			field_type = ogr.OFTString
		return field_type


	def addfield(self,tablename,name,field_type=None):
		field_type = self.getOGRFieldType(field_type)
		try:
			lyr=self.layers[tablename]
		except KeyError :
			lyr = self.ds.GetLayerByName(tablename)

		#Only create if not already present...
		#print "Field {n} Index {i}".format(n=name,i=lyr.GetLayerDefn().GetFieldIndex(name))
		if lyr.GetLayerDefn().GetFieldIndex(name) == -1:
			lyr.CreateField( ogr.FieldDefn(name, field_type ) )

	def build_feature(self, row, tablename, fid_name='OBJECTID'):
		"""
		Builds a feature using dummy wkt based on the
		POLYID and the feature type
		"""
		t = self.findTable(tablename)
		geom_type = geom_type_LUT[t['geometry_type']  ]

		try:
			lyr=self.layers[tablename]
		except KeyError :
			lyr = self.ds.GetLayerByName(tablename)

#		print "Building Dummy Feature"
#		print fid_name
#		print row
#		print row[fid_name]


		# Generate WKT Geometry from polyid only
		step = 1000 # metres
		x_min = 400000 +    ( int (row[ fid_name ]) % 50 ) * step
		y_min = 5900000+ int(int  (row[ fid_name ]) / 50 ) * step

		if geom_type == ogr.wkbPolygon :
			wkt = 'POLYGON(( {x_min} {y_min} , {x_min} {y_max}, {x_max} {y_max}, {x_max} {y_min}, {x_min} {y_min} ))'.format(x_min=x_min,x_max=x_min + step, y_min=y_min,y_max=y_min + step)
		if geom_type == ogr.wkbLineString:
			wkt = 'LINESTRING( {x_min} {y_min} , {x_max} {y_max} ))'.format(x_min=x_min,x_max=x_min + step, y_min=y_min,y_max=y_min + step)
		if geom_type == ogr.wkbPoint  :
			wkt = 'POINT( {x} {y} )'.format(x=x_min+step/2,y=y_min + step/2)

		feat = ogr.Feature(lyr.GetLayerDefn())
		for k,v in row.iteritems():
			try:
				#print "Trying feature {k}\t".format(k=k),
				feat[k] = v
				#print "...success"
			except ValueError as e :
				# feat[k] is either a string, and we are coercing, or an integer...
				#print "Trying feature {k}\t".format(k=k),
				#print lyr.FindFieldIndex( k, True )
				#print feat.GetFieldDefnRef(lyr.FindFieldIndex(k)).getType()
				#GetFieldDefnRef OGR_Fld_GetType( lyr.GetLayerDefn().GetField(lyr.GetLayerDefn().GetFieldIndex(k))
				#print '{v}<-{t}'.format(v=v,t=type(v))
				#print e
				#raise e
				pass
		feat.SetGeometry(ogr.CreateGeometryFromWkt(wkt))
		lyr.CreateFeature(feat)



	def write_results_file(self):
		with open( self.get_sink_directory('importer_results.txt'), 'w') as f:
			f.write ( json.dumps( self.results, sort_keys=True, indent=4, separators=(',', ': ')) )
#=======================================================================
# Testing Routine:
#=======================================================================


def main_testing(args):
	import shutil
	#filename = r"\\cihs.ad.gov.on.ca\mnrf\Groups\ROD\Northwestregion\RIAU\Data\Extended_Data\Forest\FMP\Whitefeather\FMP\2012\MU994_2012_FMP_P1.zip"
	#filename = r"C:\GIS\FMP\Dryden\FMP\2016\MU535_2011_FMP_P2.zip"
	#AR 2018 - Gordon Cosens
	#filename = r"C:\GIS\FMP\Gordon_Cosens\AR\2017\24649.zip"
	#s = importer_conductor(jsonfilename=r"c:\src\FMPDS\tech_spec\2018\ar.mm", datafilename = filename )

	# AR 2009 - Ogoki
	#filename = r"f:\Sapawe\zips\24769.zip"
	#filename = u'\\\\Lrcgikdcwhiis06\\fmp\\Martel\\zips\\23164.zip'
	#filename = u'\\\\Lrcgikdcwhiis06\\fmp\\Dryden\\zips\\7794.zip'
	#filename = u'\\\\Lrcgikdcwhiis06\\fmp\\Dryden\\zips\\24922.zip'
	#filename = u'\\\\Lrcgikdcwhiis06\\fmp\\Wabigoon\\zips\\25026.zip'
	#filename = u'\\\\Lrcgikdcwhiis06\\fmp\\Ottawa_Valley\\zips\\25039.zip'
	parms = {'jsonfilename':r"c:\src\FMPDS\tech_spec\2018\aws.mm",
		'datafilename':u'c:\\gis\\fmp_new\\Kenora\\AWS\\24967.zip',
		'filemetadata':{'year': '2019','id': 24967,'fmu': 'Kenora','product': 'AWS','planyear': '2012','fmu_id': '644','phase': ''}
		}
	s = importer_conductor(sink_dir=r"c:\GIS\FMP_New" , **parms)
	#		jsonfilename=r"c:\src\FMPDS\tech_spec\2018\ar.mm", datafilename = filename, sink_dir=r"c:\GIS\FMP_New" )
	print ( "Found Portal ID {0} in zip filename".format(s.get_fi_portal_id_from_zipfilename()) )
	#print s.get_filename_from_database(fi_portal_id=6543)
	#print s.get_sink_directory()

	try:
		shutil.rmtree( s.get_sink_directory() )
	except WindowsError as e:
		print e

	s.overwrite = True

	s.go()
	return 0

def main(args):
	parms = { 'sink_dir':r"c:\GIS\FMP_New" }
	for k,v in { '-j':'jsonfilename', '-i':'datafilename','-d':'sink_dir','-w':'overwrite'}.iteritems():
		if k in args: parms[v] = args[args.index(k) + 1 ]
	for k,v in { '-y':'year', '-s':'id', '-f':'fmu', '-p':'product','-n':'planyear','-h':'phase','-m':'fmuid'}.iteritems():
		if k in args:
			if 'filemetadata' not in parms.keys():  parms['filemetadata'] = dict()
			parms['filemetadata'][v] = args[args.index(k) + 1 ]
	s = importer_conductor(**parms)
	return s.go()

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv[1:]))
