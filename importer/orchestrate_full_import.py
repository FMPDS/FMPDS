#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       untitled.py
#
#       Copyright 2018  <>
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.
import os
import sys
import subprocess

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from db_driven_importer import db_driven_importer

from run_rod_checker import run_rod_checker
from run_nw_checker import run_nw_checker
from mkrellyr import mkrellyr

def main(data_filter,doOnly=None):
	# Run the importer
	if doOnly is None:
		doOnly = [ 'gdb' , 'rod_chk', 'nw_chk' ]

	dbdi = db_driven_importer(data_filter,limit=1)
	try:
		res = dbdi.go(  )[0]
	except IndexError:
		#OK, no results, so quit quietly...
		print ("No new products available")
		return 0

	d= dict(zip(['forest','product','year','submissionid','planyear','fmuid'],res))

#	try:
	if "gdb" in doOnly:
		mkrellyr(d).go()
#	except Exception as e:
#		print "GDB/LYR Failed?..."
#		print e

#	try:
		#Run the ROD Checker
	if "rod_chk" in doOnly:
		run_rod_checker(d).go()
#	except Exception as e:
#		print "ROD Checker Failed?..."
#		print e


	#Run the NW Checker
	try:
		if "nw_chk" in doOnly:
			run_nw_checker(d).go()
	except Exception as e:
		print "NW Checker Failed?..."
		print e

	#Generate the HTML Browse


	return 0

if __name__ == '__main__':
	data_filter = { "product":"AR", "year":"2017%" }
	try:
		data_filter['forest']=sys.argv[1]
	except IndexError:
		pass

	try:
		data_filter['product']=sys.argv[2]
	except IndexError:
		pass

	try:
		data_filter['year']=sys.argv[3]
	except IndexError:
		pass

	try:
		doOnly=sys.argv[4]
	except IndexError:
		doOnly=None



	main(data_filter = data_filter, doOnly=doOnly )

