#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  qlrwriter.py
#
#  Copyright 2018 CARRAN <CARRAN@ON34C02585177>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import os,sys
import uuid

from osgeo import gdal
from osgeo import ogr
from osgeo import osr

import random

from xml.etree.ElementTree import ElementTree
import xml.etree.ElementTree as ET

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from checker.filechecker import jsonloader


initial_qlr = """
<!DOCTYPE qgis-layer-definition>
<qlr>
  <layer-tree-group name="a" checked="Qt::Checked" expanded="1">
  </layer-tree-group>
  <maplayers>
  </maplayers>
</qlr>
"""

def random_color():
	return "%06x" % random.randint(0, 0xFFFFFF )

def hex_to_rgb(value):
	""" hex_to_rgb("FFFFFF") ==> (255, 255, 255)
	"""
	value = value.lstrip('#')
	lv = len(value)
	return tuple(int(value[i:i+lv/3], 16) for i in range(0, lv, lv/3))

def hex_to_qgiscolor( value, opacity=255 ):
	#print hex_to_rgb(value)
	return "{c[0]},{c[1]},{c[2]},{o}".format( c=hex_to_rgb(value), o=opacity)

def rgb_to_hex(rgb):
	""" rgb_to_hex((255, 255, 255)) ==> FFFFFF"""
	return '%02x%02x%02x' % rgb

# in-place prettyprint formatter
# From http://effbot.org/zone/element-lib.htm#prettyprint
def indent(elem, level=0):
    i = "\n" + level*"  "
    if elem is None:
		pass
    elif len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i

def generate_symbol(name,fill_color,stroke_color):
	ss = ET.Element(tag="symbol", type="fill", name=name, alpha="1", clip_to_extent="1" )
	ssl =ET.SubElement(ss, "layer", { 'enabled':"1", 'pass':"0", 'class':"SimpleFill", 'locked':"0" } )
	ET.SubElement(ssl, "prop", k="border_width_map_unit_scale", v="3x:0,0,0,0,0,0" )
	ET.SubElement(ssl, "prop", k="color", v=hex_to_qgiscolor(fill_color) )
	ET.SubElement(ssl, "prop", k="joinstyle", v="bevel")
	ET.SubElement(ssl, "prop", k="offset", v="0,0" )
	ET.SubElement(ssl, "prop", k="offset_map_unit_scale", v="3x:0,0,0,0,0,0" )
	ET.SubElement(ssl, "prop", k="offset_unit", v="MM" )
	ET.SubElement(ssl, "prop", k="outline_color", v=hex_to_qgiscolor(stroke_color) )
	ET.SubElement(ssl, "prop", k="outline_style", v="solid" )
	ET.SubElement(ssl, "prop", k="outline_width", v="0.26" )
	ET.SubElement(ssl, "prop", k="outline_width_unit", v="MM" )
	ET.SubElement(ssl, "prop", k="style", v="solid" )

	return ss



def writeMeAQLRFile(gpkgname,jsonfilename):
	j = jsonloader(jsonfilename,ignore_non_tables=False)
	qlrname = gpkgname.replace(".gpkg",".qlr")

	qlr = ET.Element ('qlr')
	ET.SubElement(qlr, "layer-tree-group", { "name":"", "checked":"Qt::Checked", "expanded":"1"} )
	maplayers = ET.SubElement(qlr,"maplayers")


	TOC = ET.SubElement(qlr.find('layer-tree-group'), "layer-tree-group", { "name":"mu535_2016fmpdp Dryden Phase 2", "checked":"Qt::Checked", "expanded":"1"} )



	for t in j.json:
		if 'fields' in t.keys(): #It's a table... so use it
			# Add a layer in the TOC, then in the maplayers
			layer_name = t['name']
			layer_id = layer_name + str(uuid.uuid4().hex )
			layer_datasource = "./mu535_2016fmpdp.gpkg|layername="+layer_name

			ET.SubElement(TOC, "layer-tree-layer", id=layer_id, name=layer_name, source=layer_datasource, providerKey="ogr", checked="Qt::Checked", expanded="0")

			#Add the map layer and so on...
			geometry_type = t['geometry_type']
			ml = ET.SubElement(maplayers, "maplayer", type="vector", geometry=geometry_type, simplifyMaxScale="1", refreshOnNotifyEnabled="0", minScale="1e+8", autoRefreshEnabled="0" ,labelsEnabled="0", simplifyDrawingTol="1", maxScale="0", simplifyDrawingHints="1", refreshOnNotifyMessage="", readOnly="0", simplifyLocal="1", hasScaleBasedVisibilityFlag="0", simplifyAlgorithm="0", autoRefreshTime="0")
			ET.SubElement(ml, "id" ).text = layer_id
			ET.SubElement(ml, "datasource").text=layer_datasource
			ET.SubElement(ml, "layername").text=layer_name

			ds = ogr.Open(gpkgname)
			lyr = ds.GetLayerByName(layer_name)
			spatialRef=lyr.GetSpatialRef()
			srs = spatialRef.ExportToWkt()
			srid = spatialRef.GetAttrValue('AUTHORITY',1)
			authid = ':'.join([spatialRef.GetAttrValue('AUTHORITY',0), srid])

			#print srid , authid
			srs_element = ET.SubElement(ET.SubElement(ml,'srs'),'spatialrefsys')
			ET.SubElement(srs_element, 'srid').text=srid
			ET.SubElement(srs_element, 'authid').text=authid

			try:
				symb = t['symbology:']
			except KeyError:
				symb = { 'type':'singleSymbol', 'stroke color':'#777777', 'fill color':random_color() }

			if symb['type'] == 'singleSymbol' :
				r = ET.SubElement(ml,"renderer-v2", type="singleSymbol", symbollevels="0", enableorderby="0", forceraster="0" )
				s = ET.SubElement(r,"symbols")
				if  'fill color' not in symb.keys():
					symb['fill color'] = random_color()
				if 'stroke color' not in symb.keys():
					symb['stroke color'] = random_color()
				s.append(generate_symbol("0",symb['fill color'],symb['stroke color']))
			elif symb['type'] == 'categorized' :
				valueslist = [ f for f in t['fields'] if f['name'] == symb['field'] ][0]['values']

				r = ET.SubElement(ml,"renderer-v2", type="categorizedSymbol", symbollevels="0", enableorderby="0", forceraster="0", attr=symb['field'])
				c = ET.SubElement(r,"categories")
				s = ET.SubElement(r,"symbols")
				#print valueslist
				for v in valueslist:
					#print v
					if 'fill color' not in v.keys() and 'fill color' not in symb.keys():
						v['fill color'] = random_color()
					elif 'fill color' not in v.keys():
						v['fill color'] = symb['fill color']
					if 'stroke color' not in v.keys() and 'stroke color' not in symb.keys():
						v['stroke color'] = random_color()
					elif 'stroke color' not in v.keys():
						v['stroke color'] = symb['stroke color']

					s.append(generate_symbol(v['name'],v['fill color'],v['stroke color']))

					ET.SubElement(c,"category", symbol=v['name'], value=v['name'], render="true", label=v['name'] )


	#pretty print it...
	indent(qlr)
	#print ET.dump(qlr)

	with open(qlrname,'wb') as a:
		tree = ElementTree(qlr)
		tree.write(a)


def main(args):
	#print hex_to_qgiscolor(random_color())


	writeMeAQLRFile(gpkgname=r"C:\GIS\FMP_New\Dryden\fmp\2016\mu535_2016fmpdp.gpkg", \
		jsonfilename = r"C:\src\FMPDS\tech_spec\2009\fmp.mm" )

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
