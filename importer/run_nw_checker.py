#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       untitled.py
#
#       Copyright 2018  <>
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

from tape_handler import tape_handler
import os
import sys
import datetime

import glob

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
#import checker.initialize_checker
from stack.choose_a_tech_spec  import choose_a_tech_spec

class run_nw_checker( tape_handler ):
	"""
	This is actually a TAPE Handler

	It receives a dict containing:
		d= dict(zip(['forest','product','year'],["Lac_Seaul",'AR','2017']))
	From the db run or event or whatnot...

	It generates the HTML report from the checker

	T: fi_portal_imported event on a successful importation
		fi_portal_download_imported ( sink_dir name, fi_portal_id, filename )
	A: Run the NW Checker
	P: HTML Report in sink_dir
	E: fi_portal_nw_checker
			(payload->>'fi_portal_id') as fi_portal_id,
			output filename as payload
			payload->>'status' as status
			url

	"""

	eType = 'fi_portal_imported'
	successEvent = "fi_portal_nw_checker"
	channel = eType # event Type is used for the channel name... :-)
	jobType = 'fi_portal_nw_checker'
	exclusive_job = False

	#staletime = '60 minutes'
	staletime = '2 minutes'
	#backlog_frequency = 60
	backlog_frequency = 2
	#timeout = 60 # seconds
	timeout = 1 # seconds
	#This makes the default to be run a backlog every hour, and handle
	# interstitial events every minute.
	failure_count = 2

	def go(self, event=None):
		"""
		Run the NWR Checker

		"""

		print ("Running the NWChecker\n\tfi_portal_id from Payload: {0}".format(event['payload']['fi_portal_id']))

		d = self.getforestproductyear(event['payload']['fi_portal_id'])

		d['date'] = datetime.date.today()

		try:
			sink_dir = event['payload']['sink_dir']
		except KeyError as e:
			return self.status("Cannot find sink_dir in payload")
			#'sink_gpkg':['sink_gpkg']
			#'sink_dir':sink_dir


		# Try the GDB (*.gdb/)
		if os.path.isdir(sink_dir+'\\LAYERS\\MU{fmuid}_{year}_{product}.gdb'.format(**d)):
			#Run the checker on the original files (GDB's)
			d['gdbname'] = sink_dir+'\\LAYERS\\MU{fmuid}_{year}_{product}.gdb'.format(**d)

		# Try the GDB (*.gdb/)
		elif os.path.isdir(sink_dir+'\\LAYERS\\MU{fmuid}_{year_2}{product}.gdb'.format(**d)):
			#Run the checker on the original files (GDB's)
			d['gdbname'] = sink_dir+'\\LAYERS\\MU{fmuid}_{year_2}{product}.gdb'.format(**d)

		# Misnamed GDB
		elif glob.glob(sink_dir+'\\LAYERS\\*.gdb'.format(**d)) :
			d['gdbname'] = glob.glob(sink_dir+'\\LAYERS\\*.gdb'.format(**d))[0]

		# Coverage Files ( info )
		elif os.path.isdir(sink_dir+'\\LAYERS\\info'.format(**d))  :
			#Run the checker on the original files (coveragess)
			d['gdbname'] = sink_dir+'\\LAYERS'.format(**d)


		# Shapefiles (*.shp)
		elif os.path.isdir(sink_dir+'\\LAYERS\\MU{fmuid}_{year}_{product}_LAYERS'.format(**d))  :
			#Run the checker on the original files (SHP's)
			d['gdbname'] = sink_dir+'\\LAYERS\\MU{fmuid}_{year}_{product}_LAYERS'.format(**d)

		# More General Shapefiles (*.shp)
		elif glob.glob(sink_dir+'\\LAYERS\\*.shp'.format(**d)) :
			#Run the checker on the original files (SHP's)
			d['gdbname'] = sink_dir+'\\LAYERS'.format(**d)

		# GeoPackage
		elif os.path.isdir(sink_dir+'\\MU{fmuid}_{year}_{product}.gpkg'.format(**d)):
			#Run the checker on the geopackage created by the importer
			d['gdbname'] = sink_dir+'\\MU{fmuid}_{year}_{product}.gpkg'.format(**d)


		else :
			d['gdbname'] = sink_dir+'\\MU{fmuid}_{year}_{product}.gpkg'.format(**d)
			#print ("Using  {gdbname}".format(**d))
			#d['gdbname'] = event['payload']['sink_gpkg']

		print ("NW Checker called on {gdbname}".format(**d))

		#checker_call=r'"c:\osgeo4w64\osgeo4w.bat" "python.exe" U:\src\FMPDS\checker\context_checker.py -i "'+self.sink_dir+r'\{forest}\{product}\{year}\mu{fmuid}_{year}_{product}.gpkg" -p {product} -j U:\src\FMPDS\tech_spec\2018\ar.json '.format(**d)

		checker_call=['python.exe']
		#"C:\Users\carran\MNRF_NWR_RIAU_Checker\checker\context_checker.py"
		checker_call.append(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),os.path.join('checker','context_checker.py')))
		if not os.path.exists( checker_call[-1] ):
			print checker_call[-1]
			raise RuntimeError("Auuuugh\t\t" + checker_call[-1] )
		#-i "F:\Trout_Lake\AWS\2016\LAYERS\MU120_2016_AWS_layers"
		checker_call.append('-i')
		checker_call.append(d['gdbname'])
		# -p AWS
		checker_call.append('-p')
		checker_call.append(d['product'])
		# -j "C:\Users\carran\MNRF_NWR_RIAU_Checker\tech_spec\2018\aws.json"
		try:
			checker_call.append('-j')
			checker_call.append(choose_a_tech_spec( d['product'],d['year'] ))
			jsonfilename = checker_call[-1]
		except RuntimeError as e:
			print e
			return self.status('No Tech Spec')
		# -f str(d['forest']).replace("_"," ")
		checker_call.append('-f')
		checker_call.append(str(d['forest']))

		# -s event['payload']['fi_portal_id']
		checker_call.append('-s')
		checker_call.append(str(event['payload']['fi_portal_id']))

		# --year=4_DIGIT_YEAR ]            { -y= }
		checker_call.append('-y')
		checker_call.append(str(d['year']))

		# [ --planyear=4_DIGIT_YEAR ]         { -n= }
		checker_call.append('-n')
		checker_call.append(str(d['planyear']))

		# [ --outputfile=filename ]                               { -o= }
		checker_call.append('-o')
		outputfile = os.path.join(\
			sink_dir, \
			"Checker_Report_{submissionid}.html".format(**d) \
			)
		checker_call.append(outputfile)

		#Don't run the web browser
		checker_call.append('-w')

		print '\n\t'.join(['Running NWChecker...','Output File {0}'.format(outputfile),'JSON FILE NAME {0}'.format(jsonfilename)])

		checker_status = self.call( checker_call )

		if checker_status is None:
			return self.status("Failure")
		else :
			status = 0

		####################################
		# Status Report and event creation #
		####################################
		# if we get here, there is no problem...
		url = outputfile.replace('f:\\\\','/fmp_import/').replace('\\','/').replace('f:/','/fmp_import/')
		new_event_payload = dict( event['payload'], **{ 'filename':outputfile, 'status':status , 'url':url, 'checker_status':checker_status } )
		#print new_event_payload
		self.generateEvent(new_event_payload)
		return self.status("Success")


if __name__ == '__main__':
	t = run_nw_checker({})
	t.interpret_command_line(sys.argv)

