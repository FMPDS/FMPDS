#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       untitled.py
#
#       Copyright 2018  <>
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

from tape_handler import tape_handler
import os,sys
import glob

#fi_checker_location = 's:\ROD\RODOpen\Forestry\Tools_and_Scripts\FI_Checker\Deliverables\FI_Checker_v2_4b\submission_checker\Checker_v2_command_line.py'
fi_checker_location = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))),'FI_Checker\Deliverables\FI_Checker_v2_4b\submission_checker\Checker_v2_command_line.py')

class run_rod_checker( tape_handler ):
	"""
	Intended as the entry point to create the relative layer files...
	This is actually a TAPE Handler

	It receives a dict containing:
		d= dict(zip(['forest','product','year'],["Lac_Seaul",'AR','2017']))
	From the db run or event or whatnot...

	It generates the relative layer file and associated GDB file for the
	importer products

	T: fi_portal_imported event on a successful importation
	fi_portal_download_imported ( sink_dir name, fi_portal_id, filename )
	A: Run the ROD Checker
	P: HTML Report in sink_dir/.../*.html
	E: fi_portal_rod_checker
			(payload->>'fi_portal_id') as fi_portal_id,
			output filename as payload
			payload->>'status' as status
			url

	"""

	eType = 'fi_portal_imported'
	successEvent = 'fi_portal_rod_checker_complete'
	channel = eType # event Type is used for the channel name... :-)
	jobType = 'fi_portal_rod_checker'
	exclusive_job = False

	staletime = '60 minutes'
	#staletime = '2 minutes'
	#backlog_frequency = 60
	backlog_frequency = 3
	timeout = 600 # seconds
	#timeout = 1 # seconds
	#This makes the default to be run a backlog every hour, and handle
	# interstitial events every minute.


	def go(self,event=None):
		"""
		Run the ROD Checker

		dataformat_dict = {'shp':'shapefile', 'fc':'feature classes', 'cov':'coverage'}

		"""
		print ("Running the ROD Checker")
		#print event.keys()

		self.d = self.getforestproductyear(event['payload']['fi_portal_id'])
		self.d['sink_dir'] = event['payload']['sink_dir']


		print "================= Working parameters of rod_checker====="
		print self.d
		print "--------------------------------------------------------"

			#F:\Spanish\AWS\2019\LAYERS\MU210_2019_AWS.gdb

		if os.path.isdir('{sink_dir}\\LAYERS\\MU{fmuid}_{year}_{product}.gdb'.format(**self.d)):
			######\\Lrcgikdcwhiis06\fmp\Spanish\AWS\2018\LAYERS\MU210_AWS.gdb
			#\\Lrcgikdcwhiis06\fmp\Big_Pic\AR\2016\LAYERS\MU067_2016_AR.gdb
			self.d['gdbname'] = '{sink_dir}\\LAYERS\\MU{fmuid}_{year}_{product}.gdb'.format(**self.d)
			self.d['fc_type']='fc'
			output_glob='{sink_dir}\\LAYERS\\*.html'.format(**self.d)

		elif os.path.isdir('{sink_dir}\\LAYERS\\MU{fmuid}_{year_2}{product}.gdb'.format(**self.d)):
			######\\Lrcgikdcwhiis06\fmp\Spanish\AWS\2018\LAYERS\MU210_AWS.gdb
			#\\Lrcgikdcwhiis06\fmp\Big_Pic\AR\2016\LAYERS\MU067_16AR.gdb
			self.d['gdbname'] = '{sink_dir}\\LAYERS\\MU{fmuid}_{year_2}{product}.gdb'.format(**self.d)
			self.d['fc_type']='fc'
			output_glob='{sink_dir}\\LAYERS\\*.html'.format(**self.d)
		elif os.path.isdir('{sink_dir}\\LAYERS\\MU{fmuid}_{year}_{product}_LAYERS.gdb'.format(**self.d)):
			#\\Lrcgikdcwhiis06\fmp\English_River\AR\2017\LAYERS\MU230_2017_AR_LAYERS.gdb
			self.d['gdbname'] = '{sink_dir}\\LAYERS\\MU{fmuid}_{year}_{product}_LAYERS.gdb'.format(**self.d)
			self.d['fc_type']='fc'
			output_glob='{sink_dir}\\LAYERS\\*.html'.format(**self.d)

		elif os.path.isdir('{sink_dir}\\LAYERS\\MU{fmuid}_{year_2}{product}_LAYERS.gdb'.format(**self.d)):
			#\\Lrcgikdcwhiis06\fmp\English_River\AR\2017\LAYERS\MU230_2017_AR_LAYERS.gdb
			self.d['gdbname'] = '{sink_dir}\\LAYERS\\MU{fmuid}_{year_2}{product}_LAYERS.gdb'.format(**self.d)
			self.d['fc_type']='fc'
			output_glob='{sink_dir}\\LAYERS\\*.html'.format(**self.d)

		elif len (glob.glob('{sink_dir}\\LAYERS\\*.gdb'.format(**self.d)) ) >0:
			self.d['gdbname'] = glob.glob('{sink_dir}\\LAYERS\\*.gdb'.format(**self.d))[0]
			self.d['fc_type']='fc'
			output_glob='{sink_dir}\\LAYERS\\*.html'.format(**self.d)
		elif os.path.isdir('{sink_dir}\\LAYERS\\MU{fmuid}_{product}.gdb'.format(**self.d)):
			self.d['gdbname'] = '{sink_dir}\\LAYERS\\MU{fmuid}_{product}.gdb'.format(**self.d)
			self.d['fc_type']='fc'
			output_glob='{sink_dir}\\LAYERS\\*.html'.format(**self.d)

		elif os.path.isdir('{sink_dir}\\LAYERS\info'.format(**self.d))  :
			self.d['gdbname'] = '{sink_dir}\\LAYERS'.format(**self.d)
			self.d['fc_type']='cov'
			output_glob='{sink_dir}\\LAYERS\\*.html'.format(**self.d)

		elif os.path.isdir('{sink_dir}\\LAYERS\\MU{fmuid}_{year}_{product}_LAYERS'.format(**self.d))  :
			self.d['gdbname'] = '{sink_dir}\\LAYERS\\MU{fmuid}_{year}_{product}_LAYERS'.format(**self.d)
			self.d['fc_type']='shp'
			output_glob='{sink_dir}\\LAYERS\\MU{fmuid}_{year}_{product}_LAYERS\\*.html'.format(**self.d)
		# General Shapefiles (*.shp)
		elif glob.glob('{sink_dir}\\LAYERS\\*.shp'.format(**self.d)) :
			#Shapefiles in LAYERS dir
			self.d['gdbname'] = '{sink_dir}\\LAYERS'.format(**self.d)
			self.d['fc_type']='shp'
			output_glob='{sink_dir}\\LAYERS\\*.html'.format(**self.d)
		elif glob.glob('{sink_dir}\\LAYERS\\*\\*.shp'.format(**self.d)) :
			self.d['gdbname'] = os.path.dirname(glob.glob('{sink_dir}\\LAYERS\\*\\*.shp'.format(**self.d))[0])
			self.d['fc_type']='shp'
			output_glob=os.path.join(self.d['gdbname'],'*.html'.format(**self.d))

#		elif os.path.isdir('{sink_dir}\\LAYERS'.format(**self.d))  :
#			#Shapefiles in LAYERS dir
#			self.d['gdbname'] = '{sink_dir}\\LAYERS'.format(**self.d)
#			self.d['fc_type']='shp'
#			output_glob='{sink_dir}\\LAYERS\\*.html'.format(**self.d)

		else :
			print ("Cannot find files to check for {forest}\\{product}\\{year}".format(**self.d))
			#print ("Looked in :\n{0}".format( "\n\t".join([ '{sink_dir}\\LAYERS\\MU{fmuid}_{year}_{product}.gdb'.format(**self.d),'{sink_dir}\\LAYERS\\MU{fmuid}_{year}_{product}_LAYERS.gdb'.format(**self.d), '{sink_dir}\\LAYERS\\MU{fmuid}_{product}.gdb'.format(**self.d), '{sink_dir}\\LAYERS\\MU{fmuid}_{year}_{product}_LAYERS'.format(**self.d) , '{sink_dir}\\E00'.format(**self.d)  ])))
			return self.status("Cannot Find Input Files")

		self.d['spec_generation'] = 'new'
		if '2009' in os.path.dirname(self.choose_a_tech_spec(self.d['product'], self.d['year']) )  :
			self.d['spec_generation'] = 'old'

		self.d['limit']='nolimit'

		product_munge = self.d['product'] if self.d['product'] not in ['FMP_P1','FMP_P2','FMP_P','BMI','PCI','PCM','FMPDP'] else 'FMP'

		ROD_Checker_call = [self.arcpy_environment,
                        fi_checker_location,
			product_munge,
			self.d['forest'].replace('-','_'),
			str(self.d['year']),
			self.d['planyear'],
			self.d['gdbname'],
			self.d['fc_type'],
			self.d['spec_generation'],
			self.d['limit'],
			str(self.d['submissionid'])
			]

		print ' '.join(ROD_Checker_call)
		with open (os.path.join(self.d['sink_dir'],'ROD_Checker_commandline.bat'), 'w') as  batfile :
			batfile.write("REM ROD Checker bat file used - for the record.\n")
			batfile.write("REM This is not an endorsement of the ROD Checker.\n")
			batfile.write("REM The first version is for use in OSGEO4w shell.\n")
			batfile.write( ' '.join(ROD_Checker_call) )
			batfile.write('\n\n:::::::::::::::::::::::::::::::::::::\n\n')
			batfile.write('REM The second version is for cmd prompt use in ESRI\'s release of python\n')
			batfile.write('REM Assuming python is in the path\n')
			batfile.write('python.exe ')
			batfile.write( ' '.join(ROD_Checker_call[1:]))
			batfile.write('\n\n:::::::::::::::::::::::::::::::::::::\n\n')

		status = self.call(ROD_Checker_call)

		if status is None:
			return self.status("Failure")
		elif status > 0:
			return self.status(status)

		#try:
		if not [ f for f in glob.glob( output_glob ) ]:
			return self.status("Cannot Find Output Files")
		outputfile = [ f for f in glob.glob( output_glob ) ][0]

		#except TypeError :
		#	outputfile = self.d['gdbname']
		#except IndexError :
		#	outputfile = self.d['gdbname']

		if not os.path.exists(outputfile):
			return self.status("Cannot Find Output Files")

		####################################
		# Status Report and event creation #
		####################################
		# if we get here, there is no problem...
		url = outputfile.replace('f:\\','/fmp_import/').replace('\\','/')
		new_event_payload = {'filename':outputfile, 'status':status , 'url':url, 'src':self.d['gdbname'], 'fi_portal_id':self.d['submissionid']  }
		print new_event_payload
		self.generateEvent(new_event_payload)
		return self.status("Success")

if __name__ == '__main__':
	t = run_rod_checker({})
	t.interpret_command_line(sys.argv)
