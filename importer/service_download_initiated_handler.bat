::
:: This is the bat file that starts the service...
::


:: Tried this sc command line, driven from 
:: https://serverfault.com/questions/54676/how-to-create-a-service-running-a-bat-file-on-windows-2008-server
::echo sc create FMPDS_download_initiated_handler binpath="c:\windows\system32\cmd.exe /c %~dp0service_download_initiated_handler.bat"
::GOTO exit_Calmly

:: sc create FMPDS_download_initiated_handler binpath="c:\windows\system32\cmd.exe /c %~dp0service_download_initiated_handler.bat"

if exist "c:\OSGeo4W\bin\o4w_env.bat" (
set CMD="c:\OSGeo4W\bin\o4w_env.bat"
) else if exist "D:\OSGeo4W\OSGeo4W.bat" (
set CMD="D:\OSGeo4W\OSGeo4W.bat"
)

:: cmd /c 
%CMD% python.exe  %~dp0\download_initiated_handler.py

:exit_Calmly


