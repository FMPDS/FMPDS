
import os

# Grabbing dictionary merge idea from http://treyhunner.com/2016/02/how-to-merge-dictionaries-in-python/
from itertools import chain

from counter import counter

import json

#Conceptual model for building the test dataset
from build_empty_dataset import empty_dataset_builder

lower_case=True

tables_to_create = [ 'PlannedRoadCorridors' ] # Generate only this table
# tables_to_create = None #Generate All Tables

ds = empty_dataset_builder(jsonfilename=os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),'tech_spec/2017/FMP.json'),outputfilename=os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),"mu998_2020_PRC_Testing.gpkg"),lower_case=lower_case, tables_to_create=tables_to_create )

fid = counter( 0 )

for tablename in [ 'PlannedRoadCorridors' ] : #ds.getTableList(): # [ 'ForecastDepletions' , 'PlanningCompositeInventory']:                   # ds.getTableList():
	#tablename = 'ForecastDepletions'
	print "Building {layer}".format(layer=tablename)
	good_row = ds.get_sample_feature(tablename)

	row_id_name = 'POLYID' if 'POLYID' in good_row.keys() else 'OBJECTID'
	#print row_id_name

	ds.addfield(tablename,'OBJECTID', 'integer')
	ds.addfield(tablename,'Perturbation')
	ds.addfield(tablename,'Error')
	ds.addfield(tablename,row_id_name,'integer')

	row = dict(chain( good_row.items(),
		{ row_id_name: str( fid.postinc() )  }.items(),
		{ 'OBJECTID'  : fid.v                 }.items(),
		{ 'Error':'Good Row'  }.items()
		) )
	ds.build_dummy_feature(row,tablename)

	perturbations = { 'ROADID': \
			{
				"{'ROADID':None}":"ST1: <b><mark>ROADID</mark></b> must not be blank or null..." ,
				"{'ROADID':'This Road Should Never Have Been Built', 'DECOM':'SCAR'}":"You put it where? Through all those Mining Claims?",
				"{'ROADID':'Harolds New Road' }":"Really Harold... Did you have to?",
			}
		}
	#ds.get_perturbations(tablename)


	for f,p in perturbations.iteritems():
		for k,v in p.iteritems():
			row = dict(chain( good_row.items(),
				{ row_id_name: str( fid.postinc() )  }.items(),
				{ 'OBJECTID'  : fid.v                }.items(),
				{ 'Error':str(v)  }.items(),
				{ 'Perturbation':str(k)  }.items(),
				eval(k).items()
				) )
			ds.build_dummy_feature(row,tablename)


