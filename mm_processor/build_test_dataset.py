
import os

# Grabbing dictionary merge idea from http://treyhunner.com/2016/02/how-to-merge-dictionaries-in-python/
from itertools import chain

from counter import counter

import json

#Conceptual model for building the test dataset
from build_empty_dataset import empty_dataset_builder

lower_case=True

def main( args ):
	if len(args) > 1:
		filename = args[1]
		print('Filename %s '%(filename) )
	else :
		filename = 'tech_spec/2018/FMP.json'
	try:
		if args[2] is not None:
			output_gpkg = args[2]
			if (os.path.basename(output_gpkg)[6:10]).isdigit() :
				year = int(os.path.basename(output_gpkg)[6:10])
				if (os.path.basename(output_gpkg)[6:10]).upper() in [ 'FMP' ]:
					planyear = year
				else :
					planyear = year - 5
	except KeyError :
		if os.path.basename(filename.upper())=='FMP.JSON':
			year = 2020
			planyear = 2020
			output_gpkg= os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),"mu702_2020_FMPDP.gpkg")
		elif os.path.basename(filename.upper())=='AR.JSON':
			year = 2017
			planyear = 2012
			output_gpkg= os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),"mu702_2017_AR.gpkg")
		else :
			year = 2020
			planyear = 2020
			output_gpkg= os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),"mu702_2020_DP.gpkg")

	ds = empty_dataset_builder(jsonfilename=os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),filename),outputfilename=output_gpkg,lower_case=lower_case,year=year,planyear=planyear)

	testid = counter( -1 )

	for tablename in ds.getTableList(): # [ 'ForecastDepletions' , 'PlanningCompositeInventory']:                   # ds.getTableList():
		fid = counter(-1)
		#tablename = 'ForecastDepletions'
		print "Building {layer}".format(layer=tablename)
		good_row = ds.get_sample_feature(tablename)

		if 'POLYID' in good_row.keys():
			row_id_name = 'POLYID'
			row_id_is_string = True
		elif 'PITID' in good_row.keys():
			row_id_name  = 'PITID'
			row_id_is_string = True
		#elif '' in good_row.keys() row_id_name = ''
		else:
			row_id_name = 'FID'
			row_id_is_string = False
		#print row_id_name

		ds.addfield(tablename,'OBJECTID', 'integer')
		ds.addfield(tablename,'TestID', 'integer')
		ds.addfield(tablename,'Perturb')
		ds.addfield(tablename,'Error')
		ds.addfield(tablename,row_id_name, 'string' if row_id_is_string else 'integer' )
		ds.addfield(tablename,'JOIN')

		row = dict(chain( good_row.items(),
			{ row_id_name:	str( fid.preinc() ) if row_id_is_string else fid.preinc() }.items(),
			{ 'JOIN'  : '_'.join([str(fid.v),ds.findTable(tablename)['shortname'] ])  }.items(),
			{ 'OBJECTID'  : fid.v                 }.items(),
			{ 'TestID':  testid.preinc()  }.items(),
			{ 'Error':'Good Row'  }.items()
			) )
		#print (row)
		ds.build_dummy_feature(row,tablename,fid_name='TestID')
		#print ("Completed Sample Row\n====================\n")

		perturbations =  ds.get_perturbations(tablename)

		for f,p in perturbations.iteritems():
			for k,v in p.iteritems():

				row = dict(chain( good_row.items(),
					{ row_id_name:	str( fid.preinc() ) if row_id_is_string else fid.preinc() }.items(),
					#{ 'FID'  : fid.v                }.items(), # Can't set FID Directly
					{ 'JOIN'  : '_'.join([str(fid.v),ds.findTable(tablename)['shortname']] )  }.items(),
					{ 'OBJECTID'  : fid.v                }.items(),
					{ 'TestID':  testid.preinc()  }.items(),
					{ 'Error':str(v)  }.items(),
					{ 'Perturb':str(k)  }.items(),
					eval(k).items()
					) )
				#for f in row.keys(): print ("\t\t\t\tdel row['{0}']".format(f))

				#del row['FID']
				#del row['OBJECTID']
				#del row['TestID']


				#print (row)
				# fidname is used to set the coordinates. TestID is not reset by table
				#       OBJECTID is reset by table
				ds.build_dummy_feature(row,tablename,fid_name='TestID')


if __name__ == "__main__":
	import sys
	sys.exit(main(sys.argv))
