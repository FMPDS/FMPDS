import sys, os
#import multiprocessing
import time

import json
from collections import OrderedDict


#Local Imports:
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from mm_processor.freemind import fm_map
from mm_processor.freemind import fm_node
from mm_processor.freemind import fm_icon
from mm_processor.freemind import parse,parseLiteral,parseEtree,parsexml_,get_root_tag

import mm_processor.validator

from stack.css import htmlStyle, htmlStyle_flat

#Parent Sub Imports: - needed for the spcomp and FMPError
#import inspect
#currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
#parentdir = os.path.dirname(currentdir)
#checkerdir = os.path.join(parentdir,'checker')
#sys.path.insert(0,checkerdir)
sys.path.insert(0,os.path.abspath('..') )
from checker.spcomp import test_spcomp, test_SpeciesInList
from checker.fimdate import fimdate

import checker.jsonloader

doingFoldingSections = False #True
debug = True

class conductor:
	debug=True
	"""Conductor does the work
	"""
	def __init__(self):
		"""Initializes the conductor, setting up appropriate defaults"""
		self.verbose = True
		self.outputfile = ""
		self.inputfile = None
		self.overwrite = False
		self.valid = False
		self.mm2json = True #Alternatively, set this to false and we will go the other way... :-)
		self.json2mm = False
		self.json2html = False
		self.debug = True
		self.json = None
		self.mm = None
		self.html = None
		self.nodename = 'default'

#	def __init__(self,inputfile):
#		self.__init__()
#		self.inputfile = inputfile

	def validateOptions(self):
		"""Check the validity of the options provided."""
		#Check input file validity
		if (self.inputfile == None ):
			raise ValueError("No Input File Provided")


		#Check for file validity
		for n in [self.inputfile]:
			if (not os.path.isfile(n)):
				raise ValueError("Data file does not exist or is not a file:"+n)


		#Check the existence of the output directory
		if (os.path.exists(self.outputfile) and not self.overwrite):
			raise ValueError("Output file exists and won't be overwritten:"+self.outputfile)


	def describeOptions(self):
		"""Describe the options loaded..."""
		print( "Loaded Options:\n\t-o, --outputfile={0}\n\t-i, --inputfile={1}\n\t-w, --overwrite={2}".format(self.outputfile,self.inputfile,str(self.overwrite) ) )

	def Go(self):
		if not self.valid :
			self.validateOptions()

		if self.verbose:
			self.describeOptions()

		starttime = time.clock()
		if self.verbose: print ("Starttime:",starttime)

		#Meat and Potatoes go here...

		if self.mm2json:
			self.do_mm2json()
		elif self.json2mm:
			self.do_json2mm()
		elif self.json2html:
			self.do_json2html()


		endtime = time.clock()
		if self.verbose: print ("Endtime:",endtime)
		if self.verbose: print ("Seconds Elapsed:", endtime - starttime)
		if self.verbose: print ("Minutes Elapsed:", round(( endtime - starttime ) /60,0) )

	def do_mm2json(self):
		print ('Converting MM to JSON')
		#Load MM to a local copy
		self.getmm()

		#Interpret node structure to a list of thingummies (String,int,dict,list)
		self.convert_mm2json()

		#Save json file
		self.putjson()


	def do_json2mm(self):
		print ('Converting JSON to MM')

		# Load json to a local copy
		self.getjson()

		# Generate mindmap nodes
		convert_json2mm()

		# Save mindmap file
		self.putmm()

	def do_json2html(self):
		print ('Converting JSON to HTML')

		#Load json to a local copy
		self.getjson()

		#
		self.makehtml()

	############################################
	# File I/O functions
	############################################

	def getjson(self,process_includes=False):
		if self.json is None:
			json_data=open(self.inputfile)
			self.json = json.load(json_data)
		if process_includes:
			self.convert_mm2json()
			self.do_fields_include()
		return self.json

	def putjson(self,outputfilename = None):
		if self.json is None :
			self.convert_mm2json()

		data = json.JSONEncoder().encode(  self.json )
		if outputfilename is None:
			outputfilename = self.outputfile
		text_file = open(outputfilename, "w")
		text_file.write(json.dumps(self.json, indent=4 ))
		#text_file.write(data)
		text_file.close()

	def putjson_tests_only(self,outputfilename = None):
		if self.json is None :
			self.convert_mm2json()

		rules_set = dict()
		#Use validator...
		for t in self.json:
			if 'fields' in t.keys():
				rules_set[t['shortname']] =  validator.validator(json=self.json, tabletype=t['shortname']).rowvalidation

		data = json.JSONEncoder().encode(  rules_set )
		if outputfilename is None:
			outputfilename = self.outputfile
		text_file = open(outputfilename, "w")
		text_file.write(json.dumps(rules_set, indent=4 ))
		#text_file.write(data)
		text_file.close()

	def getmm(self,process_includes=False):
		if self.mm is None:
			self.mm = parse(self.inputfile, silence=True)
			self.nodename = self.mm.node.TEXT
		if process_includes:
			self.convert_mm2json()
			self.do_fields_include()

	def putmm(self,outputfilename=None):
		if outputfilename is None:
			outputfilename = self.outputfile
		f = open(outputfilename, 'w')
		self.mm.export(f,level=1)
		f.close()

	def puthtml(self, outputfilename = None):
		self.makehtml(outputfilename)

	def convert_mm2json(self):
		if self.json is None and self.mm is not None:
			self.json = self.interpretnodes(self.mm.get_node())

	def convert_json2mm(self):
		if self.mm is None and self.json is not None :
			# Generate mindmap nodes
			self.mm = fm_map(version='1.0.1')
			basenode = fm_node(TEXT = self.nodename if '.mm' in self.inputfile else os.path.basename(self.inputfile)  )
			self.add_node_or_nodes(self.makenode(self.json),basenode)
			self.mm.set_node(basenode)



    ############################################
    # Execute INCLUDE directives
    ############################################
	def do_fields_include(self):
		j = checker.jsonloader.jsonloader()
		j.json = self.json
		j.do_fields_include()
		self.json = j.json
		self.mm = None
		self.convert_json2mm()

	############################################
	# Building MM File functions (json2mm)
	############################################

	def makenode(self,i):
		"""Takes any input and makes a node..."""
		#print "Makenode Processing %s: %s"%(type(i),str(i))
		if isinstance(i,list) :
			n = self.makenodefromlist(i)
		elif isinstance(i,dict):
			n = self.makenodefromdictionary(i)
		elif isinstance(i,str) or isinstance(i,unicode):
			n = self.makenodefromstring(i)
		elif isinstance(i,int) :
			n = self.makenodefromstring(str(i))
		elif isinstance(i,float) :
			n = self.makenodefromstring(str(i))
		elif i is None :
			n = self.makenodefromstring('Null')
		else:
			print (i)
			print (type(i))
			assert(False)
		return n

	def makenodefromstring(self,i):
		#print "makenodefromstring()"
		t = str(self.ununicode(i))
		return fm_node(TEXT = t , FOLDED="true" ) #  , POSITION = p, FOLDED=False

	def makenodefromdictionary(self,i):
		"""Adds elements of dict as nodes"""
		#if self.debug : print "makenodefromdictionary(%s)"%str(i)

		try:
			dnode = fm_node(TEXT = str(i['name'])+":" , FOLDED="true")
		except KeyError :
			if len(i) == 1:
				k = i.keys()[0]
				i = i[k]
				dnode = fm_node(TEXT = str(i['name'])+":" , FOLDED="true")
			else:
				dnode = fm_node(TEXT = "Dict:" , FOLDED="true")

		for k,v in i.iteritems() :
			knode = fm_node(TEXT = k , FOLDED="true")
			self.add_node_or_nodes(self.makenode(v),knode)
			#knode.add_node(self.makenode(v))
			dnode.add_node(knode)
		return dnode

	def makenodefromlist(self,i):
		"""Adds elements of list as nodes"""
		#print "makenodefromlist()"
		#for n in i :
		#	lnode.add_node(self.makenode(n))
		return [ self.makenode(n) for n in i ]

	def add_node_or_nodes(self,i,parent):
		if isinstance(i,list):
			for n in i:
				parent.add_node(n)
		else:
			parent.add_node(i)


	############################################
	# Building HTML File functions (json2html)
	############################################
	def makehtml(self,outputfilename = None):
		"""Starting at the top node for the document title, working
		down through the layers of document to provide useful stuff..."""

		if outputfilename is None:
			outputfilename = self.outputfile

		#FIXME: When there is only one table defined in the mindmap, then
		#self.json contains that table, not the list of tables...
		print ("Creating HTML file %s"%(outputfilename))
		with open (outputfilename, 'w') as f:
			if doingFoldingSections:
				f.write(htmlStyle)
			else:
				f.write(htmlStyle_flat)
			f.write('<title>%s</title>\n'%(self.inputfile[:-5]))
			if self.debug: f.write('<META HTTP-EQUIV="refresh" CONTENT="10">\n')
			f.write('</head><body>\n')

			#Quack test for is self.json a dictionary or is it a list of dict's?
			# A list will make this a TypeError: list indices must be integers, not str
			# A Dictionary will make work, or produce a KeyError
			try:
				testvalue = self.json['name']
			except TypeError as e:
				# self.json is a list
				listOfTables = self.json
			else:
				listOfTables = [ self.json ]


			#Dump the "Document" dictionary - it describes the structure...

			for t in listOfTables:
				#print t['name']
				if t['name'].upper() == 'DOCUMENT':
					#Make a Document structure from this dictionary
					#print t
					self.makehtmlFromDocumentStructure(t,listOfTables,f,headinglevel=1)
					try:
						listOfTables.remove(t)
					except ValueError:
						pass # ignore it if it is already missing...

			# If tables are still present in self.json, then dump them now...
			for t in listOfTables:
				self.makehtmlFromTable(t,f,headingLevel=1)
			f.write ('</body></html>')





	def makehtmlFromDocumentStructure(self,t,listOfTables,f,headinglevel=1):
		"""document contains sections..."""
		#if self.debug: print t
		for h,p in t.iteritems(): # p paragraph   h heading
			#if self.debug: print "\t%sh:p\t%s	%s	%s	%s"%(''.join(['\t' for n in range(0,headinglevel) ]), h, str(type(h)), str(p), str(type(p)))
			if h == 'name':
				pass #continue #Because that would be silly..
			elif h == 'title':
				f.write('<p class="document_title">%s</p>\n'%(p))
				# no further actions on a document title...
			elif h == u'table:':
				headerFields = [ v for k,v  in p.iteritems() if isinstance(v,unicode) or isinstance(v,str) ]
				columnNames  = [ k for k,v  in p.iteritems() if isinstance(v,unicode) or isinstance(v,str) ]
				cells        = [ v for k,v  in p.iteritems() if isinstance(v,dict) or isinstance(v,OrderedDict) ]
				self.makehtmlTableFromlistOfDictionaries(headerFields=headerFields,columnNames=columnNames,cells=cells,f=f)
			elif self.findTablebyNameorShortName(listOfTables,h): #Assume we are including a table description...
				#This allows us to control order despite the best efforts of people in the mindmap... :-)
				#f.write('<h%d>%s</h%d>\n'%(headinglevel,h,headinglevel))
				self.makehtmlFromTable(self.findTablebyNameorShortName(listOfTables,h),f,headingLevel=headinglevel)
				try:
					listOfTables.remove(self.findTablebyNameorShortName(listOfTables,h))
				except ValueError:
					pass # ignore it if it is already missing...
			else : # Treat it as generic text with a heading and body
				if p in [ None, '' , u'']:
					f.write('<p class="body_text">%s</h>\n'%(h))
				elif isinstance(p, dict) or isinstance(p,OrderedDict):
					f.write('<h%d>%s</h%d>\n'%(headinglevel,p['name'],headinglevel))
					self.makehtmlFromDocumentStructure(p,listOfTables,f,headinglevel=headinglevel+1)
				elif isinstance(p,str) or isinstance(p,unicode):
					f.write('<h%d>%s</h%d>\n'%(headinglevel,h,headinglevel))
					f.write('<p class="body_text">%s</h>\n'%(p))
				elif isinstance(p,list):
					f.write('<h%d>%s</h%d>\n'%(headinglevel,h,headinglevel))
					for pp in p:
						f.write('<p class="body_text">%s</p>\n'%(pp))
				else :
					if self.debug: print ("\n!!!!!!!! can't figure out what to do with a %s... %s "%(str(type(p)),p))
					pass



	def findTablebyNameorShortName(self,listOfTables,name):
		try:
			for t in listOfTables:
				#if self.debug: print t['name']
				try:
					if str(t['name'].upper()) == str(name.upper()) :
						#if self.debug: print "Table Found %s by name in %s"%(name,[ n['name'] for n in listOfTables ])
						return t
				except KeyError:
					pass #return None
				try:
					if str(t['shortname'].upper()) == str(name.upper() ):
						#if self.debug: print "Table Found %s by shortname in %s"%(name,[ n['name'] for n in listOfTables ])
						return t
				except KeyError:
					pass
				try:
					if str(t['alias'].upper()) == str(name.upper() ):
						#if self.debug: print "Table Found %s by alias in %s"%(name,[ n['name'] for n in listOfTables ])
						return t
				except KeyError:
					pass
		except AttributeError:
			pass
		#if self.debug: print "Table Not Found %s in %s"%(name,[ n['name'] for n in listOfTables ])
		return None






	def makehtmlFromTable(self,t,f,headingLevel=1):
		#if self.debug: print "T is %s\n\n=================================================\n\n"%(str(t))
		if self.debug: print ("Operating on table %s\n"%(t['name']))
		f.write('<div id="%s" class="foldingSection" >\n'%(t['name']))
		try:
			f.write('	<a href="#hide%s" class="hide" id="hide%s">+</a>\n'%(t['shortname'],t['shortname']))
			f.write('	<a href="#show%s" class="show" id="show%s">-</a>\n'%(t['shortname'],t['shortname']))
		except KeyError as e:
			pass
		try:
			f.write('<div class="foldingtitle"><h%d>%s</h%d></div>\n'%(headingLevel,t['alias'],headingLevel))
		except KeyError as e:
			try:
				f.write('<div class="foldingtitle"><h%d>%s</h%d></div>\n'%(headingLevel,t['name'],headingLevel))
			except KeyError as e:
				print (e)
				f.write('<div class="foldingtitle"><h%d>%s</h%d></div>\n'%(headingLevel,"Table name MISSING!",headingLevel))


		f.write('<div class="foldingcontent">\n')
		########################################
		# Description
		########################################
		self.writeStrings(['description','Description'],t,f,'<h%d>Description</h%d>\n'%(headingLevel+1,headingLevel+1),'p')

		#########################################
		# Table-level Technical details
		#########################################
		f.write('<h%d>Technical Detail</h%d>\n'%(headingLevel+1,headingLevel+1))
		f.write('<ul>\n')
		tdetails = OrderedDict({'shortname':'Short Name', 'name':'Long Name', 'geometry_type':'Geometry Type'})
		for k,v in tdetails.iteritems():
			self.writeStrings([k],t,f,title='<li>%s '%(v),tag='em',foot='</li>')

		try:
			self.writeStrings(['english'],t['Validation:'],f,title='<li>Validation\n<ul>\n',tag='li',foot='</ul>\n</li>')
		except KeyError as e:
			#print "Can't find any validation..."
			pass
		f.write('</ul>\n')

		##############################################
		# Fields
		##############################################
		#if self.debug: print "T is %s"%(str(t))
		try: #When fields are not present, ignore...
			if isinstance(t['fields'],dict):
				fieldlist = [ t['fields'] ]
			else:
				fieldlist = t['fields']

			f.write('<h%d>Fields</h%d>\n'%(headingLevel+1,headingLevel+1))
			for a in fieldlist: # a for attribute, since f is taken
				try:
					if a['alias'] != a['name']:
						f.write('<h%d>%s (%s)</h%d>\n'%(headingLevel+2,a['name'],a['alias'],headingLevel+2))
					else:
						f.write('<h%d>%s</h%d>\n'%(headingLevel+2,a['alias'],headingLevel+2))
				except KeyError as e:
					f.write('<h%d>%s</h%d>\n'%(headingLevel+2,a['name'],headingLevel+2))
				except TypeError as e:
					print (e)
					print ("%s: %s"%(t['name'],str(a)))
				except AttributeError as e:
					print ("Error in Table %s, Field %s, values key"%(t['name'],a['name']))

				#f.write( "Field Dictionary: %s"%(a.keys()))
				########################################
				# Description
				########################################
				try:
					self.writeStrings(['description','Description'],a,f,'<h%d>Description</h%d>\n'%(headingLevel+3,headingLevel+3),'p')
				except AttributeError :
					#This didn't work because there is no description?
					pass
				try:
					self.writeStrings(['definition','Definition'],a,f,'<h%d>Definition</h%d>\n'%(headingLevel+3,headingLevel+3),'p')
				except AttributeError :
					#This didn't work because there is no description?
					pass


				#########################################
				# Field-level Technical details
				#########################################
				f.write('<h%d>Technical Detail</h%d>\n'%(headingLevel+3,headingLevel+3))
				f.write('<ul>\n')
				tdetails = OrderedDict({'type':'Field Type', 'name':'Name', 'length':"Length"})
				for k,v in tdetails.iteritems():
					try:
						self.writeStrings([k],a,f,title='<li>%s '%(v),tag='em',foot='</li>')
					except KeyError as e:
						pass
					except AttributeError as e:
						try:
							print ("Error in Table %s, Field %s, techdetails keys"%(t['name'],a['name']))
						except TypeError:
							pass


				try:
					fieldvalidations = a['mandatory']
					f.write('<li>Mandatory Requirements<ul>\n')
					self.writelistOfStrings(self.makeListFromStringOrList(fieldvalidations),f,title='',tag='li',foot='')
					f.write('</ul></li>\n')
				except KeyError as e:
					#print "Can't find any validation..."
					pass
				except AttributeError as e:
					print ("Error in Table %s, Field %s, mandatory key"%(t['name'],a['name']))
				except TypeError as e:
					pass

				try:
					fieldvalidations = a['Validation:']
					f.write('<li>Validation<ul>\n')
					self.writeStrings(['english'],fieldvalidations,f,'','li')
					f.write('</ul></li>\n')
				except KeyError as e:
					#print "Can't find any validation..."
					pass
				except AttributeError as e:
					print ("Error in Table %s, Field %s, Validation key"%(t['name'],a['name']))
				except TypeError as e:
					pass
				f.write('</ul>\n')

				#############################################
				# Acceptable Values Table
				#############################################
				try:
					v = a['values']
					if v:
						f.write("<h%d>Acceptable Values</h%d>\n"%(headingLevel+3,headingLevel+3))
						f.write("<table>\n")
						f.write("<tr><th>Value</th><th>Option</th><th>Description</th></tr>\n")
						for r in v:
							f.write("<tr>")
							self.writeStrings(['name'],r,f,'','td')
							self.writeStrings(['option'],r,f,'','td')
							self.writeStrings(['desctext'],r,f,'<td>','p','</td>')
							f.write("</tr>\n")
						f.write("</table>\n")
				except KeyError as e:
					pass
				except AttributeError as e:
					print ("Error in Table %s, Field %s, values key"%(t['name'],a['name']))
					#print str(e)
				except TypeError:
					pass
		except KeyError as e: #No fields are present?
			pass
		except TypeError:
			pass

		#And close the div...
		f.write('</div><!-- class foldingcontent-->\n<br>')

		f.write('</div><!-- id="%s"-->\n<br>\n\n'%(t['name']))

	def makehtmlTableFromlistOfDictionaries(self,headerFields=["Robert",'Musa acuminata'],columnNames=["Bob",'Bananas'],cells=[{ "Bob":"T Builder",'Bananas':"Yellow" }],f=None):
		try:
			v = cells
			if v:
				#f.write("<h%d>%s</h%d>\n"%(headingLevel,name,headingLevel))
				f.write("<table>\n")
				#f.write("<tr><td>Value</td><td>Option</td><td>Description</td></tr>\n")
				self.writelistOfStrings(lParagraphs=headerFields,f=f,title='<tr>',tag='th',foot='</tr>')
				for r in v:
					f.write("<tr>\n")
					f.write( "\n".join([ '<td>%s</td>'%k if isinstance(k,unicode) else '<td><p>%s</p></td>'%('</p><p>'.join(k)) for k in [ r[c] for c in columnNames ] ] ))
					#self.writeStrings(listOfKeys=columnNames, dictOfValues=r,f=f,title='<td>',tag='p',foot='</td>')
					f.write("</tr>\n")
				f.write("</table>\n")
		except KeyError as e:
			pass
		except AttributeError as e:
			print (str(e))

	def makelistfromstringOrListOfStrings(self,listOfKeys,dictOfValues):
		# Get the values from the dictionary into a list
		listOfResponses = []
		keys=dictOfValues.keys()
		for k in listOfKeys:
			if k in keys:
				v = dictOfValues[k]
				if v:
					if isinstance(v,unicode) or isinstance(v,str) or isinstance(v,int) or isinstance(v,float) and unicode(v)!= unicode(k) :
						listOfResponses.append(self.ununicode(v))
					else:
						listOfResponses.extend([self.ununicode(uv) for uv in v if unicode(uv)!= unicode(k) ] )
				else:
					listOfResponses.append('')
		return listOfResponses

	def makeListFromStringOrList(self,listOrString):
		v = listOrString
		listOfResponses = []
		if isinstance(v,unicode) or isinstance(v,str) or isinstance(v,int) or isinstance(v,float) and unicode(v)!= unicode(k) :
			listOfResponses.append(self.ununicode(v))
		else:
			listOfResponses.extend([self.ununicode(uv) for uv in v ] )
		return listOfResponses

	def writelistOfStrings(self,lParagraphs,f,title='',tag='',foot=''):
		if len(lParagraphs) > 0:
			f.write(title)
			if tag is not None:
				if isinstance(tag,str) or isinstance(tag,unicode):
					begintag = '<%s>'%(tag)
					if ' ' in tag:
						endtag   = '</%s>'%(tag.split(' ')[0])
					else:
						endtag = '</%s>'%(tag)
				elif isinstance(tag,list):
					begintag = '<%s>'%('><'.join( tag ) )
					endtag = '</%s>'%('></'.join( reversed(tag) ) )
			else:
				begintag = ''
				endtag = ''
			for p in lParagraphs:
				f.write('%s%s%s'%(begintag,p,endtag))
			f.write(foot)
			f.write('\n')

	#self.writelistOfStrings(self.makelistfromstringOrListOfStrings(['option'],r),f,'','td')
	def writeStrings(self,listOfKeys,dictOfValues,f,title='',tag='',foot=''):
		self.writelistOfStrings(self.makelistfromstringOrListOfStrings(listOfKeys,dictOfValues),f,title,tag,foot)

	############################################
	# Iterate through nodes to identify structures (mm2json)
	############################################
	def interpretnodes(self, n, inADict=False):
		#is n a list of nodes? Or is it an fm_node?
		#if self.debug : print "interpretnodes type: %s "%(type(n)),
		if isinstance(n,fm_node):
			#if self.debug : print "Node TEXT: %s"%(self.ununicode(n.TEXT))
			pass
		elif isinstance(n,str):
			#if self.debug : print "String: %s"%(n)
			pass
		elif isinstance(n,OrderedDict) or isinstance(n,dict):
			#if self.debug : print "Dict: %s"%(n)
			pass
		elif isinstance(n,list): #passed a list of nodes...
			#if self.debug : print "List: %s"%(n)
			if len(n) == 1:
				return self.interpretnodes(n[0])
			else:
				return [ self.interpretnodes(i) for i in n ]
		# So I guess it is an fm_node
		#print n.TEXT

		#is this node a leaf?
		if not n.node: #Cribbed from hasContent
			return self.nodeasleaf(n)

		#is this node a dictionary?
		#Dictionaries TEXT end with :
		#Children in dictionaries have children
		if n.TEXT[-1:] == ":" :    # and n.node[0].node:
			#if self.debug : print "interpretnodes() Found a dictionary"
			return self.nodeasdict(n)

			return [ self.interpretnodes(i) for i in n ] #FIXME: We can never get to this line...

		#n must be a list node...
		#if self.debug : print "interpretnodes() Found a list ",
		if len(n.node) == 1:
			if not n.node[0].node: #it is a leaf node
				#if self.debug : print "Containing a leaf"
				return self.nodeasleaf(n.node[0])
			else:
				#if self.debug : print "Containing only one element"
				return [self.interpretnodes(n.node[0])]
		else:
			#if self.debug : print "Containing %s elements"%(len(n.node))
			return [ self.interpretnodes(i) for i in n.node ]
			#return self.nodeaslist(n)

	def nodeasleaf(self, n):
		"""Returns a int, float, or string version of the node contents"""
		# this node is a leaf! (int / float / string)
		#print n.TEXT
		try:
			o = int(n.TEXT)
		except TypeError: # n.TEXT == None, for example - this happens when no value was present in a key value pair...
			o = ' '
		except ValueError:
			try:
				o = float(n.TEXT)
			except ValueError:
				o = n.TEXT
		#if self.debug : print "nodeasleaf Returning %s"%(self.ununicode(o))
		return o

	def nodeasdict(self,n):
		"""Dictionary nodes contain key,value pairs.
		The key is the text of the node, the value is a child
		of the sub-node, which may be a """
		d = OrderedDict()
		d['name'] = n.TEXT[:-1]
		for i in n.node:
			#if i is a leaf, then store a key,null kv pair
			if not i.node:
				d[i.TEXT] = None
			#print i.TEXT
			else:
				d[i.TEXT] = self.interpretnodes(i)

		#if self.debug : print "nodeasdict returning %s"%(str(d))
		return d

	def ununicode(self,i):
		return unicode(i).replace(u'\u201c','"').\
			replace(u'\u201d','"').\
			replace(u'\xa0',' ').\
			replace(u'\u2018',"'").\
			replace(u'\u2019',"'").\
			replace(u'\u2013',"-").\
			replace(u'\u2260',"!=")

	############################################
	# Iterate through nodes to identify structures (cleaning)
	############################################
	def iteratethroughnodes(self,n=None,callback=None):

		#if self.debug: print "iteratethroughnodes()"

		if n is None:
			n=self.mm.get_node()

		if callback is None:
			callback=self.nodeasleaf

		if isinstance(n,fm_node):
#			if self.debug : print "DEBUG Node TEXT: %s"%(self.ununicode(n.TEXT))
			pass

		#is this node a leaf?
		#if not isinstance(n,list):
		#	# What does this mean, anyway?
		#	if self.debug : print "DEBUG Node TEXT: %s"%(str([self.ununicode(nn) for nn in n]) )
		if not n.node: #Cribbed from hasContent
			#if self.debug: print "iteratethroughnodes() - Leaf"
			callback(n)
		else:
			#if self.debug: print "iteratethroughnodes() - Node"
			callback(n)
			for i in n.node:
				self.iteratethroughnodes(i,callback)

	############################################
	# Tester reads each validation section for unit tests
	# - Read unit tests at self.json[table][field]['validation']['test:']
	# - load tfvt[n].TEXT for the row to test
	#	- execute code fragment into "row"
	#	- execute tvft[n].TEXT.result into "result"
	#	- execute tfv[python][n] into resultant list
	#   - compare result with TEXT.result
	#	- if they do not match, report
	############################################
	def run_tester(self,table=None,field=None):
		if self.mm is None :
			self.getmm()
		elif self.json is None :
			self.convert_mm2json()

		if table is not None:
			listOfTables = [ t for t in self.json if t['name'] == table ]
		else :
			listOfTables = self.json

		for t in listOfTables:
			print ("Testing Table %s"%(t['name']))
			try:
				t['fields']
			except KeyError as e:
				print ("Not (Apparently) a table...")
				continue

			parameters = { 'year': '2017' , 'planyear': '2017'}

			val = validator.validator(json=self.json,tabletype=t['name'],year=parameters['year'],planyear=parameters['planyear'],debug=True )
			val.doTypeTest()


			for f in t['fields'] :
				if field is not None:
					if f['name'] != field : continue

				for q,v in val.getTests(f['name']).iteritems() :  # query and target, really
					#if isinstance(v,str) or isinstance(v,unicode): v=[v]
					row = eval(q)
					result = val.doRowValidations(row ,f['name'])

					if (len(result) > 0 and v == '' ) :
						print ("!!!!!!!!!_Expected No Error, got error\nBlank string expected, got non blank\nRow is %s"%(row))
						print ("Test Failed: %s==>%s\n\t\tExpect : %s\n\t\tProduce: %s"%(t['name'],f['name'], v, result ))
					elif (len(result) == 0 and v == '' ) :
						pass  #No result expected or returned... Perfect
					elif (len(result) == 0 and v != '' ) :
						print ("!!!!!!!!!!! no result and compared against a non-empty string\nRow is %s"%(row))
						print ("Test Failed: {table}==>{field} \n\t\tExpect :{expected}\n\t\tProduce:{observed}".format(table=t['name'],field=f['name'], expected=v, observed=result ) )
					else:
						if isinstance(v,list):
							v = str(sorted(v))
						if isinstance(result,list):
							if len(result) == 1 :
								z=result[0]
							else:
								z = str( sorted( result ) )
						else :
							z=result
						if z != v :
							#print "!!!!!!!!!!!expected and observed different???\n\nRow is %s"%(row)
							print ("Test Failed: {table}==>{field}\n\t\tExpect :{expected}\n\t\tProduce:{observed}".format(table=t['name'],field=f['name'], expected=v, observed=z ) )
						#else: print("!!!!!!!!!!!!\nI think the testing for {f} worked".format(f=f['name']))


if __name__ == "__main__":
	#main(sys.argv[1:])
	print ("\n\n\nAttempting mm2json")
	cond = conductor()
	cond.debug = False
	cond.inputfile = r'S:\ROD\Northwestregion\RIAU\Data\Extended_Data\Forest\Projects\ePCM\test_m2j.mm'
	#cond.inputfile = r'S:\ROD\Northwestregion\RIAU\Data\Extended_Data\Forest\Projects\ePCM\workingFieldDefinitions.mm'
	#cond.inputfile = r'w:/ePCM/workingFieldDefinitions.mm'
	cond.outputfile = 'test_m2j.json'
	cond.mm2json = True
	cond.overwrite = True
	cond.verbose = True
	cond.Go()

	print ("\n\n\nAttempting json2mm")
	cond2 = conductor()
	cond2.debug = False
	cond2.inputfile = 'test_m2j.json'
	cond2.outputfile = 'roundtrip.mm'
	cond2.mm2json = False
	cond2.json2mm = True
	cond2.overwrite = True
	cond2.verbose = True
	cond2.Go()

	print ("\n\n\nAttempting json2html")
	cond3=conductor()
	cond3.debug = True
	cond3.inputfile = 'test_m2j.json'
	cond3.outputfile = 'roundtrip.html'
	cond3.mm2json = False
	cond3.json2mm = False
	cond3.json2html = True
	cond3.overwrite = True
	cond3.verbose = True
	cond3.Go()


