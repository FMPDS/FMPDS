#!/usr/bin/python

import os
import sys
import time

import getopt

import sys
#sys.path.insert(0, '..')
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)))

try:
	from mm_processor import mm_cleaner
except ImportError as e:
	import mm_cleaner

ACTIONS = {
	1 : "Created",
	2 : "Deleted",
	3 : "Updated",
	4 : "Renamed from something",
	5 : "Renamed to something"
}

file_stutter_time = 15

file_time_hash = {}
try:
	import win32file
	import win32con
	class Watcher():

		def __init__(self,path_to_watch,command,extension_to_watch):
			self.path_to_watch=path_to_watch
			self.command = command
			self.extension_to_watch = extension_to_watch

		def run( self, path_to_watch=None , extension_to_watch=None):
			# Thanks to Claudio Grondi for the correct set of numbers
			FILE_LIST_DIRECTORY = 0x0001

			outputdir = None # r"Z:\src\FMPDS" # Or None for same as input
			#outputdir = os.path.join(os.path.split(os.path.abspath(os.path.dirname(sys.argv[0])))[0] , 'tech_spec', '2009'  )

			if path_to_watch is None:
				path_to_watch = self.path_to_watch

			if extension_to_watch is None:
				extension_to_watch = self.extension_to_watch


			print path_to_watch
			print outputdir

			hDir = win32file.CreateFile (
				path_to_watch,
				FILE_LIST_DIRECTORY,
				win32con.FILE_SHARE_READ | win32con.FILE_SHARE_WRITE | win32con.FILE_SHARE_DELETE,
				None,
				win32con.OPEN_EXISTING,
				win32con.FILE_FLAG_BACKUP_SEMANTICS,
				None
			)

			file_time_hash = dict()
			while 1:
				#
				# ReadDirectoryChangesW takes a previously-created
				# handle to a directory, a buffer size for results,
				# a flag to indicate whether to watch subtrees and
				# a filter of what changes to notify.
				#
				# NB Tim Juchcinski reports that he needed to up
				# the buffer size to be sure of picking up all
				# events when a large number of files were
				# deleted at once.
				#
				results = win32file.ReadDirectoryChangesW (
					hDir,
					1024,
					True,
					win32con.FILE_NOTIFY_CHANGE_FILE_NAME |
					 win32con.FILE_NOTIFY_CHANGE_DIR_NAME |
					 win32con.FILE_NOTIFY_CHANGE_ATTRIBUTES |
					 win32con.FILE_NOTIFY_CHANGE_SIZE |
					 win32con.FILE_NOTIFY_CHANGE_LAST_WRITE |
					 win32con.FILE_NOTIFY_CHANGE_SECURITY,
					None,
					None
				)
				for action, file in results:
					full_filename = os.path.join (path_to_watch, file)
					#print full_filename, ACTIONS.get (action, "Unknown")
					do_the_command(full_filename,action,target_extension=extension_to_watch, command=self.command)

except ImportError:
	from watchdog.observers import Observer
	from watchdog.events import FileSystemEventHandler
	# Linux version...
	# sudo apt-get install python-watchdog
	file_stutter_time = 1
	class Watcher:
		DIRECTORY_TO_WATCH = "/path/to/my/directory"

		def __init__(self,path_to_watch=DIRECTORY_TO_WATCH,command='tester_roundtrip(inputfile,inputfile)', target_extension='.mm'):
			self.DIRECTORY_TO_WATCH=path_to_watch
			self.command = command
			self.observer = Observer()


		def run(self):
			print "Watching %s"%(self.DIRECTORY_TO_WATCH)
			event_handler = Handler()
			self.observer.schedule(event_handler, self.DIRECTORY_TO_WATCH, recursive=True)
			self.observer.start()
			try:
				while True:
					time.sleep(5)
			except:
				self.observer.stop()
				print "Error"

			self.observer.join()


	class Handler(FileSystemEventHandler):

		@staticmethod
		def on_any_event(event):
			if event.is_directory:
				return None

			elif event.event_type == 'created':
				# Take any action here when a file is first created.
				print "Received created event - %s." % event.src_path
				do_the_command(event.src_path,1,target_extension='.mm', command='tester_roundtrip(inputfile,inputfile)')

			elif event.event_type == 'modified':
				# Taken any action here when a file is modified.
				print "Received modified event - %s." % event.src_path
				do_the_command(event.src_path,3,target_extension='.mm', command='tester_roundtrip(inputfile,inputfile)')

def tester_roundtrip(inputfile,outputfile):
	if outputfile is None:
		mm_cleaner.main(['-i', inputfile, '-t' ])
	else:
		mm_cleaner.main(['-i', inputfile, '-o', outputfile, '-m', '-t', '-w', '-f' ])

def do_the_command(file,action=None,target_extension='.mm', command='tester_roundtrip( file, file)'):
	#print "Operating on %s, ext=%s"%(file,file[-3:])
	if (file[-3:] != target_extension) : return None
	#elif file[-12:] == 'roundtrip.mm': continue

	#Sleep a bit to make sure the file is written...
	time.sleep(2)

	inputfile =  file
	try:
		do_roundtrip = (time.clock() - file_time_hash[inputfile]) > file_stutter_time  #15 ??? Is this in seconds (Win) or minutes (Linux?)
	except KeyError as e:
		do_roundtrip = True

	try:
		#run_a_roundtrip(os.path.join (path_to_watch, file),outputdir)
		if do_roundtrip :
			#tester_roundtrip(inputfile,inputfile)
			eval(command)
			file_time_hash[inputfile]= time.clock()
	except AttributeError as e:
		print "Something bad happened:: %s\n"%(e)
	except KeyboardInterrupt as e:
		print "%s\n"%(e)
		exit()
	except Exception as e:
		print "Something bad happened: %s\n"%(e)


def main(argv):
	try:
		opts, args = getopt.getopt(argv, "vi:c:e:t",
								   ["help", "verbose",
									"inputdir=","command=","extension=","testonly"
									])
	except getopt.GetoptError:
		pass
		usage()
		sys.exit(2)

	path_to_watch =  os.path.join(os.path.split(os.path.abspath(os.path.dirname(sys.argv[0])))[0] , 'tech_spec'  )
	command=None
	testonly = False
	extension_to_watch='.mm'

	for opt, arg in opts:
		if opt in ["--help"]:
			usage()
			sys.exit()
		elif opt in ["-v", "--verbose"]:
			verbose = True
		elif opt in ["-c", "--command"]:
			command = arg
		elif opt in ["-i", "--inputfile"]:
			path_to_watch=arg
		elif opt in ["-e", "--extension"]:
			extension_to_watch=arg
		elif opt in ["-t", "--testonly"]:
			testonly = True

	if command is None:
		if testonly:
			command='tester_roundtrip(file, None)'
		else:
			command='tester_roundtrip(file, file)'

	w = Watcher(path_to_watch,command,extension_to_watch)
	w.run()



def usage():
	print ' Try: python fswatcher_roundtrip.py -i "../tech_spec" -c "tester_roundtrip(file,file)" -e ".mm" -t '
	print "RTFM"

if __name__ == '__main__':
	#do_the_command(r'../tech_spec/2017/eFRI.mm',target_extension='.mm',command='tester_roundtrip(file, file)')

	#path_to_watch = r"S:\ROD\Northwestregion\RIAU\Data\Extended_Data\Forest\Projects\ePCM"#r"C:\Users\carran\Documents\src\ePCM"
	#w = Watcher('/home/angus/src/FMPDS/tech_spec/2017',target_extension='.mm', command='tester_roundtrip(file,file)')
	#w.run()

	#path_to_watch =  os.path.join(os.path.split(os.path.abspath(os.path.dirname(sys.argv[0])))[0] , 'tech_spec', '2009'  )
	import sys
	sys.exit(main(sys.argv[1:]))



