#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  increment_functions.py
#
#  Copyright 2018 CARRAN <CARRAN@ON34C02585177>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
#i = 0

def PreIncrement(name, local={}):
    #Equivalent to ++name
    if name in local:
        local[name]+=1
        return local[name]
    globals()[name]+=1
    return globals()[name]

def PostIncrement(name, local={}):
    #Equivalent to name++
    if name in local:
        local[name]+=1
        return local[name]-1
    globals()[name]+=1
    return globals()[name]-1

def PreDecrement(name, local={}):
    #Equivalent to --name
    if name in local:
        local[name]-=1
        return local[name]
    globals()[name]-=1
    return globals()[name]

def PostDecrement(name, local={}):
    #Equivalent to name--
    if name in local:
        local[name]-=1
        return local[name]+1
    globals()[name]-=1
    return globals()[name]+1

def main(args):
	i = 0
	print i
	print "\t++{i}: \t{plusplusi}".format(i=i,plusplusi=PreIncrement('i', local=locals()))
	print "\t++{i}: \t{plusplusi}".format(i=i,plusplusi=PreIncrement('i', local=locals()))
	print "\t++{i}: \t{plusplusi}".format(i=i,plusplusi=PreIncrement('i', local=locals()))
	print "\t++{i}: \t{plusplusi}".format(i=i,plusplusi=PreIncrement('i', local=locals()))
	print "\t++{i}: \t{plusplusi}".format(i=i,plusplusi=PreIncrement('i', local=locals()))
	print "\t++{i}: \t{plusplusi}".format(i=i,plusplusi=PreIncrement('i', local=locals()))


	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
