#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  validator.py
#
#  Copyright 2018 Angus Carr <angus@neva>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#


import sys
import time
import json
import os
from collections import OrderedDict

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from checker.jsonloader import jsonloader
from stack.fileloader import fileloader
#from stack.html_functions import writeHTMLstring, writeHTML, write2File
from stack.log_functions import write2LogFile

from checker.spcomp import test_spcomp, test_SpeciesInList
from checker.fimdate import fimdate

from stack.pg_db import pg_db


import checker.filechecker

class validator(checker.filechecker.tablechecker):
	"""This class exists to use the checker routines directly without loading an actual table...
	Load a mindmap file, then get row validations
	I suppose we could do the validation within this class, but I am not sure that's really what we want
	We'll see...
	"""
	json = ''
	tablejson = ''
	rowvalidation = dict()
	shapefile = None
	debug = False
	resultsdetectordictionary = {}
	tablevalidationfailures = []

	def __init__(self,json=None,jsonfilename=None,tabletype=None,year=0,planyear=0,debug=False):
		if json: self.json = json
		elif jsonfilename: self.getjson(jsonfilename)

		if tabletype:
			self.tablejson = self.findTable(tabletype)
		self.year = year
		self.planyear = planyear
		self.debug = debug

		self.LoadRowValidations()

	def doRowValidations(self,row,field):
		#FIXME- What is the one-liner of this?
		results = [ ]
		for test in self.rowvalidation[field]:
			#print "Doing Row Validations: %s on %s"%(test,str(row))
			#if test=='name':continue
			if 'POLYID' in row.keys():
				if row['POLYID'] is None:
					row['POLYID'] = "-999"
			else:
				row['POLYID'] = "-999"
			if test == """['ST1: <b><mark>POLYID</mark></b> must not be blank or null...' for row in [row] if row['POLYID'] in [ None, '', ' ', 0 ] ]""" :
				results.extend([ 'ST1: <b><mark>POLYID</mark></b> must not be blank or null...' ])
			else:
				results.extend(self.doASingleRowValidation(test,row))
		return results

	def getTests(self,fieldname):
		t = self.tablejson
		for f in t['fields'] :
			if fieldname is not None:
				if f['name'] != fieldname : continue
			#Get the tests...
			try:
				if u'validation:' in f.keys() :
					validations = f['validation:']
				elif u'Validation:' in f.keys() :
					validations = f['Validation:']
				else :
					#print "Validation not found in %s ==> %s "%(f['name'],f.keys())
					continue
			except AttributeError as e:
				print (f)
				print ("Test Failed: %s==>%s - field (?)  parsed as a list - add a blank leaf node to first validation subnode (English) to force validations to be a dictionary\n"%(t['name'],field ))
				raise e


			#print "%s Validation Keys==> %s"%(f['name'],validations.keys())
			try:
				if u'test:' not in validations.keys():
					print ("Tests Missing: %s==>%s - No tests present - using default NULL Test { '{""${name}"":None}': '' } \n"%(self.tablejson['name'],f['name'] ))
					tests = { '{"${name}":None}': '' }
				elif len(validations['test:']) == 0 :
					tests = { '{"${name}":None}': '' }
				else:
					tests = validations['test:']
			except AttributeError as e:
				print ("Test Failed: %s==>%s - validations parsed as a list - add a blank leaf node to first validation subnode (English) to force validations to be a dictionary\n"%(t['name'],f['name'] ))
				raise e
			except TypeError as e:
				print ("Test Failed: %s==>%s - test: dictionary empty"%(t['name'],f['name'] ))
				tests = { '{}': '' }

			#if tests:	print "\t%s Tests==> %s"%(f['name'],tests )

			t = [ 	(
						(self.do_variable_substitutions(q,fieldname)),
						(self.do_variable_substitutions(str(sorted([ str(vv) for vv in v] )),fieldname) if isinstance(v,list) else self.do_variable_substitutions(v,fieldname)  )
					)
					for q,v in tests.iteritems() if q != 'name' ]
			#print fieldname
			#print t
			return dict(t)
		return dict()


	def doTypeTest(self,fieldname=None):
		t=self.tablejson
		for f in self.tablejson['fields'] :
			if fieldname is not None:
				if f['name'] != fieldname : continue
			#Test for all fields being properly defined for AR_Importer...
			try:
				if (f['type']).upper() not in [ 'TEXT', 'FLOAT', 'DOUBLE', 'SHORT', 'LONG', 'DATE', 'BLOB', 'RASTER', 'GUID', 'INTEGER', 'STRING' , 'CHARACTER'] :
					print ("Field Type Failed: %s==>%s ==>%s"%(t['name'],f['name'], f['type'] ))
			except KeyError:
				pass

	def dumpRowValidations(self,onlyField = None):
		for fieldname,valstatements in self.rowvalidation.iteritems():
			if onlyField in [ None, fieldname ]:
				print ("table-field=> %s-%s ==> valstatements => \n%s\n\t"%(self.tablejson['name'],fieldname,"\n\t".join(valstatements)))

#	def getRowValidations(self):
#		return  self.rowvalidation # { field: [row validation strings]  }


	#You probably want to doASingleRowValidation(test,row)

def main(args):
	v = validator(jsonfilename=r'../tech_spec/2017/BMI.json',tabletype='BMI',year=2020, planyear=2020, debug=True)
	print ([ v.findTable(t)['fields'][1]['name'] for t in  v.getTableList() ] )
	print ([ v.findTable(t)['fields'][1]['Validation:']['test:'] for t in  v.getTableList() ] )

	fieldname = "OSPCOMP"

	print ("___ Dump Row Validations ___")
	print (v.dumpRowValidations(fieldname))

	print ("___ get Tests ___")
	print (v.getTests(fieldname))

	print ("___ do Row Validations ___")
	for q in v.getTests(fieldname).keys():
		print (q)
		print (v.getTests(fieldname)[q])
		print (eval(q))
		print ("...")
		print (v.doRowValidations(eval(q),fieldname))
	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
