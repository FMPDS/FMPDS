@echo off
SETLOCAL
REM This file is intended to identify likely locations of osgeo4w
REM at runtime

if exist "C:\Program Files\QGIS 3.8\osgeo4w.bat" (
set OSGEO4W_ENV="C:\Program Files\QGIS 3.8\osgeo4w.bat"
) else if exist "C:\Program Files\QGIS 3.6\osgeo4w.bat" (
set OSGEO4W_ENV="C:\Program Files\QGIS 3.6\osgeo4w.bat"
) else if exist "C:\Program Files\QGIS 3.4\osgeo4w.bat" (
set OSGEO4W_ENV="C:\Program Files\QGIS 3.4\osgeo4w.bat"
) else if exist "C:\Program Files\QGIS 3.2\osgeo4w.bat" (
set OSGEO4W_ENV="C:\Program Files\QGIS 3.2\osgeo4w.bat"
) else if exist "C:\Program Files (x86)\QGIS 3.2\osgeo4w.bat" (
set OSGEO4W_ENV="C:\Program Files (x86)\QGIS 3.2\osgeo4w.bat"
) else if exist "C:\Program Files\QGIS 3.0\osgeo4w.bat" (
set OSGEO4W_ENV="C:\Program Files\QGIS 3.0\osgeo4w.bat"
) else if exist "C:\Program Files (x86)\QGIS 3.0\osgeo4w.bat" (
set OSGEO4W_ENV="C:\Program Files (x86)\QGIS 3.0\osgeo4w.bat"
) else if exist "D:\OSGeo4w\OSGeo4W.bat" (
set OSGEO4W_ENV="D:\OSGeo4w\OSGeo4W.bat"
) else if exist "C:\OSGeo4W\osgeo4w.bat" (
set OSGEO4W_ENV="C:\OSGeo4W\osgeo4w.bat"
) else if exist "C:\OSGeo4W64\osgeo4w.bat" (
set OSGEO4W_ENV="C:\OSGeo4W64\osgeo4w.bat"
) else if exist "C:\Program Files\QGIS 2.18\osgeo4w.bat" (
set OSGEO4W_ENV="C:\Program Files\QGIS 2.18\osgeo4w.bat"
)


echo %OSGEO4W_ENV% %*
echo "%~dp%OSGEO4W_ENV%bin\o4w_env.bat"
call "%~dp%OSGEO4W_ENV%bin\o4w_env.bat"


rem List available o4w programs
rem but only if osgeo4w called without parameters
@echo on
@if [%1]==[] (
	echo "OSGeo (%%OSGEO4W_ENV%%) is %OSGEO4W_ENV%" & cmd.exe /k
) else (
	cmd /c "%*"
)


ENDLOCAL

TIMEOUT /T 10

EXIT /B 0
