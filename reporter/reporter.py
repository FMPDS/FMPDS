#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  mm_cleaner.py
#
#  Copyright 2017 CARRAN <CARRAN@ON34C02585177>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

# Remove the elements from mindmaps that are unhelpful... Folded=, and all the dates

#test with 	python reporter.py -i /home/angus/GIS/20180223_OUA/Course_Map.mm -o test.html -h
#			python reporter.py -i C:\GIS_Sync\20180223_OUA\Course_Map.mm -o test.html -h

import os
import sys
import getopt

from collections import OrderedDict

import sys
sys.path.insert(0, '..')

#Local Imports:
from mm_processor.freemind import fm_map
from mm_processor.freemind import fm_node
from mm_processor.freemind import fm_icon
from mm_processor.freemind import parse,parseLiteral,parseEtree,parsexml_,get_root_tag

from mm_processor.conductor import conductor
c = conductor()

from tokenize import do_backtick
import psycopg2
from stack.pg_db import pg_db
p = pg_db(host='localhost',port=5433, dbname='angus',password='banana123')

parmD = dict()
parmD['ID_travelled'] = list()
parmD['page'] = list()


def main(argv):

	# Load the mindmap
	#print "Hello, World!"

	#print argv

	cond = conductor()

	#for arg in argv:
	#	print arg

	try:
		opts, args = getopt.getopt(argv, "vo:i:wmjhtf",
								   ["help", "verbose","outputfile=",
									"inputfile=","overwrite","mindmap","json","html","test","folding"
									])
	except getopt.GetoptError:
		pass
		usage()
		sys.exit(2)

	#Set defaults for conductor:
	cond.debug = False
	cond.inputfile = r'C:\GIS_Sync\20180223_OUA\Course_Map.mm' #'/home/angus/GIS/20180223_OUA/Course_Map.mm'
	cond.outputfile = 'clean.mm'
	cond.overwrite = False
	cond.verbose = True

	# Local options for convenience
	do_mindmap = False
	global do_folding
	do_folding = False
	do_json = False
	do_html = True
	do_tester = False

	for opt, arg in opts:
		if opt in ["--help"]:
			usage()
			sys.exit()
		elif opt in ["-v", "--verbose"]:
			cond.verbose = True
		elif opt in ["-o", "--outputfile"]:
			cond.outputfile = arg
		elif opt in ["-i", "--inputfile"]:
			print 'Using -i %s'%(arg)
			cond.inputfile = arg
		elif opt in ["-w", "--overwrite"]:
			cond.overwrite = True
		elif opt in ["-m", "--mindmap" ]:
			do_mindmap = True
		elif opt in ["-j", "--json"]:
			do_json = True
		elif opt in ["-h", "--html"]:
			do_html = True
		elif opt in ["-t", "--test"]:
			do_tester = True
		elif opt in ["-f", "--folding"]:
			do_folding = True


	if  cond.inputfile == cond.outputfile and not cond.overwrite  :
		print 'Name collision. Backing away from rewriting input as output...'
		raise ValueError('Name collision. Backing away from rewriting input as output...')


	#print cond.inputfile[-4:]
	if cond.inputfile[-2:] == 'mm':
		cond.getmm()
	elif cond.inputfile[-4:] == 'json':
		cond.getjson()
		cond.convert_json2mm()

	# Iterate the nodes
	cond.iteratethroughnodes(callback=callback)

	#Unfold the Root
	if do_folding: cond.mm.get_node().set_FOLDED(None)

	if do_tester :
		#cond.run_tester(table="ScheduledHarvest", field="SILVSYS")
		#cond.run_tester()
		report_back()


	# Save the mindmap - use default or parameter
	if do_mindmap:
		cond.putmm()

	if do_json :
		cond.convert_mm2json()

		#Save json file
		cond.putjson( cond.outputfile.replace(".mm",".json") )

	if do_html :
		cond.convert_mm2json()

		#Save json file
		#cond.debug = True
		f = open(cond.outputfile.replace(".mm",".html"),'w')
		f.write("%s\n</body></html>"%("\n".join(parmD['page'])))
		f.close()

	return 0



def old_callback(n):
	global do_folding
	#print "Node TEXT: %s"%(n.TEXT)
	## Remove "Folded" attribute or Set to None
	## Remove other extraneous attributes in the same way...
	n.set_TEXT(c.ununicode(n.TEXT))
	if do_folding: n.set_FOLDED('true')
	n.set_CREATED(None)
	n.set_MODIFIED(None)


def do_variables( n ):
	global parmD
	#print "Doing ID %s"%( n.ID )
	(parmD['ID_travelled']).append( n.ID )
	if ('variables' not in parmD.keys()):
		parmD['variables'] = dict()
	if n.node :
		for nn in n.node :
			if nn.node:
				parmD['variables'][subst_variables(nn.TEXT)] = subst_variables(nn.node[0].TEXT)
				(parmD['ID_travelled']).append(nn.node[0].ID )
				(parmD['ID_travelled']).append(nn.ID )
			else:
				if nn.TEXT not in parmD['variables'] :
					parmD['variables'][subst_variables(nn.TEXT)] = None
				(parmD['ID_travelled']).append(nn.ID )
#	print parmD['variables']

def do_pageheader( n ):
	global parmD
	#parmD['page'].append("Page Header")
	parmD['page'].append("""<html><head>""")
	for nn in n.node:
		#each node is a tagged entity
		do_simple_tag(nn)
#		parmD['page'].append("<title>%s</title>"%(subst_variables(n.node[0].TEXT)))
#		parmD['ID_travelled'].append(n.node[0].ID )
	parmD['page'].append("""</head>
		<body>
		""")
	set_visited(n)

def do_text(n):
	global parmD
	parmD['page'].append( subst_variables(n.TEXT))
	set_visited(n)

def do_para(n):
	global parmD
	parmD['page'].append('<p%s%s>%s</p>'%(get_class_from_node(n,tagtype='class'), get_class_from_node(n,tagtype='style'), subst_variables(n.TEXT)))
	set_visited(n)

def do_tag(n):
	global parmD
	parmD['page'].append("<%s%s%s>"%(subst_variables(n.TEXT), get_class_from_node(n,tagtype='class'),get_class_from_node(n,tagtype='style') ) )

	for nn in n.node :
		callback(nn)

	parmD['page'].append("</"+subst_variables(n.TEXT)+">")
	set_visited(n)

def do_image_tag(n):
	global parmD
	parmD['page'].append("<%s %s>"%(subst_variables(n.TEXT), ' '.join([get_class_from_node(n) ,get_class_from_node(n,'src'),get_class_from_node(n,'height'),get_class_from_node(n,'width')])))

	for nn in n.node :
		callback(nn)

	#parmD['page'].append("</"+subst_variables(n.TEXT)+">")
	set_visited(n)

def do_svg_tag(n) :
	global parmD

	sql_groups = [subst_variables(nn.TEXT) for nn in n.node if nn.TEXT[0:6] == 'SELECT' or nn.TEXT[0:4] == 'WITH' ]
	viewbox = ''

	if 'viewbox' not in [nn.TEXT for nn in n.node ]:
		#we will have to use the default...
		viewbox_sql = "SELECT test.viewbox( " + \
			" || ".join ( [ "ARRAY ( %s ) "%(sql) for sql in sql_groups ] ) + \
			")"
		#print viewbox_sql
		viewbox = p.run_select_get_value_or_rowlist(viewbox_sql)
	#print "vb: %s "%(viewbox)

	parmD['page'].append("<%s %s>"%(subst_variables(n.TEXT), ' '.join(
		[
			get_class_from_node(n) ,
			get_class_from_node(n,'height', default_failure='height="300px"'),
			get_class_from_node(n,'width', default_failure='width="300px"'),
			get_class_from_node(n,'viewbox', default_failure='viewbox="%s"'%(viewbox)),

		]
	)))
	# Now we need to identify the rows...
	for sql in sql_groups:
		parmD['page'].append("<g>%s</g>"%(p.run_select_get_value_or_rowlist("SELECT ARRAY(%s)::text"%(sql))))

	for nn in n.node:
		if nn.TEXT[0:6] == 'SELECT' or nn.TEXT[0:4] == 'WITH' :
			set_visited(nn)
		if nn.TEXT == 'text':
			do_svg_text_tag(nn)

	#close tag
	parmD['page'].append("</"+subst_variables(n.TEXT)+">")
	set_visited(n)

def do_svg_text_tag(n):
	global parmD
	l = get_classes_from_node( n, tagtypes=['class','x','y','fill','transform'], default_failures=['','','','',''] )
	if not l: l=[]
	parmD['page'].append("<%s%s>"+subst_variables(n.TEXT)+
		' '.join(l)
		)
	for nn in n.node:
		if get_visited(nn): continue
		parmD['page'].append("%s"%(subst_variables(nn.TEXT)))
	parmD['page'].append("</"+subst_variables(n.TEXT)+">")
	set_visited(n)

def do_simple_tag(n):
	# For use with h1, etc...
	parmD['page'].append("<%s%s%s%s>"%(subst_variables(n.TEXT), get_class_from_node(n,tagtype='class'), get_class_from_node(n,tagtype='style'),get_class_from_node(n,tagtype='type') ) )

	for nn in n.node :
		if nn.ID not in parmD['ID_travelled'] :
			do_text(nn)
	parmD['page'].append("</"+subst_variables(n.TEXT)+">")
	set_visited(n)

def do_table(n):
	#if there are tr's in n.node[], then this is a manual table...
	# If not, then the query should come back with a table...

	if len([nn for nn in n.node if nn.TEXT.lower() == 'tr']) > 0 :
		#We have TR's - treat as a manual table
		do_tag(n)
	else:
		#We have a table to parse
		#Get rows from somewhere
		rows = get_rows_from_node(n)
		#tableheader
		parmD['page'].append("<tr>%s</tr>\n"%( "".join([ "<th>%s</th>"%(str(k)) for k,v in row[0].iteritems() ] ) ) )

		#table rows
		for r in rows:
			parmD['page'].append("<tr>%s</tr>\n"%( "".join([ "<td>%s</td>"%(str(v)) for k,v in r.iteritems() ] ) ) )
		# tag subnodes
		set_visited(n)




def do_NOOP(n):
	set_visited(n)

def get_rows_from_node(n):
	#This is a convenience function that accumulates a list of rows
	#from all the subnodes of a node
	# Think of it as a union where there is no checking for column similarity
	# This is of course dangerous - never trust your users!
	rows = list()
	for nn in n.node():
		if 'SELECT' in nn.TEXT[0:6].upper():
			# assume we have a query
			rows.extend( p.run_select_get_value_or_rowlist(subst_variables(nn.TEXT)) )
		elif 'SELECT' in nn.TEXT.upper():
			# assume we have a query
			rows.extend( p.run_select_get_value_or_rowlist(subst_variables(nn.TEXT)) )
		elif '[' in nn.TEXT and '{' in nn.TEXT :
			# assume we have a str(list(dict)) structure to eval
			rows.extend( eval (subst_variables(nn.TEXT)) )

def get_classes_from_node(n, tagtypes=[], default_failures=[] ):
	""" Convenience function to string things along...
	uses get_class_from_node to do the action """
	global parmD
	for d,f in dict(zip(tagtypes,default_failures)).iteritems():
		parmD['page'].append(get_class_from_node(n, tagtype = d, default_failure=f))
	#parmD['page'].append( subst_variables(n.TEXT))
	set_visited(n)

def get_class_from_node(n, tagtype = 'class', default_failure=''):
	l = list()
	if n.node:
		#if tagtype == 'viewbox': print 'Testing for viewbox in %s'%(n.TEXT)
		for nn in n.node:
			if get_visited(nn): continue
			if subst_variables(nn.TEXT).lower() == tagtype.lower() :
				for nnn in nn.node :
					l.append( '%s="%s"'%(tagtype, subst_variables(nnn.TEXT) )  )
					set_visited(nnn)
				set_visited(nn)
		if len(l) != 0:
			return ' %s'%(" ".join(l))
#		return " ".join([ " ".join([ 'class="%s"'%( subst_variables(nnn.TEXT) ) for nnn in nn.node ] ) for nn in n.node if subst_variables(nn.TEXT).lower() == 'class'] )
	#	else: #No subnode containing "class" only
	if default_failure == '':
		return default_failure
	else :
		return ' %s'%(default_failure)


def set_visited(n,and_children = False ):
	parmD['ID_travelled'].append(n.ID)
	if and_children:
		for nn in n.node:
			set_visited(nn, and_children )

def get_visited(n ):
	return (n.ID in parmD['ID_travelled'])

funcdict = OrderedDict({
	'variables':	do_variables,
	'pageheader':	do_pageheader,
	'text':			do_text,
	'para':			do_para,
	'tr':			do_tag,
	'td':			do_tag,
	'h1':			do_simple_tag,
	'h2':			do_simple_tag,
	'h3':			do_simple_tag,
	'h4':			do_simple_tag,
	'h5':			do_simple_tag,
	'h6':			do_simple_tag,
	'div':			do_tag,
	'table':		do_table,
	'img':			do_image_tag,
	'svg':			do_svg_tag,
})

def callback(n):
	global parmD
	#print n.TEXT
	if len(parmD['ID_travelled']) == 0:
		#ignore first node... name of the mindmap...
		do_NOOP(n)
	elif n.ID not in parmD['ID_travelled']:
		try:
			funcdict[n.TEXT.lower().replace(' ','')](n)
		except KeyError:
			funcdict['para'](n)

def subst_variables( t ):
	global parmD

	# Substitute variables
	if ('variables' in parmD.keys()):
		for k,v in parmD['variables'].iteritems():
			t = t.replace('${%s}'%(k),str(v))

	# Check for backticks
	t = do_backtick(t,p=p)

	return t

def report_back():
	#print "ID Travelled:\n%s\n"%(parmD['ID_travelled'])
	#print "Variables:\n%s\n"%(parmD['variables'])
	print "Page:\n%s\n</body>\n</html>"%("\n".join(parmD['page']))


if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv[1:]))
