#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  tokenize.py
#
#  Copyright 2017 Angus Carr <angus@neva>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

BACKTICK = '`'

import os
import sys
sys.path.insert(0, '..')

import psycopg2
from stack.pg_db import pg_db
# p = pg_db()

import psycopg2
import psycopg2.extras

def do_backtick(s,p=None):
	if p is None:
		p = pg_db()

	r = list()
	t = s.split(BACKTICK)

	#print t

	for i in range(0,len(t)):
		if i%2 == 0:
			if t[i] != '':
				#print "Appending %s "%(t[i])
				r.append(t[i])
		else :
			#print "Interpreting %s "%(t[i]),
			try:
				r.append(p.run_select_get_value_or_rowlist(t[i]))
			except psycopg2.ProgrammingError as e:
				r.append(t[i]+str(e))
			#print " ==> %s"%(r[-1])

	return "".join([str(rr) for rr in r])


def main(args):
	t = 'Hello my name is `SELECT \'Bob\' as name `! '
	print do_backtick(t)

	print do_backtick('`SELECT 5 as five`')

	t = """		[
		{'five':'5','ten':'10'},
		{'six':'6'}
		]"""
	print do_backtick(t)

	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
