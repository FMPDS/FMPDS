#This program imports from FGDB's into the PG DB
#It relies on external commands found in the OSGeo4W distribution Shell
#
#
#
import psycopg2
import os, sys
import subprocess

import logging
#logging.warning("Using Logging...")
#logging.basicConfig(filename='example.log',level=logging.DEBUG)
#logging.debug('This message should go to the log file')
#logging.info('So should this')
#logging.warning('And this, too')


#tableCount = 0

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import checker.jsonloader
import checker.initialize_checker


class consolidator():
	conn = None
	cur = None
	parameters = None

	def __init__(self,fmu,product,year,fi_portal_details,src=None,dst=None,DropSubmissionSchema=True):
		self.parameters = self.consolidateparameters(src,fmu,product,year,fi_portal_details,dst)
		print "Logging to: %s"%(self.parameters['log'])
		logging.basicConfig(filename=self.parameters['log'],format='%(asctime)s %(message)s',level=logging.DEBUG)
		#import logging
		#logging.basicConfig(format='%(asctime)s %(message)s',level=logging.DEBUG)

		logging.debug( "consolidator got src=%s"%(src))

		self.parameters['DropSubmissionSchema']=DropSubmissionSchema
		if (self.parameters['repostyle'] == 'MUnnn_yyPP'):
			self.parameters['gdb'] = self.get_gdbfilename('MU'+str(self.parameters['fmu_num'])+'_'+str(self.parameters['year'])[-2:]+self.parameters['product'].upper())
		else:
			self.parameters['gdb'] = self.get_gdbfilename()
		logging.info( "GDB Name: %s"%(self.parameters['gdb']))
		self.getdb(self.parameters['dst'])
		self.print_parameters()

	def go(self):
		self.create_submission_table_entry()
		self.createsubmissionschema(self.parameters['submission'])
		self.loadtables()
		if self.parameters['DropSubmissionSchema']:
			self.dropsubmissionchema()
		#print "++++++++++++++++++++++++++++++++++++++++\n\n\n"

	def loadtables(self):
		################################################################
		# Get the JSON Table list and go through the tables needed
		################################################################
		# Load the JSON Table list...
		tables=self.getjson(self.parameters['product'],self.parameters['year'])


		################################################################
		#go through the tables needed
		################################################################
		#for each table
		for t in tables:
			if (self.parameters['DoOnlyTable'] is not None) and (t['name'] != self.parameters['DoOnlyTable'])  : continue
			logging.info( "Table %s\\%s"%(t['path'],t['name']))
			##Generate the ogr2ogr command
			##ogr2ogr -f "PostgreSQL" PG:"dbname=fmp user=carran password=*** host=localhost " -nln public.temp mu350_11FMP\FMP_Schema.gdb PlannedClearcuts
			cmd = 'ogr2ogr -overwrite -f "PostgreSQL" PG:"%s " -nln %s.%s %s %s '%\
				(self.parameters['dst'],self.parameters['schema'],t['name'],self.parameters['gdb'], t['name'] )
			logging.debug( "OGR Command: %s"%(cmd))

			##Run OGR2OGR
			try:
				subprocess.check_call(cmd)
			except subprocess.CalledProcessError as e:
				#print e
				print (t['name'])+'is missing.' #james
				continue #Table isn't present- don't bother

			##Generate sql to copy data over to final table
			### Get Field names from src (json)
			#print "fields: "+str(t.keys())
			#tableCount = tableCount+1
			print "Processing "+str(t['name'])+'\n'

			srcF = [ n['name'].lower() for n in t['fields'] ]
			logging.debug(str(srcF))
			for n in ['shape_length','shape_area','ogc_fid','wkb_geometry'  ]:
				try:
					srcF.remove(n)
				except:
					pass
			logging.warn("Got src gdb field names: %s"%(srcF))

			### Get Field names from dst (db)
			sql = "SELECT attname FROM  pg_attribute WHERE  attrelid = '%s'::regclass AND    attnum > 0 AND    NOT attisdropped ORDER  BY attnum; "%(self.parameters['product'].lower()+"."+t['name'].lower() )
			#print sql
			try:
				self.cur.execute(sql)
			except psycopg2.ProgrammingError as e:
				logging.error("\n".join( "================", sql, "================", str(e)))
				self.conn.commit()
				continue

			dstF = [ r[0].lower() for r in self.cur.fetchall() ]
			logging.warn("Got pgdb field names: %s"%(dstF))
			dstF.remove('id')
			dstF.remove('shape')
			dstF.remove('submission')
			dstF.remove('additional')
			transF = dict()
			transF['shape'] = 'ST_Multi(wkb_geometry)'
			### Get submission id in here
			transF['submission'] = str(self.parameters['submission_id'])

			### Identify fields to copy directly
			for n in dstF:
				if n in srcF:
					transF[n]=n
					srcF.remove(n)

			### Identify fields to copy into json additional field
			add_fragment = "(SELECT row_to_json(_) FROM (SELECT "
			add_fragment += ', '.join(srcF)
			add_fragment += ") AS _ )  "
			transF['additional'] = add_fragment


			### Compose sql
			k = transF.keys()
			sql = "INSERT INTO %s ("%(self.parameters['product'].lower()+"."+t['name'].lower() )
			sql += ', '.join(k)
			sql += ") \nSELECT \n\t"
			sql += ',\n\t '.join( [ transF[kk]+" as "+kk for kk in k ] )
			sql += "\nFROM %s.%s "%(self.parameters['schema'],t['name'].lower())

			#Make some inline corrections...
			if t['name'].lower() == 'renewalandtending' :
				sql+= '\nWHERE term = 1 OR term = 2'

			logging.error(sql)
			print sql

			### Run the SQL and report errors

			try:
				self.cur.execute(sql)
			except psycopg2.ProgrammingError as e:
				logging.error("\n".join([ "================", sql, "================", str(e)]))
				self.conn.rollback()
			except psycopg2.DataError as e:
				logging.error("\n".join([ "================", sql, "================", str(e)]))
				self.conn.rollback()
			else:
				#logging.error( sql )
				logging.debug('SQL worked')
				self.conn.commit()

	def dropsubmissionchema(self):
		#Drop the submission schema
		sql = "DROP SCHEMA %s CASCADE "%(self.parameters['schema'])
		self.cur.execute(sql)
		self.conn.commit()

	def execution_path(self):
		try:
			#print "Using __file__"
			filename = os.path.dirname(__file__)
		except:
			#print "Using sys.argv[0]"
			filename = os.path.dirname(sys.argv[0])

		x = os.path.join(os.path.dirname(sys._getframe(1).f_code.co_filename), filename)

		if x=='':
			x='..\\consolidator'

		#print "execution_path returns %s"%(str(os.path.split(x)))
		return x

	def getjson(self,product,year):
		s=checker.initialize_checker.submission_checker()
		return checker.jsonloader.jsonloader( s.get_jsonfilename(product,year) ).json

	def old_getjson(self,product,year):
		import json

		#Location to detect plan year and apply appropriate tech spec json.  Pre 2020 plans use the 09 spec.  Others use the 17 spec.

		#The [PCI|BMI|OPI|AWS|FMP|AR].json files are one level up in our tree, then in the checker/deliverables/json/year(of tech spec) folder.

		#if the plan year < 2020 use the 2009 tech spec version of the json
		if int(year) < 2020:
			json_data=open(os.path.join(os.path.split(self.execution_path())[0]+r'\tech_spec\2009',product.upper()+'.json'))
		#if the plan year > 2020 use the 2017 version of the json
		else:
			json_data=open(os.path.join(os.path.split(self.execution_path())[0]+r'\tech_spec\2017',product.upper()+'.json'))

		data = json.load(json_data)
		return data

	def getdb(self,dst):
		self.conn = psycopg2.connect(dst)
		self.cur = self.conn.cursor()


	def createsubmissionschema(self,submission):
		################################################################
		#Create the submission schema
		################################################################
		logging.debug( "createsubmissionschema(%s)"%(str(submission)))
		try:
			self.cur.execute("DROP SCHEMA sub_%s CASCADE"%(str(submission)))
		except psycopg2.ProgrammingError as e:
			# Schema doesn't exist... Just create it...
			self.conn.rollback() #Clears the block after a failed transaction
		else:
			self.conn.commit()

		try:
			self.cur.execute("CREATE SCHEMA IF NOT EXISTS sub_%s "%(str(submission)))
		except psycopg2.ProgrammingError as e:
			# Schema can't exist
			self.conn.rollback() #Clears the block after a failed transaction
			raise e
		else:
			self.conn.commit()


	def consolidateparameters(self,src,fmu,product,year,fi_portal_details,dst):
		#logging.debug("consolidateparameters(%s,%s,%s,%s,%s,%s"%(src,fmu,product,year,fi_portal_details,dst))
		#Check for presence of things that default to None:
		for p in [ 'submission','fmu_num','fmu','src','product','year','DoOnlyTable', 'repostyle'] :
			if p not in fi_portal_details : fi_portal_details[p] = None

		#Check for presence of things that default to False:
		for p in [ 'readyforreview','inreview','finishedreview','approved'] :
			if p not in fi_portal_details : fi_portal_details[p] = False

		fi_portal_details['fmu'] = fmu

		if src is None:
			#fi_portal_details['src']=r'\\cihs.ad.gov.on.ca\lrc1\MNR\groups\ROD\NorthwestRegion\RIAU\Data\Extended_Data\Forest\FMP'
			fi_portal_details['src']=r'd:\NWR_Admin\FMP'
		else:
			fi_portal_details['src'] = src

		if dst is None:
			# If not defined, then assume from username
			# Depends on a file %APPDATA%\postgresql\pgpass.conf containing lines like this
			#    localhost:5432:*:username:password
			from username import current_user
			default_db_string = "dbname='fmp' user='%s' host='lrcgikdcwhiis06' "%(current_user())
			fi_portal_details['dst'] = default_db_string
		else:
			fi_portal_details['dst'] = dst
		fi_portal_details['product'] = product
		fi_portal_details['year'] = year

		if fi_portal_details['fmu_num'] is None:
			# Get the fmu_lut value out of the database
			conn = psycopg2.connect(fi_portal_details['dst'])
			cur = conn.cursor()
			sql = "SELECT fmu_num FROM fmp.fmu_lut WHERE fmu = '%s'"%(fi_portal_details['fmu'])
			print sql
			cur.execute(sql)
			r = cur.fetchone()
			fi_portal_details['fmu_num'] = r[0]

		#Derivative parameters
		fi_portal_details['schema'] = "sub_%s"%(str(fi_portal_details['submission']))
		fi_portal_details["submission_id"] = -1

		#Log filename
		fi_portal_details['log'] = os.path.join(str(fi_portal_details['src']), \
			str(fi_portal_details['fmu']), \
			str(fi_portal_details['product']), \
			str(fi_portal_details['year']), \
			r"consolidator.log" )
		print "Logging to %s"%(fi_portal_details['log'])
		return fi_portal_details

	def print_parameters(self):
		logging.info( "=========================================================" )
		for k,v in self.parameters.items():
			logging.info( "Parameter['%s']='%s'"%(k,str(v)))
		logging.info( "=========================================================" )



	def create_submission_table_entry(self):
		logging.debug( "create_submission_table_entry()" )
		################################################################
		#Create a submission table entry
		################################################################
		try:
			sql = """INSERT INTO fmp.submissions (fi_portal_id, year, product, fmu, status)
				VALUES (%d,%d,'%s','%s','%s')"""%(int(self.parameters['submission']),
				int(self.parameters['year']),
				self.parameters['product'].lower(),
				self.parameters['fmu'],
				self.parameters['status'] )
			self.cur.execute(sql)
			self.conn.commit()
		except psycopg2.IntegrityError as e:
			#If it is a duplicate key, then delete it first... moohoohoohaaaaaaa...
			# print str(e)
			if (str(e).split('\n'))[0] == 'duplicate key value violates unique constraint "submissions_fi_portal_id_idx"' :
				logging.info( "\tfi_portal_id %s already exists... Deleting..."%(str(self.parameters['submission'])) )
				self.conn.rollback() #Clears the block after a failed transaction
				self.cur.execute("DELETE FROM fmp.submissions WHERE fi_portal_id = %s "%(str(self.parameters['submission'])))
				self.conn.commit()

				#Now re-run the transaction...
				self.cur.execute(sql)
				self.conn.commit()
			else:
				raise e
		# Now grab the submission table id number...
		self.cur.execute("SELECT id FROM fmp.submissions WHERE fi_portal_id = %s"%(str(self.parameters['submission'])))
		r = self.cur.fetchone()
		self.parameters["submission_id"] = r[0]

		logging.debug('self.parameters["submission_id"] is %s'%(self.parameters["submission_id"]) )

	def get_gdbfilename(self,repo='_data'):
		logging.debug( "get_gdbfilename(%s)"%(''))

		self.parameters['gdb'] = os.path.join(self.parameters['src'],
			self.parameters['fmu'],
			self.parameters['product'],
			str(self.parameters['year']) ,
			repo,
			'FMP_Schema.gdb'
			)
		#print "get_gdbfilename(%s): %s"%(repo,self.parameters['gdb'])

		if   (not os.path.exists(self.parameters['gdb'])) and repo == '_data':
                        return self.get_gdbfilename('MU'+str(self.parameters['fmu_num'])+'_'+str(self.parameters['year'])[-2:]+self.parameters['product'].upper())
		elif (not os.path.exists(self.parameters['gdb'])) and repo == 'MUnnn_yyPP':
                        return self.get_gdbfilename()
		elif (not os.path.exists(self.parameters['gdb'])):
                        assert("Can't find the FGDB")
		else:
                        return self.parameters['gdb']


	def makeView(self):
		pass


def load_2_fmpds(submissions, product, fmu, year=None, src=None, DoOnlyTable=None, DropSubmissionSchema=True):
	logging.debug( "load_2_fmpds(%s,%s,%s,%s,%s,%s,%s)"%(submissions, product, fmu, year, src, DoOnlyTable, DropSubmissionSchema))
	if year is not None:
		submissions = {year:submissions[year]}
	if src is None:
			src = r'\\cihs.ad.gov.on.ca\MNRF\groups\ROD\NorthwestRegion\RIAU\Data\Extended_Data\Forest\FMP'

	fi_portal_details={ "readyforreview":False,
			"inreview":False,
			"finishedreview":False,
			"approved":True
			}
	if DoOnlyTable is not None:
			fi_portal_details["DoOnlyTable"] = DoOnlyTable

	for y in submissions.keys():
                fi_portal_details["submission"]= submissions[y]
		c = consolidator(
			src=src,
			fmu=fmu,
			product=product,
			year=y,
			fi_portal_details=fi_portal_details,
			DropSubmissionSchema = DropSubmissionSchema
			)
		c.go()
		del c



if __name__ == "__main__":
	#for n in range(1,100): print ""

	c = consolidator(
		#src=r'd:\NWR_Admin\FMP',
		#src=r'\\cihs.ad.gov.on.ca\lrc1\MNR\groups\ROD\NorthwestRegion\RIAU\Data\Extended_Data\Forest\FMP',
		src=r"\\lrcpthbafp00002\NWSI-Extended\Extended_Data\Forest\FMP",
		dst=None,
		fmu="Black_Spruce",
		product="AR",
		year=2011,
		fi_portal_details={ "submission": 12427,'status':'Default'},
		DropSubmissionSchema=False
		)
	#c.loadtables()
	c.go()
