from consolidator import consolidator
import os

class ingestor():
	metadata = None
	def __init__(self,fmu_product_year,metadata,path_to_metadata,DropSubmissionSchema=True):
		print "ingesting \t%s\n\t%s\n\t%s"%(fmu_product_year,metadata,path_to_metadata)
		#print "\n".join(["" for n in range(1,10)])
		self.fmu,self.product,self.year = fmu_product_year
		self.path_to_metadata = path_to_metadata
		self.metadata = self.parse_metadata(metadata)
		
		self.DropSubmissionSchema = DropSubmissionSchema
		
		print 'fmu product year'
		print self.fmu,self.product,self.year
		print 'metadata'
		print self.metadata
		print 'path_to_metadata'
		print self.path_to_metadata
		
		#submission
		#Find submission in metadata or get it from the filename
		#self.go()
		
	def parse_metadata(self,metadata):
		m = dict()
		for n in metadata :
			if "=" not in n: continue
			k,v = n.lower().split("=")
			if k is not None and v is not None:
				m[k]=v
		#m = dict([ n.lower().split("=") for n in metadata ])
		if 'status' not in m.keys() :
			m['status'] = 'default'
		if 'submission' not in m.keys() :
			#submission not in the metadata - get from filename
			m['submission'] = int(os.path.split(self.path_to_metadata)[1].lower().replace('fi_submission_','').replace('.txt',''))
		return m

		
	def go(self):
		c = consolidator(
		#src=r'd:\NWR_Admin\FMP',
		#src=r'\\cihs.ad.gov.on.ca\lrc1\MNR\groups\ROD\NorthwestRegion\RIAU\Data\Extended_Data\Forest\FMP',
		# Three levels up from the metadata file is... src
		src = os.path.split(os.path.split(os.path.split(os.path.split(self.path_to_metadata)[0])[0])[0])[0] ,
		dst=None,
		fmu=self.fmu,
		product=self.product.upper(),
		year=self.year,
		fi_portal_details={ "submission": self.metadata['submission'],
			"status":self.metadata['status']
			},
		DropSubmissionSchema=self.DropSubmissionSchema
		)
		#c.loadtables()
		c.go()



if __name__ == "__main__":
	i = ingestor( ('Lakehead', 'AWS', '2011'),\
		['fmu=Lakehead', 'fmu_num=796', 'product=AWS', 'year=2011', 'status=Approved', 'date_submitted=2015-02-05', 'date_approved=', 'date_downloaded=2015-11-27', 'downloader=Jevon Hagens', 'export_path=\\\\cihs.ad.gov.on.ca\\lrc1\\MNR\\groups\\ROD\\NorthwestRegion\\RIAU\\Data\\Extended_Data\\Forest\\FMP', 'filetype=E00', 'submission=8526'],
		r"\\lrcpthbafp00002\NWSI-Extended\Extended_Data\Forest\FMP\Lakehead\AWS\2011\fi_Submission_8526.txt")
	i.go()
