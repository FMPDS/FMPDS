try:
  import pwd
except ImportError:
  import getpass
  pwd = None

def current_user():
  if pwd:
    return pwd.getpwuid(os.geteuid()).pw_name.lower()
  else:
    return getpass.getuser().lower()
    

def default_db_string():
	return "dbname='fmp' user='%s' host='lrcgikdcwhiis06' "%(current_user())


import psycopg2
import psycopg2.extras


def run_update(sql):
	#print sql
	try:
		conn = psycopg2.connect(default_db_string())
		cur = conn.cursor()
		cur.execute(sql)
		conn.commit()
	except psycopg2.ProgrammingError as e:
		conn.commit()
		print e
		print sql
		return None
	except Exception as e:
		print e
		print sql
		raise e
    
    
def run_select(sql):
	""" Return first value from the first row of the return from SQL"""
	#print sql
	try:
		conn = psycopg2.connect(default_db_string())
		cur = conn.cursor()
		cur.execute(sql)
		r = cur.fetchone()
		return r[0]
	except TypeError as e:
		print e
		print sql
		return None
	except psycopg2.ProgrammingError as e:
		print e
		print sql
		return None
	except Exception as e:
		print e
		print sql
		raise e

def run_select_get_dict(sql):
	""" Return first value from the first row of the return from SQL"""
	#print sql
	try:
		conn = psycopg2.connect(default_db_string())
		cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		cur.execute(sql)
		r = cur.fetchall()
		return r
	except TypeError as e:
		print e
		print sql
		return None
	except psycopg2.ProgrammingError as e:
		print e
		print sql
		return None
	except Exception as e:
		print e
		print sql
		raise e

   	
def run_sql_onevalue(sql):
	""" Return first value from the first row of the return from SQL"""
	if sql.lower().startswith('select'):
		return run_select(sql)
	else:
		return run_update(sql)
	

    
if __name__ == "__main__":    
	print current_user()
	#print run_sql_onevalue("UPDATE fmp.submissions SET consolidation_success = '%d format: a number is required, not str' WHERE fi_portal_id = 17227")
	print run_sql_onevalue("SELECT date_part('epoch',consolidation_time) FROM fmp.submissions WHERE fi_portal_id=%s"%( str(17227) ))
	print run_select_get_dict("SELECT * FROM fmp.submissions WHERE fi_portal_id>%s"%( str(17800) ))
	sql = """
		SELECT id, 
			import_path || '\\' || fmu || '\\' ||  product || '\\' || year || '\\fi_submission_' || fi_portal_id || '.txt '  as filename , 
			fi_portal_status 
		FROM fmp.consolidation_failures
		WHERE failure_type = 'import'
			AND fmu = 'Trout_Lake'
		LIMIT 1
		"""
	print run_select_get_dict(sql)
