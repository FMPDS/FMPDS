#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  choose_a_tech_spec.py
#
#  Copyright 2019 CARRAN <CARRAN@ON34C02585177>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#


# Use by:
# from stack.choose_a_tech_spec import choose_a_tech_spec
# choose_a_tech_spec(product,year)
# choose_a_tech_spec(*(user_spec.split('_')))
#	where user_spec is like 'AWS_2018'



import os

def choose_a_tech_spec( product, year ):
	#print ("{0}_{1}".format(product,year))
	product = product.upper()
	if product in [ 'PCI','BMI','FMPDP', 'EFRI', 'FMP_P1','FMP_P2', 'FMP_P','PLANNING INVENTORY', 'FINAL PLAN', 'BASE MODEL INVENTORY', 'DRAFT PLAN' ]:
		product='FMP'
	year = int(year)
	productlist= { 'AR': dict(dict(zip( range(2017,2030),[ '2018' for y in range(2017,2030) ] )) , **dict(zip( range(2008,2017),[ '2009' for y in range(2008,2017) ] )) ),
		'AWS': dict(dict(zip( range(2018,2030),[ '2018' for y in range(2018,2030) ] )) , **dict(zip( range(2008,2018),[ '2009' for y in range(2008,2018) ] )) ),
		'FMP': dict(dict(zip( range(2018,2030),[ '2018' for y in range(2018,2030) ] )) , **dict(zip( range(2008,2018),[ '2009' for y in range(2008,2018) ] )) )
		}
	productlist['FMP'][2007]='2007' 
	try:
		TSyear = productlist[product][year]
	except KeyError as e:
		raise RuntimeError("Cannot identify applicable Spec file {0}\{1}".format(product, year))

	jsonfilename=os.path.abspath(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
		r'tech_spec/{year}/{product}.json'.format( year=str(TSyear), product=product)))
	return jsonfilename

def main(args):
	testdict = {("AR","2008"):"AR_2009",("AR",2010):"AR_2009",("AR","2017"):"AR_2018",("Banana",9999):"Banana-IsNonexistent_9999"}
	for k,v in testdict.iteritems():
		try:
			print ("{0} should be {1}: {2}".format( k,v,choose_a_tech_spec(*k)) )
		except RuntimeError as e:
			print ("{0} should be {1}: {2}".format( k,v,str(e)) )

	for k,v in (lambda f: f.__class__(map(reversed, f.items())) )(testdict).iteritems():
		try:
			print ("{0} from {1}: {2}".format( k,v,choose_a_tech_spec(*(k.split('_')))) )
		except RuntimeError as e:
			print ("{0} from {1}: {2}".format( k,v,str(e)) )
		except TypeError as e:
			print ("{0} has too many parts...\n\t{1}".format(k,str(e)) )


	return 0



if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
