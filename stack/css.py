#-------------------------------------------------------------------------------
# Name:        css.py
# Purpose:      defines css of the html report
#-------------------------------------------------------------------------------
import webbrowser


htmlTop= '''
<!DOCTYPE html>
<html>
<head>
'''

FMPStyles = '''
<style>

#t01 {
	border-collapse: collapse;
	background-color: white;
}

#t01 td {
	border: 1px solid white;
    background-color: white;
}

#t02 {
	border-collapse: collapse;
}

#t02 td {
	border: 1px solid black;
	#width: 100%;
}
#t02 th {
	border: 1px solid black;
    background-color: #708090;
    color: white;
}

#t02 tr:nth-child(even) {background-color:#E6E6E6;}
#t02 tr:nth-child(odd) {background-color:#FFFFFF;}
#t02 tr {vertical-align: top;}

table {
	border-collapse: collapse;
}

table td {
	border: 1px solid blue;
	#width: 100%;
	vertical-align: bottom;
}
table th {
    background-color: #4CAF50;
    color: white;
	text-align: left;
	height: 50px;
}

table tr {
	background-color: PeachPuff  ;
}

tr.warning{
    background-color:yellow;
}

h1 {
	display: block;
	font-size: 1.5em;
	margin-top: 0.67em;
	margin-bottom: 0.67em;
	margin-left: 0;
	margin-right: 0;
	font-weight: bold;
	background-color: #B8B8B8;
}

h2 {
	display: block;
	font-size: 1.3em;
	margin-top: 0.83em;
	margin-bottom: 0.83em;
	margin-left: 0;
	margin-right: 0;
	font-weight: bold;
	background-color: #C0C0C0;
}

h3 {
	display: block;
	font-size: 1.17em;
	margin-top: 1em;
	margin-bottom: 1em;
	margin-left: 0;
	margin-right: 0;
	font-weight: bold;
	background-color: #E6E6E6;
}

h4 {
	display: block;
	margin-top: 1.33em;
	margin-bottom: 1.33em;
	margin-left: 0;
	margin-right: 0;
	font-weight: bold;
	background-color: #F1F1F1;
}

h5 {
	display: block;
	font-size: .83em;
	margin-top: 1.67em;
	margin-bottom: 1.67em;
	margin-left: 0;
	margin-right: 0;
	font-weight: bold;
}

h6 {
	display: block;
	font-size: .67em;
	margin-top: 2.33em;
	margin-bottom: 2.33em;
	margin-left: 0;
	margin-right: 0;
	font-weight: bold;
}

#p01 {
    color: green;
    font-weight: bold;
}

#p02 {
    color: orange;
    font-weight: bold;
}

#p03 {
    color: red;
    font-weight: bold;
}

p.body_text {
	color: black;
}

p.document_title {
	display: block;
	font-size: 2.0em;
	margin-top: 1em;
	margin-bottom: 1em;
	margin-left: 0;
	margin-right: 0;
	font-weight: bold;
	background-color: #B8B8B8;
}

mark {
    background-color: yellow;
    color: black;
    font-weight: bold;
}
</style>
'''


css_foldable_sections = '''
<style>
<!-- Foldable Sections in CSS... Section from http://jsfiddle.net/dmarvs/94ukA/4/ -->
<!-- Use class="foldingtitle" to be the title of the folded section -->

<!-- Example
<div id="ScheduledRoadCorridors" class="foldingSection" >
	<a href="#hide1" class="hide" id="hide1">+</a>
	<a href="#show1" class="show" id="show1">-</a>
	<div class="foldingtitle"><h1>Scheduled Road Corridors (SRC)</h1></div>
	<div class="foldingcontent"><h2>Technical Detail</h2>
		<p> blah blah Blah</p>
	</div>
</div>
-->

.foldingSection {
    height:auto !important;
}
.foldingcontent {
    display:none;
    height:auto;
}
.show {
    display: none;
}
.hide:target + .show {
    display: inline;
}
.hide:target {
    display: none;
}
.hide:target ~ .foldingcontent {
    display:inline;
}

/*style the (+) and (-) */
.hide, .show {
	width: 20px;
	height: 20px;
	border-radius: 20px;
	font-size: 15px;
	color: #fff;
	text-shadow: 0 1px 0 #666;
	text-align: center;
	text-decoration: none;
	box-shadow: 1px 1px 2px #000;
	background: #cccbbb;
	opacity: .95;
	margin-right: 0;
	float: left;
	margin-bottom: 10px;
}

.hide:hover, .show:hover {
	color: #eee;
	text-shadow: 0 0 1px #666;
	text-decoration: none;
	box-shadow: 0 0 4px #222 inset;
	opacity: 1;
	margin-bottom: 25px;
}

.foldingcontent p{
    height:auto;
    margin:0;
}
.foldingtitle {
	height: auto;
	padding-left: 20px;
	margin-bottom: 25px;
}


</style>
'''
css_hide_foldable_section_buttons = '''
<style>
.show { display: none; }
.hide { display: none; }
</style>
'''

css_details_sections = """
<style>
details {
  font: 16px "Open Sans", "Arial", sans-serif;
  /*width: 620px; */
}

details > summary {
  padding: 2px 6px;
  width: 15em;
/*  background-color: #ddd; */
  border: none;
/*  box-shadow: 3px 3px 4px black; */
}

details > p {
  border-radius: 0 0 10px 10px;
  background-color: #ddd;
  padding: 2px 6px;
  margin: 0;
  box-shadow: 3px 3px 4px black;
}

</style>
"""

# We need to pull them all together for simplity and backward compatibility
htmlStyle = htmlTop + FMPStyles + css_foldable_sections + css_details_sections
htmlStyle_flat = htmlTop + FMPStyles + css_hide_foldable_section_buttons



if __name__ == '__main__':
	tableSumDict = {'MU796_09AGG00': {'lyrName': u'AGG', 'reference': '', 'fieldDefinitionCom': 'All mandatory fields exist.', 'fieldValidation': 'Valid', 'recordValidation': 'Valid', 'prj': u'NAD 1983 UTM Zone 16N', 'recordValidationCom': 'Total number of records: 0'}}
	htmlfile = r'C:\temp\testhtml.html'
	rep = open(htmlfile,'w')

	htmltext = htmlStyle
	htmltext += '''
	<table id="t02">
	  <tr>
		<th>Layer File Name</th>
		<th>Layer Short Name</th>
		<th>Projection</th>
		<th>Field Validation</th>
		<th style="width: 200px;">Field Validation Comments</th>
		<th>Record Validation</th>
		<th>Record Validation Comments</th>
		<th>FIM Reference</th>
	  </tr>
	  '''

	for k,v in tableSumDict.iteritems():
		htmltext += '<tr>'
		htmltext += '<td>%s</td>'%(k)
		htmltext += '<td>%s</td>'%(v['lyrName'])
		htmltext += '<td>%s</td>'%(v['prj'])
		htmltext += '<td><p id="p03">  %s  </p></td>'%(v['fieldValidation'])
		htmltext += '<td style="width: 200px;">%s</td>'%(v['fieldDefinitionCom'])
		htmltext += '<td>%s</td>'%(v['recordValidation'])
		htmltext += '<td>%s</td>'%(v['recordValidationCom'])
		htmltext += '<td>%s</td>'%(v['reference'])
		htmltext += '</tr>'

	rep.write(htmltext)

	rep.close()
	webbrowser.open(htmlfile,new=2)
