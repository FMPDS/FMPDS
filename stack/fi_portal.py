#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  fi_portal.py
#
#  Copyright 2018 CARRAN <CARRAN@ON34C02585177>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#


#######################################################################
# fi_portal.py contains functions associated with fi portal info
#######################################################################


import os
import sys

def findSubID(mainfolder):
	"""FindSubID Identifies the submission id from the fi_submission_*.txt filename"""
	if os.path.exists (mainfolder):
		files = os.listdir(mainfolder)
		try:
			fitxt = [s for s in files if "FI_SUBMISSION" in s.upper()] #list of files that has "fi_Submission" in its filename.
			return int(fitxt[0].split('.')[0].split('_')[-1]) #returning submission number
		except:
			try:
				lyrfile = [s for s in files if ".lyr" in s] #list of layer files (there should be just one).
				return int(lyrfile[0].split('.')[0].split('_')[-1]) # returning submission number at the end of layer filename.
			except Exception:
				return ''
	else:
		return ''

def main(args):
	print findSubID(r"\\lrcpsoprfp00001\GIS-DATA\WORK-DATA\FMPDS\Abitibi_River\AWS\2017")
	print findSubID(os.path.dirname(os.path.dirname(r"\\lrcpptboshfs001\MNR_SR_External\FOREST_MGMT\FMPDS\Ottawa_Valley\AWS\2018\_data\FMP_Schema.gdb")))
	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
