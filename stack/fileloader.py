#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  fileloader.py
#

########################################################################
#
# fileloader loads files (usually spatial) using either arcpy or ogr
# and abstracts the differences between the two stacks.
#
# This file is the core of the multi-stack nature of the checker.
#
########################################################################

import os
import sys
import glob

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from stack.stack_choice import usingArcpy, usingOGR
if usingArcpy:
	from stack.stack_choice import arcpy
	from stack.typeLUT import arcpy_fieldTypeLUT_reverse as fieldTypeLUT
elif usingOGR:
	from stack.stack_choice import ogr
	from stack.typeLUT import fieldTypeLUT_reverse as fieldTypeLUT


# These are the general classes of file we use:
fileformatetypes = set([
	'Database',					#PG Database Connection String needs a layer
	'Single_FC_file',			#e.g. Shapefile - defaults to layer 0, the only one present.
	'Multi_FC_file',			# GPKG, FGDB as seen by OGR,
	'Multi_FC_file_schema',		# FGDB as seen by ArcPy (datasets...)
	'Multi_FC_Dir'				# Directory of Single_FC_file s
	])

class fileloader():
	fields = [ ]
	fieldtypes = dict()
	dataSource = None
	geometry_type = None

	effective_filename = None
	layer = None

	SRName = None
	SRCode = None
	SRAuth = None

	knows_about_null = True

	global usingOGR
	global usingArcpy

	def __init__(self,filename,layer=None,geometry_type='Polygon',openTable=True):
		global usingOGR
		global usingArcpy

		self.filename = filename
		self.layer = layer

		self.layername = layer #Keep what was actually requested...

		self.effective_filename = self.filename

		self.geometry_type = geometry_type

		#print ("FileLoader Loading {0}!{1}!{2}".format(self.effective_filename,str(layer),geometry_type))

		self.openDataSource()

		if openTable:
			if  self.openTable():
				#print ("\t\tfileloader opened data table")
				pass
			else:
				#print ("\t\tfileloader probably failed to open data table")
				raise(RuntimeError("\t\tfileloader failed to open data table"))

	def openTable(self):
		if usingArcpy :
			tmp = self._arcpy_wrap_SearchCursor()
			if tmp is None: return None

			tmpfields = tmp.fields
			newfields = [ f  for f in tmp.fields if f != u'Shape' ]

			newfields.append("SHAPE@")
			newfields.append("SHAPE@TRUECENTROID")
			self.arcpycursor = self._arcpy_wrap_SearchCursor(field_names=newfields)

			#self.layerDefinition = arcpy.Describe(self.effective_filename,field="FID")
			#print [ "%s: %s"%(c.name,"c.dataType") for c in self.layerDefinition.fields ]
			self.fields     = [ f.upper() for f in self.arcpycursor.fields ]
			#FIXME!!! fieldTypeLUT
			self.fieldtypes = dict([ (f.name.upper(),fieldTypeLUT[f.type]) for f in arcpy.ListFields(self.effective_filename)  ])


		elif usingOGR:

			if not self.dataSource:
				self.dataSource = self.openDataSource()
			try:
				self.ogrDataLayer = self.dataSource.GetLayer(self.layer)
			except AttributeError as e:
				print (e)
				print("Error opening Layer... Does it exist?")
				raise (e)

			try:
				self.layerDefinition = self.ogrDataLayer.GetLayerDefn()
			except AttributeError as e:
				#Can't find the layer
				print (e)
				print("Error getting Layer Definition... Does the layer exist?")
				raise (e)

			self.fields = [ self.layerDefinition.GetFieldDefn(i).GetName().upper() for i in range(self.layerDefinition.GetFieldCount()) ]
			#for i in range(self.layerDefinition.GetFieldCount()):
			#	print ( "{name} {rawtype}: {cookedtype}".format( \
			#		name=self.layerDefinition.GetFieldDefn(i).GetName().upper() ,
			#		rawtype=self.layerDefinition.GetFieldDefn(i).GetType(),
			#		cookedtype=fieldTypeLUT[self.layerDefinition.GetFieldDefn(i).GetType()]
			#		)
			#	)
			self.fieldtypes = dict([ ( self.layerDefinition.GetFieldDefn(i).GetName().upper(), fieldTypeLUT[self.layerDefinition.GetFieldDefn(i).GetType()]  )     for i in range(self.layerDefinition.GetFieldCount()) ] )
			#print ("Field Types for Table {tablename} : {ft}".format(ft=self.fieldtypes, tablename=self.layerDefinition.GetName()))


		self.getSRName()
		self.get_knows_about_null()

		return True

	def openDataSource(self):
		"""
		opens the datasource (ogr terms, that is the DataSource..., arcpy the container only)
		This function is used so we can then proceed to list layers...
		"""
		if usingArcpy :
			pass
		elif usingOGR:
			# If we are using OGR, then adjust the parameters from "ArcPy Format"
			(self.effective_filename, self.layer, self.fileformattype) = self.convert_arcpy_featureclass_name_to_ogr(self.filename, self.layer, self.geometry_type)
			self.dataSource = ogr.Open(self.effective_filename)
		return self.dataSource


	def _arcpy_wrap_SearchCursor(self, fn=None, lyr=None, field_names=['*']):
		"""Uses Duck testing to find an openable table in arcpy
		Tries to open with self.filename. If that fails, open with self.filename\self.layer
		"""
		if fn is None: fn=self.filename
		if lyr is None: lyr=self.layer

		#Is it a dir of coverages?
		#if self.check_for_shapefile_dir(self.filename):
		#	return self.get_shapefile_layers(self.filename)
		if self.check_for_coverage_dir(self.filename):
			#return self.get_coverage_layers(self.filename)
			if lyr is None:
				self.get_coverage_layers(self.filename)[0]
		elif lyr is None:
			lyr= ''

		try:
			self.effective_filename = fn
			return arcpy.da.SearchCursor(fn,field_names=field_names)
		except RuntimeError:
			pass

		# try it again concatenating filename and layer - did that help?
		try:
			self.effective_filename = '\\'.join([fn,lyr])
			self.layer = lyr
			return arcpy.da.SearchCursor(self.effective_filename,field_names=field_names)
		except RuntimeError:
			pass

		# try it again concatenating filename, geometry_type - did that help?
		try:
			self.effective_filename = '\\'.join([fn,self.geometry_type])
			self.layer = lyr
			return arcpy.da.SearchCursor(self.effective_filename,field_names=field_names)
		except RuntimeError:
			pass

		# try it again concatenating filename, layer, geometry_type - did that help?
		try:
			self.effective_filename = '\\'.join([fn,lyr,self.geometry_type])
			self.layer = lyr
			return arcpy.da.SearchCursor(self.effective_filename,field_names=field_names)
		except RuntimeError:
			pass

		# try it again concatenating filename, layer and extension - did that help?
		try:
			self.effective_filename = '\\'.join([fn,lyr+'.shp'])
			self.layer = lyr
			return arcpy.da.SearchCursor(self.effective_filename,field_names=field_names)
		except RuntimeError:
			pass

		# try again, but this time with the name of the first layer encountered...
		try:
			lyr = self.listLayers()[0]
			self.layer=lyr
			self.effective_filename= '\\'.join([ fn,lyr ])
			#print ("\t\t\t\t_arcpy_wrap_SearchCursor trying {0}".format(self.effective_filename))
			return arcpy.da.SearchCursor(self.effective_filename,field_names=field_names)
		except RuntimeError:
			pass

		self.effective_filename = None
		return None #failsafe return... making implicit explicit

	def check_for_coverage(self,filename=None):
		"""Checks if a filename is a coverage. A Coverage has a parent
		directory subdirectory info and contains ADF files"""
		if filename is None:
			filename = self.filename
		try:
			if os.path.isdir(filename) and os.path.isdir(os.path.join(os.path.dirname(filename),'info')):
				#glob to get list of directories containing ADF files
				# Return List...
				#print "\t\tget_coverage_layers(%s): %s"%(filename,[os.path.split(g)[1] for g in glob.glob(os.path.join(filename,'*')) if glob.glob(g+r'\*.adf') ])
				return len( [os.path.split(g)[1] for g in glob.glob(os.path.join(filename,'*.adf')) ] ) > 0
		except TypeError: #This is what happens with a Unicode filename...
			pass
		return False

	def check_for_coverage_dir(self,filename=None):
		"""If this is a coverage, it will have an info directory and
		a series of layer directories containing .adf files...
		Return true if that is the case.
		"""
		if filename is None:
			filename = self.filename
		try:
			if len(self.get_coverage_layers(filename)) > 0:
				return True
		except TypeError:
			pass  #Returned not a list...
		return False

	def get_coverage_layers(self,filename=None):
		"""If this is a coverage, it will have an info directory and
		a series of layer directories containing .adf files...
		Return the series of layers.
		"""
		if filename is None:
			filename = self.filename
		if os.path.isdir(filename) and os.path.isdir(os.path.join(filename,'info')):
			#glob to get list of directories containing ADF files
			# Return List...
			#print "\t\tget_coverage_layers(%s): %s"%(filename,[os.path.split(g)[1] for g in glob.glob(os.path.join(filename,'*')) if glob.glob(g+r'\*.adf') ])
			return [os.path.split(g)[1] for g in glob.glob(os.path.join(filename,'*')) if glob.glob(os.path.join(g,'*.adf')) ]

	def get_coverage_type(self, filename, layer=None):
		tempfilehandle = ogr.Open(filename)
		if tempfilehandle is not None:
			#This is where ogr and arcpy differ... None means the file is not openable...
			#print "File is a %s"%(tempfilehandle.GetDriver().GetName())
			if tempfilehandle.GetDriver().GetName() == 'AVCBin' :
				if layer in [None] :
					# Assume polygon if present, else polyline if present, else point...
					if 'PAL' in self.listLayers(dataSource=tempfilehandle):
						layer = 'PAL'
					elif 'ARC' in self.listLayers(dataSource=tempfilehandle):
						layer = 'ARC'
					elif 'LAB' in self.listLayers(dataSource=tempfilehandle):
						layer = 'LAB'
			#if tempfilehandle[layer]: print "Found layer %s"%(tempfilehandle[layer].GetName())
			tempfilehandle.Destroy()
		return layer

	def check_for_shapefile_dir(self,filename=None):
		"""If this is a coverage, it will have a series of .shp files in it...
		Return true if that is the case.
		"""
		if filename is None:
			filename = self.filename
		try:
			if len(self.get_shapefile_layers(filename))>0:
				return True
		except TypeError:
			pass
		return False

	def get_shapefile_layers(self,filename=None):
		"""If this is a shapefiledir, it will have a series of .shp files in it...
		Return the series of *.shp.
		"""
		if filename is None:
			filename = self.filename
		if os.path.isdir(filename) :
			#glob to get list of SHP files
			# Return List...
			return [os.path.splitext(  os.path.split(g)[1] )[0] for g in glob.glob(os.path.join(filename,'*.shp')) ]

	def check_for_fgdb(self,filename):
		""" If this is a fgdb, it will have *.gdbindexes..."""
		if os.path.isdir(filename) :
			#glob to get list of SHP files
			# Return List...
			if [os.path.split(g) for g in glob.glob(os.path.join(filename,'*.gdbindexes')) ]:
				return True
		return False


	def close(self):
		pass

	def row(self,do_upper=False):
		global usingOGR
		global usingArcpy
		if usingOGR:
			try:
				inFeature = self.ogrDataLayer.GetNextFeature()
				values = [ inFeature.GetField(i) for i in range(len(self.fields)) ]
				theRow =  dict(zip(self.fields,values))
				theRow['shape'] = inFeature.GetGeometryRef()
			except AttributeError as e:
				raise StopIteration

			#Isolate the crash fault that comes from this pesky thing...
			try:
				theRow['__centroid__'] = inFeature.GetGeometryRef().Centroid().ExportToWkt()
			except Exception as e :  # IllegalArgumentException as e:
				# OGR seems to require that we cannot specify the error
				# so we are going to catch anything...
				# As a secondary attempt, we will use the bounding box
				# coordinates to generate the WKT string from the centre
				# of the bounding box.
				geom_poly = inFeature.GetGeometryRef()
				geom_poly_envelope = geom_poly.GetEnvelope()
				minX = geom_poly_envelope[0]
				minY = geom_poly_envelope[2]
				maxX = geom_poly_envelope[1]
				maxY = geom_poly_envelope[3]
				coord4 = minX+(maxX-minX)/2, minY+(maxY-minY)/2
				theRow['__centroid__'] = 'POINT ({0} {1})'.format(*coord4)
				#print ("Exception {0}".format(str(e)))


		elif usingArcpy:
			inFeature = self.arcpycursor.next()
			theRow =  dict(zip(self.fields,inFeature))
			theRow['__centroid__'] = 'POINT ({0} {1})'.format(*theRow['SHAPE@TRUECENTROID'])
		else:
			raise StopIteration

		# Upper case all text strings
		if do_upper:
			for k,v in theRow.iteritems():
				try:
					theRow[k] = v.upper()
				except AttributeError:
					pass
		self.currentRow = theRow
		return theRow

	def reset(self):
		"""Reset to first row in table"""
		global usingOGR
		global usingArcpy
		if usingArcpy :
			self.arcpycursor.reset()
		elif usingOGR :
			self.ogrDataLayer.ResetReading()

	def uniqueValueTest(self,fieldname):
		"""Count Rows in Table. Warning - resets place in table..."""
		self.reset()
		u = []
		try:
			while True:
				u.append( self.row()[fieldname])
		except StopIteration :
			pass
		self.reset()
		su = set(u)
		return ( len(u) == len(su) ) #True means they are the same...

	def numRows(self):
		"""Count Rows in Table. Warning - resets place in table..."""
		if usingArcpy :
			#SearchCursor doesn't tell us how many... so go count...
			i=0
			self.reset()
			try:
				while True:
					self.arcpycursor.next()
					i += 1
			except StopIteration :
				pass
			self.reset()
		elif usingOGR :
			self.ogrDataLayer.GetFeatureCount(force = 1)
			self.reset()

	def numLayers(self):
		return len(self.listLayers()) #cycle wasteful, but keyboard efficient

	def listLayers(self,filename=None,dataSource=None, coverage_topology=False):
		global usingOGR
		global usingArcpy

		if filename is None:
			filename = self.filename

		if self.check_for_shapefile_dir(filename):
			return self.get_shapefile_layers(filename)
		if self.check_for_coverage_dir(filename):
			return self.get_coverage_layers(filename)

		if usingOGR:
			# FIXME: Adapt for using the multi-fc-directory
			if self.check_for_coverage(filename):
				if not coverage_topology:
					return [ os.path.basename(filename) ]

			if dataSource is None:
				dataSource = self.dataSource
			if dataSource is None:  # still None...
				dataSource = self.openDataSource()
			#if dataSource is None:  # Maybe it's a shapefile dir...

			try:
				#if dataSource is not None:
				return [ l.GetName() for l in dataSource ]
			except AttributeError : # Probably a coverage...
				pass
			except TypeError : # Not open (dataSource is None...
				return self.get_shapefile_layers()

			### Do something...
			return None

		elif usingArcpy:
			arcpy.env.workspace = filename
			if self.check_for_coverage(filename):
				if coverage_topology:
					return arcpy.ListFeatureClasses()
				else:
					return [ os.path.basename(filename) ]

			return arcpy.ListFeatureClasses()

	def get_spatialdataformat(self):
		if usingOGR:
			return self.dataSource.GetDriver().GetName()
		else:
			return None

	def convert_arcpy_featureclass_name_to_ogr(self, filename, layer, geometry_type=None):
		"""arcpy featureclass names may be = FGDB_filename.gdb\featureclass or FGDB_filename.gdb\dataset\featureclass .
		This function attempts to identify an OGR (filename,layer) from a arcpy filename
		"""
		#Get the parent and grandparent containers, in case of multi-featureclass
		#  file formats (GPKG, SQLite, FGDB)
		global usingOGR
		global usingArcpy

		fileformattype = None
		#	[ 'Database' ,'Single Feature Class per file' , 'Multiple Feature Class per file', 'Multiple Feature Class per file with Schemas', ]
		parent,child = os.path.split(filename)
		grandparent,grandchild = os.path.split(parent)[0], os.path.join(os.path.split(parent)[1],child)

		#Check that the layer is a string, because unicode borks things..
		if isinstance(layer,unicode):
			layer = str(layer)


		if usingOGR:
			if filename[0:3] == 'PG:' :
				# filename is a postgis connection string...
				fileformattype = 'Database'
			elif os.path.exists(filename) and not os.path.isdir(filename):
				#File is openable, use default layers if not spec'd
				#This is either a single featureclass container, or a
				# multi-featureclass container where the user specified
				# The package name, not os.path.join(package,layer)...
				try:
					#Test if file is openable... Just for a duck test...
					tempfilehandle = ogr.Open(filename)
					if tempfilehandle is not None:
						#This is where ogr and arcpy differ... None means the file is not openable...
						#print "File is a %s"%(tempfilehandle.GetDriver().GetName())
						fileformattype = 'Single_FC_file'
						if tempfilehandle.GetDriver().GetName() == 'AVCBin' :
							if layer in [None,0] and geometry_type is not None:
								layer = {'Polygon':'PAL','Point':'LAB','Polyline':'ARC'}[geometry_type]
							elif layer in [None, 0] :
								# Assume polygon if present, else polyline if present, else point...
								if 'PAL' in tempfilehandle.listLayers():
									layer = 'PAL'
								elif 'ARC' in tempfilehandle.listLayers():
									layer = 'ARC'
								elif 'LAB' in tempfilehandle.listLayers():
									layer = 'LAB'
						elif len(tempfilehandle) > 1:
							fileformattype = 'Multi_FC_file'
						if layer is None:
							# default to the first layer if they don't specify it.
							#			This allows us to use None as a "Take the defaults"
							#			option, rather than 0, which explicitly
							#			specifies the first layer.
							layer = 0
						tempfilehandle.Destroy()
				except ImportError:
					pass
				except NameError:
					pass
			elif os.path.isdir(filename):
				# Is this a coverage?
				if self.check_for_coverage(filename):
				#if (self.get_coverage_type(filename,None)):
					fileformattype = 'Single_FC_file'
					#print ("Coverage basename!layer: {0}!{1} as {2}".format(os.path.basename(filename),layer,geometry_type))
					#This is a coverage...
					if layer in [None,0, os.path.basename(filename) ] and geometry_type is not None:
						layer = {'Polygon':'PAL','Point':'LAB','Polyline':'ARC'}[geometry_type]
					else:
						layer = self.get_coverage_type(filename,None)
					#print ("Coverage filename!layer: {0}!{1}".format(filename,layer))
				#is this a directory of shapefiles or coverages?
				#####################
				# Coverage Dir
				elif (self.check_for_coverage_dir(filename)):
					fileformattype = 'Multi_FC_Dir'
					if layer in [ None, 0 ]:
						layer = self.get_coverage_layers(filename)[0]

					filename = os.path.join(filename, layer )
					if geometry_type is not None:
						layer = {'Polygon':'PAL','Point':'LAB','Polyline':'ARC'}[geometry_type]
					else:
						layer = self.get_coverage_type(filename,None)
					print ("CovDir filename!layer: {0}!{1}".format(filename,layer))
				#####################
				# SHAPEFILE Dir
				elif (self.check_for_shapefile_dir(filename)):
					fileformattype = 'Multi_FC_Dir'
					if layer in [ None, 0 ]:
						layer = self.get_shapefile_layers(filename)[0]
						#filename = os.path.join(filename, layer+".shp"  )
					#else:
						#filename = os.path.join(filename, layer+".shp" )
					#layer = 0
					#print ("I think I am a shapefile dir") #FIXME - Remove Print
				#####################
				# FGDB
				elif (self.check_for_fgdb(filename)):
					fileformattype = 'Multi_FC_file'
					tempfilehandle = ogr.Open(filename)
					#print "File is a %s"%(tempfilehandle.GetDriver().GetName())   #FIXME - Remove Print
					if tempfilehandle is not None:
						if layer is None: layer = self.listLayers(dataSource=tempfilehandle)[0]
						tempfilehandle.Destroy()
			elif os.path.exists(parent):
				print ("trying to open parent => %s"%(parent))
				#Test if file is openable... Just for a duck test...
				tempfilehandle =  ogr.Open(parent)
				if tempfilehandle is not None:
					#This is where ogr and arcpy differ... None means the file is not openable...
					#print "File is a %s"%(tempfilehandle.GetDriver().GetName())
					fileformattype = 'Multi_FC_file'
					if layer is None:
						layer = child # default to the child layer if they don't specify it.
						#			This allows us to use None as a "Take the defaults"
						#			option, rather than 0, which explicitly
						#			specifies the first layer.
					self.filename = parent
					#if tempfilehandle[layer]: print "Found layer %s"%(tempfilehandle[layer].GetName())
					tempfilehandle.Destroy()
			elif os.path.exists(grandparent):
				#print "trying to open grandparent => %s"%(grandparent)
				#Test if file is openable... Just for a duck test...
				tempfilehandle =  ogr.Open(grandparent)
				if tempfilehandle is not None:
					#This is where ogr and arcpy differ... None means the file is not openable...
					#print "File is a %s"%(tempfilehandle.GetDriver().GetName())
					fileformattype = 'Multi_FC_file_schema'
					if layer not in tempfilehandle.listLayers():
						if grandchild in tempfilehandle.listLayers():
							layer = grandchild
							fileformattype = 'Multi_FC_file_schema'
						elif child in tempfilehandle.listLayers():
							layer = child
							fileformattype = 'Multi_FC_file'
							# OGR Treats the schema concept differently than ArcPy... I don't know if we could ever get here, but it's worth checking...
					#if tempfilehandle[layer]: print "Found layer %s"%(tempfilehandle[layer].GetName())
					self.filename = grandparent
					tempfilehandle.Destroy()

			#FIXME: Handle the case for Multi_FC_Dir that is not a FGDB...
			# This was removed, but the ideas need to be kept...
			#elif os.path.isdir(filename):
				## We have a directory of single-file entities masquerading
				## as a Multi_FC_File...
				#listoftablefilenames = glob.glob(os.path.join(filename, '*.shp'))
				#if listoftablefilenames:
					#if layer in [None, 0]:
						#layer = listoftablefilenames[0]
					#else:
						##look for layer by wildcard...
						#self.filename = glob.glob(os.path.join(filename, '*%s*.shp'))[0]
					#
					#fileformattype="Multi_FC_Dir"
					#return (self.filename, None, fileformattype)

			#print "\t(filename=%s, layer=%s, fileformattype=%s)"%(filename, layer, fileformattype)
			return (filename, layer, fileformattype)

	def getSRName(self):
		global usingOGR
		global usingArcpy
		#Daniel - ArcPy code to get the Spatial Reference name for the summary report
		if usingArcpy :
			#import arcpy
			#cf.pyprint("ArcPy self.filename value: %s"%(self.filename))
			desc = arcpy.Describe(self.effective_filename)
			SRName = desc.spatialReference.name.replace('_',' ')
			self.SRAuth = 'EPSG'
			self.SRCode = desc.spatialReference.factoryCode

		#James - OSGEO code to get the Spatial Reference name for the summary report for file geodatabases
		if usingOGR:	#OSGEO file geodatabase driver access to spatial reference
			#import ogr
			sr = self.ogrDataLayer.GetSpatialRef()
			if not sr:
				SRName = "Unknown SRS"
			elif sr.IsProjected:
				SRName = sr.GetAttrValue('projcs')
			else:
				SRName = sr.GetAttrValue('geogcs')

			self.SRAuth = sr.GetAttrValue("AUTHORITY", 0)
			self.SRCode = sr.GetAttrValue("AUTHORITY", 1)

			self.SRName = "{0}:{1}".format(self.SRAuth,self.SRCode)
			print ("\tTESTING: SRName is: {0}, ( {1}:{2} )".format(SRName, self.SRAuth, self.SRCode ))

			#Troubleshooting:   <sigh>
			if SRName == 'UTM Zone 18, Northern Hemisphere':  self.SRAuth, self.SRCode  = ( 'EPSG','26918' )
			if SRName == 'UTM Zone 17, Northern Hemisphere':  self.SRAuth, self.SRCode  = ( 'EPSG','26917' )
			if SRName == 'UTM Zone 16, Northern Hemisphere':  self.SRAuth, self.SRCode  = ( 'EPSG','26916' )
			if SRName == 'UTM Zone 15, Northern Hemisphere':  self.SRAuth, self.SRCode  = ( 'EPSG','26915' )

		#print ("SRS IS {0}".format(SRName))
		self.SRName = SRName
		return SRName

	def get_knows_about_null(self):
		global usingOGR
		global usingArcpy
		if usingArcpy :
			desc = arcpy.Describe(self.effective_filename)
			if hasattr(desc, "dataType"):
				if desc.dataType == "CoverageFeatureClass":
					self.knows_about_null = False
		if usingOGR:
			if self.dataSource.GetDriver().GetName() == 'AVCBin':
				self.knows_about_null = False

		#print "===================== self.knows_about_null = %s"%(str(self.knows_about_null))

def main(args):
	if usingOGR:
		print ("Using OGR")
	#f = fileloader(r'C:\GIS\FMPDSCache\NWR\fmpds_NWR.gpkg')
	#C:\GIS\lappe\OCupData.sqlite
	filename = r'\GIS\Black_Sturgeon_FMP_2011\MU815_2011_FMP_P1.gpkg\pcc'
	print ("GPKG Test with layer spec'd inline to filename ==> %s"%(filename))
	f = fileloader(filename)
	print (f.listLayers())
	f.close()

	print ("\n\n")
	filename = r'\GIS\Black_Sturgeon_FMP_2011\MU815_2011_FMP_P1.gpkg'
	layer='pcc'
	print ("GPKG Test with layer spec'd parallel to filename ==> %s %s"%(filename,layer))
	f = fileloader(filename,layer)
	f.close()


	print ("\n\n")
	print ("Reporting Filetype...")
	for filename in [  \
#			r'\GIS\Black_Sturgeon_FMP_2011\MU815_2011_FMP_P1.gpkg',
#			'C:\GIS\Black_Sturgeon_FMP_2011\original_E00\mu815_11aoc02.e00',
#			'C:\GIS\lappe\classic_interval.geojson',
#			'C:\GIS\Black_Sturgeon_FMP_2011\E00\mu815_11aoc02',
#			'C:\GIS\Black_Sturgeon_FMP_2011\E00',
#			r'C:\GIS\LAStools\data',
#			r'C:\src\FMPDS\mu999_2020_fmpdp\mu999_20wxi00.shp',
#			r'C:\src\FMPDS\mu999_2020_fmpdp',
			r'h:\FOREST_MGMT\FMPDS\Bancroft_Minden\FMP\2016\E00\mu220_16prc00',
			r'h:\FOREST_MGMT\FMPDS\Bancroft_Minden\FMP\2016\E00',
			#r'\\lrcpptboshfs001\MNR_SR_External\FOREST_MGMT\FMPDS\Bancroft_Minden\FMP\2016\E00\mu220_16prc00',
			#r'\\lrcpptboshfs001\MNR_SR_External\FOREST_MGMT\FMPDS\Bancroft_Minden\FMP\2016\E00',
			]:
		print ("Trying %s"%(filename))
		f = fileloader(filename)
		#print f.effective_filename
		print ("\tfiletype %s ==> %s"%( f.get_spatialdataformat(), f.listLayers()) )
		f.close()
		#print ""
		print ("Listing Layers without opening the file... %s"%(fileloader(filename,openTable=False).listLayers()))

		print (" ... Complete")



	return 0

def arcpytest():
	filename = r'\\lrcpptboshfs001\MNR_SR_External\FOREST_MGMT\FMPDS\Algonquin_Park\AR\2016\_data\FMP_Schema.gdb'
	layer= 'mu451_16wtx00'
	f = fileloader(filename,layer)

#For Testing in ArcPy:
"""
import sys
sys.path.insert(0, '..')
import stack.fileloader
reload( stack.fileloader )
stack.fileloader.fileloader(filename=r'\\lrcpptboshfs001\MNR_SR_External\FOREST_MGMT\FMPDS\Algonquin_Park\AR\2016\_data\FMP_Schema.gdb',layer='mu451_16wtx00').fields
stack.fileloader.fileloader(filename=r'\\lrcpptboshfs001\MNR_SR_External\FOREST_MGMT\FMPDS\Algonquin_Park\AR\2016\_data\FMP_Schema.gdb',layer='mu451_16wtx00',openTable=False).listLayers()
"""

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
