#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  fs_context.py
#
#  Copyright 2019 CARRAN <CARRAN@ON34C02585177>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
import os
import glob
import json

from static_db import static_db


#filesystem_context Thingummy....
#    "fmu": "Kenora",
#    "fmu_id": "644",
#    "id": 24743,
#    "name": "metadata",
#    "path": "{fmu}\\{product}\\{year}",
#    "phase": "",
#    "planyear": "2012",
#    "product": "AR",
#    "submission_max_year": 2030,
#    "submission_min_year": 2017,
#    "submission_type": "AR_2018",
#    "year": "2017"

class fs_context():
	def __init__(self,path):
		self.path = path
		self.context = self.get_context(n=4)
		if self.context is None:
			self.context = self.get_filesystem_context(n=5)


	def dirname(self, f=None, n=1):
		if f is None:
			f = self.path
		if n < 1:
			return f
		elif n==1:
			return os.path.dirname( f )
		else :
			return self.dirname(os.path.dirname( f ),n-1)

	def get_context(self,f=None,n=4,pattern="fi_submission_*.txt"):
		if f is None:
			f = self.path

		for i in range(1,n):
			fnlist = glob.glob(os.path.join(self.dirname(n=i),pattern))
			if fnlist == []:
				continue # trye the next level
			elif len(fnlist) > 1 :
				return None # I can't tell what file to use...
			else:
				return json.load(open(fnlist[0],"r"))

	def get_filesystem_context(self,f=None,n=4):
		year = None
		product = None
		fmu = None
		rdict = dict()
		if f is None:
			f = self.path

		rdict['path']=self.dirname(n=1)
		rdict["submission_type"]=None
		rdict['id']='99999'
		rdict['fmu_id']= '999'
		rdict["planyear"]=9999

		for i in range(0,n):
			#print "\ti {0}".format(i)
			v = os.path.basename(self.dirname(n=i))
			#print "\t\tv {0}".format(v)
			if v.lower() == 'consolidated':
				#print "\t\tconsolidated"
				rdict['year'] = 9999
			elif v.isdigit() and  'product' not in rdict.keys():
				#print "\t\tyear {0}".format(v)
				year=int(v)
			elif 'product' not in rdict.keys() and v.lower() in ['ar','aws','fmp','pci','bmi' ] :
				#print "\t\tproduct {0}".format(v)
				rdict['product'] = v.upper()
			elif 'product' in rdict.keys():
				#print "\t\tfmu {0}".format(v)
				#Next one after product is FMU
				fmu = v.replace('-','_')
				rdict['fmu'] = fmu
				try:
					rdict['fmu_id']= static_db[fmu][0]
				except IndexError:
					rdict['fmu_id']= '999'

				if product in ['FMP','PCI','BMI' ]:
					planyear = year
				else:
					try:
						# plan start year has to be less than or equal to the product submission year
						query = [planyr for planyr in static_db[fmu][1] if planyr <= int(rdict['year']) ]
						# if there are more than one plan year that is less than or equal to current submission year, choose the most recent one
						query.sort()
						if len(query)>0:
							planyear =  query[-1]
						# if there's no plan year that fits the above criteria, return 0
						else:
							planyear = 0
					except IndexError:
						planyear=9999

				rdict["planyear"]=planyear
				#print rdict
				return rdict
		return rdict


def main(args):
	print r"Starting with F:\Kenora\AR\2017\LAYERS\MU644_2017_AR.gdb"
#	print fs_context(r'F:\Kenora\AR\2017\LAYERS\MU644_2017_AR.gdb')
#	print fs_context(r'F:\Kenora\AR\2017\LAYERS\MU644_2017_AR.gdb').dirname()
#	print fs_context(r'F:\Kenora\AR\2017\LAYERS\MU644_2017_AR.gdb').dirname(n=3)
#	print fs_context(r'F:\Kenora\AR\2017\LAYERS\MU644_2017_AR.gdb').context
	print fs_context(r'F:\Kenora\AR\CONSOLIDATED\consolidated_ar.gpkg').context
	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
