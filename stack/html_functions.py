#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  html_functions.py
#  
#  Copyright 2018 CARRAN <CARRAN@ON34C02585177>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import os
import sys
import css
import datetime
import re
from inspect import currentframe, getframeinfo

def write2File(outfilePath, outfileName, infilePath, inFileName, writeMethod, textVariable, inputType,logFile):
	blnLog = write2LogFile(logFile,"def-write2File module." + '\n' + "Input variables - out file path: " + outfilePath + "\n out file name: " + outfileName + "\n in file path: " + infilePath + "\n in file name: " + inFileName + "\n write method: " + writeMethod + "\n input type: " + inputType + "\n String length: " + str(len(textVariable)))
	if inputType == 'file':
		frameInfo = getframeinfo(currentframe())
		blnLog = write2LogFile(logFile, "Module: " + frameInfo.filename + " at line no : " + str(frameInfo.lineno) + "\n")
		blnLog = write2LogFile(logFile,"Processing a file: " + inFileName)
		if os.path.isfile(os.path.join(infilePath,inFileName)) and os.access(os.path.join(infilePath,inFileName),os.R_OK):
			#valid path and file is readable
			frameInfo = getframeinfo(currentframe())
			blnLog = write2LogFile(logFile, "Module: " + frameInfo.filename + " at line no : " + str(frameInfo.lineno) + "\n")
			try:
				with open(os.path.join(infilePath,inFileName),'r') as inputFile:
					if writeMethod == 'w':
						#writing to file
						newFile = open(os.path.join(outfilePath,outfileName),'w')
						for line in inputFile:
							newFile.write(line)

##						newFile.close()
						return True

					else:
						#appending to a file
						#is the output file path valid
						frameInfo = getframeinfo(currentframe())
						blnLog = write2LogFile(logFile, "I am in: " + frameInfo.filename + " at line no : " + str(frameInfo.lineno) + "\n")
	 				   	if os.path.isfile(os.path.join(outfilePath,outfileName)) and os.access(os.path.join(outfilePath,outfileName),os.R_OK):
							with open(os.path.join(outfilePath,outfileName),'a') as appendFile:
								for line in inputFile:
									appendFile.write(line)

							return True

			except IOError as e:
				blnLog = write2LogFile(logFile,"I/O Error({0}): {1}".format(e.errno,e.strerror))
				return False
			except:
				blnLog = write2LogFile(logFile,"Unexpected error: ",sys.exc_info()[0])
				return False
		else:
			#something is wrong with the input path and  or file is not readable
			pyprint("Unexpected error: invalid input path or file is not readable.")
			return False
	else:
		#text or string
		#write to new file
		frameInfo = getframeinfo(currentframe())
		blnLog = write2LogFile(logFile, "Module: " + frameInfo.filename + " at line no : " + str(frameInfo.lineno) + "\n")
		blnLog = write2LogFile(logFile,"Processing as string.")
		blnLog = write2LogFile(logFile,"Length of text variable: " + str(len(textVariable)))
		if len(textVariable) != 0:
			#valid text variable i.e., it is not empty
			try:
				if writeMethod == 'w':
					with open(os.path.join(outfilePath,outfileName),'w') as newFile:
						newFile.write(textVariable)

					return True
				else:
					#we are appending to an existing file
					 if os.path.isfile(os.path.join(outfilePath,outfileName)) and os.access(os.path.join(outfilePath,outfileName),os.R_OK):
							with open(os.path.join(outfilePath,outfileName),'a') as appendFile:
								appendFile.write(textVariable)

							return True

			except IOError as e:
				frameInfo = getframeinfo(currentframe())
				blnLog = write2LogFile(logFile, "Module: " + frameInfo.filename + " at line no : " + str(frameInfo.lineno) + "\n")
				blnLog = write2LogFile(logFile,"I/O Error({0}): {1}".format(e.errno,e.strerror))
				return False
			except:
				frameInfo = getframeinfo(currentframe())
				blnLog = write2LogFile(logFile, "Module: " + frameInfo.filename + " at line no : " + str(frameInfo.lineno) + "\n")
				blnLog = write2LogFile(logFile,"Unexpected error: ",sys.exc_info()[0])
				return False
		else:
			#something is wrong with the input path and  or file is not readable
			frameInfo = getframeinfo(currentframe())
			blnLog = write2LogFile(logFile, "Module: " + frameInfo.filename + " at line no : " + str(frameInfo.lineno) + "\n")
			blnLog = write2LogFile(logFile,"Unexpected error: invalid input path or file is not readable.")
			return False

def writeHTML(summarydict,reportfile):
	"""Write an HTML Table to a file containing a report on validation.
	
	This function writes an HTML table to a file assuming a dictionary
	with these fields:
		summarydict={
		'dummy1':{
			'lyrName':'Bob1', 
			'prj':26915, 
			'fieldValidation':'Valid',
			'fieldDefinitionCom':'IsThisAComment?' , 
			'recordValidation':'invalid record',
			'recordValidationCom':'for some reason', 
			'reference':'Book of Maynard, Chapters 5 through 23',
			} 
		}
	
	
	"""
   	tableSumDict = summarydict
	htmlfile = reportfile
	rep = open(htmlfile,'a')

	htmltext = ''
	htmltext += '''
	<table id="t02" style="font-family:arial;">
	  <tr>
		<th>Layer File Name</th>
		<th>Layer Short Name</th>
		<th>Projection</th>
		<th>Field Validation</th>
		<th style="width: 200px;">Field Validation Comments</th>
		<th>Record Validation</th>
		<th>Record Validation Comments</th>
		<th>FIM Reference</th>
	  </tr>
	  '''

	for k,v in tableSumDict.iteritems():
		htmltext += '<tr>'
		htmltext += '<td>%s</td>'%(k)
		htmltext += '<td>%s</td>'%(v['lyrName'])
		htmltext += '<td>%s</td>'%(v['prj'])

		if str(v['fieldValidation']) == 'Valid':
			htmltext += '<td><p id="p01">  %s  </p></td>'%(v['fieldValidation'])
		else:
			htmltext += '<td><p id="p03">  %s  </p></td>'%(v['fieldValidation'])

		htmltext += '<td style="width: 200px;">%s</td>'%(v['fieldDefinitionCom'])
		if str(v['recordValidation']) == 'Valid':
			htmltext += '<td><p id="p01">  %s  </p></td>'%(v['recordValidation'])
		else:
			htmltext += '<td><p id="p03">  %s  </p></td>'%(v['recordValidation'])

		htmltext += '<td>%s</td>'%(v['recordValidationCom'])
		htmltext += '<td>%s</td>'%(v['reference'])
		htmltext += '</tr>'

	htmltext += '</table>'

	rep.write(htmltext)

	rep.close()

def writeHTMLstring(summarydict, allShortnameNAlias, allExistingTables):
	"""Information required to create a summary table is parsed in the form of a dictionary - summarydict
		This function turns the dictionary into html string with table tags. This function also determines which layers are missing
        using a dictionary of all the layer names (allShortnameNAlias) and a list of existing tables (allExistingTables)
        
    Write an HTML Table to a string containing a report on validation.
	
	Assuming dictionaries with these fields:
	summarydict={
		'dummy1':{
			'lyrName':'Bob1', 
			'prj':26915, 
			'fieldValidation':'Valid',
			'fieldDefinitionCom':'IsThisAComment?' , 
			'recordValidation':'invalid record',
			'recordValidationCom':'for some reason', 
			'reference':'Book of Maynard, Chapters 5 through 23',
			} 
		}
	allShortnameNAlias={
		'SHR':'Short Name For Table',
		'MST':'Missing Table'
		}
	allExistingTables=[
		'mu493_SHR00.shp'
	]
	
	
	"""
   	tableSumDict = summarydict
##	htmlfile = reportfile
##	rep = open(htmlfile,'a')
##	frameInfo = getframeinfo(currentframe())
##	blnLog = write2LogFile(logFile, "Module: " + frameInfo.filename + " at line no : " + str(frameInfo.lineno) + "\n")

	# looking for the missing layers:
#	lyrList = allShortnameNAlias.keys() #[k for k in allShortnameNAlias]
#	existingLyrList = []
#	for filename in allExistingTables:
#		for lyrshortname in lyrList:
#			if lyrshortname in filename.upper():
#				existingLyrList.append(lyrshortname)

	#This should probably look more like:
	#Get list of layers common to both lists (needed and found)
	lyrList = allShortnameNAlias.keys()
	existingLyrList = sum([ [ s for s in allShortnameNAlias.keys() if s in n ]  for n in allExistingTables ],[])
	
	missingLyr = list(set(lyrList)^set(existingLyrList)) ## this will be an empty list if all the layers are there
	missingLyrFullName = [str(allShortnameNAlias[str(shortname)]) for shortname in missingLyr]

	# writing the html text:
	htmltext = ''
	if missingLyr:
		htmltext += '<p id="p02">Missing Layer(s): %s</p>'%(", ".join(missingLyrFullName))
	htmltext += '''
	<table id="t02" style="font-family:arial;">
	  <tr>
		<th>Layer File Name</th>
		<th>Layer Short Name</th>
		<th>Projection</th>
		<th>Field Validation</th>
		<th style="width: 200px;">Field Validation Comments</th>
		<th>Record Validation</th>
		<th>Record Validation Comments</th>
		<th>FIM Reference</th>
	  </tr>\n'''

	for k,v in tableSumDict.iteritems():
		htmltext += '\t<tr>\n'
		htmltext += '\t\t<td>%s</td>\n'%(k)
		htmltext += '\t\t<td>%s</td>\n'%(v['lyrName'])
		htmltext += '\t\t<td>%s</td>\n'%(v['prj'])

		if str(v['fieldValidation']) == 'Valid':
			htmltext += '\t\t<td><p id="p01">  %s  </p></td>\n'%(v['fieldValidation'])
		else:
			htmltext += '\t\t<td><p id="p03">  %s  </p></td>\n'%(v['fieldValidation'])

		htmltext += '\t\t<td style="width: 200px;">%s</td>\n'%(v['fieldDefinitionCom'])
		if str(v['recordValidation']) == 'Valid':
			htmltext += '\t\t<td><p id="p01">  %s  </p></td>\n'%(v['recordValidation'])
		else:
			htmltext += '\t\t<td><p id="p03">  %s  </p></td>\n'%(v['recordValidation'])

		htmltext += '\t\t<td>%s</td>\n'%(v['recordValidationCom'])
		htmltext += '\t\t<td>%s</td>\n'%(v['reference'])
		htmltext += '\t</tr>\n'

	htmltext += '</table>\n'
	if missingLyr:
		htmltext += '<p id="p02">Missing Layer(s): %s</p>'%(", ".join(missingLyrFullName))
	return htmltext


def test(args=None):
	summarydict={
		'dummy1':{
			'lyrName':'Bob1', 
			'prj':26915, 
			'fieldValidation':'Valid',
			'fieldDefinitionCom':'IsThisAComment?' , 
			'recordValidation':'invalid record',
			'recordValidationCom':'for some reason', 
			'reference':'Book of Maynard, Chapters 5 through 23',
			} 
		}
	writeHTML(summarydict=summarydict,reportfile=r"c:\temp\writeHTML_test.html")
	
	allShortnameNAlias={
		'SHR':'Short Name For Table',
		'MST':'Missing Table'
		}
	allExistingTables=[
		'mu493_SHR00.shp'
	]
	print writeHTMLstring(summarydict=summarydict, allShortnameNAlias=allShortnameNAlias, allExistingTables=allExistingTables)

	return 0

if __name__ == '__main__':
    import sys
    sys.exit(test(sys.argv))
