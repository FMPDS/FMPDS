#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  log_functions.py
#
#  Copyright 2018 CARRAN <CARRAN@ON34C02585177>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

######################################################################
# log file functions, as opposed to interface output
######################################################################

import os
import sys
import datetime


sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from stack.file_extras import fileExists

from stack.stack_choice import usingArcpy, usingOGR
if usingArcpy:
	from stack.stack_choice import arcpy
elif usingOGR:
	from stack.stack_choice import ogr


def get_temp_dir():
	"""Find a temp dir to use. Use environment variable $TEMP or $TMP, or if not ."""
	o = [ os.getenv(t) for t in [ 'TEMP', 'TMP', 'temp', 'tmp' , 'USERPROFILE', 'HOME'] if os.getenv(t) is not None ]
	if o:
		return o[0]
	else:
		return '.'
	#return r'c:\TEMP' # Most likely result, frankly...

def createLogFile(logFileName = "FICheckerLogFile.txt"):
	tempDir = get_temp_dir()

	log = os.path.join(tempDir,logFileName)
	try:
		if os.path.exists(log):
			os.remove(log)
	except OSError:
		pass
	return log

def createRowValidationsFile(rowvalidation_filename="rowValidationsMsgs.txt"):
	"""Create Row Validations uses createLogFile with a specific filename...
	Created to maintain backward compatibility.
	"""
	#FIXME:
	raise DeprecationWarning("createRowValidationsFile is deprecated. Use createLogFile with a specified filename instead.")
	createLogFile(rowvalidation_filename)

def write2LogFile(inFileName, message2Write):
	logDateTime = datetime.datetime.now().strftime("%y-%m-%d %H:%M:%S")
	if fileExists(inFileName=inFileName):
		try:
			with open(inFileName,'a') as logFile:
				logFile.write(logDateTime + " : " + message2Write +"\n")
			return True

		except IOError as e:
			interface_output.pyprint(logDateTime + " : " +"I/O Error({0}): {1}".format(e.errno,e.strerror))
			return False
		except:
			interface_output.pyprint(logDateTime + " : " +"Unexpected error: ",sys.exc_info()[0])
			return False
	else:
		#log file does not exist so create it and write message
		try:
			with open(inFileName,'w') as logFile:
				logFile.write(logDateTime + " : " + "initialized log file.\n")
				logFile.write(logDateTime + " : " + message2Write +"\n")
			return True
		except IOError as e:
			interface_output.pyprint(logDateTime + " : " +"I/O Error({0}): {1}".format(e.errno,e.strerror))
			return False
		except:
			interface_output.pyprint(logDateTime + " : " +"Unexpected error: "+str(sys.exc_info()[0]))
			return False

class logfile():
	filename = None
	handle = None
	closed = True
	use_datestamps = True

	accumulated_lines = [ ]

	def __init__(self,filename="app_logfile.txt",use_datestamps = True):
		self.filename = createLogFile(filename)
		with open(self.filename,'w') as logFile:
			logFile.write("")
		self.use_datestamps = use_datestamps

	def accumulatelines(self,linelist):
		self.accumulated_lines.extend(linelist)

	def accumulate(self,line):
		self.accumulated_lines.append(line)

	def writeaccumulate(self):
		self.writelines( self.accumulated_lines )
		self.clearaccumulate()

	def clearaccumulate(self):
		del self.accumulated_lines[:]

	def write(self,output):
		self.writelines( [output])

	def writelines(self,outputList):
		try:
			with open(self.filename,'a') as logFile:
				for output in outputList:
					if self.use_datestamps:
						logFile.write("%s - %s\n"%(datetime.datetime.now().strftime("%y-%m-%d %H:%M:%S") , output ) )
					else:
						logFile.write("%s\n"%(output))
			return True
		except IOError as e:
			return None



log=logfile()
if usingArcpy:
	def pyprint(string):
		''' Input: message string.  Output: both arcpy.AddMessage and print'''
		arcpy.AddMessage(string)
		log.write(string)
elif usingOGR:
	def pyprint(string):
		print (string)
		log.write(string)
pyprint("Logfile Created / Opened")


def main(args):
	fn = createLogFile("testing.txt")
	write2LogFile(r"c:\temp\dummylog.txt","Testing")
	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
