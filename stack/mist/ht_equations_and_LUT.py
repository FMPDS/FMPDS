#
# Equations and concepts derived from MIST and (hopefully) refined.
#
# The code at the bottom will produce a lookup table to operationalize this.
#
# This is the "Calculate HT" Code.
#
#
#
import sys
import os
import math

sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)))

# Done ### FIXME: is EXP equivalent to math.exp?... math.exp(x) is e ** x ... equivalent to e ** x... in postgresql is is too... so YES!
# Done ### FIXME: ^ is not a python operator... :-( Replace with **

ht_equations = {
	"AB": lambda AGE,SI,AGE2BH=5.0,BH_INDX_AGE=50.0:(4.2286*(SI*3.28)**0.7857*(1-math.exp(-0.0178*AGE))**(4.6219 *(SI*3.28)**(-0.3591)))/3.28 ,
	"AW": lambda AGE,SI,AGE2BH=5.0,BH_INDX_AGE=50.0:(4.1492*(SI*3.28)**0.7531*(1-math.exp(-0.0269*AGE))** (14.5384*(SI*3.28)**(-0.5811)))/3.28 ,
	"BD": lambda AGE,SI,AGE2BH=5.0,BH_INDX_AGE=50.0:(4.7633*(SI*3.28)**0.7576*(1-math.exp(-0.0194*AGE))**(6.5110 *(SI*3.28)**(-0.4156)))/3.28 ,
	"BF": lambda AGE,SI,AGE2BH=5.0,BH_INDX_AGE=50.0:(4.5 + 8.12*(SI*3.28)**0.6748*(1-math.exp(-0.0111*(AGE-AGE2BH)))**(6.4229*(SI*3.28)**(-0.4586)))/3.28 ,
	"BW": lambda AGE,SI,AGE2BH=5.0,BH_INDX_AGE=50.0:(4.5 + 2.4321*(SI*3.28-4.5)**0.9207*(1-math.exp(-0.0168*(AGE-AGE2BH)))**(1.5247*(SI*3.28-4.5)**(-0.1042)))/3.28 ,
	"BY": lambda AGE,SI,AGE2BH=5.0,BH_INDX_AGE=50.0:(6.0522*(SI*3.28)**0.6768*(1-math.exp(-0.0217*AGE))**(15.4232 *(SI*3.28)**(-0.6354)))/3.28 ,
	"CE": lambda AGE,SI,AGE2BH=5.0,BH_INDX_AGE=50.0:(1.5341*(SI*3.28)**1.0013*(1-math.exp(-0.0208*AGE))**(0.9986 *(SI*3.28)**(0.0012)))/3.28 ,
	"CH": lambda AGE,SI,AGE2BH=5.0,BH_INDX_AGE=50.0:(5.1493*(SI*3.28)**0.6948*(1-math.exp(-0.0248*AGE))**(20.9210 *(SI*3.28)**(-0.7143)))/3.28 ,
	"EW": lambda AGE,SI,AGE2BH=5.0,BH_INDX_AGE=50.0:(6.4362*(SI*3.28)**0.6827*(1-math.exp(-0.0194*AGE))**(10.9767 *(SI*3.28)**(-0.5477)))/3.28 ,
	"HE": lambda AGE,SI,AGE2BH=5.0,BH_INDX_AGE=50.0:(2.1493*(SI*3.28)**0.9979*(1-math.exp(-0.0175*AGE))** (1.4086*(SI*3.28)**(-0.0008)))/3.28 ,
	"LA": lambda AGE,SI,AGE2BH=5.0,BH_INDX_AGE=50.0:(4.5 + 1.5470*(SI*3.28-4.5)*(1-math.exp(-0.0225*(AGE-AGE2BH)))**1.1129)/3.28 ,
	"MH": lambda AGE,SI,AGE2BH=5.0,BH_INDX_AGE=50.0:(6.1308*(SI*3.28)**0.6904*(1-math.exp(-0.0195*AGE))**(10.1563*(SI*3.28)**(-0.5330)))/3.28 ,
	"OR": lambda AGE,SI,AGE2BH=5.0,BH_INDX_AGE=50.0:(6.1785*(SI*3.28)**0.6619*(1-math.exp(-0.0241*AGE))**(25.0185 *(SI*3.28)**(-0.7400)))/3.28 ,
	"PJ": lambda AGE,SI,AGE2BH=5.0,BH_INDX_AGE=50.0:1.3 + (4.1459*(SI-1.3)**0.6224)*(1- (1-((SI-1.3)/(4.1459*(SI-1.3)**0.6224))**(1/(1.3723*(SI-1.3)**(-0.0802))))**((AGE - AGE2BH)/BH_INDX_AGE))**(1.3723*(SI-1.3)**(-0.0802)) ,
	"PO": lambda AGE,SI,AGE2BH=5.0,BH_INDX_AGE=50.0:1.3+4.36*(SI-1.3)**0.6654*((1-(1-((SI-1.3)/(4.36*(SI-1.3)**0.6654))**(1/(1.2137*(SI-1.3)**(-0.0761))))**((AGE-AGE2BH)/50))**(1.2137*(SI-1.3)**(-0.0761))) ,
	"PR": lambda AGE,SI,AGE2BH=5.0,BH_INDX_AGE=50.0:0.3048*(1.8604*SI/0.3048*(1-math.exp(-0.020098*AGE))**1.439) ,
	"PW": lambda AGE,SI,AGE2BH=5.0,BH_INDX_AGE=50.0:0.3048*math.exp(0.8417*math.exp(8.6177/AGE)*(math.log(SI/0.3048)-0.5920)-74.70990/AGE+2.0862) ,
	"SB": lambda AGE,SI,AGE2BH=5.0,BH_INDX_AGE=50.0:1.3 + (16.95*(SI-1.3)**0.1136)*(1-(1-((SI-1.3)/(16.95*(SI-1.3)**0.1136))**(1/(0.6167*(SI-1.3)**(0.3116))))**((AGE-AGE2BH)/(BH_INDX_AGE)))**(0.6167 *(SI-1.3)**(0.3116)) ,
	"SW": lambda AGE,SI,AGE2BH=5.0,BH_INDX_AGE=50.0:1.3 + (16.5383*(SI-1.3)**0.2336)*(1-(1-((SI-1.3)/(16.5383*(SI-1.3)**0.2336))**(1/(2.8896*(SI-1.3)**(-0.2556))))**((AGE-AGE2BH)/(BH_INDX_AGE)))**(2.8896*(SI-1.3)**(-0.2556))
}


def test():
	from test_si_parameters import siid_tbl
	sp = "SB"
	SI = 11

	AGE=float(50)
	AGE2BH = float(siid_tbl[sp]['age2bh'])
	BH_INDX_AGE=float(siid_tbl[sp]['bh_indx_age'])

	print ("\tht_equations[sp]({age},{si},{age2bh},{bh_indx_age})".format(age=AGE,si=float(SI),age2bh=AGE2BH, bh_indx_age=BH_INDX_AGE))


	print ( "At {SI}: \tFunction" )
	for SI in range(10,25):

		e_ht = ht_equations[sp](AGE,float(SI),AGE2BH,BH_INDX_AGE)

		print ( "At  {SI} : \t{Function}".format(Function=e_ht, SI=SI ) )

	print ("Now try it with default a2bh and bh_indx_age")
	SI = 11

	print ("\tht_equations[sp]({age},{si})".format(age=AGE,si=float(SI)))
	print ("\t\t{results})".format(results=ht_equations[sp](AGE,float(SI))))

def main(*args):
	msg = list()

	sp_max_ages = dict(zip(ht_equations.keys(),[ 150 for sp in ht_equations.keys() ]))
	sp_max_ages['PJ'] = 120
	sp_max_ages['PO'] = 110
	sp_max_ages['BW'] = 110
	#TOL HDWDS 210
	#sp_max_ages[''] = 210
	sp_max_ages['PW'] = 210
	sp_max_ages['PR'] = 180

	divisor = 10
	if '--sqlite' in args:
		schema = ''
		insert_statement = "INSERT INTO ht_lut ( sp, age, si, ht ) VALUES ( '{sp}', {age}, {si}, {ht}) ;"
	else :
		schema = 'code.'
		insert_statement = "INSERT INTO code.ht_lut ( sp, age, si, ht ) VALUES ( '{sp}', {age}, {si}, {ht}) ;"
	msg.append(  "CREATE TABLE {schema}ht_lut ( id serial PRIMARY KEY, sp character varying(2), age numeric, si numeric, ht numeric, CONSTRAINT ht_lut_sp_age_si_key UNIQUE (sp, age, si)) ; ".format(schema=schema) )
	if '--sqlite' in args:
		msg.append( "CREATE INDEX ht_lut_sp_age_si_idx ON ht_lut (sp, age, si);")
	else:
		msg.append( "CREATE INDEX ht_lut_sp_age_si_idx ON {schema}ht_lut USING btree (sp COLLATE pg_catalog.\"default\", age, si);".format(schema=schema) )


	msg.append( "BEGIN ;")
	for sp,max_age in sp_max_ages.items():
		for age in range(20,max_age  ):
			for si_times_divisor in range(0,30*divisor+1):
				si = float(si_times_divisor) / float(divisor)
				try:
					v=dict(zip(['sp','age','si','ht'],[sp,age,si,ht_equations[sp](float(age),float(si))]))
					if v['ht'] > 0.0 and v['ht'] < 40:  # a reasonable valid range...
						msg.append( insert_statement.format(**v) )
				except TypeError as e:
					# Error caused by returned complex number
					# TypeError: '>' not supported between instances of 'complex' and 'float'
					pass
				except ValueError:
					# Can't always calculate a value...
					pass
				except ZeroDivisionError:
					# Can't always calculate a value...
					pass

	msg.append( "COMMIT ;")
	return msg

if __name__ == '__main__':  #generate the ht lookup table
	main(sys.argv[1:])
	for f in msg:
		print (f)
