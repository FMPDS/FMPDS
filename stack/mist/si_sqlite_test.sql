CREATE TABLE si_lut ( id serial PRIMARY KEY, sp character varying(2), age numeric, ht numeric, si numeric, CONSTRAINT si_lut_sp_age_ht_key UNIQUE (sp, age, ht)) ;
CREATE INDEX si_lut_sp_age_ht_idx ON si_lut (sp , age, ht);
BEGIN ;
INSERT INTO si_lut ( sp, age, ht, si ) VALUES ( 'BD', 20, 1.4, 10.4574031333) ;
INSERT INTO si_lut ( sp, age, ht, si ) VALUES ( 'BD', 20, 1.5, 6.69738846837) ;
INSERT INTO si_lut ( sp, age, ht, si ) VALUES ( 'BD', 20, 1.6, 6.66710538129) ;
INSERT INTO si_lut ( sp, age, ht, si ) VALUES ( 'BD', 20, 1.7, 6.89594238275) ;
INSERT INTO si_lut ( sp, age, ht, si ) VALUES ( 'BD', 20, 1.8, 7.18740834876) ;
COMMIT ;
