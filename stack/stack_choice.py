#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  stack_choice.py

try:
	import arcpy
	usingArcpy = True
	usingOGR = False
except ImportError as e:
	usingOGR = True
	usingArcpy = False
	# Import OGR for reading all those pesky shapes:
	try:
		from osgeo import ogr
	except ImportError:
		import ogr
	ogr.UseExceptions()

def main(args):
	if usingArcpy:
		print ("using ArcPy")
	if usingOGR:
		print ("using OGR")
	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
