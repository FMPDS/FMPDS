#static_db is a mock of pg_db that just returns static results...

static_db = {
#           fmu                     code        plan start years    region
        'Abitibi_River':        [   '110',      [2012, 2022],       'NE'    ],
        'Algoma':               [   '615',      [2010, 2020],       'NE'    ],
        'Algonquin_Park':       [   '451',      [2010, 2020],       'S'     ],
        'Armstrong':            [   '444',      [2005],             'NW'    ], #outdated?
        'Bancroft_Minden':      [   '220',      [2011, 2021],       'S'     ],
        'Big_Pic':              [   '067',      [2007, 2017],       'NE'    ], # will be a part of Pic_Forest in 2019
        'Black_River':          [   '370',      [2006],             'NW'    ], #outdated?
        'Black_Spruce':         [   '035',      [2011, 2021],       'NW'    ],
        'Black_Sturgeon':       [   '815',      [2011],             'NW'    ],
        'Caribou':              [   '175',      [2008, 2018],       'NW'    ],
        'Crossroute':           [   '405',      [2007, 2017],       'NW'    ],
        'Dog_River_Matawin':    [   '177',      [2009, 2019],       'NW'    ],
        'Dryden':               [   '535',      [2011, 2021],       'NW'    ],
        'English_River':        [   '230',      [2009, 2019],       'NW'    ],
        'French_Severn':        [   '360',      [2009, 2019],       'S'     ],
        'Gordon_Cosens':        [   '438',      [2010, 2020],       'NE'    ],
        'Hearst':               [   '601',      [2007, 2017],       'NE'    ],
        'Kenogami':             [   '350',      [2011, 2021],       'NW'    ],
        'Kenora':               [   '644',      [2012, 2022],       'NW'    ],
        'Lac_Seul':             [   '702',      [2011, 2021],       'NW'    ],
        'Lake_Nipigon':         [   '815',      [2011, 2021],       'NW'    ],
        'Lakehead':             [   '796',      [2007, 2017],       'NW'    ],
        'Magpie':               [   '565',      [2009, 2019],       'NE'    ],
        'Martel':               [   '509',      [2011, 2021],       'NE'    ],
        'Mazinaw_Lanark':       [   '140',      [2011, 2021],       'S'     ],
        'Nagagami':             [   '390',      [2011, 2021],       'NE'    ],
        'Nipissing':            [   '754',      [2009, 2019],       'NE'    ],
        'Northshore':           [   '680',      [2010, 2020],       'NE'    ],
        'Ogoki':                [   '415',      [2008, 2018],       'NW'    ],
        'Ottawa_Valley':        [   '780',      [2011, 2021],       'S'     ],
        'Pic_Forest':           [   '966',      [2019],             'NE'    ], # Amalgamation of Big_Pic and Pic_River as of 2019 plan
        'Pic_River':            [   '965',      [2006, 2013],       'NE'    ], # will be a part of Pic_Forest in 2019
        'Pineland':             [   '421',      [2011, 2021],       'NW'    ],
        'Red_Lake':             [   '840',      [2008, 2018],       'NW'    ],
        'Romeo_Malette':        [   '930',      [2009, 2019],       'NE'    ],
        'Sapawe':               [   '853',      [2010, 2020],       'NW'    ],
        'Spanish':              [   '210',      [2010, 2020],       'NE'    ],
        'Sudbury':              [   '889',      [2010, 2020],       'NE'    ],
        'Temagami':             [   '898',      [2009, 2019],       'NE'    ],
        'Timiskaming':          [   '280',      [2011, 2021],       'NE'    ],
        'Trout_Lake':           [   '120',      [2009, 2019],       'NW'    ],
        'Wabigoon':             [   '130',      [2008, 2018],       'NW'    ],
        'Whiskey_Jack':         [   '490',      [2012, 2022],       'NW'    ],
        'White_River':          [   '060',      [2008, 2018],       'NE'    ],
        'Whitefeather':         [   '994',      [2012, 2022],       'NW'    ]
        }


class pg_db():
	db_string = None
	db_type = "static"

	def __init__(self,db_string=None):
		pass

	def run_update(self,sql):
		pass


	def run_select(self,sql):
		pass

	def run_select_get_dict(self,sql):
		pass

	def run_sql_onevalue(self,sql):
		pass

	def get_list_of_fmus(self):
		listOfFMU = static_db.keys()
		return listOfFMU

	def get_list_of_fmus_that_arent(self,listtoignore):
		""" This is not actually doing its job"""
		#sql = "SELECT fmu from fmp.fmu_lut WHERE fmu not in ( %s ) ORDER BY id"%( "'" + "','".join( listtoignore ) + "'")
		#return [ n[0] for n in self.run_select_get_dict(sql) ]
		#FIXME:
		return [ 'Hearst','Algonquin_Park', 'Kenora' , 'Ottawa_Valley', 'Bancroft_Minden' , 'Black_Spruce', 'Sapawe', 'Crossroute', 'Dog_River_Matawin', 'Algoma','Northshore','Sudbury','Nipissing','Nagagami','Timiskaming','Martel','Gordon_Cosens','Romeo_Malette','Pineland','Spanish','Abitibi_River','Temagami','White_River']

	def get_plan_submission_id(self,fmu,product,year):
		#sql = "SELECT id FROM fmp.submission WHERE upper(fmu) = upper(%s) and upper(product) == upper(%s) AND year <= %s "%(fmu,'plan',str(year))
		#return [ n[0] for n in self.run_select_get_dict(sql) ]
		return 4201 #Trent Severn, FMP 2009 #FIXME


	def get_plan_year(self,fmu,year):
		# plan start year has to be less than or equal to the product submission year
		query = [planyr for planyr in static_db[fmu][1] if planyr <= int(year) ]

		# if there are more than one plan year that is less than or equal to current submission year, choose the most recent one
		query.sort()
		if len(query)>0:
			return query[-1]
        # if there's no plan year that fits the above criteria, return 0
		else:
			return 0


		# this function doesn't work for live db
	def FMUCodeConverter(self, x):
	    # if the input x is fmu name, the output is the code and vice versa.
	    # note that the input should be String format.
	    try:
	        return static_db[x][0]
	    except:
	        try:
	            fmu = next(key for key, value in static_db.items() if ( value[0] == x ) or (str(value[0]).zfill(3) == str(x).zfill(3)) )
	            return fmu
	        except Exception:
	            print "ERROR: " + x + " is neither fmu name nor the code."
	            return None


if __name__ == "__main__":
	p= pg_db()
##	print p.get_list_of_fmus()
##	print p.get_plan_year('Pic_Forest',2018)

	a = p.FMUCodeConverter('110')

	print a

	print p.FMUCodeConverter('Whitefeather')
	print p.FMUCodeConverter('405')
	print p.FMUCodeConverter(405)
