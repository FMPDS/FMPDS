    <map version="1.0.1">
        <node TEXT="AWS_2009">
            <node TEXT="ScheduledHarvest:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledHarvest" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AWS_YR:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AWS year" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AWS_YR" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The annual work schedule fiscal year attribute identifies the fiscal year to which the AWS applies. Only those areas scheduled in the AWS require this field to be populated." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="At least one feature must be populated with the AWS start year identified in the FI Portal submission record. Features where the AWS_YR is not populated are considered to not be scheduled." FOLDED="true"/>
                                <node TEXT="There cannot be any values less than the FMP plan period start year (except for 0) or greater than the plan period end year minus 1." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must &gt;= ${planyear} and &lt; ${planyear} + 10' for row in [row] if row['${name}'] is not None if ( row['${name}'] &gt; 0) and ( row['${name}'] &lt; ${planyear} or row['${name}'] &gt;= ${planyear} + 10 )     ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="SILVSYS:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="silvicultural system" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SILVSYS" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="SILVSYS" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and  row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [  'CC','SE','SH'  ]    and str(row['${name}']).strip() &lt;&gt; '' and row['${name}'] is not None ]   " FOLDED="true"/>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}' : 'CC' , 'AWS_YR': ${year}}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="CC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CC" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="clearcut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="selection" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="shelterwood" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="HARVCAT:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="harvest category" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="HARVCAT" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="HARVCAT" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="REGULAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REGULAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="regular harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="BRIDGING:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BRIDGING" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="bridging harvest areas" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="REDIRECT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REDIRECT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="redirected harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="ACCELER:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ACCELER" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="accelerated harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="FRSTPASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FRSTPASS" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="modified cut: first pass" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCNDPASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCNDPASS" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Second-pass harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SALVAGE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SALVAGE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="salvage harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="ROADROW:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ROADROW" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="road right-of-way" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                                <node TEXT="Bridging (HARVCAT = BRIDGING) is only available when the AWS start year is equal to the first year of the 10 year plan period." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and  row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'REGULAR', 'BRIDGING', 'REDIRECT', 'ACCELER', 'FRSTPASS', 'SCNDPASS', 'SALVAGE', 'ROADROW' , None, '' ]  ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} must not be bridging unless AWS_YR = First Plan Year' for row in [row] if row['${name}'] == 'BRIDGING' and ${year} &lt;&gt; ${planyear}      ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="FUELWOOD:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="fuelwood area" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="FUELWOOD" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'Y', 'N', None, '' ] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Harvest (SHR)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SHR" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true">
                        <node TEXT="[ 'Field %s not found in table'%(f)  for f in [ 'AWS_YR','SILVSYS','HARVCAT','FUELWOOD' ] if f not in fields ] " FOLDED="true"/>
                    </node>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ScheduledAreaofConcern:" FOLDED="true" POSITION="right">
                <icon BUILTIN="button_ok"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledAreaofConcern" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AOCID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AOC Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="15" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AOCID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The AOC identifier attribute is the label assigned to a specific AOC prescription which must correspond to the label on FMP and AWS Areas Selected for Operations maps and the area of concern prescriptions in table FMP-10. The prescription can represent either a group of areas of concern with a common prescription or an individual area of concern with a unique prescription." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="Must be a code from FMP-10" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null' for row in [row] if row['${name}'] is None    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank' for row in [row] if str(row['${name}']).strip() == ''    ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="AOCTYPE:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AOC Type" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AOCTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="AOCTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The AOC type attribute indicates the type of AOC prescription as either modified or reserved." FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="M:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="M" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="modified" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Areas which are scheduled for operations but have specific conditions or restrictions on operations as required by an AOC prescription." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="R:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="R" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="reserved" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Areas which are reserved (prohibited) from operations as required by an AOC prescription." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="A blank or null value is not a valid code" FOLDED="true"/>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null' for row in [row] if row['${name}'] is None    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank' for row in [row] if str(row['${name}']).strip() == ''    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'M', 'R', None, '' ] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Area of Concern (SAC)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SAC" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ScheduledResidualPatches:" FOLDED="true" POSITION="right">
                <icon BUILTIN="button_ok"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledResidualPatches" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AWS_YR:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AWS Year" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AWS_YR" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The annual work schedule fiscal year attribute identifies the fiscal year to which the AWS applies. Only those areas scheduled in the AWS require this field to be populated." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="At least one feature must be populated with the AWS start year identified in the FI Portal submission record. Features where the AWS_YR is not populated are considered to not be scheduled." FOLDED="true"/>
                                <node TEXT="There cannot be any values less than the FMP plan period start year (except for 0) or greater than the plan period end year minus 1." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must &gt;= ${planyear} and &lt; ${planyear} + 10' for row in [row] if row['${name}'] is not None if ( row['${name}'] &gt; 0) and ( row['${name}'] &lt; ${planyear} or row['${name}'] &gt;= ${planyear} + 10 )     ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="RESID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Residual Patch Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="10" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="RESID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The residual patch identifier attribute is a number, label or name assigned to a residual patch(es) as defined in the FMP or AWS text.." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and  row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Residual Patches (SRP)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SRP" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ScheduledRoadCorridors:" FOLDED="true" POSITION="right">
                <icon BUILTIN="button_ok"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledRoadCorridors" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AWS_YR:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AWS year" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AWS_YR" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The annual work schedule fiscal year attribute identifies the fiscal year to which the AWS applies. Only those areas scheduled in the AWS require this field to be populated." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="At least one feature must be populated with the AWS start year identified in the FI Portal submission record. Features where the AWS_YR is not populated are considered to not be scheduled." FOLDED="true"/>
                                <node TEXT="There cannot be any values less than the FMP plan period start year (except for 0) or greater than the plan period end year minus 1." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must &gt;= ${planyear} and &lt; ${planyear} + 10' for row in [row] if row['${name}'] is not None if ( row['${name}'] &gt; 0) and ( row['${name}'] &lt; ${planyear} or row['${name}'] &gt;= ${planyear} + 10 )     ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ROADID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="30" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ROADID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road identifier attribute is the unique number, label or name assigned to the forest access road that this polygon is a part of." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and  row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ROADCLAS:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Class" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ROADCLAS" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="ROADCLAS" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and  row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'B', 'O', 'P', None, '' ] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="B:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="B" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="branch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="O:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="O" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Operational" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="P:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="P" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Primary" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="AOCXID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AOC Crossing Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="15" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AOCXID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="NOXING:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Crossing Prohibited" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="15" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="NOXING" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true"/>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ACCESS:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Access Control" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ACCESS" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The access control attribute identifies where access control activities are scheduled to be implemented during the year on primary or branch roads that will be constructed during the year. This attribute is to be used when scheduled activities will render the road inaccessible for purposes other than meeting the conditions required for transferring responsibility for the road to the Crown." FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="When present, the population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="If ACCESS = Y, CONTROL1 must be populated and DECOM must be N if present in the file structure." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and  row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ 'When ${name} is Y, CONTROL1 must be populated' for row in [row] if row['${name}'] == 'Y' if row['CONTROL1'] in [ None, '', ' ' ]      ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'Y', 'N', None, '' ] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="DECOM:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Decommissioning" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="DECOM" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT='If DECOM = Y, then ACCESS must be "N", if present in the file structure..' FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and  row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'Y', 'N', None, '' ] ]" FOLDED="true"/>
                                <node TEXT="[ 'If ${name} is Y then ACCESS must be N' for row in [row] if row['${name}'] == 'Y' and row['ACCESS'] &lt;&gt; 'N'  ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="CONTROL1:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Access Control / Decom Type" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CONTROL1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="CONTROL" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The access control or decommissioning type attributes indicate the method of access control to be implemented during the year on primary or branch roads that will be constructed during the year. This attribute may also be used to identify the type of decommissioning activities scheduled on primary or branch roads that will be constructed during the year." FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The presence and population of CONTROL1 is mandatory where ACCESS = Y" FOLDED="true"/>
                                <node TEXT="The second field must only have a control type when the first field has a control type (error when CONTROL1 = null and CONTROL2 != null)" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null' for row in [row] if row['${name}'] is None and row['ACCESS'] == 'Y'   ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank' for row in [row] if str(row['${name}']).strip() == '' and row['ACCESS'] == 'Y'   ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'BERM', 'GATE', 'PRIV', 'SCAR', 'SIGN', 'SLSH', 'WATX', '', None, ' ' ] ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be null when CONTROL2 is not null ' for row in [row] if row['${name}'] is None and row['CONTROL2'] not in [ None, '', ' ' ] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BERM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BERM" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="berm and/or ditch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="GATE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GATE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="gated / physical barrier" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PRIV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRIV" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="private land" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="scarify and/or plant road" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIGN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIGN" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="signed" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SLSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SLSH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pile slash" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="WATX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="water crossing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="CONTROL2:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Access Control / Decom Type 2" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CONTROL2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="CONTROL" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="The second field must only have a control type when the first field has a control type (error when CONTROL1 = null and CONTROL2 != null)" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must be null or blank when CONTROL1 is blank or null' for row in [row] if str(row['${name}']) not in [ '', None, ' ' ]     and row['CONTROL1']  in [ None, '',' ' ]  ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'BERM', 'GATE', 'PRIV', 'SCAR', 'SIGN', 'SLSH', 'WATX', '', None, '' ] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BERM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BERM" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="berm and/or ditch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="GATE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GATE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="gated / physical barrier" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PRIV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRIV" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="private land" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="scarify and/or plant road" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIGN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIGN" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="signed" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SLSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SLSH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pile slash" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="WATX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="water crossing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Road Corridors (SRC)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SRC" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ScheduledOperationalRoadBoundaries:" FOLDED="true" POSITION="right">
                <icon BUILTIN="button_ok"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledOperationalRoadBoundaries" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AWS_YR:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AWS year" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AWS_YR" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The annual work schedule fiscal year attribute identifies the fiscal year to which the AWS applies. Only those areas scheduled in the AWS require this field to be populated." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="At least one feature must be populated with the AWS start year identified in the FI Portal submission record. Features where the AWS_YR is not populated are considered to not be scheduled." FOLDED="true"/>
                                <node TEXT="There cannot be any values less than the FMP plan period start year (except for 0) or greater than the plan period end year minus 1." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must &gt;= ${planyear} and &lt; ${planyear} + 10' for row in [row] if row['${name}'] is not None if ( row['${name}'] &gt; 0) and ( row['${name}'] &lt; ${planyear} or row['${name}'] &gt;= ${planyear} + 10 )     ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ORBID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Operational Road Boundary ID" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="20" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ORBID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The operational road boundary identifier attribute indicates the user defined unique number, label or name assigned to the operational road boundaries. The operational road boundary is the perimeter of, the scheduled harvest area plus the area from an existing road or scheduled road corridor to the harvest area within which an operational road is scheduled to be constructed." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and  row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Operational Road Boundaries (SOR)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SOR" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ScheduledExistingRoadActivities:" FOLDED="true" POSITION="right">
                <icon BUILTIN="button_ok"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledExistingRoadActivities" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AWS_YR:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AWS year" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AWS_YR" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The annual work schedule fiscal year attribute identifies the fiscal year to which the AWS applies. Only those areas scheduled in the AWS require this field to be populated." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="At least one feature must be populated with the AWS start year identified in the FI Portal submission record. Features where the AWS_YR is not populated are considered to not be scheduled." FOLDED="true"/>
                                <node TEXT="There cannot be any values less than the FMP plan period start year (except for 0) or greater than the plan period end year minus 1." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must &gt;= ${planyear} and &lt; ${planyear} + 10' for row in [row] if row['${name}'] is not None if ( row['${name}'] &gt; 0) and ( row['${name}'] &lt; ${planyear} or row['${name}'] &gt;= ${planyear} + 10 )     ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ROADID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="30" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ROADID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road identifier attribute is the unique number, label or name assigned to the forest access road that this polygon is a part of." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and  row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ROADCLAS:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Class" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ROADCLAS" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="ROADCLAS" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and  row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'B', 'O', 'P', None, '' ] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="B:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="B" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="branch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="O:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="O" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Operational" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="P:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="P" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Primary" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="ACCESS:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Access Control" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ACCESS" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The access control attribute identifies where access control activities are scheduled to be implemented during the year on primary or branch roads that will be constructed during the year. This attribute is to be used when scheduled activities will render the road inaccessible for purposes other than meeting the conditions required for transferring responsibility for the road to the Crown." FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="When present, the population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="If ACCESS = Y, CONTROL1 must be populated and DECOM must be N if present in the file structure." FOLDED="true"/>
                                <node TEXT="None" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and  row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'Y', 'N', None, '' ] ]" FOLDED="true"/>
                                <node TEXT="[ 'When ${name} is Y, CONTROL1 must be populated' for row in [row] if row['${name}'] == 'Y' if row['CONTROL1'] in [ None, '', ' ' ]      ]" FOLDED="true"/>
                                <node TEXT="[ 'At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM = Y or MAINTAIN = Y or MONITOR = Y or ACCESS = Y) where AWS_YR equals the fiscal year to which the AWS applies. (ACCESS)'  for row in [row]   if row['AWS_YR'] == 2017   if (    row['DECOM'] &lt;&gt; 'Y' and    row['MAINTAIN'] &lt;&gt; 'Y' and    row['MONITOR'] &lt;&gt; 'Y'  and    row['ACCESS'] &lt;&gt; 'Y' ) ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="DECOM:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Decommissioning" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="DECOM" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT='If DECOM = Y, then ACCESS must be "N", if present in the file structure..' FOLDED="true"/>
                                <node TEXT="None" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and  row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'Y', 'N', None, '' ] ]" FOLDED="true"/>
                                <node TEXT="[ 'If ${name} is Y then ACCESS must be N' for row in [row] if row['${name}'] == 'Y' and row['ACCESS'] &lt;&gt; 'N'  ]" FOLDED="true"/>
                                <node TEXT="[ 'At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM = Y or MAINTAIN = Y or MONITOR = Y or ACCESS = Y) where AWS_YR equals the fiscal year to which the AWS applies. (ACCESS)'  for row in [row]   if row['AWS_YR'] == 2017   if (    row['DECOM'] &lt;&gt; 'Y' and    row['MAINTAIN'] &lt;&gt; 'Y' and    row['MONITOR'] &lt;&gt; 'Y'  and    row['ACCESS'] &lt;&gt; 'Y' ) ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="MAINTAIN:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Maintenance" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MAINTAIN" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT='If DECOM = Y, then ACCESS must be "N", if present in the file structure..' FOLDED="true"/>
                                <node TEXT="None" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and  row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'Y', 'N', None, '' ] ]" FOLDED="true"/>
                                <node TEXT="[ 'If ${name} is Y then ACCESS must be N' for row in [row] if row['${name}'] == 'Y' and row['ACCESS'] &lt;&gt; 'N'  ]" FOLDED="true"/>
                                <node TEXT="[ 'At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM = Y or MAINTAIN = Y or MONITOR = Y or ACCESS = Y) where AWS_YR equals the fiscal year to which the AWS applies. (ACCESS)'  for row in [row]   if row['AWS_YR'] == 2017   if (    row['DECOM'] &lt;&gt; 'Y' and    row['MAINTAIN'] &lt;&gt; 'Y' and    row['MONITOR'] &lt;&gt; 'Y'  and    row['ACCESS'] &lt;&gt; 'Y' ) ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="MONITOR:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Monitoring" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MONITOR" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT='If DECOM = Y, then ACCESS must be "N", if present in the file structure..' FOLDED="true"/>
                                <node TEXT="None" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and  row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'Y', 'N', None, '' ] ]" FOLDED="true"/>
                                <node TEXT="[ 'If ${name} is Y then ACCESS must be N' for row in [row] if row['${name}'] == 'Y' and row['ACCESS'] &lt;&gt; 'N'  ]" FOLDED="true"/>
                                <node TEXT="[ 'At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM = Y or MAINTAIN = Y or MONITOR = Y or ACCESS = Y) where AWS_YR equals the fiscal year to which the AWS applies. (ACCESS)'  for row in [row]   if row['AWS_YR'] == 2017   if (    row['DECOM'] &lt;&gt; 'Y' and    row['MAINTAIN'] &lt;&gt; 'Y' and    row['MONITOR'] &lt;&gt; 'Y'  and    row['ACCESS'] &lt;&gt; 'Y' ) ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="CONTROL1:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Access Control / Decom Type" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CONTROL1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="CONTROL" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The access control or decommissioning type attributes indicate the method of access control to be implemented during the year on primary or branch roads that will be constructed during the year. This attribute may also be used to identify the type of decommissioning activities scheduled on primary or branch roads that will be constructed during the year." FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="The presence and population of CONTROL1 is mandatory where ACCESS = Y" FOLDED="true"/>
                                <node TEXT="The second field must only have a control type when the first field has a control type (error when CONTROL1 = null and CONTROL2 != null)" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when ACCESS = Y' for row in [row] if row['${name}'] is None   and row['ACCESS'] == 'Y' ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank' for row in [row] if str(row['${name}']).strip() == ''    and row['ACCESS'] == 'Y'  ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'BERM', 'GATE', 'PRIV', 'SCAR', 'SIGN', 'SLSH', 'WATX', '', None, ' ' ] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BERM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BERM" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="berm and/or ditch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="GATE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GATE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="gated / physical barrier" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PRIV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRIV" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="private land" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="scarify and/or plant road" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIGN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIGN" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="signed" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SLSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SLSH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pile slash" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="WATX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="water crossing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="CONTROL2:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Access Control / Decom Type 2" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CONTROL2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="CONTROL" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="The second field must only have a control type when the first field has a control type (error when CONTROL1 = null and CONTROL2 != null)" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must be null or blank when CONTROL1 is blank or null' for row in [row] if str(row['${name}']) not in [ '', None, ' ' ]     and row['CONTROL1']  in [ None, '',' ' ]  ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'BERM', 'GATE', 'PRIV', 'SCAR', 'SIGN', 'SLSH', 'WATX', '', None, ' ' ] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BERM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BERM" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="berm and/or ditch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="GATE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GATE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="gated / physical barrier" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PRIV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRIV" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="private land" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="scarify and/or plant road" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIGN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIGN" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="signed" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SLSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SLSH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pile slash" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="WATX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="water crossing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polyline" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Existing Road Activities (SRA)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SRA" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ScheduledWaterCrossingActivities:" FOLDED="true" POSITION="right">
                <icon BUILTIN="button_ok"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledWaterCrossingActivities" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AWS_YR:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AWS year" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AWS_YR" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The annual work schedule fiscal year attribute identifies the fiscal year to which the AWS applies. Only those areas scheduled in the AWS require this field to be populated." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="At least one feature must be populated with the AWS start year identified in the FI Portal submission record. Features where the AWS_YR is not populated are considered to not be scheduled." FOLDED="true"/>
                                <node TEXT="There cannot be any values less than the FMP plan period start year (except for 0) or greater than the plan period end year minus 1." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must &gt;= ${planyear} and &lt; ${planyear} + 10' for row in [row] if row['${name}'] is not None if ( row['${name}'] &gt; 0) and ( row['${name}'] &lt; ${planyear} or row['${name}'] &gt;= ${planyear} + 10 )     ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="WATXID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Water Crossing Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="12" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="WATXID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The water crossing type attribute identifies the type of water crossing structure being scheduled." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="A blank or null value is not a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null' for row in [row] if row['${name}'] is None    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank' for row in [row] if str(row['${name}']).strip() == ''    ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="WATXTYPE:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Water Crossing Type" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="WATXTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="WATXTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The water crossing type attribute identifies the type of water crossing structure being scheduled." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="A blank or null value is not a valid code" FOLDED="true"/>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null' for row in [row] if row['${name}'] is None    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank' for row in [row] if str(row['${name}']).strip() == ''    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'BRID', 'CULV', 'FORD', 'ICE', None, '' ] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BRID:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BRID" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="bridge" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CULV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CULV" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="culvert" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="FORD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FORD" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="engineered ford" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="ICE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ICE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ice crossing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="CONSTRCT:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Construction" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CONSTRCT" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The construction attribute identifies water crossings are scheduled to be constructed during the operating year of the AWS or the following AWS." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="At a minimum, one of Construction, Monitoring, Remove or Replace must occur for each record (CONSTRCT = Y or MONITOR = Y or REMOVE = Y or REPLACE = Y) where AWS_YR equals the fiscal year to which the AWS applies or the following year." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and  row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'Y', 'N', None, '' ] ]" FOLDED="true"/>
                                <node TEXT="[ 'At a minimum, one of Construction, Monitoring, Remove or Replace must occur for each record (CONSTRCT = Y or MONITOR = Y or REMOVE = Y or REPLACE = Y) where AWS_YR equals the fiscal year to which the AWS applies or the following year. ' for row in [row]  if   row['CONSTRCT'] &lt;&gt; 'Y' and   row['MONITOR']     &lt;&gt; 'Y' and   row['REMOVE']       &lt;&gt; 'Y' and   row['REPLACE']     &lt;&gt; 'Y' and   row['AWS_YR']    == ${year}     ] " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="MONITOR:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Monitoring" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MONITOR" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The monitoring attribute identifies water crossings scheduled to be monitored during the operating year of the AWS." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="At a minimum, one of Construction, Monitoring, Remove or Replace must occur for each record (CONSTRCT = Y or MONITOR = Y or REMOVE = Y or REPLACE = Y) where AWS_YR equals the fiscal year to which the AWS applies or the following year." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and  row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'Y', 'N', None, '' ] ]" FOLDED="true"/>
                                <node TEXT="[ 'At a minimum, one of Construction, Monitoring, Remove or Replace must occur for each record (CONSTRCT = Y or MONITOR = Y or REMOVE = Y or REPLACE = Y) where AWS_YR equals the fiscal year to which the AWS applies or the following year. ' for row in [row]  if   row['CONSTRCT'] &lt;&gt; 'Y' and   row['MONITOR']     &lt;&gt; 'Y' and   row['REMOVE']       &lt;&gt; 'Y' and   row['REPLACE']     &lt;&gt; 'Y' and   row['AWS_YR']    == ${year}     ] " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="REMOVE:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Decommissioning" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="REMOVE" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="At a minimum, one of Construction, Monitoring, Remove or Replace must occur for each record (CONSTRCT = Y or MONITOR = Y or REMOVE = Y or REPLACE = Y) where AWS_YR equals the fiscal year to which the AWS applies or the following year." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and  row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'Y', 'N', None, '' ] ]" FOLDED="true"/>
                                <node TEXT="[ 'At a minimum, one of Construction, Monitoring, Remove or Replace must occur for each record (CONSTRCT = Y or MONITOR = Y or REMOVE = Y or REPLACE = Y) where AWS_YR equals the fiscal year to which the AWS applies or the following year. ' for row in [row]  if   row['CONSTRCT'] &lt;&gt; 'Y' and   row['MONITOR']     &lt;&gt; 'Y' and   row['REMOVE']       &lt;&gt; 'Y' and   row['REPLACE']     &lt;&gt; 'Y' and   row['AWS_YR']    == ${year}     ] " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="REPLACE:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Replacement" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="REPLACE" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The removal attribute identifies water crossings scheduled to be removed during the operating year of the AWS." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="At a minimum, one of Construction, Monitoring, Remove or Replace must occur for each record (CONSTRCT = Y or MONITOR = Y or REMOVE = Y or REPLACE = Y) where AWS_YR equals the fiscal year to which the AWS applies or the following year." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'Y', 'N', None, '' ] ]" FOLDED="true"/>
                                <node TEXT="[ 'At a minimum, one of Construction, Monitoring, Remove or Replace must occur for each record (CONSTRCT = Y or MONITOR = Y or REMOVE = Y or REPLACE = Y) where AWS_YR equals the fiscal year to which the AWS applies or the following year. ' for row in [row]  if   row['CONSTRCT'] &lt;&gt; 'Y' and   row['MONITOR']     &lt;&gt; 'Y' and   row['REMOVE']       &lt;&gt; 'Y' and   row['REPLACE']     &lt;&gt; 'Y' and   row['AWS_YR']    == ${year}     ] " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="ROADID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="30" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ROADID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road identifier attribute is the unique number, label or name assigned to the forest access road that this polygon is a part of." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and  row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Point" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Water Crossings Activities (SWC)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SWC" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ScheduledNonWaterAOCCrossing:" FOLDED="true" POSITION="right">
                <icon BUILTIN="button_ok"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledNonWaterAOCCrossing" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AWS_YR:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AWS year" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AWS_YR" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The annual work schedule fiscal year attribute identifies the fiscal year to which the AWS applies. Only those areas scheduled in the AWS require this field to be populated." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="At least one feature must be populated with the AWS start year identified in the FI Portal submission record. Features where the AWS_YR is not populated are considered to not be scheduled." FOLDED="true"/>
                                <node TEXT="There cannot be any values less than the FMP plan period start year (except for 0) or greater than the plan period end year minus 1." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must &gt;= ${planyear} and &lt; ${planyear} + 10' for row in [row] if row['${name}'] is not None if ( row['${name}'] &gt; 0) and ( row['${name}'] &lt; ${planyear} or row['${name}'] &gt;= ${planyear} + 10 )     ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="AOCXID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AOC Crossing Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="15" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AOCXID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The AOC crossing identifier attribute is a unique identifier for the crossing feature." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and  row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ROADID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="30" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ROADID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road identifier attribute is the number, label or name assigned to the road or network of roads that the crossing feature is located on. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and  row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polyline" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Non-Water AOC Crossing (SNW)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SNW" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ScheduledAggregateExtraction:" FOLDED="true" POSITION="right">
                <icon BUILTIN="button_ok"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledAggregateExtraction" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AWS_YR:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AWS year" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AWS_YR" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The annual work schedule fiscal year attribute identifies the fiscal year to which the AWS applies. Only those areas scheduled in the AWS require this field to be populated." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="At least one feature must be populated with the AWS start year identified in the FI Portal submission record. Features where the AWS_YR is not populated are considered to not be scheduled." FOLDED="true"/>
                                <node TEXT="There cannot be any values less than the FMP plan period start year (except for 0) or greater than the plan period end year minus 1." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must &gt;= ${planyear} and &lt; ${planyear} + 10' for row in [row] if row['${name}'] is not None if ( row['${name}'] &gt; 0) and ( row['${name}'] &lt; ${planyear} or row['${name}'] &gt;= ${planyear} + 10 )     ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="AGAREAID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Aggregate Extraction Area Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="15" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AGAREAID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and  row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Aggregate Extraction (SAG)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SAG" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ScheduledSitePreparationTreatments:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledSitePreparationTreatments" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AWS_YR:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AWS year" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AWS_YR" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The annual work schedule fiscal year attribute identifies the fiscal year to which the AWS applies. Only those areas scheduled in the AWS require this field to be populated." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="At least one feature must be populated with the AWS start year identified in the FI Portal submission record. Features where the AWS_YR is not populated are considered to not be scheduled." FOLDED="true"/>
                                <node TEXT="There cannot be any values less than the FMP plan period start year (except for 0) or greater than the plan period end year minus 1." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must &gt;= ${planyear} and &lt; ${planyear} + 10' for row in [row] if row['${name}'] is not None if ( row['${name}'] &gt; 0) and ( row['${name}'] &lt; ${planyear} or row['${name}'] &gt;= ${planyear} + 10 )     ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD1:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 1" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="If the second treatment field is populated (TRTMTHD2 not null) then the first treatment field must also be populated (TRTMTHD1 not null)" FOLDED="true"/>
                                <node TEXT="If the third treatment field is populated (TRTMTHD3 not null) then the first and second treatment fields must also be populated (TRTMTHD1 not null and TRTMTHD2 not null). This validation would expand when more than three TRTMTHD attributes are being used." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and  row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'SIPMECH','SIPCHEMA', 'SIPCHEMG','SIPPB', None, '', ' ' ] ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be null when TRTMTHD2 is not null' for row in [row] if row['${name}'] is None and row['TRTMTHD2'] is not None   ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be null when TRTMTHD3 is not null' for row in [row] if row['${name}'] is None and row['TRTMTHD3'] is not None   ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="SIPMECH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPMECH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="mechanical" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIPCHEMA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPCHEMA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical : aerial application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIPCHEMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical : ground application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIPPB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPPB" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="prescribed burn - conventional burn / high complexity burn" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD2:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 2" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="If the third treatment field is populated (TRTMTHD3 not null) then the first and second treatment fields must also be populated (TRTMTHD1 not null and TRTMTHD2 not null). This validation would expand when more than three TRTMTHD attributes are being used." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'SIPMECH','SIPCHEMA', 'SIPCHEMG','SIPPB', None, '', ' ' ] ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be null when TRTMTHD3 is not null' for row in [row] if row['${name}'] is None and row['TRTMTHD3'] is not None   ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="SIPMECH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPMECH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="mechanical" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIPCHEMA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPCHEMA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical : aerial application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIPCHEMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical : ground application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIPPB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPPB" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="prescribed burn - conventional burn / high complexity burn" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD3:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 3" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD3" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'SIPMECH','SIPCHEMA', 'SIPCHEMG','SIPPB', None, '', ' ' ] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="SIPMECH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPMECH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="mechanical" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIPCHEMA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPCHEMA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical : aerial application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIPCHEMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical : ground application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIPPB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPPB" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="prescribed burn - conventional burn / high complexity burn" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Site Preparation Treatments (SSP)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SSP" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ScheduledRegenerationTreatments:" FOLDED="true" POSITION="right">
                <icon BUILTIN="button_ok"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledRegenerationTreatments" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AWS_YR:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AWS year" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AWS_YR" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The annual work schedule fiscal year attribute identifies the fiscal year to which the AWS applies. Only those areas scheduled in the AWS require this field to be populated." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="At least one feature must be populated with the AWS start year identified in the FI Portal submission record. Features where the AWS_YR is not populated are considered to not be scheduled." FOLDED="true"/>
                                <node TEXT="There cannot be any values less than the FMP plan period start year (except for 0) or greater than the plan period end year minus 1." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must &gt;= ${planyear} and &lt; ${planyear} + 10' for row in [row] if row['${name}'] is not None if ( row['${name}'] &gt; 0) and ( row['${name}'] &lt; ${planyear} or row['${name}'] &gt;= ${planyear} + 10 )     ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD1:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 1" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD-REGN" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="If the second treatment field is populated (TRTMTHD2 not null) then the first treatment field must also be populated (TRTMTHD1 not null)" FOLDED="true"/>
                                <node TEXT="If the third treatment field is populated (TRTMTHD3 not null) then the first and second treatment fields must also be populated (TRTMTHD1 not null and TRTMTHD2 not null). This validation would expand when more than three TRTMTHD attributes are being used." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and  row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'CLAAG', 'NATURAL', 'HARP', 'PLANT', 'SCARIFY', 'SEED', 'SEEDSIP', 'SEEDTREE', 'STRIPCUT', None, '' ] ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be null when TRTMTHD2 is not null' for row in [row] if row['${name}'] is None and row['TRTMTHD2'] is not None   ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be null when TRTMTHD3 is not null' for row in [row] if row['${name}'] is None and row['TRTMTHD3'] is not None   ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="CLAAG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLAAG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration -- careful logging around advance growth / regeneration" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="NATURAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NATURAL" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration - conventional clearcut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="HARP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="HARP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration -- harvest with regeneration protection" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PLANT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLANT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - planting" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCARIFY:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCARIFY" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - scarification" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SEED:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEED" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - seeding" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SEEDSIP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDSIP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - seeding with site preparation" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SEEDTREE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDTREE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration - seed tree cut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="STRIPCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="STRIPCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration - strip cut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD2:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 2" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD-REGN" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="If the third treatment field is populated (TRTMTHD3 not null) then the first and second treatment fields must also be populated (TRTMTHD1 not null and TRTMTHD2 not null). This validation would expand when more than three TRTMTHD attributes are being used." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when TRTMTHD3 is not null' for row in [row] if row['${name}'] is None and row['TRTMTHD3'] is not None   ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'CLAAG', 'NATURAL', 'HARP', 'PLANT', 'SCARIFY', 'SEED', 'SEEDSIP', 'SEEDTREE', 'STRIPCUT', None, '' ] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="CLAAG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLAAG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration -- careful logging around advance growth / regeneration" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="NATURAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NATURAL" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration - conventional clearcut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="HARP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="HARP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration -- harvest with regeneration protection" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PLANT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLANT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - planting" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCARIFY:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCARIFY" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - scarification" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SEED:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEED" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - seeding" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SEEDSIP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDSIP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - seeding with site preparation" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SEEDTREE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDTREE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration - seed tree cut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="STRIPCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="STRIPCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration - strip cut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD3:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 3" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD3" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD-REGN" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'CLAAG', 'NATURAL', 'HARP', 'PLANT', 'SCARIFY', 'SEED', 'SEEDSIP', 'SEEDTREE', 'STRIPCUT', None, '' ] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="CLAAG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLAAG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration -- careful logging around advance growth / regeneration" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="NATURAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NATURAL" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration - conventional clearcut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="HARP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="HARP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration -- harvest with regeneration protection" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PLANT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLANT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - planting" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCARIFY:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCARIFY" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - scarification" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SEED:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEED" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - seeding" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SEEDSIP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDSIP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - seeding with site preparation" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SEEDTREE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDTREE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration - seed tree cut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="STRIPCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="STRIPCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration - strip cut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Regeneration Treatments (SRG)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SRG" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ScheduledTendingTreatments:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledTendingTreatments" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AWS_YR:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AWS year" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AWS_YR" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The annual work schedule fiscal year attribute identifies the fiscal year to which the AWS applies. Only those areas scheduled in the AWS require this field to be populated." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="At least one feature must be populated with the AWS start year identified in the FI Portal submission record. Features where the AWS_YR is not populated are considered to not be scheduled." FOLDED="true"/>
                                <node TEXT="There cannot be any values less than the FMP plan period start year (except for 0) or greater than the plan period end year minus 1." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must &gt;= ${planyear} and &lt; ${planyear} + 10' for row in [row] if row['${name}'] is not None if ( row['${name}'] &gt; 0) and ( row['${name}'] &lt; ${planyear} or row['${name}'] &gt;= ${planyear} + 10 )     ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD1:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 1" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD-TND" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="If the second treatment field is populated (TRTMTHD2 not null) then the first treatment field must also be populated (TRTMTHD1 not null)" FOLDED="true"/>
                                <node TEXT="If the third treatment field is populated (TRTMTHD3 not null) then the first and second treatment fields must also be populated (TRTMTHD1 not null and TRTMTHD2 not null). This validation would expand when more than three TRTMTHD attributes are being used." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and  row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'CLCHEMA', 'CLCHEMG', 'CLMANUAL', 'CLMECH', 'CLPB', 'IMPROVE', 'THINPRE', 'CULTIVAT', 'PRUNE', None, '' ] ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be null when TRTMTHD2 is not null' for row in [row] if row['${name}'] is None and row['TRTMTHD2'] is not None   ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be null when TRTMTHD3 is not null' for row in [row] if row['${name}'] is None and row['TRTMTHD3'] is not None   ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="CLCHEMA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLCHEMA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - chemical : aerial application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CLCHEMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - chemical : ground application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CLMANUAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLMANUAL" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - manual" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CLMECH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLMECH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - mechanical" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CLPB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLPB" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - prescribed burn / high complexity prescribed burn" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="IMPROVE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="IMPROVE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spacing / pre-commercial thin / improvement cut - uneven-aged" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="THINPRE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="THINPRE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spacing / pre-commercial thin / improvement cut - even aged" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CULTIVAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CULTIVAT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cultivation" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PRUNE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRUNE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pruning" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD2:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 2" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD-TND" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="If the third treatment field is populated (TRTMTHD3 not null) then the first and second treatment fields must also be populated (TRTMTHD1 not null and TRTMTHD2 not null). This validation would expand when more than three TRTMTHD attributes are being used." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when TRTMTHD3 is not null' for row in [row] if row['${name}'] is None and row['TRTMTHD3'] is not None   ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'CLCHEMA', 'CLCHEMG', 'CLMANUAL', 'CLMECH', 'CLPB', 'IMPROVE', 'THINPRE', 'CULTIVAT', 'PRUNE', None, '' ] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="CLCHEMA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLCHEMA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - chemical : aerial application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CLCHEMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - chemical : ground application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CLMANUAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLMANUAL" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - manual" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CLMECH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLMECH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - mechanical" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CLPB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLPB" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - prescribed burn / high complexity prescribed burn" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="IMPROVE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="IMPROVE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spacing / pre-commercial thin / improvement cut - uneven-aged" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="THINPRE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="THINPRE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spacing / pre-commercial thin / improvement cut - even aged" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CULTIVAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CULTIVAT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cultivation" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PRUNE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRUNE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pruning" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD3:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 3" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD3" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD-TND" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'CLCHEMA', 'CLCHEMG', 'CLMANUAL', 'CLMECH', 'CLPB', 'IMPROVE', 'THINPRE', 'CULTIVAT', 'PRUNE', None, '' ] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="CLCHEMA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLCHEMA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - chemical : aerial application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CLCHEMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - chemical : ground application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CLMANUAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLMANUAL" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - manual" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CLMECH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLMECH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - mechanical" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CLPB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLPB" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - prescribed burn / high complexity prescribed burn" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="IMPROVE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="IMPROVE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spacing / pre-commercial thin / improvement cut - uneven-aged" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="THINPRE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="THINPRE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spacing / pre-commercial thin / improvement cut - even aged" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CULTIVAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CULTIVAT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cultivation" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PRUNE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRUNE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pruning" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Tending Treatments (STT)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="STT" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ScheduledProtectionTreatment:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledProtectionTreatment" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AWS_YR:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AWS Year" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AWS_YR" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="At least one feature must be populated with the AWS start year identified in the FI Portal submission record. Features where the AWS_YR is not populated are considered to not be scheduled." FOLDED="true"/>
                                <node TEXT="There cannot be any values less than the FMP plan period start year (except for 0) or greater than the plan period end year minus 1." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must &gt;= ${planyear} and &lt; ${planyear} + 10' for row in [row] if row['${name}'] is not None if ( row['${name}'] &gt; 0) and ( row['${name}'] &lt; ${planyear} or row['${name}'] &gt;= ${planyear} + 10 )     ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD1:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 1" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD-PROTECT" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="If the second treatment field is populated (TRTMTHD2 not null) then the first treatment field must also be populated (TRTMTHD1 not null)" FOLDED="true"/>
                                <node TEXT="If the third treatment field is populated (TRTMTHD3 not null) then the first and second treatment fields must also be populated (TRTMTHD1 not null and TRTMTHD2 not null). This validation would expand when more than three TRTMTHD attributes are being used." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when AWS_YR is ${year}' for row in [row] if row['${name}'] is None and row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank when AWS_YR is ${year}' for row in [row] if str(row['${name}']).strip() == ''  and  row['AWS_YR'] == ${year}    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'CLAAG', 'NATURAL', 'HARP', 'PLANT', 'SCARIFY', 'SEED', 'SEEDSIP', 'SEEDTREE', 'STRIPCUT', None, '' ] ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be null when TRTMTHD2 is not null' for row in [row] if row['${name}'] is None and row['TRTMTHD2'] is not None   ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be null when TRTMTHD3 is not null' for row in [row] if row['${name}'] is None and row['TRTMTHD3'] is not None   ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD2:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 2" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD-PROTECT" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="If the third treatment field is populated (TRTMTHD3 not null) then the first and second treatment fields must also be populated (TRTMTHD1 not null and TRTMTHD2 not null). This validation would expand when more than three TRTMTHD attributes are being used." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when TRTMTHD3 is not null' for row in [row] if row['${name}'] is None and row['TRTMTHD3'] is not None   ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'CLAAG', 'NATURAL', 'HARP', 'PLANT', 'SCARIFY', 'SEED', 'SEEDSIP', 'SEEDTREE', 'STRIPCUT', None, '' ] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD3:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 3" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD3" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD-PROTECT" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 'CLAAG', 'NATURAL', 'HARP', 'PLANT', 'SCARIFY', 'SEED', 'SEEDSIP', 'SEEDTREE', 'STRIPCUT', None, '' ] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Protection Treatment (SPT)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SPT" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ForestryAggregatePits:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ForestryAggregatePits" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="PIT_ID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="PIT_ID" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="15" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="PIT_ID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="A blank or null value is not a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null' for row in [row] if row['${name}'] is None    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank' for row in [row] if str(row['${name}']).strip() == ''    ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="PIT_OPEN:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="PIT_OPEN" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="9" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="PIT_OPEN" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="A blank or null value is not a valid code" FOLDED="true"/>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null' for row in [row] if row['${name}'] is None    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank' for row in [row] if str(row['${name}']).strip() == ''    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must be a valid date' for row in [row] if fimdate(row['${name}']) is None ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="PITCLOSE:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="PITCLOSE" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="9" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="PITCLOSE" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true"/>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="The date cannot be greater than 10 years from PIT_OPEN date. " FOLDED="true"/>
                                <node TEXT="If PITCLOSE is not null, CAT9APP must be null." FOLDED="true"/>
                                <node TEXT="If PITCLOSE is null, CAT9APP cannot be null." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null' for row in [row] if row['${name}'] is None    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank' for row in [row] if str(row['${name}']).strip() == ''    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must be a valid date' for row in [row] if fimdate(row['${name}']) is None ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must be within 10 years of pit open data' for row in [row] if row['${name}'] is not None and str(row['${name}']).strip() &lt;&gt; ''  and  ( fimdate( row['${name}']  ) &gt; fimdate( str(int(row['PIT_OPEN'][0:4])+10) + row['PIT_OPEN'][4:] ) ) ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be null when CAT9APP is null' for row in [row] if row['${name}'] in [ None, '', ' '] and row['CAT9APP']  in   [ None, '', ' ']    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must be null when CAT9APP is not null' for row in [row] if row['${name}'] not in [ None, '', ' '] and row['CAT9APP']  not in   [ None, '', ' ']    ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="CAT9APP:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="CAT9APP" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="9" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CAT9APP" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="The date cannot be greater than 10 years from PIT_OPEN date. " FOLDED="true"/>
                                <node TEXT="If PITCLOSE is not null, CAT9APP must be null." FOLDED="true"/>
                                <node TEXT="If PITCLOSE is null, CAT9APP cannot be null." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Point" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Forestry Aggregate Pits (AGP)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="AGP" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="metadata:" FOLDED="true" POSITION="left">
                <node TEXT="submission_type" FOLDED="true">
                    <node TEXT="AWS_2009" FOLDED="true"/>
                </node>
                <node TEXT="submission_min_year" FOLDED="true">
                    <node TEXT="2009" FOLDED="true"/>
                </node>
                <node TEXT="submission_max_year" FOLDED="true">
                    <node TEXT="2017" FOLDED="true"/>
                </node>
                <node TEXT="id" FOLDED="true">
                    <node TEXT="fi_portal_id" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="{fmu}\{product}\{year}" FOLDED="true"/>
                </node>
                <node TEXT="Document" FOLDED="true">
                    <node STYLE="fork" TEXT="MU{fmu_id}_{year}_{product}_TXT_Text.pdf" FOLDED="true"/>
                    <node TEXT="MU{fmu_id}_{year}_{product}_TBL_Tables.pdf" FOLDED="true"/>
                    <node TEXT="MU{fmu_id}_{year}_{product}_MAP_Sum.pdf" FOLDED="true"/>
                    <node TEXT="MU{fmu_id}_{year}_{product}_MAP_SumFR.pdf" FOLDED="true"/>
                    <node TEXT="MU*_MAP_SUM*.pdf" FOLDED="true"/>
                    <node STYLE="fork" TEXT="MU*TXT*.pdf" FOLDED="true"/>
                    <node TEXT="*.pdf" FOLDED="true"/>
                </node>
                <node TEXT="Document_Directory" FOLDED="true">
                    <node TEXT="" FOLDED="true"/>
                </node>
                <node TEXT="Layer" FOLDED="true">
                    <node TEXT="mu{fmu_id}_{year}_AWS.gpkg" FOLDED="true"/>
                    <node TEXT="mu{fmu_id}_{year}_AWS.gdb" FOLDED="true"/>
                    <node TEXT="*.gdb/*" FOLDED="true"/>
                    <node TEXT="*.e00" FOLDED="true"/>
                    <node TEXT="*.shp" FOLDED="true"/>
                    <node TEXT="*.dbf" FOLDED="true"/>
                    <node TEXT="*.shx" FOLDED="true"/>
                    <node TEXT="*.prj" FOLDED="true"/>
                    <node TEXT="*.shp.xml" FOLDED="true"/>
                </node>
                <node TEXT="Layer_Filename" FOLDED="true">
                    <node TEXT="mu{fmu_id}_{year}_AWS" FOLDED="true"/>
                </node>
                <node TEXT="DeferredLayer" FOLDED="true">
                    <node TEXT="*.atx" FOLDED="true"/>
                    <node TEXT="*.spx" FOLDED="true"/>
                    <node TEXT="*.gdbindexes" FOLDED="true"/>
                    <node TEXT="*.gdbtablx" FOLDED="true"/>
                    <node TEXT="*.gdbtable" FOLDED="true"/>
                    <node TEXT="*.freelist" FOLDED="true"/>
                    <node STYLE="fork" TEXT="*/gdb" FOLDED="true"/>
                    <node TEXT="*/timestamps" FOLDED="true"/>
                </node>
                <node TEXT="DeferredLayer_Directory" FOLDED="true">
                    <node TEXT="LAYERS/mu{fmu_id}_{year}_AWS.gdb" FOLDED="true"/>
                </node>
                <node TEXT="Maps" FOLDED="true">
                    <node TEXT="MU{fmu_id}_{planyear}_{product}_{phase}_MAP*.eps" FOLDED="true"/>
                    <node TEXT="*.eps" FOLDED="true"/>
                    <node TEXT="*_MAP*.pdf" FOLDED="true"/>
                    <node TEXT="*_MAP*.eps" FOLDED="true"/>
                </node>
                <node TEXT="Maps_Directory" FOLDED="true">
                    <node STYLE="fork" TEXT="Maps" FOLDED="true"/>
                </node>
                <node TEXT="Internal_zipfile" FOLDED="true">
                    <node TEXT="mu{fmu_id}_{year}_AWS.zip" FOLDED="true"/>
                    <node STYLE="fork" TEXT="*.zip" FOLDED="true"/>
                </node>
                <node TEXT="Internal_zipfile_Directory" FOLDED="true">
                    <node TEXT="" FOLDED="true"/>
                </node>
                <node TEXT="Catchall" FOLDED="true">
                    <node TEXT="" FOLDED="true"/>
                </node>
                <node TEXT="Catchall_Directory" FOLDED="true">
                    <node TEXT="Other" FOLDED="true"/>
                </node>
                <node TEXT="Trash" FOLDED="true">
                    <node TEXT="Thumbs.db" FOLDED="true"/>
                    <node TEXT="*.sbn" FOLDED="true"/>
                    <node TEXT="*.sbx" FOLDED="true"/>
                    <node TEXT="*.cpg" FOLDED="true"/>
                </node>
            </node>
        </node>
    </map>
