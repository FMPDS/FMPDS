    <map version="1.0.1">
        <node TEXT="FMP_2009">
            <node TEXT="PlanningComposite:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="PlanningComposite" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="PCM" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="POLYID:" FOLDED="true">
                        <icon BUILTIN="flag-green"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="25" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="POLYID" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Polygon Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The planning inventory polygon identifier attribute is a unique id or label for the polygon often based on geographic location." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: Blank or null values are not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: Attribute must contain a unique value" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="POLYTYPE:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="POLYTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="POLYTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Polygon Type" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The polygon type attribute indicates the classification of the area within the polygon boundaries into one of several generalized water and land types." FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="The polygon type is determined from the classification of area on a forest management unit into different water and land types. Some polygon types are derived from inventory base features, while other polygons must be created from a classification process.  All remaining land areas within a designated forest management unit are classified into various non-forested or forested lands." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST2: If POLYTYPE attribute does not equal FOR, then all forest-specific attributes should be empty." FOLDED="true">
                                    <icon BUILTIN="bookmark"/>
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST2: If a delineated polygon is an island or is located on an island, the management consideration attribute is set to island MGMTCON1 = ISLD. This applies to all polygon types. This apparent redundancy in classification for polygons having a type of island i.e., POLYTYPE = ISL   and   MGMTCON1 = ISLD allow resource managers to easily identify:   1. all polygons located on islands regardless of polygon type by querying on MGMTCON1 = ISLD  2. just the small uninterpreted islands by querying on POLYTYPE = ISL depending upon the desired analysis." FOLDED="true">
                                    <icon BUILTIN="bookmark"/>
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: ${name} ISL implies MGMTCON1 ISLD' ${:}  row['${name}'] == 'ISL' and row['MGMTCON1'] not in [  'ISLD', None, '' , ' '  ] ]" FOLDED="true"/>
                                <node TEXT="[ 'ST2: &lt;mark&gt;${name}&lt;/mark&gt; non-FOR -&gt; forest attributes should all be null' ${:} row['${name}'] != 'FOR' and len([ row[a] for a in [ 'FORMOD', 'DEVSTAGE', 'YRDEP', 'SPCOMP', 'WG', 'YRORG', 'HT', 'STKG', 'SC', 'ECOSRC', 'ECOSITE1', 'ECOPCT1', 'ECOSITE2', 'ECOPCT2', ' ACCESS1', 'ACCESS2', 'MGMTCON1', 'MGMTCON2', 'MGMTCON3', 'AGS_POLE', 'AGS_SML', 'AGS_MED', 'AGS_LGE', 'UGS_POLE', 'UGS_SML', 'UGS_MED', 'UGS_LGE', 'USPCOMP', 'UWG', 'UYRORG', 'UHT', 'USTKG', 'USC', 'MANAGED', 'TYPE', 'MNRCODE', 'SMZ', 'OMZ', 'PLANFU', 'AGESTR', 'AGE', 'AVAIL', 'SILVSYS', 'NEXTSTG', 'SI', 'SISRC'] if row[a] not in ${list_NULL} ]) != 0  ]" FOLDED="true"/>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':'RP',  'DEVSTAGE':None, 'YRDEP':None, 'SPCOMP':None, 'WG':None, 'YRORG':None, 'HT':None, 'STKG':None, 'SC':None, 'ECOSRC':None, 'ECOSITE1':None, 'ECOPCT1':None, 'ECOSITE2':None, 'ECOPCT2':None, ' ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'AGS_POLE':None, 'AGS_SML':None, 'AGS_MED':None, 'AGS_LGE':None, 'UGS_POLE':None, 'UGS_SML':None, 'UGS_MED':None, 'UGS_LGE':None, 'USPCOMP':None, 'UWG':None, 'UYRORG':None, 'UHT':None, 'USTKG':None, 'USC':None, 'MANAGED':None, 'TYPE':None, 'MNRCODE':None, 'SMZ':None, 'OMZ':None, 'PLANFU':None, 'AGESTR':None, 'AGE':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'SI':None, 'SISRC':None }" FOLDED="true">
                                    <node TEXT="ST2: &lt;mark&gt;${name}&lt;/mark&gt; non-FOR -&gt; forest attributes should all be null" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="WAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WAT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node STYLE="fork" TEXT="All water areas.  Includes lakes, ponds, reservoirs i.e., inland basin areas containing water and wide two sided rivers.  These are rivers that can be defined by area.  Generally, these rivers and streams are consistently wider than 20 meters when portrayed at a mapping scale of 1:10,000 or consistently wider than 40 meters when portrayed at a mapping scale of 1:20,000.  Smaller or narrower rivers and streams are maintained as linear features in a centre-line layers." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Water" FOLDED="true"/>
                                </node>
                                <node TEXT="fill color" FOLDED="true">
                                    <node TEXT="#6bcbff" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DAL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Lands which are cultivated for growing crops, orchards, floral gardens, etc. These areas may include abandoned agricultural lands." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Developed agricultural land" FOLDED="true"/>
                                </node>
                                <node TEXT="fill color" FOLDED="true">
                                    <node TEXT="#d6f0ff" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="GRS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GRS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Farm areas devoted to pasture for domesticated animals. These areas may also include abandoned grass and meadows, but are not part of the productive forest land base and do not include barren and scattered areas. These areas are similar to barren and scattered, but are located near developed agriculture land or unclassified areas and are usually fenced." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Grass and meadow" FOLDED="true"/>
                                </node>
                                <node TEXT="fill color" FOLDED="true">
                                    <node TEXT="#c9ffce" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ISL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ISL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Islands less than 8 hectares in size, down to a lower limit of 0.0025 hectares or 25 square meters in size e.g., 5 meters X 5 meters are recorded during the inventory production process, but are not interpreted or typed for practicality and cost considerations.  Only islands 8 hectares and larger are interpreted and assigned an appropriate POLYTYPE code, such as FOR or BSH." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Small island" FOLDED="true"/>
                                </node>
                                <node TEXT="fill color" FOLDED="true">
                                    <node TEXT="#ffffff" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="UCL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="UCL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Non-forested areas which were created for specific uses other than timber production, such as  roads, railroads, logging camps, mines, utility corridors, logging camps, gravel pits, airports, etc." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Unclassified" FOLDED="true"/>
                                </node>
                                <node TEXT="fill color" FOLDED="true">
                                    <node TEXT="#bfbfbf" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="BSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BSH" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Areas covered with non-commercial tree species or shrubs.  These areas are normally associated with wetlands or water features." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Brush and alder" FOLDED="true"/>
                                </node>
                                <node TEXT="fill color" FOLDED="true">
                                    <node TEXT="#9cbc9f" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="RCK:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="RCK" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Areas of barren or exposed rock e.g., bedrock, cliff face, talus slope which may support a few scattered trees, but is less that 25 percent crown closure." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Rock" FOLDED="true"/>
                                </node>
                                <node TEXT="fill color" FOLDED="true">
                                    <node TEXT="#b2b2b2" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="TMS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="TMS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Areas of dry or wet muskeg on which stunted trees occur as widely spaced individuals or in small groups." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Treed wetland" FOLDED="true"/>
                                </node>
                                <node TEXT="fill color" FOLDED="true">
                                    <node TEXT="#9cbc9f" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OMS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OMS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Wet areas of mosses, grasses, sedges, and small herbaceous plants, often interspersed with small areas of open water" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Open Wetland" FOLDED="true"/>
                                </node>
                                <node TEXT="fill color" FOLDED="true">
                                    <node TEXT="#9cbc9f" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FOR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Areas that are capable of producing trees and can support tree growth.  These areas may or may not be capable of supporting the harvesting of timber on a sustained yield basis.  Some areas may have physical and/or biological characteristics which effect land use.  Thus this polygon type includes both production and protection forest areas." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Productive forest" FOLDED="true"/>
                                </node>
                                <node TEXT="fill color" FOLDED="true">
                                    <node TEXT="#52b770" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="FOR" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="OWNER:" FOLDED="true">
                        <icon BUILTIN="flag-green"/>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="OWNER" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="OWNER" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Ownership" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="This ownership attribute contains the traditional FRI ownership information as assigned by the Office of the Surveyor General." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="The ownership designation attribute is derived from the ownership and land tenure, and parks and reserves layers which are maintained in the MNRFs values information system. This attribute also identifies the managed Crown area in a forest management unit. The ownership information is used to create table FMP-1 Part B, Section 8.0." FOLDED="true"/>
                            <node TEXT="Any discrepancies regarding the information contained in the ownership designation attribute should be reported to the appropriate MNRF district office. The most accurate source for ownership information is located in the appropriate regional Land Registry Office." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'u'}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'1'}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="0:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="0" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="None" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Unknown or unassigned ownership" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="1:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="1" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Crown Managed" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Crown Land" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="2:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="2" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Patented Crown Timber" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Patent land with timber rights reserved to the Crown" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="3:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="3" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Non Crown" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Patented land fee simple private" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="4:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="4" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Non Crown" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Patented land Company Freehold" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="5:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="5" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Crown Other" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Provincial Park" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="6:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="6" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Non Crown" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Indian Reserve" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="7:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="7" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Crown Other" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Recreation Reserve" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="8:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="8" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Non Crown" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Agreement Forest" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="9:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="9" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Crown Federal" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Federal Reserve" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="1 " FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="AUTHORTY:" FOLDED="true">
                        <icon BUILTIN="flag-green"/>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AUTHORTY" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="string" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="AUTHORTY" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Ownership Designation" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The ownership designation attribute indicates the custodian responsible for providing the inventory information about the polygon.  (The party responsible for maintaining and providing the forest resources inventory information in accordance with the Forest Information Manual; that is, for determining the polygon description.) " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="As of 2006 the forest resource inventory is the responsibility of MNR and therefore the default value for the inventory is MNR.  " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="SFL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SFL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Licensee" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MNR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MNR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Ministry of Natural Resources" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="YRUPD:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="YRUPD" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Year of Data Update" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The year of data update attribute contains a four digit number representing the calendar year (January 1 to December 31) of the source data used to determine update the polygon description, in particular the height attribute." FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="The year of data update identifies the calendar year that information about a polygon was last confirmed or modified based on field inspection, photo interpretation, analysis of satellite imagery, or conversion or update to spatial or tabular data. The year of data update should not be changed to reflect error corrections to tabular attributes." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A zero or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: YRSOURCE must be a year less than the plan period start year" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="Must be a year greater than or equal to 1975" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 1975' ${:N} row['${name}']  &lt;1975  ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <icon BUILTIN="full-4"/>
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be less than  ${planyear} ' ${:N} row['${name}']  &gt;= ${planyear}  ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':1950 }" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 1975" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="SOURCE:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                        <icon BUILTIN="flag"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SOURCE" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="SOURCE" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Source of Update Data " FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The source of data update attribute identifies the methodology by which the information stored in the other tabular attributes that are associated with the same polygon was determined (i.e., how the polygon description was determined)." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="The source of data update attribute does not apply to the changes or updates made to the ecosite attribute." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                            <node TEXT="english" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A value of FORECAST is only valid for the SOURCE attribute in the Base Model Inventory.  NO NEED TO CHECK IN BMI." FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: If the DEVSTAGE attribute starts with EST, then the SOURCE attribute should not equal ESTIMATE" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST2: If the DEVSTAGE attribute starts with NEW, then the SOURCE attribute should be ESTIMATE. FIM Difference - Allow ESTIMATE in EPC product to allow new treatment estimates." FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be FORECAST unless table is Base Model Inventory' ${:}  ${PCM} and row['${name}']  == 'FORECAST'   ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be ESTIMATE when DEVSTAGE LIKE EST' ${:}  row['${name}']  == 'ESTIMATE' and row['DEVSTAGE'] in [ 'ESTNAT','ESTPLANT','ESTSEED' ] ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                            <node TEXT="BASECOVR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BASECOVR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Information that is provided by the MNRF  (e.g., water or evaluated wetlands)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="planimetric base layer" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DIGITALA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DIGITALA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Digital analysis, automated processing. (e.g., Ecognition)" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="multispectral scanning (digital image) - automated process" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DIGITALP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DIGITALP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Photo-interpreted by softcopy systems" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="multispectral scanning (digital image) - manual process" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ESTIMATE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ESTIMATE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This source option is only for use in areas that have been recently renewed and have not been revisited since the renewal work was performed.  That is, where a follow-up survey has not yet been performed.  Therefore, the description of the newly regenerated stand is a 'best estimate' of the expected outcome / result of the renewal treatment that was applied to the area based on past silvicultural successes." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="expected / estimated outcome / result" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FOC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FOC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Inspection of a site after silvicultural treatment to determine whether an operator / operations conforms to the approved plan or permit.   The evaluation of any harvest, renewal, maintenance, or access forest management activity (e.g., post-harvest site inspection) can be included here." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="forest operations compliance inspection" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FORECAST:" FOLDED="true" BACKGROUND_COLOR="#99ff99">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FORECAST" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This indicates a polygon description that was updated based on expected outcomes of planned operations (which have not yet been implemented) for the remainder of the current plan term.  This code is only valid in the Base Model Inventory. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="forecasted description" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FRICNVRT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FRICNVRT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Current polygon description based on data conversion from traditional FRI" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="forest resources inventory conversion" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="INFRARED:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="INFRARED" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Infrared satellite imagery" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MARKING:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MARKING" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Assessment of the trees in a stand for purpose of establishing a silvicultural or operational prescription.  Selecting and marking the trees to be harvested and/or the trees to be left to grow; to sustain and enhance the stand for timber management, wildlife habitat management, aesthetics, recreation, biodiversity and other environmental and heritage concerns." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pre-harvest site inspection / marking" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OCULARA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OCULARA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Visual assessment of a stand from a helicopter or fixed wing aircraft." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="aerial survey / reconnaissance" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OCULARG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OCULARG" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Visual assessment of a stand using extensive ground survey methodologies (i.e., no detailed measurements)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ocular estimate (ground)" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OPC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OPC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Measuring standing trees to determine the volume of wood on a given tract of land." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="operational cruise" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Photography at a conventional scale of 1:10,000, 1:15,840, or 1:20,000" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="air photo interpretation" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTOLS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTOLS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Photography at a scale larger than 1:10,000  (e.g., 1:500, 1:1000)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="large scale aerial photography" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTOSS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTOSS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Photography at a scale smaller than 1:20,000  (e.g., 1:100,000)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="small scale aerial photography" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PLOTFIXD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLOTFIXD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Fixed area plot" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="fixed area plot" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PLOTVAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLOTVAR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Variable area plots." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="variable area (radius) plot" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="RADAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="RADAR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="radar satellite imagery" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="REGENASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REGENASS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Survey of a regenerated area to determine how well the new stand is growing.  This includes seeding, survival, and stocking assessments.." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="regeneration assessment" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEMEXTEN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEMEXTEN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An appraisal of a forest stand's structure and composition using generalized survey sampling methodologies to determine if regeneration or management objectives have been met (i.e., determine if the expected results were achieved).   Extensive survey methods are generally used where there are obvious successes or failure, or to identify problem areas requiring more intensive assessment." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="extensive silvicultural effectiveness monitoring survey" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEMINTEN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEMINTEN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An appraisal of a forest stand's structure and composition using rigorous survey sampling methodologies to determine if regeneration or management objectives have been met (i.e., determine if the expected results were achieved).  Intensive survey methods are intended for stands where the status of regeneration is uncertain or specific quantitative data is required to determine the silvicultural effectiveness for operational treatments." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="intensive silvicultural effectiveness monitoring survey" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SPECTRAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SPECTRAL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spectral satellite imagery" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="FORMOD:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                        <icon BUILTIN="flag-green"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="FORMOD" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="FORMOD" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The productive forest modifier attribute represents a further classification (sub-division) of productive forest areas based on the presence or absence of physical or biological factors which may influence the ability to practice forest management." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="The productive forest modifier must be identified for every productive forest stand and must be used in conjunction with the management consideration attribute." FOLDED="true"/>
                            <node TEXT="There is a relationship between the site class (SC) and the productive forest modifier (FORMOD) values assigned to a forested area. Generally, areas assigned a site class value of 4 are also assigned a productive forest modifier value of PF (protection forest), but it is not a requirement.  Areas assigned a site class value other than 4 (e.g., 3) can also be assigned the productive forest modifier value of PF. Conversely areas assigned a site class value of 4 may be assigned a productive forest modifier value of production forest (RP) instead of protection forest.  The apparent discrepancy in the protection forest assignment between the site class attribute and the productive forest modifier attribute reflects the difference between a calculated assessment of site conditions (SC) versus a forest management decision (FORMOD) that is based on more than just site class." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                            <node TEXT="english" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: FORMOD MUST be NULL when POLYTYPE is NOT equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST2: IF SC attribute equals 4, then FORMOD should be PF (see SC - BMI/OPI only)" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                                    <icon BUILTIN="password"/>
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="RP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="RP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas at various stages of growth and development, including areas that have been recently disturbed (by harvest or natural causes) or renewed (by artificial or natural means), that are capable of producing adequate growth of timber to support harvesting on a sustained yield basis.  These areas have no significant physical or biological limitations on the ability to practice forest management, but may include areas which pose an operational challenge in terms of harvest, access, protection, silviculture, or renewal." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Production Forest - Regular" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas which are considered to be production forest, but that are unavailable for timber production as determined through the forest management planning process.  That is, these areas have been identified as no-cut areas through area of concern planning for the purpose of protecting values (e.g., reserve buffers applied to protect tourism values)." FOLDED="true"/>
                                    <node TEXT="Designated management reserves are areas that were managed as an operational reserve during previous forest management plan terms. That is, they are actual operational or management reserve areas created by the implementation of forest management operations around them. As guides are updated, these areas should be reviewed as some may return to production forest. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Production Forest - Designated Management Reserve" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PF:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PF" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest land on which forest management activities cannot normally be practiced without incurring deleterious environmental effects because of obvious physical limitation such as steep slopes or shallow soils over bedrock." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Protection Forest" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="DEVSTAGE:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="DEVSTAGE" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Stage of Development" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="DEVSTAGE" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The stage of development attribute indicates the current state of growth and development for a productive forest stand.  Note that some stands stages are best described based on the last major silvicultural treatment that was applied to a stand, if the stand is being managed for timber production." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="The stage of development attribute is not a derived attribute and is commonly modified from the first submission in the FRI. The population of this field requires a management decision based on both the current state and the past treatments. As an example, a forest stand was planted and the plantation failed (e.g. ingress, insect damage) yet it eventually met the regeneration standards and was declared established. The forest manager will now need to decide whether this stand receives a code of ESTPLANT or ESTNAT depending on the presence and potential influence of the planted stems that may still remain. The development stage assignment will be independent of the yield assignment despite the fact that there may be a connection between the two. The stage of development will be used to populate the stage of management in the landbase tables in the FMP  (i.e. FMP-3 and FMP-5). " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: If the STKG attribute equals 0.00, then the DEVSTAGE attribute should be either DEP* when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="If the YRDEP attribute equals 0 (unknown), and DEPTYPE is not empty, then the DEVSTAGE attribute should only contain LOWNAT FTGNAT values when POLYTYPE is equal to FOR    *** FIM DIFFERENCE - added DEPTYPE clause for unknown deptypes" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                                <node TEXT="If SC or USC  equals 4, then the DEVSTAGE attribute should not be DEPHARV" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be DEP* when POLYTYPE is FOR and STKG is 0.00 or NULL' ${:NF} row['${name}'] not in [  'DEPHARV','DEPNAT'] and row['STKG'] in [ 0, '', ' ', None ]  ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="[ '${name} must be LOWNAT or FTGNAT when POLYTYPE is FOR and ( YRDEP = 0 and DEPTYPE is not null) ' for row in [row] if row['${name}'] not in [ 'LOWNAT','FTGNAT'  ] and row['POLYTYPE'] == 'FOR'  and row['YRDEP'] == 0 and ( row['DEPTYPE'] != '' and row['DEPTYPE'] is not None )  ]   " FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                                <node TEXT="[ '${name} must not be DEPHARV SC or USC = 4' ${:F} row['${name}'] == 'DEPHARV' and 4 in [row['SC'] , row['USC'] ]  ]   " FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="DEPHARV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DEPHARV" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest area that was recently disturbed by clearcut harvesting and has not received a silvicultural treatment such as natural regeneration, seeding or planting.  These areas do not have advanced regeneration, or a distinct or established regeneration layer that would be released and/or protected as part of the depletion operation." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="recently disturbed by harvest and has received no regeneration/renewal treatment " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DEPNAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DEPNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest area that was recently disturbed by natural causes (i.e., fire, blowdown, ice damage, insect and disease) and has not received a silvicultural treatment such as natural regeneration, seeding or planting.  These areas do not have advanced regeneration, or a distinct or established regeneration layer." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="recently disturbed by natural causes and has received no regeneration / renewal treatment " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="LOWMGMT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LOWMGMT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest stands which were previously harvested and have not reached the regeneration standards as described in an approved forest management plan within the estimated timeframe.  Further, these areas require additional silvicultural treatment to bring them up to regeneration standards.  This does not include areas that have been recently disturbed or recently renewed.  However, it may include areas which have received renewal treatments in the past that have failed to produce a regenerated forest to the applicable regeneration standards.  This option may also include those areas which have traditionally been designated as barren and scattered (i.e., stocking less than 25%).  " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Below regeneration standards due to past management " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="LOWNAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LOWNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest stands which were previously disturbed by natural causes and have not reached the regeneration standards as described in an approved forest management plan within the estimated timeframe.  Further, these areas require additional silvicultural treatment to bring them up to regeneration standards.  This does not include areas that have been recently disturbed or recently renewed.  However, it may include areas which have received renewal treatments in the past that have failed to produce a regenerated forest to the applicable regeneration standards.  This option may also include those areas which have traditionally been designated as barren and scattered (i.e., stocking less than 25%).  " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Below regeneration standards due to natural causes / succession " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NEWPLANT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NEWPLANT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas which have been regenerated predominantly by planting, but have not been assessed as free-to-grow" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="recently renewed : mainly planted" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NEWSEED:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NEWSEED" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas which have been regenerated predominantly by seeding, but have not been assessed as free-to-grow" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="recently renewed : mainly seeded " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NEWNAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NEWNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas which have been regenerated predominantly by natural means, but have not been assessed as free-to-grow" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="recently renewed : mainly natural regeneration " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FTGPLANT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FTGPLANT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas which were regenerated predominantly from planted stock and which have been assessed as free-to-grow.  " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="free-to-grow mainly planted" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FTGSEED:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FTGSEED" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas which were regenerated predominantly by seeding and which have been assessed as free-to-grow.  " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="free-to-grow mainly seeded" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FTGNAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FTGNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas which were regenerated predominantly by natural means and which have been assessed as free-to-grow. This classification will also be used to describe the forested areas that have never been treated to date (original forest) that would be considered as free growing.  " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="free-to-grow mainly natural regeneration " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="THINPRE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="THINPRE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Free-growing productive forest areas which have received a mid-rotation thinning/spacing to promote the growth of the best quality trees.  The trees selected for removal do not result in a harvest of merchantable volume. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received pre-commercial thinning/spacing treatment" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="THINCOM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="THINCOM" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Free-growing productive forest areas which have received a mid-rotation partial harvest designed to promote the growth of the best quality trees.  The harvested trees are removed from the site and used for commercial purposes. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received commercial thinning/spacing treatment " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="BLKSTRIP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BLKSTRIP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of a portion of the existing trees in a stand in progressive strips in more than one operation so that the non-depleted portion of the stand is left primarily to provide a natural seed source for regeneration of the depleted area. Several cutting patterns are available to achieve same goal." FOLDED="true"/>
                                    <node TEXT="The removal of trees in one or more passes in a system of strips of various widths; where each strip is less than or equal to 100 meters (5 chains) wide. It is designed to encourage regeneration on difficult and/or fragile sites." FOLDED="true"/>
                                    <node TEXT="Note: Harvesting where the cut strips are greater than 100 meters wide (&gt; 5 chains) should be recorded as clearcut." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="modified cut: strip" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FRSTPASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FRSTPASS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A partial harvest where the first harvest operation removes target/specific merchantable tree species from a forest stand. The remaining species are merchantable and are intended to be harvested by another logger/contractor/forest resource licence holder in the next pass.  A first pass should be recorded if merchantable tree species remain in the forest stand which have been allocated for harvest - but have not yet been harvested." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="modified cut: first pass " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PREPCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PREPCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management designed to remove undesirable species of any species from the stand and to select trees to remain that will provide the best seed source and to improve conditions for seed production and natural regeneration." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a preparatory cut " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEEDCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management where trees are removed from a mature stand to create openings in the canopy / create spaces and to prepare sites for natural regeneration while maintaining the seed-bearing trees and protecting any existing advance regeneration." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a seed cut" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FIRSTCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FIRSTCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management where overstorey trees are removed in one or more harvests in order to release the established seedlings from competition." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a first removal harvest " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="IMPROVE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="IMPROVE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A selection silvicultural system stage of management where a cut is made in an uneven-aged stand primarily to improve stand composition, distribution and quality by removing less desirable trees of any species." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received an improvement cut " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SELECT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SELECT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A selection silvicultural system stage of management where individual trees or groups of trees are selected for cutting in order to recover the yield and develop a balanced uneven-aged structure, while providing the cultural measures required for tree growth and seeding establishment. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a selection harvest " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SNGLTREE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SNGLTREE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The stand canopy is (periodically) opened uniformly throughout  the entire stand to achieve a post-harvest, basal area target. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Selection:  single-tree " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="GROUPSE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GROUPSE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The stand canopy is (periodically) opened by harvesting trees  in small groups. The resulting canopy opening usually occupies  a fraction of a hectare. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Selection: Group" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="DEPTYPE:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="DEPTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Type of Disturbance" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The type of disturbance attribute identifies the disturbance that occurred in the year recorded in the companion attribute YRDEP (year of disturbance).  The disturbance may have affected the entire stand or only a portion of it." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme where YRDEP &lt;&gt; 0" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A null or blank value is not a valid code where YRDEP &lt;&gt; 0" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST2: If the development stage is depleted, new or established (DEVSTAGE = DEP, NEW or EST) then the disturbance type should not have a value of unknown (DEPTYPE = UNKNOWN)" FOLDED="true" BACKGROUND_COLOR="#ffffff">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST2: If the disturbance type is not null (DEPTYPE &lt;&gt; null) then the disturbance year should not be equal to zero (YRDEP = 0). Should actually be &gt;= 1900" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <icon BUILTIN="password"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be UNKNOWN if DEVSTAGE are DEP, NEW or EST' ${:} row['${name}'] == 'UNKNOWN' and row['DEVSTAGE'] in ['DEPHARV', 'DEPNAT', 'NEWPLANT', 'NEWSEED', 'NEWNAT', 'ESTPLANT', 'ESTSEED', 'ESTNAT'] ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be populated when YRDEP is populated'  ${:F} ${NIN} and row['YRDEP'] not in ${list_NULL} ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BLOWDOWN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BLOWDOWN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="wind / blowdown" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DISEASE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DISEASE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="disease" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DROUGHT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DROUGHT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="drought" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FIRE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FIRE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="fire" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FLOOD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FLOOD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="flood" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="HARVEST:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="HARVEST" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Partial or full stand removal of timber.  This includes mid-rotation or stand improvement operations where merchantable timber is removed." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="harvest" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ICE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ICE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ice damage" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="INSECTS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="INSECTS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="insects" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SNOW:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="INSECTS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="insects" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="UNKOWN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="UNKNOWN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Unknown" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="YRDEP:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="YRDEP" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Year of Last Depletion or Disturbance" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The year of last disturbance attribute indicates a four digit number of the most recent (or latest) fiscal year (April 1 to March 31) that a productive forest area was disturbed, completely or partially, by harvest or by natural causes. This includes mid-rotation or stand improvement operations where merchantable timber is removed. This is actual and known disturbances and not calculated from year of origin." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="The planning composite inventory must be updated with all disturbances (harvest and natural) which are identified in annual reports. The year of last disturbance for each productive forest stand must correspond to the year that the disturbance occurred, as recorded in the applicable annual reports.  Forest stands that are managed under the selection or shelterwood silvicultural systems must also have a year of last disturbance. The year of last disturbance provides an estimate of the stage of forest stand development within a cutting cycle for selection stands or within the current stage of a shelterwood system.  In the case of a partial harvest where certain tree species have been removed, such as a first pass where merchantable trees remain in a forest stand, the year that the first pass was carried out must be entered as the year of last disturbance. If an additional harvest or pass is conducted, the year of last disturbance is changed to the year in which the most recent harvest/pass operation was carried out.  Commercial thinning is a mid-rotation treatment that is recorded as a disturbance and the stand area is considered as disturbed area in forest management planning. Forest stands that have received a commercial thinning must show the applicable year of last disturbance in the forest polygon coverage.  The year of last disturbance is also used to identify other stand improvement operations. A stand improvement operation is normally associated with the selection system.   Year of last disturbance must not be used to indicate tending operations, such as a chemical or manual release which are required to bring a forest stand to free-to-grow standards. " FOLDED="true"/>
                            <node TEXT="The year of depletion attribute (YRDEP) and the associated depletion type attribute (DEPTYPE) are only completed during the photo interpretation process when the stage of development attribute is set to one of the 'newly depleted' options (i.e., DEVSTAGE = DEPHARV or DEPNAT).  The fields are also generally completed for newly regenerated stages of development (i.e., DEVSTAGE = NEWNAT, NEWPLANT, or NEWSEED).  For other stages of development, the fields may be completed if the interpreter has access to additional information pertaining to past disturbances." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be 0 when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST2: The value must be greater than or equal to 1900 when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="The YRDEP attribute value should not be less than the YRORG attribute value when POLYTYPE is equal to FOR, unless the flag value 0 is used.***** FIM DIFFERENCE - removed this rule. Counterexamples abound..." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be greater than or equal to 1900 unless it is 0' ${:NF}  row['${name}']  &lt;1900 ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="SPCOMP:" FOLDED="true">
                        <icon BUILTIN="flag-green"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Species Composition" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="120" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SPCOMP" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT=" This species composition attribute indicates the tree species that are present in the stand canopy and the relative proportion of the canopy that each species occupies." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: Pattern is SSSPPPSSSPPP   example: PJ  80PO  20  (there are two blanks between the species and the proportion) when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: No duplicate species codes allowed in the string" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: Proportion values in the string must sum to 100" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST1: The tree species in the composition are to be coded using the scheme listed here" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="If SOURCE is NOT plot based then allow specific species in SPCOMP (eFRI tech spec)" FOLDED="true">
                                    <icon BUILTIN="yes"/>
                                </node>
                                <node TEXT="Tree species in the list [     'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn',     'CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf',     'OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX',     'OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX',     'Sb','Sw','la','Wb','Wi'     ]" FOLDED="true">
                                    <icon BUILTIN="yes"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST%s: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; %s'%( '2' if 'WARNING' in i else '1'   ,i)    for i in test_spcomp(row['${name}']) if ${NINN} if i not in  [ None, [ None ] , []  ] ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <icon BUILTIN="full-5"/>
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; Invalid species code' for s in test_SpeciesInList(row['${name}'],  [ 'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn','CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf','OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX', 'Sb','Sw','la','Wb','Wi' ]         )  if ${FOR} and ${NINN} and row['SOURCE'][:4] != 'PLOT'    ]" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; - %s is not a valid species code'%(s) for s in test_SpeciesInList(row['${name}'],    [ 'AL','Aq','Ap','Ag','Bc','Bp','Gb','Bb','Cat','Cc','Cm', 'Cp','Cs','Ct','Er','Eu','Hk','Ht','Hl','Hb','Hm','Hp', 'Hs','Hc','Kk','Le','Lj','Bl','Ll','Lb','Gt','Mb','Mf','Mm','Mt','Mn','Mp','AM','Ema','Mo','Obl','Ob','Och','Op', 'Os','Osw','Pa','Pn','Pp','Pc','Ph','Pe','Red','Ss','Sc','Sk','Sn','Sr','Sy','Tp','Haz' ,                               'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn','CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf','OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX', 'Sb','Sw','la','Wb','Wi'    ]     ) if ${FOR} and ${NINN} and row['SOURCE'][:4] == 'PLOT'  ]" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None, 'POLYTYPE':'FOR', 'SOURCE':'Banana'}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'An Invalid Species Comp', 'POLYTYPE':'FOR', 'SOURCE':'Banana'}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; SPCOMP not multiples of 6 characters: " FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'SB 100', 'POLYTYPE':'FOR', 'SOURCE':'Banana'}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="WG:" FOLDED="true">
                        <icon BUILTIN="flag-green"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Working Group" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="WG" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="WG" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BF:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BF" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="balsam fir" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="CE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cedar" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="HE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="HE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="hemlock" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="LA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="larch" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PJ:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PJ" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="jack pine" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="red pine" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="scots pine" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PW:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PW" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="white pine" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SB" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="black spruce" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SW:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SW" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="white spruce" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SX" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spruce mix" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="other conifer" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="AX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="AX" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ash mix" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="BG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BG" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="gray birch" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="BW:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BW" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="white birch" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="BY:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BY" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yellow birch" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MH" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="hard / sugar maple" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="soft / red maple" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MX" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="maple mix" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OX" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="oak mix" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Poplar" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PB" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="balsam poplar" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OH" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="other hardwoods" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The working group attribute indicates a grouping of productive forest stands for timber management purposes. Stands which are grouped together have the same predominant species composition and are being managed under the same silvicultural system. The grouping of stands is generally labelled based on the species which dictates the management regime to be applied. Therefore, the working group normally indicates the dominant species in a forest stand based on the tree species that occupies the greatest canopy closure or the greatest amount of basal area in the stand as indicated by the species composition attribute. " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code', 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="YRORG:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <icon BUILTIN="messagebox_warning"/>
                        <icon BUILTIN="bookmark"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Year of Origin" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="YRORG" FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="The year of origin attribute contains a four digit number representing the average year that the predominant species (i.e., the species within the stand having the greatest relative abundance in terms of basal area), came into existence. " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" The year of origin is used to calculate the average age of a productive forest stand. The year of origin is determined in relation to the age of the trees and the year that regeneration (natural or  Appendix 1 Tabular Attribute Descriptions artificial) was established on a site. The year of origin is synonymous to the year of germination. If regeneration has not been established, then the year of origin will be the same as the year of last disturbance. Once trees are established on the site, the year of origin must be adjusted to reflect the average age of the established regeneration. Once a forest stand has been assessed as free-to-grow, based on attaining the regeneration/management standards, the year of origin should be adjusted to reflect the average age of the growing stock on that site. In determining the year of origin, age differences that result from natural and artificial growing stock, or age differences that occur due to modified clear-cut harvesting or shelterwood management, must be considered. For example, if a productive forest stand is harvested in two strips/passes which occur in different years (e.g., a type of modified clear-cut), the forest stand will technically have two ages for a period of time; one age for the recently disturbed strip and another age for the non-disturbed strip. The eventual strategy is to produce an even-aged forest stand. Therefore, the year of origin will eventually be determined as one average value for the forest stand. If the remaining strip is not harvested for ten-years (assuming that the harvested strip requires the same time period to reach free-growing conditions), the harvested and regenerated strip will be described by the understorey forest stand characteristics and, therefore, have a separate year of origin. The year of origin and the forest stand characteristics for the overstorey will likely not change. Once the remaining strip has been harvested and the regeneration in that strip has achieved free-to-grow status, the average age of the dominant tree species in each of the strips should be determined and then used to calculate the appropriate year of origin for the entire stand. At this point, understorey forest stand characteristics are no longer required. Normally, updates to the year of origin have a corresponding change to the year of update attribute as this indicates the currency/vintage of the information and when the year of origin value was determined. Year of origin information is not required for non-forested and non-productive forest land types." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <icon BUILTIN="messagebox_warning"/>
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="A zero value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="The YRORG attribute value must be greater than or equal to 1600 when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="The YRORG attribute must be less than the plan period start year when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The YRDEP attribute value should not be less than the YRORG attribute value (addressed in YRDEP)" FOLDED="true"/>
                                <node TEXT="The YRORG attribute value should not be greater than YRUPD attribute value" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <icon BUILTIN="messagebox_warning"/>
                                <node TEXT="[ '${name} must not be = 0' for row in [row] if row['${name}']  == 0 and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} must be &gt;= 1600 ' for row in [row] if row['${name}']  &lt;1600 and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} must be &lt;= YRSOURCE' for row in [row] if row['${name}']  &gt; row['YRSOURCE']  and row['POLYTYPE'] == 'FOR']   " FOLDED="true">
                                    <icon BUILTIN="messagebox_warning"/>
                                </node>
                                <node TEXT="[ '${name} must be &lt;= YRUPD ' for row in [row] if row['${name}']  &gt; row['YRUPD']  and row['POLYTYPE'] == 'FOR']   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="HT:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Height" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="HT" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The height attribute indicates the estimated average tree height (in meters) of the predominant species as inventoried in the year of update. Estimates can be made from interpreted crown closure or field samples, or from growth algorithms. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT="If the DEVSTAGE attribute does not start with DEP, NEW or LOW then the HT attribute must be greater than zero when POLYTYPE is equal to FOR." FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT="The value of HT should not be greater than 0.5 when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT=" If the DEVSTAGE attribute starts with FTG, then the HT attribute should be greater than or equal to 0.8 when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT="Valid numeric values are from 0 through 40.0 when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null' for row in [row] if row['${name}'] is None and row['POLYTYPE'] == 'FOR']" FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT="[ '${name} must not be blank' for row in [row] if str(row['${name}']).strip() == '' and row['POLYTYPE'] == 'FOR']" FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT="[ '${name} must not be 0 if DEVSTAGE is not DEPHARV or DEPNAT or NEWNAT or NEWPLANT or NEWSEED, LOWNAT, LOWMGMT when POLYTYPE is FOR. Consider updating DEVSTAGE?' for row in [row] if row['DEVSTAGE']  not in [ 'DEPHARV','DEPNAT','NEWNAT','NEWPLANT','NEWSEED','LOWNAT',' LOWMGMT' ] and row['POLYTYPE'] == 'FOR'  and row['${name}'] == 0.0 ]   " FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT="[ 'WARNING: ${name} / (${year} - YRORG) should not be &gt; 0.5 when POLYTYPE is FOR' for row in [row] if (row['POLYTYPE'] == 'FOR' and row['YRORG'] &gt; 0) if row['${name}'] / (${year} - row['YRORG']) &gt; 0.5  ]   " FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT="[ '${name} must be &gt;= 0.8 when DEVSTAGE like FTG% and POLYTYPE is FOR' ${:F} row['${name}'] &lt; 0.8 and row['DEVSTAGE'] in [ 'FTGNAT','FTGPLANT','FTGSEED']  ]   " FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT="[ '${name} must be &gt;= 0 and  ${name} &lt;= 40 and POLYTYPE is FOR' for row in [row] if (row['${name}'] &lt; 0 or row['${name}'] &gt; 40) and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT="[ '${name} must be &gt;= 0 and  ${name} &lt;= 40 and POLYTYPE is FOR' for row in [row] if (row['${name}'] &lt; 0 or row['${name}'] &gt; 40) and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="Height / Age relationships should be in the range of Plonski and others." FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="STKG:" FOLDED="true" BACKGROUND_COLOR="#ff9999">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="double" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="32" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="STKG" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Stocking" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The stocking attribute indicates a qualitative measure of the density of tree cover in a forest stand.  It is expressed as a percentage value ranging from zero, for recently disturbed stands, to a maximum of 4.00, although 2.50 is the typical maximum value encountered in the field.  Stocking of a forest stand refers to all species that make up the stand's canopy, but it is generally based on the species with the most basal area. " FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="Stocking is determined by comparing the actual basal area measured from field sampling to the relative basal area of a fully-stocked stand using Plonski's Normal Yield Tables. (Plonski's Normal Yield Tables were developed from permanent sample plots established for several of the major tree species in Ontario.) Stocking can also be determined from aerial photography based on the degree of canopy closure, average age, height, and species composition. Actual basal area collected from field sampling is used to calibrate stocking assessments made from photo-interpretation.  The silvicultural ground rules in a forest management plan describe the standards for assessing the regeneration of forest stands, based on forest unit, desired species composition, age, height, and stocking. If the stocking of a productive forest stand does not meet the regeneration standards in the silvicultural ground rules of an approved forest management plan, the forest stand will be considered as NSR and must be classified into the appropriate below regeneration standards stage of development.  In some cases, the regeneration and/or management standards of a silvicultural ground rule may be expressed as a density, which usually describes the frequency or number of stems per hectare. Where density information (stems/hectare) has been collected or determined from a regeneration survey, this information must be converted to a stocking value for the purpose of updating the forest stand description information. " FOLDED="true"/>
                            <node TEXT="This definition needs to be brought up to date using the current eFRI specification. There may (or may not) be a difference between traditional FRI and eFRI stocking calculations." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory  when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT=" ST1: The attribute population must follow the correct format  when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="* Valid numeric values are from 0 through 4.00" FOLDED="true"/>
                                <node TEXT="If the DEVSTAGE attribute starts with FTG, then the STKG attribute should be greater than or equal to 0.40   when POLYTYPE is equal to FOR." FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node COLOR="#999900" TEXT="If DEVSTAGE attribute starts with DEP, then STKG attribute should equal 0.00   when POLYTYPE is equal to FOR and VERT is not SV (Veterans contribute to stocking) " FOLDED="true"/>
                                <node TEXT="If DEVSTAGE attribute starts with DEP, then STKG attribute should not exceed 0.1   when POLYTYPE is equal to FOR and VERT is SV (Veterans contribute to stocking) " FOLDED="true"/>
                                <node TEXT="If DEVSTAGE attribute starts with DEP, then STKG attribute should not exceed 0.3 when POLYTYPE is equal to FOR and VERT is TU or MU " FOLDED="true"/>
                                <node TEXT="ST1: If the DEVSTAGE attribute starts with FTG, then the STKG attribute must be greater than 0.0" FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must be &gt;= 0 and  ${name} &lt;= 4.0 and POLYTYPE is FOR' for row in [row] if (row['${name}'] &lt; 0 or row['${name}'] &gt; 4.0) and row['POLYTYPE'] == 'FOR'  ]   " FOLDED="true"/>
                                <node TEXT="[ 'ST2: ${name} should be &gt; 0.4 when DEVSTAGE is FTG* when POLYTYPE is FOR.' ${:NF} row['DEVSTAGE']  in [ 'FTGNAT','FTGPLANT','FTGSEED' ] and row['${name}'] &lt; 0.4 ]   " FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="[ '${name} must = 0.0 when DEVSTAGE is DEP* when POLYTYPE is FOR in single-canopy stands' ${:F} row['DEVSTAGE']  in [ 'DEPNAT','DEPHARV' ] and row['${name}'] &gt; 0 and row['VERT'] not in [ 'SV','TU','MU']  ]   " FOLDED="true"/>
                                <node TEXT="[ 'WARNING: ${name} should not exceed 0.1 when DEVSTAGE is DEP* when POLYTYPE is FOR and VERT is SV.' for row in [row] if row['DEVSTAGE']  in [ 'DEPNAT','DEPHARV' ] and row['POLYTYPE'] == 'FOR'  and row['${name}'] &gt; 0.1 and row['VERT'] == 'SV' ]   " FOLDED="true"/>
                                <node TEXT="[ 'WARNING: ${name} should not exceed 0.3 when DEVSTAGE is DEP* when POLYTYPE is FOR and VERT is TU or MU.' for row in [row] if row['DEVSTAGE']  in [ 'DEPNAT','DEPHARV' ] and row['POLYTYPE'] == 'FOR'  and row['${name}'] &gt; 0.3 and row['VERT'] in ['TU','MU'] ]   " FOLDED="true"/>
                                <node TEXT="[ 'ST1: ${name} must be &gt; 0.0 when DEVSTAGE is FTG* when POLYTYPE is FOR.' ${:F} ${NIN} and row['DEVSTAGE']  in [ 'FTGNAT','FTGPLANT','FTGSEED' ]  ]   " FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'STKG':0.39 , 'POLYTYPE':'FOR', 'DEVSTAGE':'FTGNAT' }   " FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST2: ${name} should be &gt; 0.4 when DEVSTAGE is FTG* when POLYTYPE is FOR." FOLDED="true"/>
                                </node>
                                <node TEXT="{ 'STKG':0.0 , 'POLYTYPE':'FOR', 'DEVSTAGE':'FTGNAT' }   " FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                    <node TEXT="ST1: ${name} must be &gt; 0.0 when DEVSTAGE is FTG* when POLYTYPE is FOR." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="SC:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Site Class" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SC" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="SC" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The site class attribute indicates a site quality estimate for a stand.  Site class is an indicator of site productivity and is determined using the average height, age, and working group, based on the dominant tree species in a forest stand. These attributes are compared against height and age growth curves in Plonski's Normal Yield Tables for different species to determine the relative growth rate for a forest stand.  " FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="There is a relationship between the site class (SC) and the productive forest modifier (FORMOD) values assigned to a forested area. Generally, areas assigned a site class value of 4 (protection forest) are also assigned a productive forest modifier value of PF (protection forest), but it is not a requirement. Areas assigned a site class value other than 4 (e.g., 3) can also be assigned the productive forest modifier value of PF. Conversely areas assigned a site class value of 4 may be assigned a productive forest modifier value of production forest (RP) instead of protection forest.  The apparent discrepancy in the protection forest assignment between the site class attribute and the productive forest modifier attribute reflects the difference between a calculated assessment of site conditions (SC) versus a timber management decision (FORMOD) that is based on more than just site class. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT=" ST1: The attribute population must follow the correct format  when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="If the FORMOD attribute equals PF, then the SC attribute should equal 4  when POLYTYPE is equal to FOR and the VERT is not TU or MU" FOLDED="true"/>
                                <node TEXT="PROPOSED BY LARRY WATKINS...  (previously foresters would change areas unavailable to SC4 so they would be considered PFR rather than making it unavailable)  applies to USC as well.  Need to check when PCM come in that changes to SC as presented in the eFRI have been altered.  Larry says this was common in the past.  LARRYS COMMENT on Site Class:  For some reason the old site class 4 sort from FORTRAN keeps creeping into the FRI spec inappropriately. SC4 should me SC4 - the productivity of the site. Don't call site class 1 trees on an island or on a rocky site 4 to match a  1970s coding issue, but rather flag the other appropriate fields.  " FOLDED="true">
                                    <icon BUILTIN="info"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 0,1,2,3,4 ] and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} must be 4 when FORMOD is PF' for row in [row] if row['${name}'] not in [ 4 ] and row['POLYTYPE'] == 'FOR' and row['FORMOD'] == 'PF' ]   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="0:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="0" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Best" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="1:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="1" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Better" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="2:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="2" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Good" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="3:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="3" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Poor" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="4:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="4" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest land on which forest management activities cannot normally be practiced without incurring deleterious environmental effects because of obvious physical limitation such as steep slopes or shallow soils over bedrock." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Protection Forest" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ECOSRC:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Ecosite Source of Update" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ECOSRC" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="ECOSRC" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="ALGO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ALGO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="algorithm / model" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DIGITALA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DIGITALA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="multispectral scanning (digital image) - automated process Digital analysis, automated processing. (e.g., Ecognition)" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DIGITALP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DIGITALP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="multispectral scanning (digital image) - manual process photo-interpreted by softcopy systems" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FOC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FOC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="forest operations compliance inspection" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FRICNVRT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FRICNVRT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="forest resources inventory conversion" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MARKING:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MARKING" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pre-harvest site inspection / marking" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OCULARA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OCULARA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="aerial survey/reconnaissance" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OCULARG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OCULARG" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ocular estimate (ground)" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OPC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OPC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="operational cruise" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="air photo interpretation" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTOLS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTOLS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="large scale aerial photography" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTOPLS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTOPLS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="air photo interpretation" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PLUS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLUS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="soils data" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTOSS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTOSS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="small scale aerial photography" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PLOTFIXD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLOTFIXD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="fixed area plot" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PLOTVAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLOTVAR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="variable area (radius) plot" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="REGENASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REGENASS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="regeneration assessment" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEMEXTEN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEMEXTEN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="extensive silvicultural effectiveness monitoring survey" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEMINTEN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEMINTEN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="intensive silvicultural effectiveness monitoring survey" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The ecosite source of update attribute identifies the methodology by which the ecosite information associated with the polygon was determined (i.e., how the polygon's ecosite description was determined). " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="Note that this attribute is similar to the SOURCE attribute which indicates the methodology by which the overall stand description was determined. The main difference between the SOURCE attribute and this ECOSRC attribute is that there are two additional methodologies that are commonly used when determining ecosite information. These additional methodologies are: determination of ecosite by algorithm and determination by supplementing air photo interpretation with soils data. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code', 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="ALGO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ALGO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT=" " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="algorithm / model " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DIGITALA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DIGITALA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Digital analysis, automated processing. (e.g., Ecognition)" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="multispectral scanning (digital image) - automated process" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DIGITALP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DIGITALP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="photo-interpreted by softcopy systems" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="multispectral scanning (digital image) - manual process " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FOC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FOC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Inspection of a site after silvicultural treatment was applied to determine whether an operator / operations conforms to the approved plan or permit. The evaluation of any harvest, renewal, maintenance, or access forest management activity (e.g., post-harvest site inspection) can be included here. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="forest operations compliance inspection" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FRICNVRT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FRICNVRT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The current polygon description is based on a data conversion from traditional FRI. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="forest resources inventory conversion" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MARKING:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MARKING" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Assessment of the trees in a stand for purpose of establishing a silvicultural or operational prescription. Selecting and marking the trees to be harvested and/or the trees to be left to grow; to sustain and enhance the stand for timber management, wildlife habitat management, aesthetics, recreation, biodiversity and other environmental and heritage concerns. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pre-harvest site inspection / marking" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OCULARA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OCULARA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Visual assessment of a stand from a helicopter or fixed wing aircraft. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="aerial survey/reconnaissance" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OCULARG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OCULARG" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Visual assessment of a stand using extensive ground survey methodologies (i.e., no detailed measurements). " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ocular estimate (ground)" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OPC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OPC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Measuring standing trees to determine the volume of wood on a given tract of land. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="operational cruise" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Photography at a conventional scale of 1:10,000, 1:15,840, or 1:20,000 " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="air photo interpretation " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTOLS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTOLS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Photography at a scale larger than 1:10,000 (e.g., 1:500, 1:1000). " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="large scale aerial photography" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTOPLS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTOPLS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="air photo interpretation PLUS soils data" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTOSS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTOSS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Photography at a scale smaller than 1:20,000 (e.g., 1:100,000). " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="small scale aerial photography " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PLOTFIXD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLOTFIXD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="fixed area plot " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PLOTVAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLOTVAR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="variable area (radius) plot" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="REGENASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REGENASS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Survey of a regenerated area to determine how well the new stand is growing. This includes seeding, survival, and stocking assessments. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="regeneration assessment " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEMEXTEN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEMEXTEN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An appraisal of a forest stand's structure and composition using generalized survey sampling methodologies to determine if regeneration or management objectives have been met (i.e., determine if the expected results were achieved). Extensive survey methods are generally used where there are obvious successes or failure, or to identify problem areas requiring more intensive assessment. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="extensive silvicultural effectiveness monitoring survey " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEMINTEN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEMINTEN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An appraisal of a forest stand's structure and composition using rigorous survey sampling methodologies to determine if regeneration or management objectives have been met (i.e., determine if the expected results were achieved). Intensive survey methods are intended for stands where the status of regeneration is uncertain or specific quantitative data is required to determine the silvicultural effectiveness for operational treatments. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="intensive silvicultural effectiveness monitoring survey " FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ECOSITE1:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Ecosite 1" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="10" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ECOSITE1" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The ecosite attribute identifies an ecological landscape type that is present in the polygon and the regional ecological land classification system being used to define the ecosite code. Soil depth modifier information may also be included, if relevant. " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="Refer to the appropriate ecosystem field guide for a list of valid ecosite codes and their definitions. Ecosite is also referred to as site type. " FOLDED="true"/>
                            <node TEXT='When recording the ecosite information in this attribute, the regional land classification identifier code value (from table below) replaces the first 2 characters of the ecosite code (either "ES" or "ST"). For example, if the northwest regional ecosite code of ES11 was determined to be appropriate for the stand, it would be recorded as NW11.' FOLDED="true"/>
                            <node TEXT='Additionally, some ecosites, especially in the northwest region, can have soil depth modifier information associated with them. To record soil depth modifier information, add one of the codes from the table below to the end of the ecosite code. For example, northwest ecosite code of ES13 with a soil depth modifier of "shallow" would be recorded as:' FOLDED="true"/>
                            <node TEXT="Ecosite information which is collected or created in accordance with an ecosystem classification (e.g., wetlands ecosystem classification) may also be entered for non-productive forest lands, if desired, but it is not required. Determination of ecosite is a prerequisite of scheduling a forest stand for silvicultural treatment and is required for verifying the selected forest operations prescription in accordance with the silvicultural ground rules of an approved forest management plan. Ecosite must be identified for all free-growing, (free-to-grow) forest stands based on the pre-harvest condition, and must reflect the characteristics of the site in terms of vegetation, soils, drainage, and tree species in accordance with a forest ecosystem classification. Ecosite should not be determined after a forest stand has been subjected to forest management operations. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory for ECOSITE1 when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="The population of this attribute is only mandatory for ECOSITE1 when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must follow coding: %s'%(row['${name}']) ${:F} row['${name}'][0:2] not in ('CE', 'NE', 'NW')  ]   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ECOSITE2:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Ecosite 2" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="10" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ECOSITE2" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The ecosite attribute identifies an ecological landscape type that is present in the polygon and the regional ecological land classification system being used to define the ecosite code. Soil depth modifier information may also be included, if relevant. " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="Refer to the appropriate ecosystem field guide for a list of valid ecosite codes and their definitions. Ecosite is also referred to as site type. " FOLDED="true"/>
                            <node TEXT='When recording the ecosite information in this attribute, the regional land classification identifier code value (from table below) replaces the first 2 characters of the ecosite code (either "ES" or "ST"). For example, if the northwest regional ecosite code of ES11 was determined to be appropriate for the stand, it would be recorded as NW11.' FOLDED="true"/>
                            <node TEXT='Additionally, some ecosites, especially in the northwest region, can have soil depth modifier information associated with them. To record soil depth modifier information, add one of the codes from the table below to the end of the ecosite code. For example, northwest ecosite code of ES13 with a soil depth modifier of "shallow" would be recorded as:' FOLDED="true"/>
                            <node TEXT="Ecosite information which is collected or created in accordance with an ecosystem classification (e.g., wetlands ecosystem classification) may also be entered for non-productive forest lands, if desired, but it is not required. Determination of ecosite is a prerequisite of scheduling a forest stand for silvicultural treatment and is required for verifying the selected forest operations prescription in accordance with the silvicultural ground rules of an approved forest management plan. Ecosite must be identified for all free-growing, (free-to-grow) forest stands based on the pre-harvest condition, and must reflect the characteristics of the site in terms of vegetation, soils, drainage, and tree species in accordance with a forest ecosystem classification. Ecosite should not be determined after a forest stand has been subjected to forest management operations. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="Blank or null values ARE valid" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ECOPCT1:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Ecosite 1 Percentage" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ECOPCT1" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The ecosite percentage attribute identifies the percentage of an ecological landscape type that is present in the polygon. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory for ECOPCT1 when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="The population of this attribute is only mandatory for ECOPCT1 when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: A zero or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="Adding all of ECOPCT attributes should equal 100 when POLYTYPE is equal to FOR" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST2: Sum of &lt;mark&gt;ECOPCT&lt;/mark&gt; attributes should equal 100' ${:NF} row['${name}'] + ( 0 if row['ECOPCT2'] in ${list_NULL} else row['ECOPCT2'] ) != 100 ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'POLYTYPE':'FOR', '${name}':99, 'ECOPCT2':None }" FOLDED="true">
                                    <node TEXT="ST2: Sum of &lt;mark&gt;ECOPCT&lt;/mark&gt; attributes should equal 100" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ECOPCT2:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Ecosite 2 Percentage" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ECOPCT2" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The ecosite percentage attribute identifies the percentage of an ecological landscape type that is present in the polygon. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ECOPCT2 is NOT mandatory" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="ACCESS1:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Accessibility Indicator" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ACCESS1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="FMP_ACCESS" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The accessibility indicator attributes specifies whether or not there are any restrictions to accessing a productive forest stand. These restrictions may be legal (i.e., ownership), political / land use policy (i.e., land use designation, road closures), and/or a natural barrier. The focus of this field is identification of Crown productive forest stands that are inaccessible and therefore are not considered as part of the managed landbase for forest management planning purposes, but the principle may be applied to any area." FOLDED="true"/>
                            <node TEXT=" NOTE:   These attributes are not completed by the photo interpreters during inventory production. Access restriction information is to be determined by the forest management planning team. " FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="GEO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GEO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Area is not accessible due to geographic reasons. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="geography" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="LUD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LUD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area is not accessible for forest management purposes due to land use designation (e.g., a provincial or federal park, agreement forest, mining claim, native lands, federal lands). " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="land use designation" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NON:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NON" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The area is accessible/reachable." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="no accessibility considerations" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OWN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OWN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area of Crown land that is unreachable because it is surrounded by lands owned by an other party/parties (e.g., an area of Crown productive forest land that is not accessible for forest management because it is surrounded by private land)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ownership" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PRC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area that is no longer accessible due to the permanent closure of the only road leading into the area." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="road closure" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="STO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="STO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area of land that is owned by a party/parties other than the Crown (e.g., a parcel of private land) and where the access conditions are applied by the land owner. Note that ownership and access conditions can change over time." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="subject to ownership" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                            <node TEXT="english" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory for ACCESS1" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR ( for ACCESS1)" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR (ACCESS1)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: If ACCESS2 is not equal to NON then ACCESS1 must not be NON when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: if ACCESS2 != NON then &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not = NON'  ${:F} row['${name}'] == 'NON' and row['ACCESS2'] != 'NON' and row['ACCESS2'] not in ${list_NULL}  ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="['ST1: if ACCESS2 != NON then &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not = NON'  for row in [row] if row['${name}'] == 'NON'       and (             row['ACCESS2'] != 'NON' and             str(row['ACCESS2']).strip() != ''              and row['ACCESS2'] is not None and row['POLYTYPE'] == 'FOR'             ) ]" FOLDED="true">
                                        <icon BUILTIN="full-6"/>
                                    </node>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code', 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'LUD', 'ACCESS2':'LUD', 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="{'${name}':'NON', 'ACCESS2':'LUD', 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST1: if ACCESS2 != NON then &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not = NON" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'NON', 'POLYTYPE':'FOR', 'ACCESS2':None}" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="ACCESS2:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Accessibility Indicator 2" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ACCESS2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="FMP_ACCESS" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="Note: The ACCESS2 attribute may not exist in the submitted coverage as this attribute is not mandatory.  If this attribute exists, it may not be populated.  Therefore, this validation will be performed on the ACCESS2 attribute for the application of the correct coding scheme.  " FOLDED="true"/>
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory for ACCESS1 when POLYTYPE is equal to FOR " FOLDED="true"/>
                                <node TEXT=" ST1: The population of this attribute is mandatory for ACCESS1 when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT=" ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="If ACCESS1 or ACCESS2 do not equal NON then AVAIL should be U when POLYTYPE is equal to FOR. Tested by the AVAIL attribute" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must follow coding: %s'%(row['${name}']) for row in [row] if row['${name}'] not in [ 'GEO','LUD','NON','OWN','PRC','STO', '', ' ', None ] and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="GEO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GEO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Area is not accessible due to geographic reasons. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="geography" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="LUD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LUD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area is not accessible for forest management purposes due to land use designation (e.g., a provincial or federal park, agreement forest, mining claim, native lands, federal lands). " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="land use designation" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NON:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NON" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The area is accessible/reachable." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="no accessibility considerations" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OWN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OWN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area of Crown land that is unreachable because it is surrounded by lands owned by an other party/parties (e.g., an area of Crown productive forest land that is not accessible for forest management because it is surrounded by private land)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ownership" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PRC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area that is no longer accessible due to the permanent closure of the only road leading into the area." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="road closure" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="STO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="STO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area of land that is owned by a party/parties other than the Crown (e.g., a parcel of private land) and where the access conditions are applied by the land owner. Note that ownership and access conditions can change over time." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="subject to ownership" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="MGMTCON1:" FOLDED="true">
                        <icon BUILTIN="messagebox_warning"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MGMTCON1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="MGMTCON" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Management Consideration" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The management consideration attributes indicates whether or not ecological/landscape features or site conditions are present within the polygon. These features and conditions may require special consideration during resource management planning. " FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="The photo interpreters will only use the MGMTCON1 and MGMTCON2 attributes during inventory production. Use of the MGMTCON3 attribute is reserved for the forest management planning team." FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="COLD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="COLD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site with poor or unstable growing conditions due to the soil being frozen year-round." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="permafrost" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DAMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DAMG" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A stand which contains trees that are damaged, dead and/or dying due to natural causes (e.g., ice damage, blowdown, insect/disease damage)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="physical/natural damage  " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ISLD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ISLD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The area is or is located on an island  (i.e., an area of land that is totally surrounded by water)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="island" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NATB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NATB" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A productive stand that is unreachable due to the physical features of the surrounding area (e.g., the area is a mesa or is productive forest surrounded by non-forested wetland)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural barrier" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NONE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NONE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="There are no physical or ecological 'restrictions' in the site that need to be considered when determining management of the stand." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="no management consideration" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PENA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PENA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area of land that is nearly surrounded by water and is connected to the mainland." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="peninsula" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="POOR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="POOR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A stand exhibiting stagnated growth with no discernible cause for the poor growing condition." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="stagnated, poor tree growth - no indicator" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ROCK:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ROCK" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where the rocky conditions limit accessibility by forest management equipment and/or present a potential for soil erosion due to operations." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="exposed bedrock / rocky outcrops" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SAND:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SAND" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential for erosion." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="blow sand / exposed fine sand, shallow or no humus" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SHRB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SHRB" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult without major silvicultural intervention due to shrub or brush competition." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="heavy shrub / brush" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SOIL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SOIL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area with minimal soil depth where forest regeneration will be limited as the site could be damaged or experience degradation due to operations (e.g., potential for root damage or erosion)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="shallow soils" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="STEP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="STEP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where the degree of incline is dangerous for equipment operation and presents a potential for erosion. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="steep slopes" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="WATR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential of rising surface water." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="telluric / highly fluctuating, moving ground water" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="WETT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WETT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential of rising water tables." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="poorly drained - high water table" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <icon BUILTIN="button_ok"/>
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory  when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory for MGMTCON1 when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code for MGMTCON1" FOLDED="true"/>
                                <node TEXT="ST1: IF ACCESS1 or ACCESS2 is equal to GEO then MGMTCON1 must not be NONE" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST1: If FORMOD equals PF, then the MGMTCON1 attribute should not equal NONE" FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                </node>
                                <node TEXT="If the SC attribute equals 4, then the MGMTCON1 attribute should not equal NONE" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="full-0"/>
                                </node>
                                <node TEXT="If MGMTCON2 is not NONE then MGMTCON1 must not be NONE" FOLDED="true"/>
                                <node TEXT="If MGMTCON3 is not NONE then the MGMTCON2 and MGMTCON1 must not be NONE" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be blank or null' for row in [row] if row['${name}'] in [ None, '', ' ' ] and row['POLYTYPE'] == 'FOR'   ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding: %s'%(row['${name}']) for row in [row] if row['${name}'] not in [ 'COLD','DAMG','ISLD','NATB','NONE','PENA','POOR','','ROCK','SAND','SHRB','SOIL','STEP','WATR','WETT'] and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} must not be none when MGMTCON2 is not None' for row in [row] if row['${name}'] in [ 'NONE' ] and row['MGMTCON2'] not in [ 'NONE' , '' ,' '] ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be none when MGMTCON3 is not None' for row in [row] if row['${name}'] in [ 'NONE' ] and row['MGMTCON3'] not in [ 'NONE' , '' ,' '] ]" FOLDED="true"/>
                                <node TEXT="['ST1: If ACCESS1 or ACCESS2 are GEO, &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be NONE' ${:F} row['${name}'] == 'NONE' and row['ACCESS1'] == 'GEO' ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST1: If ACCESS1 or ACCESS2 are GEO, &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be NONE' ${:F} row['${name}'] == 'NONE' and row['ACCESS2'] == 'GEO'  ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST1: If FORMOD equals PF, &lt;b&gt;&lt;mark&gt;MGMTCON1&lt;/mark&gt;&lt;/b&gt; should not equal NONE when POLYTYPE equals FOR' ${:F} row['${name}'] == 'NONE' and row['FORMOD'] == 'PF' ]" FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                </node>
                                <node TEXT="['ST1: If SC equals 4, &lt;b&gt;&lt;mark&gt;MGMTCON1&lt;/mark&gt;&lt;/b&gt; should not equal NONE when POLYTYPE equals FOR' ${:F} row['${name}'] == 'NONE' and row['SC'] == 4 ]" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="full-0"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="MGMTCON2:" FOLDED="true">
                        <icon BUILTIN="messagebox_warning"/>
                        <icon BUILTIN="button_cancel"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MGMTCON2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="MGMTCON" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Management Consideration" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The management consideration attributes indicates whether or not ecological/landscape features or site conditions are present within the polygon. These features and conditions may require special consideration during resource management planning. " FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="The photo interpreters will only use the MGMTCON1 and MGMTCON2 attributes during inventory production. Use of the MGMTCON3 attribute is reserved for the forest management planning team." FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="COLD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="COLD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site with poor or unstable growing conditions due to the soil being frozen year-round." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="permafrost" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DAMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DAMG" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A stand which contains trees that are damaged, dead and/or dying due to natural causes (e.g., ice damage, blowdown, insect/disease damage)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="physical/natural damage  " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ISLD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ISLD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The area is or is located on an island  (i.e., an area of land that is totally surrounded by water)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="island" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NATB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NATB" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A productive stand that is unreachable due to the physical features of the surrounding area (e.g., the area is a mesa or is productive forest surrounded by non-forested wetland)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural barrier" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NONE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NONE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="There are no physical or ecological 'restrictions' in the site that need to be considered when determining management of the stand." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="no management consideration" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PENA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PENA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area of land that is nearly surrounded by water and is connected to the mainland." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="peninsula" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="POOR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="POOR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A stand exhibiting stagnated growth with no discernible cause for the poor growing condition." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="stagnated, poor tree growth - no indicator" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ROCK:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ROCK" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where the rocky conditions limit accessibility by forest management equipment and/or present a potential for soil erosion due to operations." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="exposed bedrock / rocky outcrops" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SAND:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SAND" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential for erosion." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="blow sand / exposed fine sand, shallow or no humus" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SHRB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SHRB" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult without major silvicultural intervention due to shrub or brush competition." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="heavy shrub / brush" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SOIL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SOIL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area with minimal soil depth where forest regeneration will be limited as the site could be damaged or experience degradation due to operations (e.g., potential for root damage or erosion)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="shallow soils" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="STEP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="STEP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where the degree of incline is dangerous for equipment operation and presents a potential for erosion. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="steep slopes" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="WATR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential of rising surface water." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="telluric / highly fluctuating, moving ground water" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="WETT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WETT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential of rising water tables." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="poorly drained - high water table" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory  when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT=" The attribute population must follow the correct format, or empty when POLYTYPE is equal to FOR" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must follow coding: %s'%(row['${name}']) for row in [row] if row['${name}'] not in [ 'COLD','DAMG','ISLD','NATB','NONE','PENA','POOR','','ROCK','SAND','SHRB','SOIL','STEP','WATR','WETT','',None] and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} must not be none when MGMTCON3 is not None' for row in [row] if row['${name}'] in [ 'NONE' ] and row['MGMTCON3'] not in [ 'NONE' , '' ,' ' ] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="MGMTCON3:" FOLDED="true">
                        <icon BUILTIN="messagebox_warning"/>
                        <icon BUILTIN="button_cancel"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MGMTCON3" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="MGMTCON" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Management Consideration" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The management consideration attributes indicates whether or not ecological/landscape features or site conditions are present within the polygon. These features and conditions may require special consideration during resource management planning. " FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="The photo interpreters will only use the MGMTCON1 and MGMTCON2 attributes during inventory production. Use of the MGMTCON3 attribute is reserved for the forest management planning team." FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="COLD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="COLD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site with poor or unstable growing conditions due to the soil being frozen year-round." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="permafrost" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DAMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DAMG" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A stand which contains trees that are damaged, dead and/or dying due to natural causes (e.g., ice damage, blowdown, insect/disease damage)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="physical/natural damage  " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ISLD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ISLD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The area is or is located on an island  (i.e., an area of land that is totally surrounded by water)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="island" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NATB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NATB" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A productive stand that is unreachable due to the physical features of the surrounding area (e.g., the area is a mesa or is productive forest surrounded by non-forested wetland)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural barrier" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NONE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NONE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="There are no physical or ecological 'restrictions' in the site that need to be considered when determining management of the stand." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="no management consideration" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PENA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PENA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area of land that is nearly surrounded by water and is connected to the mainland." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="peninsula" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="POOR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="POOR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A stand exhibiting stagnated growth with no discernible cause for the poor growing condition." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="stagnated, poor tree growth - no indicator" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ROCK:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ROCK" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where the rocky conditions limit accessibility by forest management equipment and/or present a potential for soil erosion due to operations." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="exposed bedrock / rocky outcrops" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SAND:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SAND" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential for erosion." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="blow sand / exposed fine sand, shallow or no humus" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SHRB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SHRB" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult without major silvicultural intervention due to shrub or brush competition." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="heavy shrub / brush" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SOIL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SOIL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area with minimal soil depth where forest regeneration will be limited as the site could be damaged or experience degradation due to operations (e.g., potential for root damage or erosion)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="shallow soils" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="STEP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="STEP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where the degree of incline is dangerous for equipment operation and presents a potential for erosion. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="steep slopes" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="WATR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential of rising surface water." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="telluric / highly fluctuating, moving ground water" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="WETT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WETT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential of rising water tables." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="poorly drained - high water table" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory  when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT=" The attribute population must follow the correct format, or empty when POLYTYPE is equal to FOR" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must follow coding: %s'%(row['${name}']) for row in [row] if row['${name}'] not in [ 'COLD','DAMG','ISLD','NATB','NONE','PENA','POOR','','ROCK','SAND','SHRB','SOIL','STEP','WATR','WETT','',None] and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="AGS_POLE:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Polewood Acceptable Growing Stock" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AGS_POLE" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                                <node TEXT=" If the DEVSTAGE attribute is IMPROVE, SELECT, SNGLTREE or GROUPSE, then one of  these attributes must be greater than zero" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="AGS_SML:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Sm Sawlog Acceptable Growing Stock" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AGS_SML" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                                <node TEXT=" If the DEVSTAGE attribute is IMPROVE, SELECT, SNGLTREE or GROUPSE, then one of  these attributes must be greater than zero" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="AGS_MED:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Med Sawlog Acceptable Growing Stock" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AGS_MED" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                                <node TEXT=" If the DEVSTAGE attribute is IMPROVE, SELECT, SNGLTREE or GROUPSE, then one of  these attributes must be greater than zero" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="AGS_LGE:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Lg Sawlog Acceptable Growing Stock" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AGS_LGE" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                                <node TEXT=" If the DEVSTAGE attribute is IMPROVE, SELECT, SNGLTREE or GROUPSE, then one of  these attributes must be greater than zero" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="UGS_POLE:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Polewood Unacceptable Growing Stock" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="UGS_POLE" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                                <node TEXT=" If the DEVSTAGE attribute is IMPROVE, SELECT, SNGLTREE or GROUPSE, then one of  these attributes must be greater than zero" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="UGS_SML:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Sm Sawlog Unacceptable Growing Stock" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="UGS_SML" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                                <node TEXT=" If the DEVSTAGE attribute is IMPROVE, SELECT, SNGLTREE or GROUPSE, then one of  these attributes must be greater than zero" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="UGS_MED:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Med Sawlog Unacceptable Growing Stock" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="UGS_MED" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                                <node TEXT=" If the DEVSTAGE attribute is IMPROVE, SELECT, SNGLTREE or GROUPSE, then one of  these attributes must be greater than zero" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="UGS_LGE:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Lg Sawlog Unacceptable Growing Stock" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="UGS_LGE" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                                <node TEXT=" If the DEVSTAGE attribute is IMPROVE, SELECT, SNGLTREE or GROUPSE, then one of  these attributes must be greater than zero" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="USPCOMP:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Understorey Species Composition" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="120" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="USPCOMP" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="This understorey species composition attribute indicates the tree species that are present in the understorey portion of the forest stand canopy and the proportion of cover that each species occupies within the understorey. " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="See SPCOMP list in eFRI tech spec for list of acceptable species" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <icon BUILTIN="messagebox_warning"/>
                                <node TEXT="Pattern is SSSPPPSSSPPP   example: PJ  80PO  20  (there are two blanks between the species and the proportion) " FOLDED="true"/>
                                <node TEXT=" No duplicate species codes allowed in the string" FOLDED="true"/>
                                <node TEXT="Proportion values in the string must sum to 100 " FOLDED="true"/>
                                <node TEXT="The tree species in the composition are to be coded using the scheme listed here.   " FOLDED="true"/>
                                <node TEXT="*each species code is 3 characters (including blanks) and is left justified" FOLDED="true"/>
                                <node TEXT="*each proportion is 3 characters which represents an integer value from 1 to 100 and is right justified. Note that initial inventory values determined by photo interpretation will not go below 10% and are rounded to the nearest 10%  (e.g., 10, 20, 30). During inventory update, proportions based on subsequent field survey sampling that are of a finer resolution can be entered (e.g., go below 10% or be a value in between the initial rounded proportions such as 15). " FOLDED="true"/>
                                <node TEXT="*maximum of 10 species and proportions pairs in the string" FOLDED="true"/>
                                <node TEXT="If SOURCE is NOT plot based then allow specific species in SPCOMP (eFRI tech spec)" FOLDED="true"/>
                                <node TEXT="Tree species in the list [     'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn',     'CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf',     'OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX',     'OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX',     'Sb','Sw','la','Wb','Wi'     ]" FOLDED="true"/>
                                <node TEXT="If SOURCE is plot based (PLOTVAR, PLOTFIXD) then allow other species in SPCOMP (eFRI tech spec)" FOLDED="true"/>
                                <node TEXT="Plot Species are [     'AL','Aq','Ap','Ag','Bc','Bp','Gb','Bb','Cat','Cc','Cm',     'Cp','Cs','Ct','Er','Eu','Hk','Ht','Hl','Hb','Hm','Hp',     'Hs','Hc','Kk','Le','Lj','Bl','Ll','Lb','Gt','Mb','Mf',     'Mm','Mt','Mn','Mp','AM','Ema','Mo','Obl','Ob','Och','Op',     'Os','Osw','Pa','Pn','Pp','Pc','Ph','Pe','Red','Ss','Sc',     'Sk','Sn','Sr','Sy','Tp','Haz'     ]" FOLDED="true"/>
                                <node TEXT="If SPCOMP percentages are not in 10's then SOURCE must be plot based. 1's not 10's" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST%s: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; %s'%( '2' if 'WARNING' in i else '1'   ,i)    for i in test_spcomp(row['${name}']) if ${NINN} if i not in  [ None, [ None ] , []  ] ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <icon BUILTIN="full-5"/>
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="[ '%s is not a valid species code'%(s) for s in test_SpeciesInList(row['${name}'], [ 'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn','CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf','OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX', 'Sb','Sw','la','Wb','Wi' ]        ) if row['POLYTYPE'] == 'FOR'  and row['SOURCE'][:4] != 'PLOT' and row['VERT'] in ['TO','TU','MO','MU']  and row['${name}'] is not None ]" FOLDED="true"/>
                                <node TEXT="[ '%s is not a valid species code'%(s) for s in test_SpeciesInList(row['${name}'],    [ 'AL','Aq','Ap','Ag','Bc','Bp','Gb','Bb','Cat','Cc','Cm', 'Cp','Cs','Ct','Er','Eu','Hk','Ht','Hl','Hb','Hm','Hp', 'Hs','Hc','Kk','Le','Lj','Bl','Ll','Lb','Gt','Mb','Mf','Mm','Mt','Mn','Mp','AM','Ema','Mo','Obl','Ob','Och','Op', 'Os','Osw','Pa','Pn','Pp','Pc','Ph','Pe','Red','Ss','Sc','Sk','Sn','Sr','Sy','Tp','Haz' ,                               'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn','CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf','OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX', 'Sb','Sw','la','Wb','Wi'    ]     ) if row['POLYTYPE'] == 'FOR' and row['SOURCE'][:4] == 'PLOT' and row['VERT'] in ['TO','TU','MO','MU'] and row['${name}'] is not None ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="UWG:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Understory Working Group" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="UWG" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code', 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BF:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BF" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="balsam fir" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="CE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cedar" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="HE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="HE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="hemlock" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="LA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="larch" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PJ:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PJ" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="jack pine" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="red pine" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="scots pine" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PW:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PW" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="white pine" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SB" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="black spruce" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SW:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SW" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="white spruce" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SX" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spruce mix" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="other conifer" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="AX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="AX" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ash mix" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="BG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BG" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="gray birch" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="BW:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BW" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="white birch" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="BY:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BY" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yellow birch" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MH" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="hard / sugar maple" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="soft / red maple" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MX" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="maple mix" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OX" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="oak mix" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Poplar" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PB" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="balsam poplar" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OH" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="other hardwoods" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="UYRORG:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Understory Year of Origin" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="UYRORG" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The understorey year of origin attribute indicates the average year that the species having the greatest relative abundance (in terms of basal area) within the understorey came into existence." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="The year of origin is used to calculate the average age of a productive forest stand. The year of origin is determined in relation to the age of the trees and the year that regeneration (natural or artificial) was established on a site. The year of origin is synonymous to the year of germination. If regeneration has not been established, then the year of origin will be the same as the year of last disturbance. Once trees are established on the site, the year of origin must be adjusted to reflect the average age of the established regeneration.   Once a forest stand has been assessed as free-to-grow, based on attaining the regeneration/management standards, the year of origin should be adjusted to reflect the average age of the growing stock on that site.   In determining the year of origin, age differences that result from natural and artificial growing stock, or age differences that occur due to modified clear-cut harvesting or shelterwood management, must be considered.  For example, if a productive forest stand is harvested in two strips/passes which occur in different years (e.g., a type of modified clear-cut), the forest stand will technically have two ages for a period of time; one age for the recently disturbed strip and another age for the non-disturbed strip. The eventual strategy is to produce an even-aged forest stand. Therefore, the year of origin will eventually be determined as one average value for the forest stand. If the remaining strip is not harvested for ten-years (assuming that the harvested strip requires the same time period to reach free-growing conditions), the harvested and regenerated strip will be described by the understorey forest stand characteristics and, therefore, have a separate year of origin. The year of origin and the forest stand characteristics for the overstorey will likely not change. Once the remaining strip has been harvested and the regeneration in that strip has achieved free-to-grow status, the average age of the dominant tree species in each of the strips should be determined and then used to calculate the appropriate year of origin for the entire stand. At this point, understorey forest stand characteristics are no longer required.  Normally, updates to the year of origin have a corresponding change to the year of update attribute as this indicates the currency/vintage of the information and when the year of origin value was determined.   Year of origin information is not required for non-forested and non-productive forest land types. " FOLDED="true"/>
                            <node TEXT="This is a calculated value. Understorey year of origin is calculated as the year of the data source from which age is determined  minus  the understorey age as determined using the source  (i.e.,  UYRORG  =  year of age source - UAGE).  For example, using imagery taken in 2007, the age of the stand is interpreted to be 50 years at that time (in 2007).  Thus the year of origin for the stand is 2007 - 50 = 1957.  Note that the source used to determine age may not be the data source listed in the SOURCE attribute if multiple data sources were used to generate the stand description.  " FOLDED="true">
                                <icon BUILTIN="help"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: The UYRORG attribute value must be greater than or equal to 1600 when POLYTYPE is equal to FOR" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be &gt;= 1600' ${:NF} row['${name}']  &lt;1600  ]   " FOLDED="true"/>
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; should be &gt; YRORG' ${:NF} row['${name}']  &lt;= row['YRORG']  ]   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':1599, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be &gt;= 1600" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':1899, 'POLYTYPE':'FOR', 'YRORG':1930 }" FOLDED="true">
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; should be &gt; YRORG" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="UHT:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Understory Height" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="UHT" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The understorey height attribute indicates the estimated average tree height (in meters) of the species that has the most basal area as inventoried in the Year of Update. Estimates can be made from interpreted crown closure or field samples, or from growth algorithms." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="Valid numeric values are from 0 through 40.0 when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="Value should be less than HT when POLYTYPE is equal to FOR." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must be &gt;= 0 and  ${name} &lt;= 40 and POLYTYPE is FOR' for row in [row] if (row['${name}'] &lt; 0 or row['${name}'] &gt; 40) and row['POLYTYPE'] == 'FOR'  ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} should be &lt;= HT' for row in [row] if row['${name}']  &gt; row['HT']  and row['POLYTYPE'] == 'FOR']   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="Height / Age relationships should be in the range of Plonski and others." FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="USTKG:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Understory Stocking" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="USTKG" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The understorey stocking attribute indicates a qualitative measure of the density of tree cover within the understorey.  It is expressed as a percentage value ranging from zero to a maximum of 4.00, although 2.50 is the typical maximum value encountered in the field." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct format when the POLYTYPE is equal to FOR" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must be &gt;= 0 and  ${name} &lt;= 4.00 and POLYTYPE is FOR' for row in [row] if (row['${name}'] &lt; 0 or row['${name}'] &gt; 4.01) and row['POLYTYPE'] == 'FOR'  ]   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="USC:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="USC" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Understorey Site Class" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The understorey site class attribute indicates a site quality estimate for the understorey of a forest stand. It is determined using the average height, age, and working group, based on the dominant tree species of the understorey. These attributes are compared against height and age growth curves in Plonski's Normal Yield Tables for different species to determine the relative growth rate for a forest stand.   " FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="0:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="0" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Best" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="1:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="1" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Better" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="2:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="2" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Good" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="3:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="3" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Poor" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="4:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="4" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest land on which forest management activities cannot normally be practiced without incurring deleterious environmental effects because of obvious physical limitation such as steep slopes or shallow soils over bedrock." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Protection Forest" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" ST1: The attribute population must follow the correct format  when POLYTYPE is equal to FOR" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 0,1,2,3,4 ] and row['POLYTYPE'] == 'FOR'  ]   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="VERDATE:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Date" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="VERDATE" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Verification Status Date" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The verification status date attribute contains the date that the geographic unit was verified/validated.  " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="SENSITIV:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SENSITIV" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Data Sensitivity Indicator" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The data sensitivity indicator attribute contains an indication of whether the geographic unit is classified as sensitive or not." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                                <node TEXT="Contains yes or no" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Planning Composite (PCM)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="Plan" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                        <node TEXT="POLYID must contain unique values" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true">
                        <node TEXT="[ 'POLYID Field values must be unique' for f in ['POLYID' ] if not self.uniqueValueTest(f) ]" FOLDED="true"/>
                    </node>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
                <node TEXT="symbology:" FOLDED="true">
                    <node TEXT="type" FOLDED="true">
                        <node TEXT="categorized" FOLDED="true"/>
                    </node>
                    <node TEXT="field" FOLDED="true">
                        <node TEXT="POLYTYPE" FOLDED="true"/>
                    </node>
                    <node TEXT="stroke color" FOLDED="true">
                        <node TEXT="#777777" FOLDED="true"/>
                    </node>
                    <node TEXT="fill color" FOLDED="true">
                        <node TEXT="#FFFFFF" FOLDED="true"/>
                    </node>
                </node>
            </node>
            <node TEXT="BaseModelInventory:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="BaseModelInventory" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="BMI" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields_oldversion" FOLDED="true">
                    <node TEXT="FMFOBJID:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="FMFOBJID" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="13" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="FMFOBJID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition:" FOLDED="true">
                            <node TEXT="The FMF Object Identifier attribute information is supplied by MNR and is intended to be used to identify / track change data in the future. This is a numeric value supplied by MNR. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="This field is optional and will be blank for new SFL maintained polygons" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="shortname" FOLDED="true">
                            <node TEXT="FMFOBJID" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="GEOGNUM:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="GEOGNUM" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="7" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="GEOGNUM" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The geographic unit type number attribute information is supplied by MNR. This is a numeric value supplied by MNR. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="This field is optional and will be blank for new SFL maintained polygons" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="shortname" FOLDED="true">
                            <node TEXT="POLYID" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="POLYID:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="25" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="POLYID" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Polygon Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The polygon identifier attribute is a unique identifier / label for the polygon. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true"/>
                                <node TEXT="Attribute must contain a unique value" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null' for row in [row] if row['${name}'] is None    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank' for row in [row] if str(row['${name}']).strip() == ''    ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="shortname" FOLDED="true">
                            <node TEXT="POLYID" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="POLYTYPE:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="POLYTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="POLYTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Polygon Type" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The polygon type attribute indicates the classification of the area within the polygon boundaries into one of several generalized water and land types. " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="The polygon type is determined from the classification of area on a forest management unit into different water and land types. Some polygon types are derived from inventory base features, while other polygons must be created from a classification process.  All remaining land areas within a designated forest management unit are classified into various non-forested or forested lands. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true"/>
                                <node TEXT="If POLYTYPE attribute does not equal FOR, then FORMOD, DEVSTAGE, YRDEP, SPCOMP, WG, YRORG, HT, STKG, SC, ECOSRC, ECOSITE1, ECOPCT1, ECOSITE2, ECOPCT2, ACCESS1, ACCESS2, MGMTCON1, MGMTCON2, MGMTCON3, AGS_POLE, AGS_SML, AGS_MED, AGS_LGE, UGS_POLE, UGS_SML, UGS_MED, UGS_LGE, USPCOMP, UWG, UYRORG, UHT, USTKG, USC, MANAGED, TYPE, MNRCODE, SMZ, OMZ, PLANFU, AGESTR, AGE, AVAIL, SILVSYS, NEXTSTG, SI, and SISRC attributes should be empty" FOLDED="true">
                                    <icon BUILTIN="stop-sign"/>
                                </node>
                                <node TEXT="If a delineated polygon is an island or is located on an island, the management consideration attribute is set to island (MGMTCON1 = ISLD). This applies to all polygon types. This apparent redundancy in classification for polygons having a type of island (i.e., POLYTYPE = ISL   and   MGMTCON1 = ISLD) allow resource managers to easily identify:   1.) all polygons located on islands regardless of polygon type (by querying on MGMTCON1 = ISLD)  2.) just the small uninterpreted islands (by querying on POLYTYPE = ISL) depending upon the desired analysis. " FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must follow coding: %s'%(row['${name}']) for row in [row] if row['${name}'] not in [ 'WAT', 'DAL', 'GRS', 'ISL', 'UCL', 'BSH', 'RCK', 'TMS', 'OMS', 'FOR'] ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} must not be null' for row in [row] if row['${name}'] is None    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank' for row in [row] if str(row['${name}']).strip() == ''    ]" FOLDED="true"/>
                                <node TEXT="[ &quot;POLYTYPE ISL implies MGMTCON1 ISLD&quot; for row in [ row ] if  row['POLYTYPE'] == 'ISL' and row['MGMTCON1'] != 'ISLD' ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="WAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WAT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node STYLE="fork" TEXT="All water areas.  Includes lakes, ponds, reservoirs (i.e., inland basin areas containing water) and wide (two sided) rivers.  These are rivers that can be defined by area.  Generally, these rivers/streams are consistently wider than 20 meters when portrayed at a mapping scale of 1:10,000 or consistently wider than 40 meters when portrayed at a mapping scale of 1:20,000.  Smaller/narrower rivers and streams are maintained as linear features in a centre-line layer(s)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Water" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DAL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Lands which are cultivated for growing crops, orchards, floral gardens, etc. These areas may include abandoned agricultural lands. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Developed agricultural land" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="GRS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GRS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT='Farm areas devoted to pasture for domesticated animals. These areas may also include abandoned grass and meadows, but are not part of the productive forest land base and do not include "barren and scattered" areas. These areas are similar to barren and scattered, but are located near developed agriculture land or unclassified areas and are usually fenced. ' FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Grass and meadow" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ISL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ISL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Islands less than 8 hectares in size, down to a lower limit of 0.0025 hectares or 25 square meters in size (e.g., 5 meters X 5 meters) are recorded during the inventory production process, but are not interpreted/typed for practicality and cost considerations. Only islands 8 hectares and larger are interpreted and assigned an appropriate POLYTYPE code, such as FOR or BSH. * " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Small island" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="UCL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="UCL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="None" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Unclassified" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="BSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BSH" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Areas covered with 'non-commercial' tree species or shrubs.  These areas are normally associated with wetlands or water features." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Brush and alder" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OMS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OMS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Wet areas of mosses, grasses, sedges, and small herbaceous plants, often interspersed with small areas of open water." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Open wetland" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="RCK:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="RCK" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Areas of barren or exposed rock (e.g., bedrock, cliff face, talus slope) which may support a few scattered trees, but is less that 25% stocked." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Rock" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="TMS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="TMS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Areas of dry or wet muskeg on which stunted trees occur as widely spaced individuals or in small groups." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Treed wetland" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FOR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Areas that are capable of producing trees and can support tree growth.  These areas may or may not be capable of supporting the harvesting of timber on a sustained yield basis.  Some areas may have physical and/or biological characteristics which effect land use.  Thus this polygon type includes both production and protection forest areas. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Productive forest" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="OWNER:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="OWNER" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="string" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="OWNER" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Ownership" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="This ownership attribute contains the traditional FRI ownership information as assigned by the Office of the Surveyor General." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="The ownership designation attribute is derived from the ownership and land tenure, and parks and reserves layers which are maintained in the MNR's values information system. This attribute also identifies the managed Crown area in a forest management unit.  Any discrepancies regarding the information contained in the ownership designation attribute should be reported to the appropriate MNR district office. The most accurate source for ownership information is located in the appropriate regional Land Registry Office. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null' for row in [row] if row['${name}'] is None    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding: %s'%(row['${name}']) for row in [row] if row['${name}'] not in [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9] ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank' for row in [row] if str(row['${name}']).strip() == ''    ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="0:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="0" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Unknown / unassigned ownership" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="1:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="1" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Crown land" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="2:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="2" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Patent land - with timber rights reserved to the Crown" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="3:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="3" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Patent land - fee simple (private)" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="4:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="4" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Patent land- Company Freehold" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="5:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="5" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Provincial Park" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="6:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="6" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Indian Reserve" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="7:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="7" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Recreation Reserve" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="8:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="8" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Agreement Forest" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="9:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="9" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Federal Reserve" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="AUTHORTY:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AUTHORTY" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="string" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="AUTHORTY" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Ownership Designation" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The ownership designation attribute indicates the custodian responsible for providing the inventory information about the polygon.  (The party responsible for maintaining and providing the forest resources inventory information in accordance with the Forest Information Manual; that is, for determining the polygon description.) " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="As of 2006 the forest resource inventory is the responsibility of MNR and therefore the default value for the inventory is MNR.  " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must follow coding: %s'%(row['${name}']) for row in [row] if row['${name}'] not in [ 'SFL', 'MNR'] ]   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="SFL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SFL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Licensee" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MNR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MNR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Ministry of Natural Resources" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="YRUPD:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Year of Update" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="YRUPD" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The year of update attribute contains a four-digit number representing the calendar year (January 1 to December 31) of the source data used to determine / update the polygon description, in particular the height attribute (A1.1.15). " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="The year of update identifies the calendar year that information about a polygon was last confirmed or modified based on field inspection, photo interpretation, analysis of satellite imagery, or conversion or update to spatial or tabular data. The year of update should not be changed to reflect error corrections to tabular attributes. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                                <node TEXT="A zero value is not a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="  [ '${name} must be &gt;= 1975 and &lt; %[year[' for row in [row] if row['${name}']  &lt;1975 ]   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="SFL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SFL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Licensee" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MNR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MNR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Ministry of Natural Resources" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="SOURCE:" FOLDED="true" BACKGROUND_COLOR="#ff9999">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SOURCE" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="SOURCE" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Source of Update Data " FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The source of update data  attribute identifies the methodology by which the information stored in the other tabular attributes that are associated with the same polygon was determined (i.e., how the polygon description was determined). " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="The source of data update attribute does not apply to the changes or updates made to the ecosite attribute." FOLDED="true"/>
                            <node TEXT="FIXME: Is this value list correct? What is SPECTRAL? What is INFRARED?" FOLDED="true">
                                <icon BUILTIN="messagebox_warning"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true"/>
                                <node TEXT="A value of FORECAST is only valid for the SOURCE attribute in the Base Model Inventory" FOLDED="true"/>
                                <node TEXT="If the DEVSTAGE attribute starts with FTG, then the SOURCE attribute should not equal ESTIMATE" FOLDED="true"/>
                                <node TEXT="If the DEVSTAGE attribute starts with NEW, then the SOURCE attribute should be ESTIMATE. **** FIM Difference - Allow ESTIMATE in EPC product to allow new treatment estimates." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null' for row in [row] if row['${name}'] is None    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank' for row in [row] if str(row['${name}']).strip() == ''    ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be ESTIMATE when DEVSTAGE LIKE FTG% ' for row in [row] if row['${name}']  == 'ESTIMATE' and row['DEVSTAGE'] in [ 'FTGNAT','FTGPLANT','FTGSEED' ] ]   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BASECOVR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BASECOVR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Information that is provided by the MNR  (e.g., water or evaluated wetlands)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="planimetric base layer" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DIGITALA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DIGITALA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Digital analysis, automated processing. (e.g., Ecognition)" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="multispectral scanning (digital image) - automated process" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DIGITALP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DIGITALP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Photo-interpreted by softcopy systems" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="multispectral scanning (digital image) - manual process" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ESTIMATE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ESTIMATE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This source option is only for use in areas that have been recently renewed and have not been revisited since the renewal work was performed.  That is, where a follow-up survey has not yet been performed (e.g., regeneration survey, FTG survey).  Therefore, the description of the newly regenerated stand is a 'best estimate' of the expected outcome / result of the renewal treatment that was applied to the area based on past silvicultural successes." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="expected / estimated outcome / result" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FOC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FOC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Inspection of a site after silvicultural treatment to determine whether an operator / operations conforms to the approved plan or permit.   The evaluation of any harvest, renewal, maintenance, or access forest management activity (e.g., post-harvest site inspection) can be included here." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="forest operations compliance inspection" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FORECAST:" FOLDED="true" BACKGROUND_COLOR="#99ff99">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FORECAST" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This indicates a polygon description that was updated based on expected outcomes of planned operations (which have not yet been implemented) for the remainder of the current plan term.  This code is only valid in the Base Model Inventory. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="forecasted description" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FRICNVRT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FRICNVRT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Current polygon description based on data conversion from traditional FRI  " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="forest resources inventory conversion" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="INFRARED:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="INFRARED" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Infrared satellite imagery" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MARKING:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MARKING" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Assessment of the trees in a stand for purpose of establishing a silvicultural or operational prescription.  Selecting and marking the trees to be harvested and/or the trees to be left to grow; to sustain and enhance the stand for timber management, wildlife habitat management, aesthetics, recreation, biodiversity and other environmental and heritage concerns." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pre-harvest site inspection / marking" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OCULARA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OCULARA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Visual assessment of a stand from a helicopter or fixed wing aircraft. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="aerial survey / reconnaissance" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OCULARG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OCULARG" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Visual assessment of a stand using extensive ground survey methodologies (i.e., no detailed measurements)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ocular estimate (ground)" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OPC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OPC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Measuring standing trees to determine the volume of wood on a given tract of land." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="operational cruise" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Photography at a conventional scale of 1:10,000, 1:15,840, or 1:20,000" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="air photo interpretation" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTOLS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTOLS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Photography at a scale larger than 1:10,000  (e.g., 1:500, 1:1000)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="large scale aerial photography" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTOSS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTOSS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Photography at a scale smaller than 1:20,000  (e.g., 1:100,000)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="small scale aerial photography" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PLOTFIXD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLOTFIXD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="fixed area plot" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PLOTVAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLOTVAR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="variable area (radius) plot" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="RADAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="RADAR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="radar satellite imagery" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="REGENASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REGENASS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Survey of a regenerated area to determine how well the new stand is growing.  This includes seeding, survival, and stocking assessments." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="regeneration assessment" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEMEXTEN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEMEXTEN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An appraisal of a forest stand's structure and composition using generalized survey sampling methodologies to determine if regeneration or management objectives have been met (i.e., determine if the expected results were achieved).   Extensive survey methods are generally used where there are obvious successes or failure, or to identify problem areas requiring more intensive assessment." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="extensive silvicultural effectiveness monitoring survey" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEMINTEN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEMINTEN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An appraisal of a forest stand's structure and composition using rigorous survey sampling methodologies to determine if regeneration or management objectives have been met (i.e., determine if the expected results were achieved).  Intensive survey methods are intended for stands where the status of regeneration is uncertain or specific quantitative data is required to determine the silvicultural effectiveness for operational treatments. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="intensive silvicultural effectiveness monitoring survey" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SPECTRAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SPECTRAL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spectral satellite imagery" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SUPINFO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SUPINFO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Stand update information from a source other than those listed/defined in the other coding options that is provided by either MNR or Licensee. For example, disturbance records. The date of information capture/acquisition and the age of the stand as of the date of information was acquired must be supplied for the information to be incorporated into the inventory." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Supplied information" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="FORMOD:" FOLDED="true">
                        <icon BUILTIN="full-1"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="FORMOD" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="FORMOD" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="RP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="RP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Production Forest - Regular" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas which are considered to be production forest, but that are unavailable for timber production as determined through the forest management planning process. That is, these areas have been identified as no-cut areas through area of concern planning for the purpose of protecting values (e.g., reserve buffers applied to protect tourism values)." FOLDED="true"/>
                                    <node TEXT="Designated management reserves are areas that were managed as an operational reserve during previous forest management plan terms. That is, they are actual operational or management reserve areas created by the implementation of forest management operations around them. As guides are updated, these areas should be reviewed as some may return to production forest. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Production Forest - Designated Management Reserve" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PF:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PF" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest land on which forest management activities cannot normally be practiced without incurring deleterious environmental effects because of obvious physical limitation such as steep slopes or shallow soils over bedrock." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Protection Forest" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The productive forest modifier attribute represents a further classification (sub-division) of productive forest areas based on the presence or absence of physical or biological factors which may influence the ability to practice timber management." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="The productive forest modifier must be identified for every productive forest stand and must be used in conjunction with the management consideration attribute.   There is a relationship between the site class (SC) and the productive forest modifier (FORMOD) values assigned to a forested area. Generally, areas assigned a site class value of 4 (protection forest) are also assigned a productive forest modifier value of PF (protection forest), but it is not a requirement.  Areas assigned a site class value other than 4 (e.g., 3) can also be assigned the productive forest modifier value of PF. Conversely areas assigned a site class value of 4 may be assigned a productive forest modifier value of production forest (RP) instead of protection forest.  The apparent discrepancy in the protection forest assignment between the site class attribute and the productive forest modifier attribute reflects the difference between a calculated assessment of site conditions (SC) versus a timber management decision (FORMOD) that is based on more than just site class.  " FOLDED="true"/>
                            <node TEXT="If the productive forest modifier is set to protection forest (FORMOD = PF), then the reason for the designation must be entered in the management consideration attribute (MGMTCON1 &lt;&gt; NONE).  " FOLDED="true"/>
                            <node TEXT="There is a relationship between the site class (OSC, USC) and the productive forest modifier (FORMOD) values assigned to a forested area. Generally, areas assigned a site class value of 4 (protection forest) are also assigned a productive forest modifier value of PF (protection forest), but it is not a requirement. Areas assigned a site class value other than 4 (e.g., 3) can also be assigned the productive forest modifier value of PF. Conversely areas assigned a site class value of 4 may be assigned a productive forest modifier value of production forest (RP) instead of protection forest. The apparent discrepancy in the protection forest assignment between the site class attribute and the productive forest modifier attribute reflects the difference between a calculated assessment of site conditions (OSC, USC) versus a timber management decision (FORMOD) that is based on more than just site class.  Our validation has changed from FIM Tech Specs." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="A value of MR is only valid for the FORMOD attribute in the Base Model Inventory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null' for row in [row] if row['${name}'] is None and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank' for row in [row] if str(row['${name}']).strip() == ''  and row['POLYTYPE'] == 'FOR' ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [  'RP','PF','MR'  ] and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="DEVSTAGE:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="DEVSTAGE" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Stage of Development" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="DEVSTAGE" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="LOWMGMT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LOWMGMT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest stands which were previously harvested and have not reached the regeneration standards as described in an approved forest management plan within the estimated timeframe.  Further, these areas require additional silvicultural treatment to bring them up to regeneration standards.  This does not include areas that have been recently disturbed or recently renewed.  However, it may include areas which have received renewal treatments in the past that have failed to produce a regenerated forest to the applicable regeneration standards.  This option may also include those areas which have traditionally been designated as barren and scattered (i.e., stocking less than 25%).  " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Below regeneration standards due to past management " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="LOWNAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LOWNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest stands which were previously disturbed by natural causes and have not reached the regeneration standards as described in an approved forest management plan within the estimated timeframe.  Further, these areas require additional silvicultural treatment to bring them up to regeneration standards.  This does not include areas that have been recently disturbed or recently renewed.  However, it may include areas which have received renewal treatments in the past that have failed to produce a regenerated forest to the applicable regeneration standards.  This option may also include those areas which have traditionally been designated as barren and scattered (i.e., stocking less than 25%).  " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Below regeneration standards due to natural causes / succession " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DEPHARV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DEPHARV" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest area that was recently disturbed by clearcut harvesting and has not received a silvicultural treatment such as natural regeneration, seeding or planting.  These areas do not have advanced regeneration, or a distinct or established regeneration layer that would be released and/or protected as part of the depletion operation." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="recently disturbed by harvest and has received no regeneration/renewal treatment " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DEPNAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DEPNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest area that was recently disturbed by natural causes (i.e., fire, blowdown, ice damage, insect and disease) and has not received a silvicultural treatment such as natural regeneration, seeding or planting.  These areas do not have advanced regeneration, or a distinct or established regeneration layer." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="recently disturbed by natural causes and has received no regeneration / renewal treatment " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NEWPLANT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NEWPLANT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas which have been regenerated predominantly by planting, but have not been assessed as free-to-grow" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="recently renewed : mainly planted" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NEWSEED:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NEWSEED" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas which have been regenerated predominantly by seeding, but have not been assessed as free-to-grow" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="recently renewed : mainly seeded " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NEWNAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NEWNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas which have been regenerated predominantly by natural means, but have not been assessed as free-to-grow" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="recently renewed : mainly natural regeneration " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FTGPLANT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FTGPLANT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas which were regenerated predominantly from planted stock and which have been assessed as free-to-grow.  " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="free-to-grow mainly planted" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FTGSEED:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FTGSEED" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas which were regenerated predominantly by seeding and which have been assessed as free-to-grow.  " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="free-to-grow mainly seeded" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FTGNAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FTGNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas which were regenerated predominantly by natural means and which have been assessed as free-to-grow. This classification will also be used to describe the forested areas that have never been treated to date (original forest) that would be considered as free growing.  " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="free-to-grow mainly natural regeneration " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="THINPRE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="THINPRE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Free-growing productive forest areas which have received a mid-rotation thinning/spacing to promote the growth of the best quality trees.  The trees selected for removal do not result in a harvest of merchantable volume. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received pre-commercial thinning/spacing treatment" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="THINCOM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="THINCOM" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Free-growing productive forest areas which have received a mid-rotation partial harvest designed to promote the growth of the best quality trees.  The harvested trees are removed from the site and used for commercial purposes. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received commercial thinning/spacing treatment " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="STRIPCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="STRIPCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of a portion of the existing trees in a stand in progressive strips in more than one operation so that the non-depleted portion of the stand is left primarily to provide a natural seed source for regeneration of the depleted area. Several cutting patterns are available to achieve same goal." FOLDED="true"/>
                                    <node TEXT="The removal of trees in one or more passes in a system of strips of various widths; where each strip is less than or equal to 100 meters (5 chains) wide. It is designed to encourage regeneration on difficult and/or fragile sites." FOLDED="true"/>
                                    <node TEXT="Note: Harvesting where the cut strips are greater than 100 meters wide (&gt; 5 chains) should be recorded as clearcut." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="modified cut: strip" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FRSTPASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FRSTPASS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A partial harvest where the first harvest operation removes target/specific merchantable tree species from a forest stand. The remaining species are merchantable and are intended to be harvested by another logger/contractor/forest resource licence holder in the next pass.  A first pass should be recorded if merchantable tree species remain in the forest stand which have been allocated for harvest - but have not yet been harvested." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="modified cut: first pass " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEEDTREE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDTREE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An even-aged, silvicultural system that retains mature standing trees scattered throughout the cutblock to provide seed sources for natural regeneration." FOLDED="true"/>
                                    <node TEXT="A method of harvesting and regenerating a forest stand in which all trees are removed from the area except for a small number of seed-bearing trees that are left singly or in small groups. The objective is to create an even-aged stand." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="modified cut: seed tree" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PREPCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PREPCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management designed to remove undesirable species of any species from the stand and to select trees to remain that will provide the best seed source and to improve conditions for seed production and natural regeneration." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a preparatory cut " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEEDCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management where trees are removed from a mature stand to create openings in the canopy / create spaces and to prepare sites for natural regeneration while maintaining the seed-bearing trees and protecting any existing advance regeneration." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a seed cut" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FIRSTCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FIRSTCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management where overstorey trees are removed in one or more harvests in order to release the established seedlings from competition." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a first removal harvest " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="LASTCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LASTCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management where all of the remaining trees in the overstorey are removed. This is the removal of the seed or shelter trees after the regeneration has been effective." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a final removal harvest " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="IMPROVE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="IMPROVE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A selection silvicultural system stage of management where a cut is made in an uneven-aged stand primarily to improve stand composition, distribution and quality by removing less desirable trees of any species." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received an improvement cut " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SELECT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SELECT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A selection silvicultural system stage of management where individual trees or groups of trees are selected for cutting in order to recover the yield and develop a balanced uneven-aged structure, while providing the cultural measures required for tree growth and seeding establishment. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a selection harvest " FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The stage of development attribute indicates the current state of growth and development for a productive forest stand.  Note that some states are best described based on the last major silvicultural treatment that was applied to a stand, if the stand is being managed for timber production." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="The stage of development will be used to populate the stage of management in the landbase tables in the forest management plan (i.e., FMP-3 and FMP-5).  The stage of development represents the current development state, and/or the current stage of silvicultural management of a productive forest stand. Productive forest polygons can be described by the following ranges of forest stand development or management: a) Recent Disturbances (harvest or natural disturbance, complete or partial)  b) Below Regeneration Standards  * Not Satisfactorily Regenerated * Renewed (artificial or natural) c) Forest Stands  * Free-growing (not disturbed)  * Free-growing (disturbed by silviculture system)  Forest stands identified by either IMPROVE, SELECT, SNGLTREE or GROUPSE - stage of development must include a complete description for the acceptable growing stock and unacceptable growing stock attributes. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="If the YRDEP attribute equals 0 (unknown), and DEPTYPE is not empty, then the DEVSTAGE attribute should only contain LOWNAT FTGNAT values when POLYTYPE is equal to FOR    *** FIM DIFFERENCE - added DEPTYPE clause for unknown deptypes" FOLDED="true"/>
                                <node TEXT="If the STKG attribute equals 0.00, then the DEVSTAGE attribute should be either DEPHARV or DEPNAT when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="If the DEVSTAGE attribute is either DEPHARV or DEPNAT, then the HT attribute should be 0.0 when POLYTYPE is equal to FOR.  This validation is taken care of in OHT" FOLDED="true"/>
                                <node TEXT="If the OSC attribute equals 4, then the DEVSTAGE attribute should not be DEPHARV when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="If the USC attribute equals 4, then the DEVSTAGE attribute should not be DEPHARV when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="Must not have two canopy stand driven by the overstory in depletion or new state when older than 20. If DEVSTAGE is in ['DEPHARV' , 'DEPNAT' , 'NEWNAT' , 'NEWPLANT' , 'NEWSEED'] AND VERT = 'TO' AND OAGE &gt;20." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null' for row in [row] if row['${name}'] is None and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank' for row in [row] if str(row['${name}']).strip() == ''  and row['POLYTYPE'] == 'FOR' ]" FOLDED="true"/>
                                <node COLOR="#338800" TEXT="[ '${name} must follow coding: %s'%(row['${name}'])  for row in [row] if row['${name}'] not in [ 'LOWMGMT','LOWNAT','DEPHARV','DEPNAT','NEWNAT','NEWPLANT','NEWSEED','FTGNAT','FTGPLANT','FTGSEED','THINCOM','THINPRE','STRIPCUT','FRSTPASS','SEEDTREE','PREPCUT','SEEDCUT','FIRSTCUT','LASTCUT','IMPROVE','SELECT'  ] and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} must be LOWNAT or FTGNAT when POLYTYPE is FOR and ( YRDEP = 0 and DEPTYPE is not null) ' for row in [row] if row['${name}'] not in [ 'LOWNAT','FTGNAT'  ] and row['POLYTYPE'] == 'FOR'  and row['YRDEP'] == 0 and ( row['DEPTYPE'] != '' and row['DEPTYPE'] is not None )  ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} must be DEPHARV or DEPNAT when POLYTYPE is FOR and STKG = 0.00' for row in [row] if row['${name}'] not in [ 'DEPHARV','DEPNAT'  ] and row['POLYTYPE'] == 'FOR'  and row['STKG'] == 0.0  ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} must not be DEPHARV when POLYTYPE is FOR and OSC = 4' for row in [row] if row['${name}'] == 'DEPHARV' and row['POLYTYPE'] == 'FOR'  and row['OSC'] == 4  ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} must not be DEPHARV when POLYTYPE is FOR and USC = 4' for row in [row] if row['${name}'] == 'DEPHARV' and row['POLYTYPE'] == 'FOR'  and row['USC'] == 4  ]   " FOLDED="true"/>
                                <node TEXT="[ 'Must not have two canopy stand (NEW/DEP) where VERT is TO older than 20...${name} in [DEPHARV , DEPNAT , NEWNAT , NEWPLANT , NEWSEED] AND VERT = TO AND OAGE &gt;20' for row in [row] if row['${name}']  in ['DEPHARV' , 'DEPNAT' , 'NEWNAT' , 'NEWPLANT' , 'NEWSEED']  and row['VERT'] == 'TO'  and row['OAGE'] &gt;20  ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="YRDEP:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="YRDEP" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Year of Last Depletion or Disturbance" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The year of last disturbance attribute indicates a four digit number of the most recent (or latest) fiscal year (April 1 to March 31) that a productive forest area was disturbed, completely or partially, by harvest or by natural causes. This includes mid-rotation or stand improvement operations where merchantable timber is removed. This is actual and known disturbances and not calculated from year of origin." FOLDED="true"/>
                            <node TEXT="A special value (0) is used to indicate FTGNAT or LOWNAT where YRDEP is unknown. The validation rules have been updated in the EPC revision to indicate valid scenarios for this flag case, and are different from the FIM 2009 PCM Spec." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="The planning composite inventory must be updated with all disturbances (harvest and natural) which are identified in annual reports. The year of last disturbance for each productive forest stand must correspond to the year that the disturbance occurred, as recorded in the applicable annual reports.  Forest stands that are managed under the selection or shelterwood silvicultural systems must also have a year of last disturbance. The year of last disturbance provides an estimate of the stage of forest stand development within a cutting cycle for selection stands or within the current stage of a shelterwood system.  In the case of a partial harvest where certain tree species have been removed, such as a first pass where merchantable trees remain in a forest stand, the year that the first pass was carried out must be entered as the year of last disturbance. If an additional harvest or pass is conducted, the year of last disturbance is changed to the year in which the most recent harvest/pass operation was carried out.  Commercial thinning is a mid-rotation treatment that is recorded as a disturbance and the stand area is considered as disturbed area in forest management planning. Forest stands that have received a commercial thinning must show the applicable year of last disturbance in the forest polygon coverage.  The year of last disturbance is also used to identify other stand improvement operations. A stand improvement operation is normally associated with the selection system.   Year of last disturbance must not be used to indicate tending operations, such as a chemical or manual release which are required to bring a forest stand to free-to-grow standards. " FOLDED="true"/>
                            <node TEXT="The year of depletion attribute (YRDEP) and the associated depletion type attribute (DEPTYPE) are only completed during the photo interpretation process when the stage of development attribute is set to one of the 'newly depleted' options (i.e., DEVSTAGE = DEPHARV or DEPNAT).  The fields are also generally completed for newly regenerated stages of development (i.e., DEVSTAGE = NEWNAT, NEWPLANT, or NEWSEED).  For other stages of development, the fields may be completed if the interpreter has access to additional information pertaining to past disturbances." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="The value must be greater than or equal to 1900 when POLYTYPE is equal to FOR, unless the special flag value 0 is used, as below." FOLDED="true"/>
                                <node TEXT="If YRDEP attribute equals 0, then DEVSTAGE attribute should only contain LOWNAT or FTGNAT values when POLYTYPE is equal to FOR.  Tested by DEVSTAGE attribute" FOLDED="true" BACKGROUND_COLOR="#ccffcc"/>
                                <node TEXT="The YRDEP attribute value should not be less than the YRORG attribute value when POLYTYPE is equal to FOR, unless the flag value 0 is used.***** FIM DIFFERENCE - removed this rule. Counterexamples abound..." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must be &gt;= 1900 unless it is 0' for row in [row] if row['${name}']  &lt;1900 and row['${name}'] != 0 ]   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="SPCOMP:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Species Composition" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="120" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SPCOMP" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT=" This species composition attribute indicates the tree species that are present in the stand canopy and the relative proportion of the canopy that each species occupies." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory " FOLDED="true"/>
                                <node TEXT="Pattern is SSSPPPSSSPPP   example: PJ  80PO  20  (there are two blanks between the species and the proportion) " FOLDED="true"/>
                                <node TEXT=" No duplicate species codes allowed in the string" FOLDED="true"/>
                                <node TEXT="Proportion values in the string must sum to 100 " FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="The tree species in the composition are to be coded using the scheme listed here.   " FOLDED="true"/>
                                <node TEXT="*each species code is 3 characters (including blanks) and is left justified" FOLDED="true"/>
                                <node TEXT="*each proportion is 3 characters which represents an integer value from 1 to 100 and is right justified. Note that initial inventory values determined by photo interpretation will not go below 10% and are rounded to the nearest 10%  (e.g., 10, 20, 30). During inventory update, proportions based on subsequent field survey sampling that are of a finer resolution can be entered (e.g., go below 10% or be a value in between the initial rounded proportions such as 15). " FOLDED="true"/>
                                <node TEXT="*maximum of 10 species and proportions pairs in the string" FOLDED="true"/>
                                <node TEXT="Plot Species are [     'AL','Aq','Ap','Ag','Bc','Bp','Gb','Bb','Cat','Cc','Cm',     'Cp','Cs','Ct','Er','Eu','Hk','Ht','Hl','Hb','Hm','Hp',     'Hs','Hc','Kk','Le','Lj','Bl','Ll','Lb','Gt','Mb','Mf',     'Mm','Mt','Mn','Mp','AM','Ema','Mo','Obl','Ob','Och','Op',     'Os','Osw','Pa','Pn','Pp','Pc','Ph','Pe','Red','Ss','Sc',     'Sk','Sn','Sr','Sy','Tp','Haz'     ]" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null' for row in [row] if row['${name}'] is None ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank' for row in [row] if str(row['${name}']).strip() == '' ]" FOLDED="true"/>
                                <node TEXT="[ i for i in [ test_spcomp(row['${name}']) for r in [row] ] if i is not None ]" FOLDED="true"/>
                                <node TEXT="[&quot;${name} should not be %s: %s&quot;%(i, r['${name}'] )  for i in [  test_spcomp(row['${name}'])   for r in [row]    if row['${name}'] is not None  ]  if i is not None ]" FOLDED="true"/>
                                <node TEXT="[ '%s is not a valid species code'%(s) for s in test_SpeciesInList(row['${name}'],    [ 'AL','Aq','Ap','Ag','Bc','Bp','Gb','Bb','Cat','Cc','Cm', 'Cp','Cs','Ct','Er','Eu','Hk','Ht','Hl','Hb','Hm','Hp', 'Hs','Hc','Kk','Le','Lj','Bl','Ll','Lb','Gt','Mb','Mf','Mm','Mt','Mn','Mp','AM','Ema','Mo','Obl','Ob','Och','Op', 'Os','Osw','Pa','Pn','Pp','Pc','Ph','Pe','Red','Ss','Sc','Sk','Sn','Sr','Sy','Tp','Haz' ,                               'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn','CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf','OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX', 'Sb','Sw','la','Wb','Wi'    ]     ) if row['${name}'] is not None ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="WG:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Working Group" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="WG" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="WG" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BF:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BF" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="balsam fir" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="CE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cedar" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="HE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="HE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="hemlock" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="LA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="larch" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PJ:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PJ" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="jack pine" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="red pine" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="scots pine" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PW:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PW" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="white pine" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SB" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="black spruce" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SW:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SW" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="white spruce" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SX" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spruce mix" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="other conifer" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="AX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="AX" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ash mix" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="BG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BG" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="gray birch" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="BW:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BW" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="white birch" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="BY:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BY" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yellow birch" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MH" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="hard / sugar maple" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="soft / red maple" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MX" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="maple mix" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OX" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="oak mix" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Poplar" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PB" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="balsam poplar" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OH" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="other hardwoods" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The working group attribute indicates a grouping of productive forest stands for timber management purposes. Stands which are grouped together have the same predominant species composition and are being managed under the same silvicultural system. The grouping of stands is generally labelled based on the species which dictates the management regime to be applied. Therefore, the working group normally indicates the dominant species in a forest stand based on the tree species that occupies the greatest canopy closure or the greatest amount of basal area in the stand as indicated by the species composition attribute. " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [  'BF','CE','HE', 'LA', 'PJ', 'PR', 'PS', 'PW', 'SB', 'SW', 'SX', 'OC', 'AX', 'BG', 'BW', 'BY', 'MH', 'MR', 'MX', 'OX', 'PO', 'PB', 'OH' ] and row['POLYTYPE'] == 'FOR' ]    " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="YRORG:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <icon BUILTIN="messagebox_warning"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Year of Origin" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="YRORG" FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="The year of origin attribute contains a four digit number representing the average year that the predominant species (i.e., the species within the stand having the greatest relative abundance in terms of basal area), came into existence. " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" The year of origin is used to calculate the average age of a productive forest stand. The year of origin is determined in relation to the age of the trees and the year that regeneration (natural or  Appendix 1 Tabular Attribute Descriptions artificial) was established on a site. The year of origin is synonymous to the year of germination. If regeneration has not been established, then the year of origin will be the same as the year of last disturbance. Once trees are established on the site, the year of origin must be adjusted to reflect the average age of the established regeneration. Once a forest stand has been assessed as free-to-grow, based on attaining the regeneration/management standards, the year of origin should be adjusted to reflect the average age of the growing stock on that site. In determining the year of origin, age differences that result from natural and artificial growing stock, or age differences that occur due to modified clear-cut harvesting or shelterwood management, must be considered. For example, if a productive forest stand is harvested in two strips/passes which occur in different years (e.g., a type of modified clear-cut), the forest stand will technically have two ages for a period of time; one age for the recently disturbed strip and another age for the non-disturbed strip. The eventual strategy is to produce an even-aged forest stand. Therefore, the year of origin will eventually be determined as one average value for the forest stand. If the remaining strip is not harvested for ten-years (assuming that the harvested strip requires the same time period to reach free-growing conditions), the harvested and regenerated strip will be described by the understorey forest stand characteristics and, therefore, have a separate year of origin. The year of origin and the forest stand characteristics for the overstorey will likely not change. Once the remaining strip has been harvested and the regeneration in that strip has achieved free-to-grow status, the average age of the dominant tree species in each of the strips should be determined and then used to calculate the appropriate year of origin for the entire stand. At this point, understorey forest stand characteristics are no longer required. Normally, updates to the year of origin have a corresponding change to the year of update attribute as this indicates the currency/vintage of the information and when the year of origin value was determined. Year of origin information is not required for non-forested and non-productive forest land types." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <icon BUILTIN="messagebox_warning"/>
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="A zero value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="The YRORG attribute value must be greater than or equal to 1600 when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="The YRORG attribute must be less than the plan period start year when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The YRDEP attribute value should not be less than the YRORG attribute value (addressed in YRDEP)" FOLDED="true"/>
                                <node TEXT="The YRORG attribute value should not be greater than YRUPD attribute value" FOLDED="true"/>
                                <node TEXT="In the Base Model Inventory, the YRORG attribute value should not be greater than the YRUPD attribute value + 5 (Addressed in BMI/eBMI)" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <icon BUILTIN="messagebox_warning"/>
                                <node TEXT="[ '${name} must not be = 0' for row in [row] if row['${name}']  == 0 and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} must be &gt;= 1600 ' for row in [row] if row['${name}']  &lt;1600 and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} must be &lt;= YRSOURCE' for row in [row] if row['${name}']  &gt; row['YRSOURCE']  and row['POLYTYPE'] == 'FOR']   " FOLDED="true">
                                    <icon BUILTIN="messagebox_warning"/>
                                </node>
                                <node TEXT="[ '${name} must be &lt;= YRUPD' for row in [row] if row['${name}']  &gt; row['YRUPD']  and row['POLYTYPE'] == 'FOR']   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="HT:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Height" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="HT" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The height attribute indicates the estimated average tree height (in meters) of the predominant species as inventoried in the year of update. Estimates can be made from interpreted crown closure or field samples, or from growth algorithms. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT="If the DEVSTAGE attribute does not start with DEP, NEW or LOW then the HT attribute must be greater than zero when POLYTYPE is equal to FOR." FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT="The value of HT should not be greater than 0.5 when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT=" If the DEVSTAGE attribute starts with FTG, then the HT attribute should be greater than or equal to 0.8 when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT="Valid numeric values are from 0 through 40.0 when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null' for row in [row] if row['${name}'] is None and row['POLYTYPE'] == 'FOR']" FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT="[ '${name} must not be blank' for row in [row] if str(row['${name}']).strip() == '' and row['POLYTYPE'] == 'FOR']" FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT="[ '${name} must not be 0 if DEVSTAGE is not DEPHARV or DEPNAT or NEWNAT or NEWPLANT or NEWSEED, LOWNAT, LOWMGMT when POLYTYPE is FOR. Consider updating DEVSTAGE?' for row in [row] if row['DEVSTAGE']  not in [ 'DEPHARV','DEPNAT','NEWNAT','NEWPLANT','NEWSEED','LOWNAT',' LOWMGMT' ] and row['POLYTYPE'] == 'FOR'  and row['${name}'] == 0.0 ]   " FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT="[ 'WARNING: ${name} / (${year} - YRORG) should not be &gt; 0.5 when POLYTYPE is FOR' for row in [row] if (row['POLYTYPE'] == 'FOR' and row['YRORG'] &gt; 0) if row['${name}'] / (${year} - row['YRORG']) &gt; 0.5  ]   " FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT="[ '${name} must be &gt;= 0.8 when DEVSTAGE like FTG% and POLYTYPE is FOR' for row in [row] if row['${name}'] &lt; 0.8 and row['DEVSTAGE'] in [ 'FTGNAT','FTGPLANT','FTGSEED'] and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT="[ '${name} must be &gt;= 0 and  ${name} &lt;= 40 and POLYTYPE is FOR' for row in [row] if (row['${name}'] &lt; 0 or row['${name}'] &gt; 40) and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT="[ '${name} must be &gt;= 0 and  ${name} &lt;= 40 and POLYTYPE is FOR' for row in [row] if (row['${name}'] &lt; 0 or row['${name}'] &gt; 40) and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="Height / Age relationships should be in the range of Plonski and others." FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="STKG:" FOLDED="true" BACKGROUND_COLOR="#ff9999">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="double" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="STKG" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Stocking" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The stocking attribute indicates a qualitative measure of the density of tree cover in a forest stand.  It is expressed as a percentage value ranging from zero, for recently disturbed stands, to a maximum of 4.00, although 2.50 is the typical maximum value encountered in the field.  Stocking of a forest stand refers to all species that make up the stand's canopy, but it is generally based on the species with the most basal area. " FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="Stocking is determined by comparing the actual basal area measured from field sampling to the relative basal area of a fully-stocked stand using Plonski's Normal Yield Tables. (Plonski's Normal Yield Tables were developed from permanent sample plots established for several of the major tree species in Ontario.) Stocking can also be determined from aerial photography based on the degree of canopy closure, average age, height, and species composition. Actual basal area collected from field sampling is used to calibrate stocking assessments made from photo-interpretation.  The silvicultural ground rules in a forest management plan describe the standards for assessing the regeneration of forest stands, based on forest unit, desired species composition, age, height, and stocking. If the stocking of a productive forest stand does not meet the regeneration standards in the silvicultural ground rules of an approved forest management plan, the forest stand will be considered as NSR and must be classified into the appropriate below regeneration standards stage of development.  In some cases, the regeneration and/or management standards of a silvicultural ground rule may be expressed as a density, which usually describes the frequency or number of stems per hectare. Where density information (stems/hectare) has been collected or determined from a regeneration survey, this information must be converted to a stocking value for the purpose of updating the forest stand description information. " FOLDED="true"/>
                            <node TEXT="This definition needs to be brought up to date using the current eFRI specification. There may (or may not) be a difference between traditional FRI and eFRI stocking calculations." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory  when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT=" ST1: The attribute population must follow the correct format  when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="* Valid numeric values are from 0 through 4.00" FOLDED="true"/>
                                <node TEXT="If the DEVSTAGE attribute starts with FTG, then the STKG attribute should be greater than or equal to 0.40   when POLYTYPE is equal to FOR." FOLDED="true"/>
                                <node COLOR="#999900" TEXT="If DEVSTAGE attribute starts with DEP, then STKG attribute should equal 0.00   when POLYTYPE is equal to FOR and VERT is not SV (Veterans contribute to stocking) " FOLDED="true"/>
                                <node TEXT="If DEVSTAGE attribute starts with DEP, then STKG attribute should not exceed 0.1   when POLYTYPE is equal to FOR and VERT is SV (Veterans contribute to stocking) " FOLDED="true"/>
                                <node TEXT="If DEVSTAGE attribute starts with DEP, then STKG attribute should not exceed 0.3 when POLYTYPE is equal to FOR and VERT is TU or MU " FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must be &gt;= 0 and  ${name} &lt;= 4.0 and POLYTYPE is FOR' for row in [row] if (row['${name}'] &lt; 0 or row['${name}'] &gt; 4.0) and row['POLYTYPE'] == 'FOR'  ]   " FOLDED="true"/>
                                <node TEXT="[ 'WARNING: ${name} should be &gt; 0.4 when DEVSTAGE is FTG* when POLYTYPE is FOR.' for row in [row] if row['DEVSTAGE']  in [ 'FTGNAT','FTGPLANT','FTGSEED' ] and row['POLYTYPE'] == 'FOR'  and row['${name}'] &lt; 0.4 ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} must = 0.0 when DEVSTAGE is DEP* when POLYTYPE is FOR in single-canopy stands' for row in [row] if row['DEVSTAGE']  in [ 'DEPNAT','DEPHARV' ] and row['POLYTYPE'] == 'FOR'  and row['${name}'] &gt; 0 and row['VERT'] not in [ 'SV','TU','MU']  ]   " FOLDED="true"/>
                                <node TEXT="[ 'WARNING: ${name} should not exceed 0.1 when DEVSTAGE is DEP* when POLYTYPE is FOR and VERT is SV.' for row in [row] if row['DEVSTAGE']  in [ 'DEPNAT','DEPHARV' ] and row['POLYTYPE'] == 'FOR'  and row['${name}'] &gt; 0.1 and row['VERT'] == 'SV' ]   " FOLDED="true"/>
                                <node TEXT="[ 'WARNING: ${name} should not exceed 0.3 when DEVSTAGE is DEP* when POLYTYPE is FOR and VERT is TU or MU.' for row in [row] if row['DEVSTAGE']  in [ 'DEPNAT','DEPHARV' ] and row['POLYTYPE'] == 'FOR'  and row['${name}'] &gt; 0.3 and row['VERT'] in ['TU','MU'] ]   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="SC:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Site Class" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SC" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="SC" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The site class attribute indicates a site quality estimate for a stand.  Site class is an indicator of site productivity and is determined using the average height, age, and working group, based on the dominant tree species in a forest stand. These attributes are compared against height and age growth curves in Plonski's Normal Yield Tables for different species to determine the relative growth rate for a forest stand.  " FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="There is a relationship between the site class (SC) and the productive forest modifier (FORMOD) values assigned to a forested area. Generally, areas assigned a site class value of 4 (protection forest) are also assigned a productive forest modifier value of PF (protection forest), but it is not a requirement. Areas assigned a site class value other than 4 (e.g., 3) can also be assigned the productive forest modifier value of PF. Conversely areas assigned a site class value of 4 may be assigned a productive forest modifier value of production forest (RP) instead of protection forest.  The apparent discrepancy in the protection forest assignment between the site class attribute and the productive forest modifier attribute reflects the difference between a calculated assessment of site conditions (SC) versus a timber management decision (FORMOD) that is based on more than just site class. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT=" ST1: The attribute population must follow the correct format  when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="If the FORMOD attribute equals PF, then the SC attribute should equal 4  when POLYTYPE is equal to FOR and the VERT is not TU or MU" FOLDED="true"/>
                                <node TEXT="PROPOSED BY LARRY WATKINS...  (previously foresters would change areas unavailable to SC4 so they would be considered PFR rather than making it unavailable)  applies to USC as well.  Need to check when PCM come in that changes to SC as presented in the eFRI have been altered.  Larry says this was common in the past.  LARRYS COMMENT on Site Class:  For some reason the old site class 4 sort from FORTRAN keeps creeping into the FRI spec inappropriately. SC4 should me SC4 - the productivity of the site. Don't call site class 1 trees on an island or on a rocky site 4 to match a  1970s coding issue, but rather flag the other appropriate fields.  " FOLDED="true">
                                    <icon BUILTIN="info"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 0,1,2,3,4 ] and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} must be 4 when FORMOD is PF' for row in [row] if row['${name}'] not in [ 4 ] and row['POLYTYPE'] == 'FOR' and row['FORMOD'] == 'PF' and row['VERT'] not in ['TU','MU'] ]   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="0:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="0" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Best" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="1:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="1" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Better" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="2:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="2" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Good" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="3:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="3" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Poor" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="4:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="4" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest land on which forest management activities cannot normally be practiced without incurring deleterious environmental effects because of obvious physical limitation such as steep slopes or shallow soils over bedrock." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Protection Forest" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ECOSRC:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Ecosite Source of Update" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ECOSRC" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="ECOSRC" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="ALGO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ALGO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="algorithm / model" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DIGITALA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DIGITALA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="multispectral scanning (digital image) - automated process Digital analysis, automated processing. (e.g., Ecognition)" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DIGITALP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DIGITALP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="multispectral scanning (digital image) - manual process photo-interpreted by softcopy systems" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FOC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FOC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="forest operations compliance inspection" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FRICNVRT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FRICNVRT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="forest resources inventory conversion" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MARKING:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MARKING" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pre-harvest site inspection / marking" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OCULARA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OCULARA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="aerial survey/reconnaissance" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OCULARG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OCULARG" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ocular estimate (ground)" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OPC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OPC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="operational cruise" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="air photo interpretation" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTOLS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTOLS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="large scale aerial photography" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTOPLS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTOPLS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="air photo interpretation" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PLUS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLUS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="soils data" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTOSS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTOSS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="small scale aerial photography" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PLOTFIXD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLOTFIXD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="fixed area plot" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PLOTVAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLOTVAR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="variable area (radius) plot" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="REGENASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REGENASS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="regeneration assessment" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEMEXTEN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEMEXTEN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="extensive silvicultural effectiveness monitoring survey" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEMINTEN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEMINTEN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="intensive silvicultural effectiveness monitoring survey" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The ecosite source of update attribute identifies the methodology by which the ecosite information associated with the polygon was determined (i.e., how the polygon's ecosite description was determined). " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="Note that this attribute is similar to the SOURCE attribute which indicates the methodology by which the overall stand description was determined. The main difference between the SOURCE attribute and this ECOSRC attribute is that there are two additional methodologies that are commonly used when determining ecosite information. These additional methodologies are: determination of ecosite by algorithm and determination by supplementing air photo interpretation with soils data. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null' for row in [row] if row['${name}'] is None and row['POLYTYPE'] == 'FOR'   ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding: %s'%(row['${name}']) for row in [row] if row['${name}'] not in [ 'ALGO', 'DIGITALA', 'DIGITALP', 'FOC', 'FRICNVRT', 'MARKING', 'OCULARA', 'OCULARG', 'OPC', 'PHOTO', 'PHOTOLS', 'PHOTOPLS', 'PHOTOSS', 'PLOTFIXD', 'PLOTVAR', 'REGENASS', 'SEMEXTEN', 'SEMINTEN'] and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank' for row in [row] if str(row['${name}']).strip() == '' and row['POLYTYPE'] == 'FOR'   ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="ALGO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ALGO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT=" " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="algorithm / model " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DIGITALA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DIGITALA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Digital analysis, automated processing. (e.g., Ecognition)" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="multispectral scanning (digital image) - automated process" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DIGITALP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DIGITALP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="photo-interpreted by softcopy systems" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="multispectral scanning (digital image) - manual process " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FOC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FOC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Inspection of a site after silvicultural treatment was applied to determine whether an operator / operations conforms to the approved plan or permit. The evaluation of any harvest, renewal, maintenance, or access forest management activity (e.g., post-harvest site inspection) can be included here. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="forest operations compliance inspection" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FRICNVRT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FRICNVRT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The current polygon description is based on a data conversion from traditional FRI. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="forest resources inventory conversion" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MARKING:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MARKING" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Assessment of the trees in a stand for purpose of establishing a silvicultural or operational prescription. Selecting and marking the trees to be harvested and/or the trees to be left to grow; to sustain and enhance the stand for timber management, wildlife habitat management, aesthetics, recreation, biodiversity and other environmental and heritage concerns. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pre-harvest site inspection / marking" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OCULARA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OCULARA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Visual assessment of a stand from a helicopter or fixed wing aircraft. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="aerial survey/reconnaissance" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OCULARG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OCULARG" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Visual assessment of a stand using extensive ground survey methodologies (i.e., no detailed measurements). " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ocular estimate (ground)" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OPC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OPC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Measuring standing trees to determine the volume of wood on a given tract of land. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="operational cruise" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Photography at a conventional scale of 1:10,000, 1:15,840, or 1:20,000 " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="air photo interpretation " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTOLS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTOLS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Photography at a scale larger than 1:10,000 (e.g., 1:500, 1:1000). " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="large scale aerial photography" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTOPLS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTOPLS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="air photo interpretation PLUS soils data" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTOSS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTOSS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Photography at a scale smaller than 1:20,000 (e.g., 1:100,000). " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="small scale aerial photography " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PLOTFIXD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLOTFIXD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="fixed area plot " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PLOTVAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLOTVAR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="variable area (radius) plot" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="REGENASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REGENASS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Survey of a regenerated area to determine how well the new stand is growing. This includes seeding, survival, and stocking assessments. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="regeneration assessment " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEMEXTEN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEMEXTEN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An appraisal of a forest stand's structure and composition using generalized survey sampling methodologies to determine if regeneration or management objectives have been met (i.e., determine if the expected results were achieved). Extensive survey methods are generally used where there are obvious successes or failure, or to identify problem areas requiring more intensive assessment. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="extensive silvicultural effectiveness monitoring survey " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEMINTEN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEMINTEN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An appraisal of a forest stand's structure and composition using rigorous survey sampling methodologies to determine if regeneration or management objectives have been met (i.e., determine if the expected results were achieved). Intensive survey methods are intended for stands where the status of regeneration is uncertain or specific quantitative data is required to determine the silvicultural effectiveness for operational treatments. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="intensive silvicultural effectiveness monitoring survey " FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ECOSITE1:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Ecosite 1" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="10" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ECOSITE1" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The ecosite attribute identifies an ecological landscape type that is present in the polygon and the regional ecological land classification system being used to define the ecosite code. Soil depth modifier information may also be included, if relevant. " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="Refer to the appropriate ecosystem field guide for a list of valid ecosite codes and their definitions. Ecosite is also referred to as site type. " FOLDED="true"/>
                            <node TEXT='When recording the ecosite information in this attribute, the regional land classification identifier code value (from table below) replaces the first 2 characters of the ecosite code (either "ES" or "ST"). For example, if the northwest regional ecosite code of ES11 was determined to be appropriate for the stand, it would be recorded as NW11.' FOLDED="true"/>
                            <node TEXT='Additionally, some ecosites, especially in the northwest region, can have soil depth modifier information associated with them. To record soil depth modifier information, add one of the codes from the table below to the end of the ecosite code. For example, northwest ecosite code of ES13 with a soil depth modifier of "shallow" would be recorded as:' FOLDED="true"/>
                            <node TEXT="Ecosite information which is collected or created in accordance with an ecosystem classification (e.g., wetlands ecosystem classification) may also be entered for non-productive forest lands, if desired, but it is not required. Determination of ecosite is a prerequisite of scheduling a forest stand for silvicultural treatment and is required for verifying the selected forest operations prescription in accordance with the silvicultural ground rules of an approved forest management plan. Ecosite must be identified for all free-growing, (free-to-grow) forest stands based on the pre-harvest condition, and must reflect the characteristics of the site in terms of vegetation, soils, drainage, and tree species in accordance with a forest ecosystem classification. Ecosite should not be determined after a forest stand has been subjected to forest management operations. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory for ECOSITE1 when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="The population of this attribute is only mandatory for ECOSITE1 when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="A blank or null value is not valid for ECOSITE1" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null' for row in [row] if row['${name}'] is None and row['POLYTYPE'] == 'FOR'   ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding: %s'%(row['${name}']) for row in [row] if row['${name}'][0:2] not in ('CE', 'NE', 'NW') and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank' for row in [row] if str(row['${name}']).strip() == '' and row['POLYTYPE'] == 'FOR'   ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ECOSITE2:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Ecosite 2" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="10" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ECOSITE2" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The ecosite attribute identifies an ecological landscape type that is present in the polygon and the regional ecological land classification system being used to define the ecosite code. Soil depth modifier information may also be included, if relevant. " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="Refer to the appropriate ecosystem field guide for a list of valid ecosite codes and their definitions. Ecosite is also referred to as site type. " FOLDED="true"/>
                            <node TEXT='When recording the ecosite information in this attribute, the regional land classification identifier code value (from table below) replaces the first 2 characters of the ecosite code (either "ES" or "ST"). For example, if the northwest regional ecosite code of ES11 was determined to be appropriate for the stand, it would be recorded as NW11.' FOLDED="true"/>
                            <node TEXT='Additionally, some ecosites, especially in the northwest region, can have soil depth modifier information associated with them. To record soil depth modifier information, add one of the codes from the table below to the end of the ecosite code. For example, northwest ecosite code of ES13 with a soil depth modifier of "shallow" would be recorded as:' FOLDED="true"/>
                            <node TEXT="Ecosite information which is collected or created in accordance with an ecosystem classification (e.g., wetlands ecosystem classification) may also be entered for non-productive forest lands, if desired, but it is not required. Determination of ecosite is a prerequisite of scheduling a forest stand for silvicultural treatment and is required for verifying the selected forest operations prescription in accordance with the silvicultural ground rules of an approved forest management plan. Ecosite must be identified for all free-growing, (free-to-grow) forest stands based on the pre-harvest condition, and must reflect the characteristics of the site in terms of vegetation, soils, drainage, and tree species in accordance with a forest ecosystem classification. Ecosite should not be determined after a forest stand has been subjected to forest management operations. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="Blank or null values ARE valid" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ECOPCT1:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Ecosite 1 Percentage" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ECOPCT1" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The ecosite percentage attribute identifies the percentage of an ecological landscape type that is present in the polygon. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory for ECOPCT1 when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="The population of this attribute is only mandatory for ECOPCT1 when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="A blank or null value is not valid for ECOSITE1 when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="A zero value is not a valid code for ECOPCT1 when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="Adding all of ECOPCT1 attributes should equal 100 when POLYTYPE is equal to FOR" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null' for row in [row] if row['${name}'] is None and row['POLYTYPE'] == 'FOR'   ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank' for row in [row] if str(row['${name}']).strip() == '' and row['POLYTYPE'] == 'FOR'   ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must be &gt;= 0 and POLYTYPE is FOR' for row in [row] if  row['${name}'] &lt; 0 and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ECOPCT2:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Ecosite 2 Percentage" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ECOPCT2" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The ecosite percentage attribute identifies the percentage of an ecological landscape type that is present in the polygon. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ECOPCT2 is NOT mandatory" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ACCESS1:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Accessibility Indicator" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ACCESS1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="FMP_ACCESS" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The accessibility indicator attributes specifies whether or not there are any restrictions to accessing a productive forest stand. These restrictions may be legal (i.e., ownership), political / land use policy (i.e., land use designation, road closures), and/or a natural barrier. The focus of this field is identification of Crown productive forest stands that are inaccessible and therefore are not considered as part of the managed landbase for forest management planning purposes, but the principle may be applied to any area." FOLDED="true"/>
                            <node TEXT=" NOTE:   These attributes are not completed by the photo interpreters during inventory production. Access restriction information is to be determined by the forest management planning team. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="Note: The ACCESS2 attribute may not exist in the submitted coverage as this attribute is not mandatory.  If this attribute exists, it may not be populated.  Therefore, this validation will be performed on the ACCESS2 attribute for the application of the correct coding scheme.  " FOLDED="true"/>
                                <node TEXT="*   If the code of GEO is entered, then a management consideration attribute (MGMTCON) must be completed with the appropriate associated explanation/details, such as island or natural barrier. Refer to the MGMTCON attribute description." FOLDED="true"/>
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory for ACCESS1 when POLYTYPE is equal to FOR " FOLDED="true"/>
                                <node TEXT=" ST1: The population of this attribute is mandatory for ACCESS1 when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code for ACCESS1 when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT=" ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR and is not blank or null" FOLDED="true"/>
                                <node TEXT="If ACCESS1 or ACCESS2 do not equal NON then AVAIL should be U when POLYTYPE is equal to FOR. Tested by the AVAIL attribute" FOLDED="true"/>
                                <node TEXT="If ACCESS2 is not equal to NON then ACCESS1 must not be NON when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT=" ACCESS1 and ACCESS2 can't contain the same value " FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null when POLYTYPE is FOR' for row in [row] if row['${name}'] is None and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank, when POLYTYPE is FOR' for row in [row] if str(row['${name}']) in  [ ' ', ''  ] and row['POLYTYPE'] == 'FOR' ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding: %s'%(row['${name}']) for row in [row] if row['${name}'] not in [ 'GEO','LUD','NON','OWN','PRC','STO', '', ' ', None ] and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                                <node TEXT="[ 'If ${name} is GEO, MGMTCON1 must not be blank' for row in [row] if row['${name}'] == 'GEO' if str(row['MGMTCON1']).strip() == ''  or row['MGMTCON1'] is None ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must != ACCESS2 ' for row in [row] if row['${name}'] == row['ACCESS2'] and str(row['${name}']).strip() != ''  and row['${name}'] is not None ]" FOLDED="true"/>
                                <node TEXT="[ 'if ACCESS2 != NON then ${name} must not = NON'  for row in [row]  if row['${name}'] == 'NON'       and (             row['ACCESS2'] != 'NON' and             str(row['ACCESS2']).strip() != ''              and row['ACCESS2'] is not None             ) ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="GEO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GEO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Area is not accessible due to geographic reasons. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="geography" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="LUD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LUD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area is not accessible for forest management purposes due to land use designation (e.g., a provincial or federal park, agreement forest, mining claim, native lands, federal lands). " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="land use designation" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NON:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NON" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The area is accessible/reachable." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="no accessibility considerations" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OWN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OWN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area of Crown land that is unreachable because it is surrounded by lands owned by an other party/parties (e.g., an area of Crown productive forest land that is not accessible for forest management because it is surrounded by private land)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ownership" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PRC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area that is no longer accessible due to the permanent closure of the only road leading into the area." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="road closure" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="STO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="STO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area of land that is owned by a party/parties other than the Crown (e.g., a parcel of private land) and where the access conditions are applied by the land owner. Note that ownership and access conditions can change over time." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="subject to ownership" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ACCESS2:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Accessibility Indicator 2" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ACCESS2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="FMP_ACCESS" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="Note: The ACCESS2 attribute may not exist in the submitted coverage as this attribute is not mandatory.  If this attribute exists, it may not be populated.  Therefore, this validation will be performed on the ACCESS2 attribute for the application of the correct coding scheme.  " FOLDED="true"/>
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory for ACCESS1 when POLYTYPE is equal to FOR " FOLDED="true"/>
                                <node TEXT=" ST1: The population of this attribute is mandatory for ACCESS1 when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT=" ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="If ACCESS1 or ACCESS2 do not equal NON then AVAIL should be U when POLYTYPE is equal to FOR. Tested by the AVAIL attribute" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must follow coding: %s'%(row['${name}']) for row in [row] if row['${name}'] not in [ 'GEO','LUD','NON','OWN','PRC','STO', '', ' ', None ] and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="GEO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GEO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Area is not accessible due to geographic reasons. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="geography" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="LUD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LUD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area is not accessible for forest management purposes due to land use designation (e.g., a provincial or federal park, agreement forest, mining claim, native lands, federal lands). " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="land use designation" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NON:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NON" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The area is accessible/reachable." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="no accessibility considerations" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OWN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OWN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area of Crown land that is unreachable because it is surrounded by lands owned by an other party/parties (e.g., an area of Crown productive forest land that is not accessible for forest management because it is surrounded by private land)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ownership" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PRC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area that is no longer accessible due to the permanent closure of the only road leading into the area." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="road closure" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="STO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="STO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area of land that is owned by a party/parties other than the Crown (e.g., a parcel of private land) and where the access conditions are applied by the land owner. Note that ownership and access conditions can change over time." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="subject to ownership" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="MGMTCON1:" FOLDED="true">
                        <icon BUILTIN="messagebox_warning"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MGMTCON1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="MGMTCON" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Management Consideration" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The management consideration attributes indicates whether or not ecological/landscape features or site conditions are present within the polygon. These features and conditions may require special consideration during resource management planning. " FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="The photo interpreters will only use the MGMTCON1 and MGMTCON2 attributes during inventory production. Use of the MGMTCON3 attribute is reserved for the forest management planning team." FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="COLD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="COLD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site with poor or unstable growing conditions due to the soil being frozen year-round." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="permafrost" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DAMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DAMG" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A stand which contains trees that are damaged, dead and/or dying due to natural causes (e.g., ice damage, blowdown, insect/disease damage)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="physical/natural damage  " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ISLD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ISLD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The area is or is located on an island  (i.e., an area of land that is totally surrounded by water)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="island" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NATB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NATB" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A productive stand that is unreachable due to the physical features of the surrounding area (e.g., the area is a mesa or is productive forest surrounded by non-forested wetland)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural barrier" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NONE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NONE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="There are no physical or ecological 'restrictions' in the site that need to be considered when determining management of the stand." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="no management consideration" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PENA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PENA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area of land that is nearly surrounded by water and is connected to the mainland." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="peninsula" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="POOR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="POOR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A stand exhibiting stagnated growth with no discernible cause for the poor growing condition." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="stagnated, poor tree growth - no indicator" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ROCK:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ROCK" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where the rocky conditions limit accessibility by forest management equipment and/or present a potential for soil erosion due to operations." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="exposed bedrock / rocky outcrops" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SAND:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SAND" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential for erosion." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="blow sand / exposed fine sand, shallow or no humus" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SHRB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SHRB" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult without major silvicultural intervention due to shrub or brush competition." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="heavy shrub / brush" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SOIL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SOIL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area with minimal soil depth where forest regeneration will be limited as the site could be damaged or experience degradation due to operations (e.g., potential for root damage or erosion)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="shallow soils" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="STEP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="STEP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where the degree of incline is dangerous for equipment operation and presents a potential for erosion. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="steep slopes" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="WATR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential of rising surface water." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="telluric / highly fluctuating, moving ground water" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="WETT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WETT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential of rising water tables." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="poorly drained - high water table" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <icon BUILTIN="button_ok"/>
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory  when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory for MGMTCON1 when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code for MGMTCON1" FOLDED="true"/>
                                <node TEXT="If ACCESS1 or ACCESS2 is equal to GEO then MGMTCON1 must be NONE" FOLDED="true"/>
                                <node TEXT="IF the FORMOD attribute equals PF, then the MGMTCON1 attribute should not equal NONE when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="If the SC attribute equals 4, then the MGMTCON1 attribute should not equal NONE" FOLDED="true"/>
                                <node TEXT="If MGMTCON2 is not NONE then MGMTCON1 must not be NONE" FOLDED="true"/>
                                <node TEXT="If MGMTCON3 is not NONE then the MGMTCON2 and MGMTCON1 must not be NONE" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must follow coding: %s'%(row['${name}']) for row in [row] if row['${name}'] not in [ 'COLD','DAMG','ISLD','NATB','NONE','PENA','POOR','','ROCK','SAND','SHRB','SOIL','STEP','WATR','WETT'] and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="MGMTCON2:" FOLDED="true">
                        <icon BUILTIN="messagebox_warning"/>
                        <icon BUILTIN="button_cancel"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MGMTCON2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="MGMTCON" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Management Consideration" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The management consideration attributes indicates whether or not ecological/landscape features or site conditions are present within the polygon. These features and conditions may require special consideration during resource management planning. " FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="The photo interpreters will only use the MGMTCON1 and MGMTCON2 attributes during inventory production. Use of the MGMTCON3 attribute is reserved for the forest management planning team." FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="COLD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="COLD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site with poor or unstable growing conditions due to the soil being frozen year-round." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="permafrost" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DAMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DAMG" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A stand which contains trees that are damaged, dead and/or dying due to natural causes (e.g., ice damage, blowdown, insect/disease damage)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="physical/natural damage  " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ISLD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ISLD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The area is or is located on an island  (i.e., an area of land that is totally surrounded by water)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="island" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NATB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NATB" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A productive stand that is unreachable due to the physical features of the surrounding area (e.g., the area is a mesa or is productive forest surrounded by non-forested wetland)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural barrier" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NONE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NONE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="There are no physical or ecological 'restrictions' in the site that need to be considered when determining management of the stand." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="no management consideration" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PENA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PENA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area of land that is nearly surrounded by water and is connected to the mainland." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="peninsula" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="POOR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="POOR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A stand exhibiting stagnated growth with no discernible cause for the poor growing condition." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="stagnated, poor tree growth - no indicator" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ROCK:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ROCK" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where the rocky conditions limit accessibility by forest management equipment and/or present a potential for soil erosion due to operations." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="exposed bedrock / rocky outcrops" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SAND:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SAND" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential for erosion." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="blow sand / exposed fine sand, shallow or no humus" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SHRB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SHRB" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult without major silvicultural intervention due to shrub or brush competition." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="heavy shrub / brush" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SOIL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SOIL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area with minimal soil depth where forest regeneration will be limited as the site could be damaged or experience degradation due to operations (e.g., potential for root damage or erosion)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="shallow soils" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="STEP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="STEP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where the degree of incline is dangerous for equipment operation and presents a potential for erosion. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="steep slopes" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="WATR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential of rising surface water." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="telluric / highly fluctuating, moving ground water" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="WETT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WETT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential of rising water tables." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="poorly drained - high water table" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory  when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT=" The attribute population must follow the correct format, or empty when POLYTYPE is equal to FOR" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must follow coding: %s'%(row['${name}']) for row in [row] if row['${name}'] not in [ 'COLD','DAMG','ISLD','NATB','NONE','PENA','POOR','','ROCK','SAND','SHRB','SOIL','STEP','WATR','WETT','',None] and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="MGMTCON3:" FOLDED="true">
                        <icon BUILTIN="messagebox_warning"/>
                        <icon BUILTIN="button_cancel"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MGMTCON3" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="MGMTCON" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Management Consideration" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The management consideration attributes indicates whether or not ecological/landscape features or site conditions are present within the polygon. These features and conditions may require special consideration during resource management planning. " FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="The photo interpreters will only use the MGMTCON1 and MGMTCON2 attributes during inventory production. Use of the MGMTCON3 attribute is reserved for the forest management planning team." FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="COLD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="COLD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site with poor or unstable growing conditions due to the soil being frozen year-round." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="permafrost" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DAMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DAMG" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A stand which contains trees that are damaged, dead and/or dying due to natural causes (e.g., ice damage, blowdown, insect/disease damage)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="physical/natural damage  " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ISLD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ISLD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The area is or is located on an island  (i.e., an area of land that is totally surrounded by water)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="island" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NATB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NATB" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A productive stand that is unreachable due to the physical features of the surrounding area (e.g., the area is a mesa or is productive forest surrounded by non-forested wetland)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural barrier" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NONE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NONE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="There are no physical or ecological 'restrictions' in the site that need to be considered when determining management of the stand." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="no management consideration" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PENA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PENA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area of land that is nearly surrounded by water and is connected to the mainland." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="peninsula" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="POOR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="POOR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A stand exhibiting stagnated growth with no discernible cause for the poor growing condition." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="stagnated, poor tree growth - no indicator" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ROCK:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ROCK" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where the rocky conditions limit accessibility by forest management equipment and/or present a potential for soil erosion due to operations." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="exposed bedrock / rocky outcrops" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SAND:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SAND" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential for erosion." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="blow sand / exposed fine sand, shallow or no humus" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SHRB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SHRB" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult without major silvicultural intervention due to shrub or brush competition." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="heavy shrub / brush" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SOIL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SOIL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area with minimal soil depth where forest regeneration will be limited as the site could be damaged or experience degradation due to operations (e.g., potential for root damage or erosion)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="shallow soils" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="STEP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="STEP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where the degree of incline is dangerous for equipment operation and presents a potential for erosion. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="steep slopes" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="WATR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential of rising surface water." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="telluric / highly fluctuating, moving ground water" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="WETT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WETT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential of rising water tables." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="poorly drained - high water table" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory  when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT=" The attribute population must follow the correct format, or empty when POLYTYPE is equal to FOR" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must follow coding: %s'%(row['${name}']) for row in [row] if row['${name}'] not in [ 'COLD','DAMG','ISLD','NATB','NONE','PENA','POOR','','ROCK','SAND','SHRB','SOIL','STEP','WATR','WETT','',None] and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="AGS_POLE:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Polewood Acceptable Growing Stock" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AGS_POLE" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="AGS_SML:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Sm Sawlog Acceptable Growing Stock" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AGS_SML" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="AGS_MED:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Med Sawlog Acceptable Growing Stock" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AGS_MED" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="AGS_LGE:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Lg Sawlog Acceptable Growing Stock" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AGS_LGE" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="UGS_POLE:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Polewood Unacceptable Growing Stock" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="UGS_POLE" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="UGS_SML:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Sm Sawlog Unacceptable Growing Stock" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="UGS_SML" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="UGS_MED:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Med Sawlog Unacceptable Growing Stock" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="UGS_MED" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="UGS_LGE:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Lg Sawlog Unacceptable Growing Stock" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="UGS_LGE" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="USPCOMP:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Understorey Species Composition" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="120" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="USPCOMP" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="This understorey species composition attribute indicates the tree species that are present in the understorey portion of the forest stand canopy and the proportion of cover that each species occupies within the understorey. " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="See SPCOMP list in eFRI tech spec for list of acceptable species" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <icon BUILTIN="messagebox_warning"/>
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="When POLYTYPE is equal to FOR:" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory when VERT is two-tiered or multitiered (T or M)" FOLDED="true"/>
                                <node TEXT="Pattern is SSSPPPSSSPPP   example: PJ  80PO  20  (there are two blanks between the species and the proportion) " FOLDED="true"/>
                                <node TEXT=" No duplicate species codes allowed in the string" FOLDED="true"/>
                                <node TEXT="Proportion values in the string must sum to 100 " FOLDED="true"/>
                                <node TEXT="The tree species in the composition are to be coded using the scheme listed here.   " FOLDED="true"/>
                                <node TEXT="*each species code is 3 characters (including blanks) and is left justified" FOLDED="true"/>
                                <node TEXT="*each proportion is 3 characters which represents an integer value from 1 to 100 and is right justified. Note that initial inventory values determined by photo interpretation will not go below 10% and are rounded to the nearest 10%  (e.g., 10, 20, 30). During inventory update, proportions based on subsequent field survey sampling that are of a finer resolution can be entered (e.g., go below 10% or be a value in between the initial rounded proportions such as 15). " FOLDED="true"/>
                                <node TEXT="*maximum of 10 species and proportions pairs in the string" FOLDED="true"/>
                                <node TEXT="If SOURCE is NOT plot based then allow specific species in SPCOMP (eFRI tech spec)" FOLDED="true"/>
                                <node TEXT="Tree species in the list [     'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn',     'CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf',     'OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX',     'OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX',     'Sb','Sw','la','Wb','Wi'     ]" FOLDED="true"/>
                                <node TEXT="If SOURCE is plot based (PLOTVAR, PLOTFIXD) then allow other species in SPCOMP (eFRI tech spec)" FOLDED="true"/>
                                <node TEXT="Plot Species are [     'AL','Aq','Ap','Ag','Bc','Bp','Gb','Bb','Cat','Cc','Cm',     'Cp','Cs','Ct','Er','Eu','Hk','Ht','Hl','Hb','Hm','Hp',     'Hs','Hc','Kk','Le','Lj','Bl','Ll','Lb','Gt','Mb','Mf',     'Mm','Mt','Mn','Mp','AM','Ema','Mo','Obl','Ob','Och','Op',     'Os','Osw','Pa','Pn','Pp','Pc','Ph','Pe','Red','Ss','Sc',     'Sk','Sn','Sr','Sy','Tp','Haz'     ]" FOLDED="true"/>
                                <node TEXT="If SPCOMP percentages are not in 10's then SOURCE must be plot based. 1's not 10's" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null in two- or multi-canopy stands' for row in [row] if row['${name}'] is None   and row['VERT'] in ['TO','TU','MO','MU'] ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank in two- or multi-canopy stands' for row in [row] if str(row['${name}']).strip() == ''  and row['VERT'] in ['TO','TU','MO','MU']  ]" FOLDED="true"/>
                                <node TEXT="[ i for i in [ test_spcomp(row['${name}']) for r in [row] if r['POLYTYPE'] == 'FOR'  and row['VERT'] in ['TO','TU','MO','MU'] and r['${name}'] is not None ] if i is not None  ]" FOLDED="true"/>
                                <node TEXT="[ '%s is not a valid species code'%(s) for s in test_SpeciesInList(row['${name}'], [ 'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn','CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf','OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX', 'Sb','Sw','la','Wb','Wi' ]        ) if row['POLYTYPE'] == 'FOR'  and row['SOURCE'][:4] != 'PLOT' and row['VERT'] in ['TO','TU','MO','MU']  and row['${name}'] is not None ]" FOLDED="true"/>
                                <node TEXT="[ '%s is not a valid species code'%(s) for s in test_SpeciesInList(row['${name}'],    [ 'AL','Aq','Ap','Ag','Bc','Bp','Gb','Bb','Cat','Cc','Cm', 'Cp','Cs','Ct','Er','Eu','Hk','Ht','Hl','Hb','Hm','Hp', 'Hs','Hc','Kk','Le','Lj','Bl','Ll','Lb','Gt','Mb','Mf','Mm','Mt','Mn','Mp','AM','Ema','Mo','Obl','Ob','Och','Op', 'Os','Osw','Pa','Pn','Pp','Pc','Ph','Pe','Red','Ss','Sc','Sk','Sn','Sr','Sy','Tp','Haz' ,                               'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn','CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf','OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX', 'Sb','Sw','la','Wb','Wi'    ]     ) if row['POLYTYPE'] == 'FOR' and row['SOURCE'][:4] == 'PLOT' and row['VERT'] in ['TO','TU','MO','MU'] and row['${name}'] is not None ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="UWG:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Understory Working Group" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="UWG" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="UYRORG:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Understory Year of Origin" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="UYRORG" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The understorey year of origin attribute indicates the average year that the species having the greatest relative abundance (in terms of basal area) within the understorey came into existence." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="The year of origin is used to calculate the average age of a productive forest stand. The year of origin is determined in relation to the age of the trees and the year that regeneration (natural or artificial) was established on a site. The year of origin is synonymous to the year of germination. If regeneration has not been established, then the year of origin will be the same as the year of last disturbance. Once trees are established on the site, the year of origin must be adjusted to reflect the average age of the established regeneration.   Once a forest stand has been assessed as free-to-grow, based on attaining the regeneration/management standards, the year of origin should be adjusted to reflect the average age of the growing stock on that site.   In determining the year of origin, age differences that result from natural and artificial growing stock, or age differences that occur due to modified clear-cut harvesting or shelterwood management, must be considered.  For example, if a productive forest stand is harvested in two strips/passes which occur in different years (e.g., a type of modified clear-cut), the forest stand will technically have two ages for a period of time; one age for the recently disturbed strip and another age for the non-disturbed strip. The eventual strategy is to produce an even-aged forest stand. Therefore, the year of origin will eventually be determined as one average value for the forest stand. If the remaining strip is not harvested for ten-years (assuming that the harvested strip requires the same time period to reach free-growing conditions), the harvested and regenerated strip will be described by the understorey forest stand characteristics and, therefore, have a separate year of origin. The year of origin and the forest stand characteristics for the overstorey will likely not change. Once the remaining strip has been harvested and the regeneration in that strip has achieved free-to-grow status, the average age of the dominant tree species in each of the strips should be determined and then used to calculate the appropriate year of origin for the entire stand. At this point, understorey forest stand characteristics are no longer required.  Normally, updates to the year of origin have a corresponding change to the year of update attribute as this indicates the currency/vintage of the information and when the year of origin value was determined.   Year of origin information is not required for non-forested and non-productive forest land types. " FOLDED="true"/>
                            <node TEXT="This is a calculated value. Understorey year of origin is calculated as the year of the data source from which age is determined  minus  the understorey age as determined using the source  (i.e.,  UYRORG  =  year of age source - UAGE).  For example, using imagery taken in 2007, the age of the stand is interpreted to be 50 years at that time (in 2007).  Thus the year of origin for the stand is 2007 - 50 = 1957.  Note that the source used to determine age may not be the data source listed in the SOURCE attribute if multiple data sources were used to generate the stand description.  " FOLDED="true">
                                <icon BUILTIN="help"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="A zero value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="The UYRORG attribute value must be greater than or equal to 1600 when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="The UYRORG attribute must be less than the plan period start year when POLYTYPE is equal to FOR" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be = 0  in two- or multi-canopy stands' for row in [row] if row['${name}']  == 0 and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} must be &gt;= 1600' for row in [row] if row['${name}']  &lt;1600 and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} must be &lt;= YRORG  in two- or multi-canopy stands' for row in [row] if row['${name}']  &gt; row['YRORG']  and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="UHT:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Understory Height" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="UHT" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The understorey height attribute indicates the estimated average tree height (in meters) of the species that has the most basal area as inventoried in the Year of Update. Estimates can be made from interpreted crown closure or field samples, or from growth algorithms." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="Valid numeric values are from 0 through 40.0 when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="Value should be less than HT when POLYTYPE is equal to FOR." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must be &gt;= 0 and  ${name} &lt;= 40 and POLYTYPE is FOR' for row in [row] if (row['${name}'] &lt; 0 or row['${name}'] &gt; 40) and row['POLYTYPE'] == 'FOR'  ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} must be &lt;= HT' for row in [row] if row['${name}']  &gt; row['HT']  and row['POLYTYPE'] == 'FOR']   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="Height / Age relationships should be in the range of Plonski and others." FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="USTKG:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Understory Stocking" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="USTKG" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The understorey stocking attribute indicates a qualitative measure of the density of tree cover within the understorey.  It is expressed as a percentage value ranging from zero to a maximum of 4.00, although 2.50 is the typical maximum value encountered in the field." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct format when the POLYTYPE is equal to FOR" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must be &gt;= 0 and  ${name} &lt;= 4.00 and POLYTYPE is FOR' for row in [row] if (row['${name}'] &lt; 0 or row['${name}'] &gt; 4.01) and row['POLYTYPE'] == 'FOR'  ]   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="USC:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="USC" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Understorey Site Class" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The understorey site class attribute indicates a site quality estimate for the understorey of a forest stand. It is determined using the average height, age, and working group, based on the dominant tree species of the understorey. These attributes are compared against height and age growth curves in Plonski's Normal Yield Tables for different species to determine the relative growth rate for a forest stand.   " FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="0:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="0" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Best" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="1:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="1" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Better" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="2:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="2" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Good" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="3:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="3" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Poor" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="4:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="4" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest land on which forest management activities cannot normally be practiced without incurring deleterious environmental effects because of obvious physical limitation such as steep slopes or shallow soils over bedrock." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Protection Forest" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" ST1: The attribute population must follow the correct format  when POLYTYPE is equal to FOR" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must follow coding' for row in [row] if row['${name}'] not in [ 0,1,2,3,4 ] and row['POLYTYPE'] == 'FOR'  ]   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="VERDATE:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Date" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="VERDATE" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Verification Status Date" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The verification status date attribute contains the date that the geographic unit was verified/validated.  " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="SENSITIV:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SENSITIV" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Data Sensitivity Indicator" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The data sensitivity indicator attribute contains an indication of whether the geographic unit is classified as sensitive or not." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                                <node TEXT="Contains yes or no" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="MANAGED:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Managed / Unmanaged Indicator" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MANAGED" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="MANAGED" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="M:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="M" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The Crown forest area can be managed for timber production." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Managed" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="U:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="U" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="There exists a legal or land use planning decision which prevents the Crown forest from being managed for timber production." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Unmanaged" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The managed / unmanaged indicator attribute applies to Crown forest areas only.  The attribute indicates whether or not there is a legal or land use planning decision which prevents the land from being managed for timber." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="TYPE:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="TYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The stand type attribute is traditional/historic forest resources inventory attribute. This  attribute contains a unique code which is used to classify and describe the area within  a polygon." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="MNRCODE:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="MNRCODE" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MNRCODE" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="MNRCODE" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The MNR code attribute is traditional/historic forest resources inventory (FRI) attribute.  This attribute provides the basic identity for all topologic features by describing the  spatial feature type, such as a polygon, point, or line, in combination with identifying  the kind of geographic feature, such as lake polygon or a shoreline - line segment.   MNR code was created to identify spatial and geographic feature types (topologic  identity) for all spatial features maintained in the Ontario Geographic Database System  or former Ontario Basic Mapping (OBM) that were utilized in the creation of a traditional  FRI (PFOREST, ALLCL, OWNER and other former base layers), which in some cases  have been maintained and incorporated into several of the forest polygon coverages. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="SMZ:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Strategic Management Zone" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="5" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SMZ" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The strategic management zone attribute indicates the unique short form identifier given to a strategic management zone.  " FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="A strategic management zone is a geographical separation or sub-division of the area within a forest management unit.  In relation to forest diversity and landscape classes, the strategic management zone refers to similar areas within a forest management unit which are physically or geographically separated from each other but are grouped for management purposes.     Examples of strategic management zones are ecological zones (eco-regions or ecodistricts), watershed zones, large landscape patches (LLP), wilderness zones, industrial working circles/operating units, and intensive forest management areas.  Strategic management zone information may be used in the preparation of forest management plan tables. Strategic management zone information may also influence forest modelling and landscape diversity analysis. If a forest management unit is divided into strategic management zones, Sustainable Forest Licence holders (plan holders) must provide the strategic management zone identification code and a name that describes the strategic management zone for all licenced Crown lands within a designated forest management unit. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="OMZ:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Operational Management Zone" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="OMZ" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The operational management zone attribute indicates the unique short form identifier given to an operational management zone.  " FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="An operational management zone represents areas with distinct operational constraints (e.g., accessibility, other constraints on harvest operations, moose emphasis areas, deer yards). Operational management zones may be used on management units with significant variation in forest-level operational characteristics.     Operational management zone information is less likely to influence forest modelling and landscape diversity analysis at a strategic scale. The operational management zone information is more likely to be used during the operational planning of the forest management plan. If a forest management unit is divided into operational management zones, Sustainable Forest Licence holders (plan holders) must provide the operational management zone identification code or a name that describes the operational management zone for all licenced Crown lands within a designated forest management unit. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory when POLYTYPE is equal to FOR" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="PLANFU:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Plan Forest Unit" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="10" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="PLANFU" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The plan forest unit attribute indicates the unique short form label / ID given to an aggregation of forest stands for management purposes which have similar species composition, develop in a similar manner (both naturally and in response to silvicultural treatments), and are managed under the same silvicultural system and is the primary regulated forest unit for which the available harvest area is determined." FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="Licensees must identify a forest unit for all productive forest areas on Crown lands within a forest management unit." FOLDED="true"/>
                            <node TEXT="The classification of the plan forest unit attribute must be applied as a set of inventory based SQL statements and must not be manually assigned.  This is to ensure repeatability and ease of documentation. The classification must be identified in the Analysis Package." FOLDED="true"/>
                            <node TEXT="The technical specifications allow FU codes of up to 10 characters but the Modelling and Inventory Support Tool (MIST) can only handle forest unit codes that are a maximum of 5 characters. This field will be truncated to 5 characters on load to MIST. Therefore, the codes must be unique in the first 5 characters to maintain proper forest unit groupings.  " FOLDED="true"/>
                            <node TEXT="Forest unit information is used to support the preparation of several other forest management plan tables, schedules, and reports as well as to support forest modeling, landscape diversity analysis, and the development of a management strategy." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'POLYTYPE':'FOR','${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="default" FOLDED="true"/>
                        </node>
                    </node>
                    <node COLOR="#000000" TEXT="AGESTR:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Age Structure Indicator" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AGESTR" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The age structure indicator attribute designates whether the range of ages of the trees in a forest stand is narrow or wide spread." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT=" ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="If the SILVSYS attribute equals CC, then the AGESTR attribute should be E when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="If the SILVSYS attribute equals SE, then the AGESTR attribute should be U when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT=" If the SILVSYS attribute equals SH, then the AGESTR attribute should be E when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be E when POLYTYPE is FOR and SILVSYS IS CC' ${:NF} row['${name}'] not in [ 'E'  ]   and row['SILVSYS'] == 'CC'  ]   " FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be U when POLYTYPE is FOR and SILVSYS IS SE' ${:NF} row['${name}'] not in [ 'U'  ]   and row['SILVSYS'] == 'SE'  ]   " FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be E when POLYTYPE is FOR and SILVSYS IS SH' ${:NF} row['${name}'] not in [ 'E'  ]   and row['SILVSYS'] == 'SH'  ]   " FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'POLYTYPE':'FOR','${name}':None}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'U', 'SILVSYS':'CC'}" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be E when POLYTYPE is FOR and SILVSYS IS CC" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'E', 'SILVSYS':'SE'}" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be U when POLYTYPE is FOR and SILVSYS IS SE" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'U', 'SILVSYS':'SH'}" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be E when POLYTYPE is FOR and SILVSYS IS SH" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="E:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="E" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An even-aged forest stand is composed of trees having no, or relatively small, differences in age, usually less than10 to 20 years.  Since the trees are approximately the same age, they generally represent one age class, however two distinct age classes may develop (be present) for a limited time, as a result of timber management techniques employed (e.g., shelterwood or modified clear-cut harvesting) or natural events. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Even-aged" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="U:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="U" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT='A forest stand is considered to be uneven-aged if there are considerable differences in the age of the trees in the stand, usually an age range of more than 10 to 20 years.  Uneven-aged stands may contain trees representing three or more age classes, or a continuous range of ages that are spatially intermixed within the stand.  These stands are also referred to as "all-aged" stands.  Uneven-aged stands being managed for timber production purposes are normally managed under the selection silvicultural system.' FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Uneven-aged" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true"/>
                    </node>
                    <node TEXT="AGE:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Age" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AGE" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The age attribute indicates the average age of the dominant and co-dominant trees based on the working group species in a forest stand as of the start date of the new plan period. This is a numeric value calculated on the difference between the plan start year and the YRORG value." FOLDED="true"/>
                            <node TEXT="The age attribute must be calculated based on the difference between the plan period start date and the forest stand year of origin. For example, if the start date of the plan period is April 1, 2013 and the year of origin for a forest stand is 1933, then the average age of the forest stand is 80 years. " FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="Age must be determined for all productive forest areas on the forest management unit and is used to determine the age class information which is required in the preparation of several forest management plan tables, schedules, and reports. Age class, similar to age, must also be determined based on the start date of the plan period." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory " FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A zero or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1:  The plan period start year minus the YRORG attribute value should equal the AGE attribute value." FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must equal plan period start year minus YRORG when POLYTYPE equals FOR' for row in [row] if ( ${planyear} - row['YRORG'] != row['${name}'])]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'POLYTYPE':'FOR','${name}':10, 'YRORG':1950 } " FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must equal plan period start year minus YRORG when POLYTYPE equals FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':0, 'DEVSTAGE':'NAT' } " FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node STYLE="fork" TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="70" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="AVAIL:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Availability Indicator" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AVAIL" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="AVAIL" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The availability indicator attribute identifies which portions of the managed Crown production forest are available for timber production or not. " FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="Licensees must identify the areas of managed Crown production forest, by forest stand, which are available or unavailable for timber production. The determination of availability is a management planning decision based on considering the productive forest modifier, recent changes to any operational guidelines since the last plan, and reserve areas that were identified during area of concern planning (both past and present).  The productive forest modifier attribute identifies whether a forest stand is designated as production forest, production forest - designated management reserve, or protection forest. Normally, productive forest areas which are designated as production forest are considered as forest stands which are available for timber production. Productive forest areas that are designated as protection forest are usually considered as forest stands which are not available for timber production. Productive forest areas which are designated as production forest - designated management reserve are also normally considered as forest stands which are not available for timber production.  The decision regarding the availability of a forest stand area for timber management must be identified in the availability indicator attribute as either available or unavailable. The sum of the available production forest area, by forest stand and age class, as determined from the age attribute, should correspond to the forest unit and age class subtotals in forest management plan table FMP-3, Summary of Managed Crown Productive Forest by Forest Unit. " FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="A:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="A" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Crown production forest that can be managed for timber production." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Available" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="U:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="U" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Managed Crown production forest that is not available for timber production as a result of reserve prescriptions developed in a forest management plan (e.g., area of concern reserve)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Unavailable" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: If the MANAGED attribute equals U, then the AVAIL attribute should be U when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST2: If the FORMOD attribute equals PF, then AVAIL attribute should be equal to U when POLYTYPE equals FOR and OWNER equals 1" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="ST2: If the SC attribute equals 4, then the AVAIL attribute should be U when POLYTYPE is equal to FOR and OWNER equals 1" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                                <node TEXT="ST2: If the ACCESS1 and ACCESS2 attributes are not equal to NON, then the AVAIL attribute should be U when POLYTYPE is equal to FOR and OWNER equals 1" FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be U when POLYTYPE is FOR and MANAGED IS U' ${:F} row['${name}'] != 'U'  and row['MANAGED'] == 'U'  ]   " FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="[ 'ST2: &lt;mark&gt;${name}&lt;/mark&gt; should be U when FORMOD is PF and OWNER is 1' ${:F} row['${name}'] != 'U' and row['FORMOD'] == 'PF' and row['OWNER'] == '1'  ]   " FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="[ 'ST2: &lt;mark&gt;${name}&lt;/mark&gt; should be U when POLYTYPE is FOR and SC equals 4 ( and OWNER 1)' ${:F} row['${name}'] != 'U' and row['SC'] == 4  and row['OWNER'] == '1']   " FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                                <node TEXT="[ 'ST2: &lt;mark&gt;${name}&lt;/mark&gt; should be U when ACCESS1 and ACCESS2 are not NON (and OWNER 1)' ${:F} row['${name}'] !='U' and row['ACCESS1'] not in [  'NON' ]  and row['ACCESS2'] not in [ None, 'NON' ] and row['OWNER'] == '1'  ]   " FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'A', 'MANAGED':'U' }" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be U when POLYTYPE is FOR and MANAGED IS U" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'A', 'OWNER':'1', 'FORMOD':'PF' , 'MGMTCON1':'ROCK' }    " FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                    <node TEXT="ST2: &lt;mark&gt;${name}&lt;/mark&gt; should be U when FORMOD is PF and OWNER is 1" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'A', 'OWNER':'1', 'SC':4 }" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                    <node TEXT="ST2: &lt;mark&gt;${name}&lt;/mark&gt; should be U when POLYTYPE is FOR and SC equals 4 ( and OWNER 1)" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'A', 'OWNER':'1', 'ACCESS1':'LUD', 'ACCESS2':'OWN' }  " FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                    <node TEXT="ST2: &lt;mark&gt;${name}&lt;/mark&gt; should be U when ACCESS1 and ACCESS2 are not NON (and OWNER 1)" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="SILVSYS:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Silvicultural System" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SILVSYS" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="SILVSYS" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The silviculture system attribute indicates the process by which a productive forest stand will be managed for timber production purposes.  The process/system is classified according to the method of harvesting that will be used. " FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="There are three basic silvicultural systems clear-cut, shelterwood, and selection. Licensees must identify the applicable silvicultural system for those forest stands that have been identified as available for timber production in the availability indicator attribute.  The stage of development attribute provides an indication of the most recent silvicultural system that was applied to each forest stand. Therefore, the silvicultural system management decision attribute should normally correspond to the silvicultural system that is associated with the stage of development attribute. However, Licensees may identify a more appropriate silvicultural system based on the forest unit for the purposes of the new forest management plan. Therefore, the silvicultural system attribute represents the silvicultural system that will be applied to a forest stand. In some cases, this may differ from the silvicultural system that was employed in the past. The silvicultural system must be identified for each stand, although it is normally assigned on a forest unit basis.  " FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="CC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A silvicultural system of regenerating an even-aged forest stand in which new seedlings become established in fully exposed micro-environments after most or all of the existing trees have been removed. Regeneration can originate naturally or can be applied artificially. Clearcutting may be done in blocks, strips or patches." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Clearcut" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An uneven-aged silvicultural system where mature and/or undesirable trees are removed individually or in small groups over the whole area, usually in the course of a cutting cycle. Regeneration is generally natural." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Selection" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SH" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An even-aged silvicultural system where mature trees are harvested in a series of two or more cuts (preparatory, seed, first removal, final removal) for the purpose of obtaining natural regeneration under shelter of the residual trees, either by cutting uniformly over the entire stand area or in narrow strips. Regeneration is natural or artificial. The regeneration interval determines the degree of even-aged uniformity." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Shelterwood" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="Rule we aren't testiing - policy compliance but not a sensible error" FOLDED="true">
                                <node TEXT="ST1: A null or blank value is not a valid code when AVAIL is equal to A" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null if POLYTYPE equals FOR and AVAIL is A' ${:F} ${NIN} and row ['AVAIL'] == 'A'    ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="NEXTSTG:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="NEXTSTG" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="NEXTSTG" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The next stage attribute indicates the next harvest or cut treatment that is planned to occur in an available productive forest stand being managed for timber production. " FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="The next stage attribute indicates the next harvest or cut treatment that is planned to occur for an available productive forest stand. The next stage depends on the silvicultural system employed. Licensees must identify the next harvest treatment that will occur for each forest stand which is available for timber production based on the availability indicator attribute.   The next stage often corresponds to the stage of development attribute. The stage of development attribute represents the current development state, and/or the current stage of silvicultural management for each productive forest stand.   The next stage is most applicable to the forest stands that have been selected for planned operations (harvest) within the new plan term. The next stage will be used to populate the stage of management in the operational tables in the forest management plan (i.e., FMP-11 and FMP-16). " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="Validation when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory " FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory " FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme " FOLDED="true"/>
                                <node TEXT=" ST1: A blank or null value is not a valid code " FOLDED="true"/>
                                <node TEXT="If the AGESTR attribute equals E, then the NEXTSTG attribute cannot be IMPROVE or SELECT " FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ '${name} must not be null' for row in [row] if row['${name}'] is None and row['POLYTYPE'] == 'FOR' ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must not be blank' for row in [row] if str(row['${name}']).strip() == '' and row['POLYTYPE']=='FOR' ]" FOLDED="true"/>
                                <node TEXT="[ '${name} must follow coding: %s'%(row['${name}']) for row in [row] if row['${name}'] not in [ 'THINPRE','THINCOM','CONVENTN','BLKSTRIP','SEEDTREE','SCNDPASS','HARP','CLAAG','PREPCUT','SEEDCUT','FIRSTCUT','LASTCUT','IMPROVE','SELECT','SNGLTREE','GROUPSE'] and row['POLYTYPE'] == 'FOR' ]   " FOLDED="true"/>
                                <node TEXT="[ '${name} must not be IMPROVE or SELECT when POLYTYPE is FOR and AGESTR IS E ' for row in [row] if row['${name}'] in [ 'IMPROVE','SELECT'  ] and row['POLYTYPE'] == 'FOR'  and row['AGESTR'] == 'E'  ]   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="comment" FOLDED="true">
                            <node TEXT=" (MGMTSTG)" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="THINPRE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="THINPRE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Free-growing productive forest areas which receive a mid-rotation partial harvest (reduction in the growing stock)  that is designed to meet various objectives such as improving tree spacing, removing trees not suited to the site, and promoting the growth of the best quality trees.  The trees selected for removal do not result in a harvest of merchantable volume. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive a pre-commercial thinning/spacing treatment" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="THINCOM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="THINCOM" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Free-growing productive forest areas which receive a mid-rotation partial harvest (reduction in the growing stock)  that is designed to meet various objectives such as improving tree spacing, removing trees not suited to the site, and promoting the growth of the best quality trees.  The harvested trees are removed from the site and used for commercial purposes. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive a commercial thinning/spacing treatment" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="CONVENTN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CONVENTN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of most or all of the existing trees in a stand (or a number of adjacent stands) in one operation, so that new seedlings become established in a fully exposed micro-environments.  Harvesting patterns include conventional clearcuts, block cuts and patch cuts.  " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive a conventional clearcut" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="BLKSTRIP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BLKSTRIP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of a portion of the existing trees in a stand in progressive strips in more than one operation so that the non-disturbed portion of the stand is left primarily to provide a natural seed source for regeneration of the disturbed area.  Several cutting patterns are available to achieve same goal.  The removal of trees in one or more passes in a system of strips of various widths; where each strip is less than or equal to 100 meters (5 chains) wide.  It is designed to encourage regeneration on difficult and/or fragile sites.  Note:  Harvesting where the cut strips are greater than 100 meters wide (&gt; 5 chains) should be recorded as clearcut. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive a modified cut: block or strip" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEEDTREE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDTREE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An even-aged, silvicultural system that retains mature standing trees scattered throughout the cutblock to provide seed sources for natural regeneration.  A method of harvesting and regenerating a forest stand in which all trees are removed from the area except for a small number of seed-bearing trees that are left singly or in small groups.  The objective is to create an even-aged stand. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive a modified cut: seed tree" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SCNDPASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="For areas managed using the clearcut silvicultural system, harvest may be planned in two passes.  This is normally when species within the stand are harvested and utilized by different logger/contractor/forest resource Licensee in different years (e.g., first pass is conifer and second-pass is hardwood).  A first pass should have been recorded in the annual report if merchantable tree species remained in the forest stand which have been allocated for harvest - but not yet harvested.  The second-pass option should be denoted when merchantable tree species are selected to be harvested from forest stands which have been previously recorded as harvested in a first pass." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive a modified cut: next / second-pass" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="HARP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="HARP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="HARP is the removal of the dominant canopy layer in uneven-aged lowland black spruce ecosystems.  HARP protects and retains stems below a set diameter limit, leaving a significant component of the overstorey.  The resulting stand is uneven-aged and uneven-sized." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive a harvest with regeneration protection  " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="CLAAG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLAAG" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An operational practice that can be applied with any harvest method under the clearcut silvicultural system, where the objective is to remove the overstorey, protect understorey advance growth, and regenerate an even-aged stand.  The resulting stand develops under full light conditions, generally with a reduced rotation length." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive a careful logging around advance growth / regeneration cut" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PREPCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PREPCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management designed to remove undesirable species of any species from the stand and to select trees to remain that will provide the best seed source.  The removal of undesirable trees opens the canopy and enables the crowns of remaining seed-bearing trees to enlarge; to improve conditions for seed production and natural regeneration." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive a preparatory cut" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEEDCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management where trees are removed from a mature stand in order to create openings in the canopy / create spaces and to prepare sites for natural regeneration while maintaining the seed-bearing trees and protecting any existing advance regeneration." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive a seed cut" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FIRSTCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FIRSTCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management where overstorey trees are removed in one or more harvests in order to release the established seedlings from competition." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive a first removal harvest" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="LASTCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LASTCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management where all of the remaining trees in the overstorey are removed.  This is the removal of the seed or shelter trees after the regeneration has been effective." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive a last removal harvest" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="IMPROVE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="IMPROVE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A selection silvicultural system stage of management where a cut is made in an uneven-aged stand primarily to improve stand composition. distribution and quality by removing less desirable trees of any species." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive an improvement cut" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SELECT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SELECT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A selection silvicultural system stage of management where individual trees or groups of trees are selected for cutting in order to recover the yield and develop a balanced uneven-aged structure, while providing the cultural measures required for tree growth and seeding establishment. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive a selection harvest" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SNGLTREE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SNGLTREE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The stand canopy is (periodically) opened uniformly throughout the entire stand to achieve a post-harvest, basal area target." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Selection: single-tree " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="GROUPSE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GROUPSE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The stand canopy is (periodically) opened by harvesting trees in small groups. The resulting canopy opening usually occupies a fraction of a hectare." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Selection: group " FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR and AVAIL is equal to A" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: If the AGESTR attribute equals E, then the NEXTSTG attribute cannot be IMPROVE or SELECT " FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be IMPROVE or SELECT when POLYTYPE is FOR and AGESTR IS E' ${:NF} row['${name}'] in [ 'IMPROVE','SELECT'  ] and row['AGESTR'] == 'E'  ]   " FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':None }" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'IMPROVE', 'AGESTR':'E' }" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be IMPROVE or SELECT when POLYTYPE is FOR and AGESTR IS E" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="CONVENT" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="SI:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Silvicultural Intensity" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="5" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SI" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="SI" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="Growth class upon which the stand is growing (or is projected to be growing) independent of silvicultural treatment" FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="The FMPM says Silvicultural Intensity means the projected yield and not the treatments to be implemented" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR,  and SILVSYS is CC or SH" FOLDED="true"/>
                                <node TEXT="ST1: A null or blank value is not a valid code when POLYTYPE is equal to FOR, AVAIL is A and SILVSYS is CC or SH" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null when POLYTYPE is FOR, AVAIL is A and SILVSYS is CC or SH' ${:F} ${NIN} and row ['AVAIL'] == 'A' and row['SILVSYS'] in ['CC','SH']  ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'POLYTYPE':'FOR','${name}':None, 'AVAIL':'A', 'SILVSYS':'CC' }" FOLDED="true">
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null when POLYTYPE is FOR, AVAIL is A and SILVSYS is CC or SH" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="SISRC:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Source of Silvi. Intensity" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SISRC" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="SISRC" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="Source of Silvicultural Intensity (SI)" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true"/>
                                <node TEXT="ST1: A null or blank value is not a valid code when POLYTYPE is equal to FOR, AVAIL is A and SILVSYS is CC or SH" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null when POLYTYPE is FOR, AVAIL is A and SILVSYS is CC or SH' ${:F} ${NIN} and row ['AVAIL'] == 'A' and row['SILVSYS'] in ['CC','SH']  ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'POLYTYPE':'FOR','${name}':None, 'AVAIL':'A', 'SILVSYS':'CC' }" FOLDED="true">
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null when POLYTYPE is FOR, AVAIL is A and SILVSYS is CC or SH" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="ACTUAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ACTUAL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Information from a survey." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ASSIGNED:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ASSIGNED" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Assigned by the planning team, based on projections or other data modelling exercise." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="comment" FOLDED="true">
                            <node TEXT="Should we refine this to include the 2 survey stages from SEI? Regen Assessment and Performance Survey" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="fields_include:" FOLDED="true">
                    <node TEXT="PCM" FOLDED="true">
                        <node TEXT="FMFOBJID:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="GEOGNUM:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="POLYID:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="POLYTYPE:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="OWNER:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="AUTHORTY:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="YRUPD:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="SOURCE:" FOLDED="true" BACKGROUND_COLOR="#ff9999">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="FORMOD:" FOLDED="true">
                            <icon BUILTIN="full-1"/>
                            <icon BUILTIN="button_ok"/>
                            <icon BUILTIN="bookmark"/>
                        </node>
                        <node TEXT="DEVSTAGE:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="YRDEP:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="SPCOMP:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="WG:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="YRORG:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                            <icon BUILTIN="messagebox_warning"/>
                            <icon BUILTIN="bookmark"/>
                        </node>
                        <node TEXT="HT:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="STKG:" FOLDED="true" BACKGROUND_COLOR="#ff9999">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="SC:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="ECOSRC:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="ECOSITE1:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="ECOPCT1:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="ECOSITE2:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="ECOPCT2:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="ACCESS1:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="ACCESS2:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="MGMTCON1:" FOLDED="true">
                            <icon BUILTIN="messagebox_warning"/>
                        </node>
                        <node TEXT="MGMTCON2:" FOLDED="true">
                            <icon BUILTIN="messagebox_warning"/>
                            <icon BUILTIN="button_cancel"/>
                        </node>
                        <node TEXT="MGMTCON3:" FOLDED="true">
                            <icon BUILTIN="messagebox_warning"/>
                            <icon BUILTIN="button_cancel"/>
                        </node>
                        <node TEXT="AGS_POLE:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="AGS_SML:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="AGS_MED:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="AGS_LGE:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="UGS_POLE:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="UGS_SML:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="UGS_MED:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="UGS_LGE:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="USPCOMP:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="UWG:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="UYRORG:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="UHT:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="USTKG:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="USC:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="VERDATE:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="SENSITIV:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                        <node TEXT="SGR:" FOLDED="true">
                            <icon BUILTIN="button_ok"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Base Model Inventory (BMI)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="Plan" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ForecastDepletions:" FOLDED="true" POSITION="right">
                <icon BUILTIN="prepare"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ForecastDepletions" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="FSOURCE:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Source of Forecast" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="FSOURCE" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BASECOVR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BASECOVR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true"/>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="DIGITALA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DIGITALA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true"/>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="DIGITALP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DIGITALP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true"/>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="ESTIMATE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ESTIMATE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true"/>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="FOC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FOC" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true"/>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="FORECAST:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FORECAST" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true"/>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="FRICNVRT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FRICNVRT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true"/>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="INFRARED:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="INFRARED" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true"/>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="MARKING:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MARKING" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true"/>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="OCULARA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OCULARA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true"/>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="OCULARG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OCULARG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true"/>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="OPC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OPC" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true"/>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PHOTO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTO" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true"/>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PHOTOLS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTOLS" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true"/>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PHOTOSS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTOSS" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true"/>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PLOTFIXD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLOTFIXD" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true"/>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PLOTVAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLOTVAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true"/>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="RADAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="RADAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true"/>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="REGENASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REGENASS" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true"/>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SEMEXTEN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEMEXTEN" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true"/>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SEMINTEN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEMINTEN" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true"/>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SPECTRAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SPECTRAL" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true"/>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="FYRDEP:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Forecast Year of Disturbance" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="FYRDEP" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: A zero or null value is not a valid code" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct format" FOLDED="true"/>
                                <node TEXT="ST1: The value must not be less than the plan period start year minus 4 and it must not be greater than or equal to the plan term start year" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be between %s and %s'%(str(${planyear}-4),str(${planyear})) ${:} row['${name}'] &lt; (${planyear}-4) or row['${name}']&gt;${planyear}]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="FDEVSTAGE:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Forecast Development Stage" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="FDEVSTAGE" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="DEPHARV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DEPHARV" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="recently disturbed by harvest and has received no regeneration/renewal treatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="DEPNAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DEPNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="recently disturbed by natural causes and has received no regeneration / renewal treatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="LOWMGMT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LOWMGMT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Below regeneration standards due to past management" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="LOWNAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LOWNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Below regeneration standards due to natural causes / succession" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="NEWPLANT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NEWPLANT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="recently renewed : mainly planted" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="NEWSEED:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NEWSEED" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="recently renewed : mainly seeded" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="NEWNAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NEWNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="recently renewed : mainly natural regeneration" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="FTGPLANT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FTGPLANT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="free-to-grow mainly planted" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="FTGSEED:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FTGSEED" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="free-to-grow mainly seeded" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="FTGNAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FTGNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="free-to-grow mainly natural regeneration" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="THINPRE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="THINPRE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received pre-commercial thinning/spacing treatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="THINCOM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="THINCOM" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received commercial thinning/spacing treatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="BLKSTRIP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BLKSTRIP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="modified cut: block or strip" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="FRSTPASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FRSTPASS" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="modified cut: first pass" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PREPCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PREPCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a preparatory cut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SEEDCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a seed cut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="FIRSTCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FIRSTCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a first removal harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="IMPROVE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="IMPROVE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received an improvement cut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SELECT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SELECT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a selection harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SNGLTREE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SNGLTREE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Selection:  single-tree" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="GROUPSE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GROUPSE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Selection: group" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Forecast Depletions (FDP)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="Plan" FOLDED="true"/>
                </node>
                <node TEXT="Definition" FOLDED="true">
                    <node TEXT=" " FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="FDP" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="PlannedHarvest:" FOLDED="true" POSITION="right">
                <icon BUILTIN="go"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="PlannedHarvest" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="PHR" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="SILVSYS:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Silvicultural System" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SILVSYS" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="SILVSYS" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The silviculture system attribute indicates the process by which a productive forest stand will be managed for timber production purposes.  The process/system is classified according to the method of harvesting that will be used. " FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="There are three basic silvicultural systems: clear-cut, shelterwood, and selection. Licensees must identify the applicable silvicultural system for those forest stands that have been identified as available for timber production in the availability indicator attribute.  The stage of development attribute provides an indication of the most recent silvicultural system that was applied to each forest stand. Therefore, the silvicultural system management decision attribute should normally correspond to the silvicultural system that is associated with the stage of development attribute. However, Licensees may identify a more appropriate silvicultural system based on the forest unit for the purposes of the new forest management plan. Therefore, the silvicultural system attribute represents the silvicultural system that will be applied to a forest stand. In some cases, this may differ from the silvicultural system that was employed in the past. The silvicultural system must be identified for each stand, although it is normally assigned on a forest unit basis. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT=" ST1: The population of this attribute is mandatory " FOLDED="true"/>
                                <node TEXT=" ST1: The attribute population must follow the correct coding scheme " FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="CC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A silvicultural system of regenerating an even-aged forest stand in which new seedlings become established in fully exposed micro-environments after most or all of the existing trees have been removed. Regeneration can originate naturally or can be applied artificially. Clearcutting may be done in blocks, strips or patches." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Clearcut" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An uneven-aged silvicultural system where mature and/or undesirable trees are removed individually or in small groups over the whole area, usually in the course of a cutting cycle. Regeneration is generally natural." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Selection" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SH" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An even-aged silvicultural system where mature trees are harvested in a series of two or more cuts (preparatory, seed, first removal, final removal) for the purpose of obtaining natural regeneration under shelter of the residual trees, either by cutting uniformly over the entire stand area or in narrow strips. Regeneration is natural or artificial. The regeneration interval determines the degree of even-aged uniformity." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Shelterwood" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="HARVCAT:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Harvest Category" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="HARVCAT" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="HARVCAT" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT=" The harvest disturbance category attribute indicates the planned type of harvest that  was completed." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="Road Right-of-Way" FOLDED="true"/>
                            <node TEXT="The attribute coding scheme for harvest disturbances contains an option for recording spatial  information on road right-of-ways, aggregate pits and landings. The code is ROADROW. The  primary purpose of this code is to identify harvest area for newly constructed road right-of-ways for  primary and branch roads, aggregate pits and landings that are outside of an area of operations,  where the depleted area is not intended for future regeneration purposes and will be maintained for  its intended use. The area associated with road right-of-way, aggregate pit and landing harvests are  not included in the balancing of available harvest area as these areas are usually accounted for in  forest modelling using the estimated roads and landings percentage allowance in SFMM. There  may be exceptions, which should be discussed locally between Districts and Licensees.   " FOLDED="true"/>
                            <node TEXT="The road right-of-way code (ROADROW) can be used for roads (primary and branch), aggregate  pits or landings delineated within an area of operations provided this consideration was included in  the strategic modeling. Normally harvest polygons within area of operations do not include the road  right-of-way area for primary and branch roads, aggregate pits and landings as part of the harvested  area. Primary and branch road areas are identified by delineating the boundary of the harvest area  up to the road edge or by buffering out the approximate road width using the road centre-line to  remove the area of road. The District and licensee may agree on other ways to identify or remove  roads, aggregate pits and landings in harvest areas.   " FOLDED="true"/>
                            <node TEXT="Operational roads within a harvest polygon will be reported as part of the harvest area. Operational  roads outside of a harvest polygon but within an area of operations are not normally reported on the  harvest disturbance layer." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BRIDGING:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BRIDGING" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="bridging harvest areas" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This category identifies the harvested areas that were approved as bridging under the FMP. This code is only valid in the first AR under a new FMP as these areas can only be harvested within the first AWS. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="CONTNGNT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CONTNGNT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="REGULAR1:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REGULAR1" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="regular harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="These harvest areas were categorized as regular under the FMP." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="REGULAR2:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REGULAR2" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="regular harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="These harvest areas were categorized as regular under the FMP." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SALVAGE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SALVAGE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="salvage harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Salvage is not considered to be the initial form of disturbance  since most areas which require a salvage operation have been  previously disturbed by natural causes, such as fire, insect,  disease, blowdown, etc. All salvage operations are considered to  be harvesting which is often the first silviculture treatment needed  to bring a forest stand back to a more productive state, more  quickly than if left for natural succession. As such, salvage is also  considered to be a form of protection from further loss of  merchantable volume due to insects or fire (e.g., bark  beetles/borers, or fire after insect damage). Salvage operations  also reduce the risk of further natural disturbances, or volume  loss. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="REDIRECT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REDIRECT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="redirected harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="These areas are harvested under a pest management plan and count against the available harvest area of the FMP." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ACCELER:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ACCELER" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="accelerated harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="These areas are harvested under a pest management plan and  are areas in addition to the available harvest area of the FMP. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FRSTPASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FRSTPASS" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="modified cut: first pass" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="For areas managed using the clearcut silvicultural system,  harvest may be planned in two passes. This is normally when  species within the stand are harvested and utilized by different  logger/contractor/forest resource Licensee in different years (e.g.,  first pass is conifer and second pass is hardwood). The first pass  should be recorded if merchantable tree species remain in the  forest stands which have been allocated for harvest, but not yet  harvested." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SCNDPASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCNDPASS" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Second-pass harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="For areas managed using the clearcut silvicultural system,  harvest may be planned in two passes. This is normally when  species within the stand are harvested and utilized by different  logger/contractor/forest resource licensee in different years (e.g.,  first pass is conifer and second pass is hardwood). The second  pass should be recorded when merchantable tree species have  been harvested from forest stands which have been previously  reported as harvested. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ROADROW:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ROADROW" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="FUELWOOD:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="fuelwood area" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="FUELWOOD" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Planned Harvest (PHR)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="Plan" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="AreaOfConcern:" FOLDED="true" POSITION="right">
                <icon BUILTIN="go"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="AreaOfConcern" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AOCID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AOC Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="15" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AOCID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The AOC identifier attribute is the label assigned to a specific AOC prescription which must correspond to the label on FMP and AWS Areas Selected for Operations maps and the area of concern prescriptions in table FMP-10. The prescription can represent either a group of areas of concern with a common prescription or an individual area of concern with a unique prescription." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true"/>
                                <node TEXT="Must be a code from FMP-10 - Manual Check" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="AOCTYPE:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AOC Type" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AOCTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="AOCTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The AOC type attribute indicates the type of AOC prescription as either modified or reserved." FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="M:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="M" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="modified" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Areas which are scheduled for operations but have specific conditions or restrictions on operations as required by an AOC prescription." FOLDED="true"/>
                                </node>
                                <node TEXT="stroke color" FOLDED="true">
                                    <node TEXT="#777777" FOLDED="true"/>
                                </node>
                                <node TEXT="fill color" FOLDED="true">
                                    <node TEXT="#4c7cc9" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="R:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="R" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="reserved" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Areas which are reserved (prohibited) from operations as required by an AOC prescription." FOLDED="true"/>
                                </node>
                                <node TEXT="stroke color" FOLDED="true">
                                    <node TEXT="#777777" FOLDED="true"/>
                                </node>
                                <node TEXT="fill color" FOLDED="true">
                                    <node TEXT="#a14cc9" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Area Of Concern (AOC)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="Plan" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="AOC" FOLDED="true"/>
                </node>
                <node TEXT="symbology:" FOLDED="true">
                    <node TEXT="type" FOLDED="true">
                        <node TEXT="categorized" FOLDED="true"/>
                    </node>
                    <node TEXT="field" FOLDED="true">
                        <node TEXT="AOCTYPE" FOLDED="true"/>
                    </node>
                    <node TEXT="stroke color" FOLDED="true">
                        <node TEXT="#777777" FOLDED="true"/>
                    </node>
                    <node TEXT="fill color" FOLDED="true">
                        <node TEXT="#FFFFFF" FOLDED="true"/>
                    </node>
                </node>
            </node>
            <node TEXT="PlannedResidualPatches:" FOLDED="true" POSITION="right">
                <icon BUILTIN="prepare"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="PlannedResidualPatches" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="PRP" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <icon BUILTIN="help"/>
                    <node TEXT="RESID:" FOLDED="true">
                        <icon BUILTIN="help"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Redisual Patch ID" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="10" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="RESID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition:" FOLDED="true">
                            <node TEXT="The residual patch identifier attribute is a number, label or name assigned to a residual patch(es) as defined in the FMP text. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <icon BUILTIN="help"/>
                            <node TEXT="english" FOLDED="true">
                                <icon BUILTIN="help"/>
                                <node TEXT="ST1: The presence of this attribute in the file structure or layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true"/>
                                <node TEXT="The value must be defined in the FMP" FOLDED="true">
                                    <icon BUILTIN="help"/>
                                </node>
                                <node TEXT="The value must be consistent with the Areas Selected for Operations Maps" FOLDED="true">
                                    <icon BUILTIN="help"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="RESID" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Planned Residual Patches (PRP)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="Plan" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="PlannedRoadCorridors:" FOLDED="true" POSITION="right">
                <icon BUILTIN="prepare"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="PlannedRoadCorridors" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="PRC" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="TERM:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Plan Term" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TERM" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TERM" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition:" FOLDED="true">
                            <node TEXT="The plan term attribute indicates the five-year operational plan term or the next tenyear plan period in which the planned road activity and/or use strategy will occur. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory." FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="A zero value is a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="1:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="1" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="First 5-year Operational Plan Term (1-5)" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="1-5" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="2:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="2" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Second 5-year Operational Plan Term (6-10)" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="6 -10" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="0:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="0" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The next ten year plan period (11-20)" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="11-20" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="ROADID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="30" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ROADID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road identifier attribute is the unique number, label or name assigned to the forest access road that this polygon is a part of." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory." FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ROADCLAS:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Class" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ROADCLAS" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="ROADCLAS" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road class attribute identifies the class of the proposed forest access road in terms of the road use management strategy in the FMP. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory." FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code." FOLDED="true"/>
                                <node TEXT="If TERM is zero, then ROADCLAS must be P" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: ${name} must be P when TERM is 0 ' ${:} row['${name}'] not in [ 'P'  ] and row['TERM'] == 0 ]   " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="B:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="B" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="branch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="P:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="P" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Primary" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="AOCXID:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <icon BUILTIN="messagebox_warning"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AOC Crossing Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="15" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AOCXID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The area of concern crossing identifier attribute indicates the unique value assigned to the preferred 100 meter area where road crossings are permitted within an area of concern. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <icon BUILTIN="messagebox_warning"/>
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or null value IS a valid code" FOLDED="true"/>
                                <node TEXT="The AOCXID attribute must contain a unique value within and across layers if one or more Planned Road Corridors layer is submitted." FOLDED="true">
                                    <icon BUILTIN="messagebox_warning"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="NOXING:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Crossing Prohibited" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="15" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="NOXING" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The crossing prohibited attribute indicates the area where road crossings are prohibited within the road corridor. These areas may coincide with area of concern (AOC) boundaries where the AOC prescription identifies that the crossing is prohibited. These areas may represent other constraints that prohibit the road from crossing a water body which do not relate to an AOC prescription. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRANS:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Transfer Year" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRANS" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road transfer year attribute indicates a four-digit number representing the first year of the 5-year operational term that the transfer of responsibility to the MNR is anticipated to take effect. If there is no intent to transfer responsibility to MNR during the future 20-year period there is no need to specify a year. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or zero value is a valid code" FOLDED="true"/>
                                <node TEXT="If TRANS value is populated, then INTENT must be populated" FOLDED="true"/>
                                <node TEXT="ST1: The value must be greater than or equal to the plan term start year" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: ${name} must be &gt;=  ${planyear} ' ${:N} row['${name}']  &lt; ${planyear} ]   " FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="ACYEAR:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Access Control Year" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ACYear" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The access control year attribute indicates a four-digit number representing the expected fiscal year (April 1 to March 31) that the access control is anticipated to take effect. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A zero value is a valid code" FOLDED="true"/>
                                <node TEXT="ST1: The value must be greater than or equal to the plan term start year" FOLDED="true"/>
                                <node TEXT="ST1: If access control year does not equal zero (ACYEAR NOT 0) then access control must be yes" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: If ${name} = 0 then ACCESS must be N' ${:} ${NIN} and row['ACCESS'] &lt;&gt; 'Y'  ] " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ACCESS:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Access Control" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ACCESS" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road access control attribute is a field used to identify where access control activities are planned to occur during the 10-year planning period on primary or branch roads that will be constructed during the 10-year planning period. " FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The population if this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                </node>
                                <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM = Y or MAINTAIN = Y or MONITOR = Y or ACCESS = Y) when plan term is not zero (TERM != 0) " FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record. (${name})' ${:} 'Y' not in [ row[fff] for fff in [ 'ACCESS','DECOM','MAINTAIN','MONITOR'] ]  and row['TERM'] in [1,2] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="DECOM:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Decommissioning" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="DECOM" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM = Y or MAINTAIN = Y or MONITOR = Y or ACCESS = Y) when plan term is not zero (TERM != 0) " FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory." FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record. (${name})' ${:} 'Y' not in [ row[fff] for fff in [ 'ACCESS','DECOM','MAINTAIN','MONITOR'] ]  and row['TERM'] in [1,2] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="INTENT:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="MNR Intent" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="30" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="INTENT" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The decommissioning attribute is a field used to identify where decommissioning activities are planned to occur during the 10-year planning period on primary or branch roads that will be constructed during the 10-year planning period. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: If TRANS value is populated, then INTENT must be populated" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: ${name} must not be null when TRANS is populated' ${:} ${NIN} and row['TRANS'] not in ${list_NULL} ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="MAINTAIN:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Maintenance" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MAINTAIN" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Road maintenance planned to occur during plan term" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Road maintenance NOT planned to occur during plan term" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM = Y or MAINTAIN = Y or MONITOR = Y or ACCESS = Y) when plan term is not zero (TERM != 0) " FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory " FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme " FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code " FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record. (${name})' ${:} 'Y' not in [ row[fff] for fff in [ 'ACCESS','DECOM','MAINTAIN','MONITOR'] ]  and row['TERM'] in [1,2] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="MONITOR:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Monitoring" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MONITOR" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Road maintenance planned to occur during plan term" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Road maintenance NOT planned to occur during plan term" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM = Y or MAINTAIN = Y or MONITOR = Y or ACCESS = Y) when plan term is not zero (TERM != 0) " FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory " FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme " FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code " FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record. (${name})' ${:} 'Y' not in [ row[fff] for fff in [ 'ACCESS','DECOM','MAINTAIN','MONITOR'] ]  and row['TERM'] in [1,2] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="CONTROL1:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Access Control / Decom Type" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CONTROL1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="CONTROL" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The access control or decommissioning type attributes indicate the method of access control to be implemented during the year on primary or branch roads that will be constructed during the year. This attribute may also be used to identify the type of decommissioning activities scheduled on primary or branch roads that will be constructed during the year." FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The presence and population of CONTROL1 is mandatory where ACCESS = Y" FOLDED="true"/>
                                <node TEXT="ST1: The second field must only have a control type when the first field has a control type (error when CONTROL1 = null and CONTROL2 != null)" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: ${name} must not be null when ACCESS is Y' ${:N} row['ACCESS'] in [ 'Y' ] ]" FOLDED="true"/>
                                <node TEXT="[ 'ST1: ${name} must not be null when CONTROL2 is not null' ${:} ${NIN} and row['CONTROL2'] not in ${list_NULL} ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BERM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BERM" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="berm and/or ditch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="GATE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GATE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="gated / physical barrier" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PRIV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRIV" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="private land" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="scarify and/or plant road" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIGN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIGN" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="signed" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SLSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SLSH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pile slash" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="WATX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="water crossing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="CONTROL2:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Access Control / Decom Type 2" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CONTROL2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="CONTROL" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="ST1: The second field must only have a control type when the first field has a control type (error when CONTROL1 = null and CONTROL2 != null)" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: ${name} must be null or blank when CONTROL1 is blank or null' ${:N} row['CONTROL1']  in ${list_NULL}  ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BERM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BERM" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="berm and/or ditch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="GATE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GATE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="gated / physical barrier" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PRIV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRIV" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="private land" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="scarify and/or plant road" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIGN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIGN" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="signed" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SLSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SLSH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pile slash" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="WATX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="water crossing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Planned Road Corridors (PRC)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="Plan" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="OperationalRoadBoundaries:" FOLDED="true" POSITION="right">
                <icon BUILTIN="prepare"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="OperationalRoadBoundaries" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="ORBID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Operational Road Boundaries ID" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="20" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ORBID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition:" FOLDED="true">
                            <node TEXT="The operational road boundaries identifier attribute indicates the user defined unique number, label or name assigned to the operational road boundaries. The operational road boundary is the perimeter of, the planned harvest area plus the area from an existing road or planned road corridor to the harvest area within which an operational road is planned to be constructed." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Operational Road Boundaries (ORB)" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node STYLE="fork" TEXT="ORB" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="Plan" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ExistingRoadUseManagementStrategy:" FOLDED="true" POSITION="right">
                <icon BUILTIN="prepare"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ExistingRoadUseManagementStrategy" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="ERU" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="ROADID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="30" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ROADID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road identifier attribute is the unique number, label or name assigned to the forest access road that this polygon is a part of." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory." FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ROADCLAS:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Class" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ROADCLAS" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="ROADCLAS" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road class attribute identifies the class of the proposed forest access road in terms of the road use management strategy in the FMP. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory." FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="P:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="P" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Primary" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Primary roads are roads that provide principal access for the management  unit, and are constructed, maintained and used as part of the main road  system on the management unit.  Primary roads are normally permanent  roads. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="B:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="B" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="branch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A branch road is a road, other than a primary road, that branches off an  existing or new primary or branch road, providing access to, through or  between areas of operations on a management unit." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="O:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="O" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Operational" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Operational roads are roads within operational road boundaries, other than  primary or branch roads, that provide short-term access for harvest,  renewal and tending operations. Operational roads are normally not  maintained after they are no longer required for forest management  purposes, and are often site prepared and regenerated." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="TRANS:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Transfer Year" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRANS" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road transfer year attribute indicates a four-digit number representing the first year of the 5-year operational term that the transfer of responsibility to the MNR is anticipated to take effect. If there is no intent to transfer responsibility to MNR during the future 20-year period there is no need to specify a year. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or zero value is a valid code" FOLDED="true"/>
                                <node TEXT="If TRANS value is populated, then INTENT must be populated" FOLDED="true"/>
                                <node TEXT="ST1: The value must be greater than or equal to the plan term start year" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: ${name} must be &gt;=  ${planyear} ' ${:N} row['${name}']  &lt; ${planyear} ]   " FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ACYEAR:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Access Control Year" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ACYear" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The access control year attribute indicates a four-digit number representing the expected fiscal year (April 1 to March 31) that the access control is anticipated to take effect. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A zero value is a valid code" FOLDED="true"/>
                                <node TEXT="ST1: The value must be greater than or equal to the plan term start year" FOLDED="true"/>
                                <node TEXT="ST1: If access control year does not equal zero (ACYEAR NOT 0) then access control must be yes" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: If ${name} = 0 then ACCESS must be N' ${:} ${NIN} and row['ACCESS'] &lt;&gt; 'Y'  ] " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ACCESS:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Access Control" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ACCESS" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road access control attribute is a field used to identify where access control activities are planned to occur during the 10-year planning period on primary or branch roads that will be constructed during the 10-year planning period. " FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM = Y or MAINTAIN = Y or MONITOR = Y or ACCESS = Y)" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory " FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme " FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code " FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record. (${name})' ${:} 'Y' not in [ row[fff] for fff in [ 'ACCESS','DECOM','MAINTAIN','MONITOR'] ] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="DECOM:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Decommissioning" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="DECOM" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM = Y or MAINTAIN = Y or MONITOR = Y or ACCESS = Y)" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory " FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme " FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code " FOLDED="true"/>
                                <node TEXT="ST1: If decommissioning is yes (DECOM = Y) then access control must be no (ACCESS =N)" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record. (${name})' ${:} 'Y' not in [ row[fff] for fff in [ 'ACCESS','DECOM','MAINTAIN','MONITOR'] ] ]" FOLDED="true"/>
                                <node TEXT="['ST1: If decommissioning is yes (DECOM = Y) then access control must be no (ACCESS =N)' ${:N} row['${name}'] == 'Y' and row['ACCESS'] == 'Y' ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="INTENT:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="MNR Intent" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="30" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="INTENT" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The decommissioning attribute is a field used to identify where decommissioning activities are planned to occur during the 10-year planning period on primary or branch roads that will be constructed during the 10-year planning period. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: If TRANS value is populated, then INTENT must be populated" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: ${name} must not be null when TRANS is populated' ${:} ${NIN} and row['TRANS'] not in ${list_NULL} ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="MAINTAIN:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Maintenance" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MAINTAIN" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Road maintenance planned to occur during plan term" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Road maintenance NOT planned to occur during plan term" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM = Y or MAINTAIN = Y or MONITOR = Y or ACCESS = Y)" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory " FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme " FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code " FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record. (${name})' ${:} 'Y' not in [ row[fff] for fff in [ 'ACCESS','DECOM','MAINTAIN','MONITOR'] ] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="MONITOR:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Monitoring" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MONITOR" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Road maintenance planned to occur during plan term" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Road maintenance NOT planned to occur during plan term" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM = Y or MAINTAIN = Y or MONITOR = Y or ACCESS = Y)" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory " FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme " FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code " FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record. (${name})' ${:} 'Y' not in [ row[fff] for fff in [ 'ACCESS','DECOM','MAINTAIN','MONITOR'] ] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="RESPONS:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Responsibility" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="RESPONS" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="RESPONS" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="SFL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SFL" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Sustainable Forest Licensee" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="MNR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MNR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Ministry of Natural Resources and Forestry" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: Attribute population must follow the correct coding scheme for roads that are the responsibility of the SFL or MNR. Roads that are the responsibility of other parties are user defined (e.g., OTH for other, MTO for Ministry of Transportation, PRV for private, JNT for joint)." FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true"/>
                                <node TEXT="At a minimum, one record should equal SFL" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="CONTROL1:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Access Control / Decom Type" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CONTROL1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="CONTROL" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The access control or decommissioning type attributes indicate the method of access control to be implemented during the year on primary or branch roads that will be constructed during the year. This attribute may also be used to identify the type of decommissioning activities scheduled on primary or branch roads that will be constructed during the year." FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BERM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BERM" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="berm and/or ditch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="GATE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GATE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="gated / physical barrier" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PRIV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRIV" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="private land" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="scarify and/or plant road" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIGN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIGN" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="signed" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SLSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SLSH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pile slash" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="WATX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="water crossing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The presence and population of CONTROL1 is mandatory where ACCESS = Y" FOLDED="true"/>
                                <node TEXT="ST1: The second field must only have a control type when the first field has a control type (error when CONTROL1 = null and CONTROL2 != null)" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: ${name} must not be null when ACCESS is Y' ${:N} row['ACCESS'] in [ 'Y' ] ]" FOLDED="true"/>
                                <node TEXT="[ 'ST1: ${name} must not be null when CONTROL2 is not null' ${:} ${NIN} and row['CONTROL2'] not in ${list_NULL} ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="CONTROL2:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Access Control / Decom Type 2" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CONTROL2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="CONTROL" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="ST1: The second field must only have a control type when the first field has a control type (error when CONTROL1 = null and CONTROL2 != null)" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: ${name} must be null or blank when CONTROL1 is blank or null' ${:N} row['CONTROL1']  in ${list_NULL}  ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BERM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BERM" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="berm and/or ditch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="GATE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GATE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="gated / physical barrier" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PRIV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRIV" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="private land" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="scarify and/or plant road" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIGN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIGN" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="signed" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SLSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SLSH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pile slash" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="WATX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="water crossing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polyline" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Existing Road Use Management Strategy (ERU)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="Plan" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="PlannedAggregateExtractionAreas:" FOLDED="true" POSITION="right">
                <icon BUILTIN="prepare"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="PlannedAggregateExtractionAreas" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AGAREAID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Aggregate Extraction Area ID" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="15" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AGAREAID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition:" FOLDED="true">
                            <node TEXT="The aggregate extraction area identifier attribute indicates the unique identifier for the area where forestry aggregate pits may be established." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Planned Aggregate Extraction Areas (PAG)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="Plan" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
                <node TEXT="mandatory" FOLDED="true">
                    <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node STYLE="fork" TEXT="PAG" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="RenewalAndTending:" FOLDED="true" POSITION="right">
                <icon BUILTIN="prepare"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="RenewalAndTending" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="PRT" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="TERM:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Plan Term" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TERM" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TERM" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition:" FOLDED="true">
                            <node TEXT="The plan term attribute indicates the five-year operational plan term or the next tenyear plan period in which the planned road activity and/or use strategy will occur. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory." FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="ST1: A zero or null value is not a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="1:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="1" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="First 5-year Operational Plan Term (1-5)" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="1-5" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="2:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="2" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Second 5-year Operational Plan Term (6-10)" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="6 -10" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="HERB:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Herbicide Application" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="HERB" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition:" FOLDED="true">
                            <node TEXT="The herbicide application attribute indicates the area as proposed for the aerial application of herbicide and identifies it as an area of special public interest. " FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The population of this attribute is mandatory." FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="INSECT:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Insecticide Application" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="INSECT" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition:" FOLDED="true">
                            <node TEXT="The insecticide application attribute indicates the area as proposed for the aerial application of insecticide, as a result of the application of the planning procedure for insect pest management programs, and identifies it as an area of special public interest." FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The population of this attribute is mandatory." FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="PB:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Prescribed Burn" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="PB" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition:" FOLDED="true">
                            <node TEXT="The prescribed burn attribute indicates the area as a candidate for a high complexity burn and identifies it as an area of special public interest. " FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The population of this attribute is mandatory." FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="PEST:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Insect Pest Management" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="PEST" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition:" FOLDED="true">
                            <node TEXT="The insect pest management attribute indicates the area as eligible for insect pest management and identifies it as an area of special public interest. " FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The population of this attribute is mandatory." FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="IMPROVE:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Tree Improvement Activities" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="IMPROVE" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition:" FOLDED="true">
                            <node TEXT="The tree improvement activities attribute indicates the area to support the production of improved seed. " FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The population of this attribute is mandatory." FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Renewal And Tending (PRT)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="Plan" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="PlannedClearcuts:" FOLDED="true" POSITION="right">
                <icon BUILTIN="go"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="PlannedClearcuts" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="PCCID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Clearcut ID" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="20" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="PCCID" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Planned Clearcuts (PCC)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="Plan" FOLDED="true"/>
                </node>
                <node TEXT="Definition" FOLDED="true">
                    <node TEXT=" " FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="PCC" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                </node>
            </node>
            <node STYLE="fork" TEXT="metadata:" FOLDED="true" POSITION="left">
                <node TEXT="submission_type" FOLDED="true">
                    <node TEXT="FMP_2009" FOLDED="true"/>
                </node>
                <node TEXT="submission_min_year" FOLDED="true">
                    <node TEXT="2009" FOLDED="true"/>
                </node>
                <node TEXT="submission_max_year" FOLDED="true">
                    <node TEXT="2019" FOLDED="true"/>
                </node>
                <node TEXT="id" FOLDED="true">
                    <node TEXT="fi_portal_id" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="{fmu}\{product}\{year}" FOLDED="true"/>
                </node>
                <node TEXT="DeferredLayerGDB" FOLDED="true">
                    <node TEXT="*.atx" FOLDED="true"/>
                    <node TEXT="*.spx" FOLDED="true"/>
                    <node TEXT="*.gdbindexes" FOLDED="true"/>
                    <node TEXT="*.gdbtablx" FOLDED="true"/>
                    <node TEXT="*.gdbtable" FOLDED="true"/>
                    <node TEXT="*.freelist" FOLDED="true"/>
                    <node STYLE="fork" TEXT="*/gdb" FOLDED="true"/>
                    <node TEXT="*/timestamps" FOLDED="true"/>
                </node>
                <node TEXT="DeferredLayerGPKG" FOLDED="true">
                    <node TEXT="*.gpkg" FOLDED="true"/>
                </node>
                <node TEXT="Document" FOLDED="true">
                    <node STYLE="fork" TEXT="*_TXT_PlanText.pdf" FOLDED="true"/>
                    <node TEXT="*_TBL_Tables.pdf" FOLDED="true"/>
                    <node TEXT="*_TXT_Sum.pdf" FOLDED="true"/>
                    <node TEXT="*_TXT_SumFR.pdf" FOLDED="true"/>
                    <node STYLE="fork" TEXT="*_TXT_SuppDoc.pdf" FOLDED="true"/>
                </node>
                <node TEXT="Document_Directory" FOLDED="true">
                    <node TEXT="" FOLDED="true"/>
                </node>
                <node TEXT="Internal_zipfile" FOLDED="true">
                    <node STYLE="fork" TEXT="*.zip" FOLDED="true"/>
                </node>
                <node TEXT="Layer" FOLDED="true">
                    <node TEXT="*.e00" FOLDED="true"/>
                    <node TEXT="*.shp" FOLDED="true"/>
                    <node TEXT="*.dbf" FOLDED="true"/>
                    <node TEXT="*.shx" FOLDED="true"/>
                    <node TEXT="*.prj" FOLDED="true"/>
                    <node TEXT="*.shp.xml" FOLDED="true"/>
                </node>
                <node TEXT="Model" FOLDED="true">
                    <node TEXT="*MODEL*" FOLDED="true"/>
                </node>
                <node TEXT="Maps" FOLDED="true">
                    <node TEXT="MU{fmu_id}_{planyear}_{product}_P{phase}_MAP*.eps" FOLDED="true"/>
                    <node TEXT="*.eps" FOLDED="true"/>
                </node>
                <node TEXT="Maps_Directory" FOLDED="true">
                    <node STYLE="fork" TEXT="Maps" FOLDED="true"/>
                </node>
                <node TEXT="Model_Directory_not_really..." FOLDED="true">
                    <node TEXT="MU{fmu_id}_{planyear}_{product}_P{phase}_MODEL" FOLDED="true"/>
                </node>
                <node TEXT="Catchall" FOLDED="true">
                    <node TEXT="" FOLDED="true"/>
                </node>
                <node TEXT="Catchall_Directory" FOLDED="true"/>
                <node TEXT="Trash" FOLDED="true">
                    <node TEXT="Thumbs.db" FOLDED="true"/>
                </node>
                <node TEXT="Layer_Filename" FOLDED="true">
                    <node TEXT="mu{fmu_id}_{year}fmpdp" FOLDED="true"/>
                </node>
                <node TEXT="DeferredLayerGPKG_Directory" FOLDED="true">
                    <node TEXT="LAYERS" FOLDED="true"/>
                </node>
                <node TEXT="DeferredLayerGDB_Directory" FOLDED="true">
                    <node TEXT="LAYERS/mu{fmu_id}_{year}_{product}.gdb" FOLDED="true"/>
                </node>
                <node TEXT="Internal_zipfile_Directory" FOLDED="true">
                    <node TEXT="" FOLDED="true"/>
                </node>
                <node TEXT="Model_Directory" FOLDED="true">
                    <node TEXT="" FOLDED="true"/>
                </node>
            </node>
        </node>
    </map>
