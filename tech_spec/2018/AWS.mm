    <map version="1.0.1">
        <node TEXT="AWS_2018">
            <node TEXT="Document:" FOLDED="true" POSITION="right">
                <node TEXT="title" FOLDED="true">
                    <node TEXT="Annual Work Schedule Technical Specification" FOLDED="true"/>
                </node>
                <node TEXT="Introduction" FOLDED="true">
                    <node TEXT="The Forest Information Manual (FIM) sets out the mandatory requirements, standards, roles and responsibilities, timelines and conditions for providing information in respect of Crown forests. The requirements for information set out in the FIM complement the planning and operational requirements of the Forest Management Planning Manual 2017 (FMPM). The FMPM and the Forest Information Manual (FIM) describe information that must be prepared and submitted for consultation, information that will be included in a forest management plan (FMP), and information that will be used by the Ministry of Natural Resources and Forestry (MNRF) to fulfill its obligations under the Crown Forest Sustainability Act (CFSA).  " FOLDED="true"/>
                    <node TEXT="The FIM provides a description of the information requirement, references the source of the requirement, describes the rationale for the requirement and, on a general level, and discusses the format of the information and the party responsible for providing the information. It is organized into four sections, and references five associated technical specifications:" FOLDED="true"/>
                    <node TEXT="&lt;ul&gt; &lt;li&gt;FIM Base and Values Technical Specifications; &lt;/li&gt; &lt;li&gt;FIM Forest Management Planning Technical Specifications (this document); &lt;/li&gt; &lt;li&gt;FIM Forest Resources Inventory Technical Specifications; &lt;/li&gt; &lt;li&gt;FIM Annual Work Schedule Technical Specifications; and &lt;/li&gt; &lt;li&gt;FIM Annual Reporting Technical Specifications. &lt;/li&gt; &lt;/ul&gt;" FOLDED="true"/>
                    <node TEXT="The Annual Work Schedule (AWS) Technical Specifications as identified in the Forest Information Manual (FIM) describes the standards (e.g. data attributes, format) for the information requirements, and the conditions for provisions (e.g. naming conventions, exchange parameters, validation standards) for the exchange of AWS information. Annual work schedules are prepared for a one-year period normally starting April 1 but always ending March 31. " FOLDED="true"/>
                    <node TEXT="This document describes the electronic exchange standards for the sustainable forest licensee  (Sustainable Forest Licence (SFL) Holders, Plan holders or other forest resource licence holders  with forest management responsibilities) and MNRF. " FOLDED="true"/>
                    <node TEXT="These specifications describe the data exchange standards only and do not affect how  information may be stored or maintained by either the sustainable forest licensee or MNRF.  Each party is expected to generate the required information products in the specified data  exchange format from their proprietary system.   " FOLDED="true"/>
                    <node TEXT="Technical specifications and any revisions are approved by the Director of the Crown Forests  and Lands Policy Branch. FIM sets out the process and parameters for periodic revision of the  technical specifications.   " FOLDED="true"/>
                    <node TEXT="A list of current FIM technical specifications and the scope of information to which they apply  will be maintained and available on the Forest Information Portal (FI Portal). The MNRF and  sustainable forest licensee are required to use the technical specifications listed on the FI  Portal." FOLDED="true"/>
                </node>
                <node TEXT="Roles and Responsibilities:" FOLDED="true">
                    <node TEXT="The roles and responsibilities as defined in the FIM and further in these technical specifications are the default. At the management unit level, roles and responsibilities may be adapted to best meet the circumstances of the unit and maintain the established relationships between the MNRF and the sustainable forest licensee." FOLDED="true"/>
                    <node TEXT="Sustainable Forest Licensee" FOLDED="true">
                        <node TEXT="The Sustainable Forest Licensee (referred to as Licensees in the remainder of this document) is responsible for production and submission of all components of the AWS submission file and AWS changes submission files. AWS changes include revisions, appended documents (i.e., prescribed burn plans, aerial herbicide or insecticide project plans), and changes to values. Submission is to be via the FI Portal. " FOLDED="true"/>
                        <node TEXT="If resubmission of a required information product is necessary, the sustainable forest licensee resubmits the entire information product, not just the changes. For example, if the submission is missing files, the resubmission would be not just the missing files, but all required files to make a complete submission." FOLDED="true"/>
                    </node>
                    <node TEXT="Ministry of Natural Resources and Forestry" FOLDED="true">
                        <node TEXT="The MNRF is responsible for providing the water crossing review results, based on the Fisheries Act review, to the Licensee for inclusion in table AWS-1 and AWS-2, Annual Schedule of Water Crossings to be constructed, replaced or decommissioned." FOLDED="true"/>
                        <node TEXT="The MNRF will verify that all information products submitted by the Licensee meet the standards defined in this FIM AWS Technical Specifications and are complete. When an information product is determined to be unacceptable, MNRF will provide the Licensee with a list of required alterations. " FOLDED="true"/>
                    </node>
                </node>
                <node TEXT="Implementation:" FOLDED="true">
                    <icon BUILTIN="help"/>
                    <node TEXT="These FIM AWS Technical Specifications are in effect upon regulation of the FIM 2017. These technical specifications apply until this document is replaced. They apply to scheduled operations beginning April 1, 2018. " FOLDED="true"/>
                    <node TEXT="Revision Notes" FOLDED="true">
                        <node TEXT="Revisions to the FIM AWS Technical Specifications include: 2018 revisions &lt;ul&gt; &lt;li&gt;Clarifications and corrections to validation logic statements; &lt;ul&gt; &lt;li&gt;General formatting, clarification, organizational and typographical corrections;  2017 revisions &lt;/li&gt;&lt;li&gt;General formatting, clarification, organizational and typographical corrections;&lt;/li&gt;&lt;li&gt;Alignment to policy changes resulting from Declaration Order (MNR-75) and FMPM and  FIM;  &lt;/li&gt;&lt;li&gt;Requirement to provide full resubmission of the harvest, road corridor, operational road  boundary, area of concern and aggregate extraction areas as geospatial data layers  which include amendments to an FMP;  &lt;/li&gt;&lt;li&gt;Changes to required content on AWS Operations Maps;  &lt;/li&gt;&lt;li&gt;New Establishment geospatial data layer submitted as AWS change information;  &lt;/li&gt;&lt;li&gt;Acceptance of additional ESRI support file formats; and &lt;/li&gt;&lt;li&gt;Improvements to attribute coding (stage of development, access, water crossing type,  decommissioning). &lt;/li&gt; &lt;/ul&gt;" FOLDED="true"/>
                    </node>
                </node>
                <node TEXT="Product Descriptions:" FOLDED="true">
                    <icon BUILTIN="help"/>
                    <node TEXT="Water Crossing Review Results:" FOLDED="true">
                        <node TEXT="Description, Intent and Intended Use" FOLDED="true">
                            <node TEXT="Where an MNRF review is required for the location and conditions of construction for water crossings identified in table AWS-1, Annual Schedule of Water Crossings to be constructed or replaced. This review will follow the direction provided in the Ministry of Natural Resources and Forestry/Fisheries and Oceans Canada Protocol for the Review and Approval of Forestry Water Crossings, meeting the FMPM, Part D, Section 3.2.5.1 requirement for a Fisheries Act review of planned water crossing construction or replacement. MNRF's review results are a requirement of table AWS-1 in the year the water crossing is scheduled for construction or replacement.  " FOLDED="true"/>
                            <node TEXT="None" FOLDED="true"/>
                        </node>
                        <node TEXT="Packaging and Naming Convention" FOLDED="true">
                            <node TEXT="There is no standard packaging and/or naming convention for this product. MNRF Districts and Licensees will exchange this product in a manner that best suits their processes and local situation. " FOLDED="true"/>
                        </node>
                        <node TEXT="Metadata" FOLDED="true">
                            <node TEXT="There is no specific metadata requirement as this is not a stand-alone information product. This information will be incorporated into the AWS submission file and the metadata for that product will apply. " FOLDED="true"/>
                        </node>
                        <node TEXT="Format" FOLDED="true">
                            <node TEXT="There is no standard format for this product. MNRF Districts and Licensees will exchange this product in a format that best suits their processes and local situation.  " FOLDED="true"/>
                        </node>
                        <node TEXT="Data Transfer and Schedule" FOLDED="true">
                            <node TEXT="MNRF will provide water crossing review results, for those crossings identified in the current AWS as being planned to be constructed, replaced or decommissioned. It is recommended that the FI Portal be used to facilitate this exchange of information. " FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="Scheduled Operations Spatial Information Specifications:" FOLDED="true">
                        <node TEXT="Description, Intent and Intended Use " FOLDED="true">
                            <node TEXT="The scheduled operations information is a set of geospatial data layers which identify and  provide information on areas specific to the AWS operating year on: &lt;ul&gt;&lt;li&gt;Harvest;&lt;/li&gt;&lt;li&gt;Areas of concern (AOCs);&lt;/li&gt;&lt;li&gt;Residual patches;&lt;/li&gt;&lt;li&gt;Road corridors;&lt;/li&gt;&lt;li&gt;Operational road boundaries;&lt;/li&gt;&lt;li&gt;Existing roads;&lt;/li&gt;&lt;li&gt;Water crossings;&lt;/li&gt;&lt;li&gt;Aggregate extraction areas;&lt;/li&gt;&lt;li&gt;Site preparation treatments;&lt;/li&gt;&lt;li&gt;Regeneration treatments;&lt;/li&gt;&lt;li&gt;Tending treatments;&lt;/li&gt;&lt;li&gt;Protection treatments;&lt;/li&gt;&lt;li&gt;Existing forestry aggregate pits; and&lt;/li&gt;&lt;li&gt;Establishment Assessments. &lt;/li&gt;&lt;/ul&gt; " FOLDED="true"/>
                            <node TEXT="These products will be used to facilitate the MNRF review of the AWS and aid MNRF staff in the  performance of their duties throughout the year. This product will also be used to aid in the  identification of persons who may be directly affected by forest management operations during  the year of the AWS, and in particular those persons who have requested notice of specific  activities that will occur in specific areas (e.g., trappers, mining claim holders). " FOLDED="true"/>
                            <node TEXT="The details of each of these geospatial information products are described in the individual  product sections starting with Section 4.2.7. " FOLDED="true"/>
                            <node TEXT="Additional non-standard geospatial information products may be included in the AWS submission." FOLDED="true"/>
                        </node>
                        <node TEXT="Packaging and Naming Convention" FOLDED="true">
                            <node TEXT="The scheduled operations geospatial information will be included in the submission zip file  according to Section 5.0." FOLDED="true"/>
                            <node TEXT="Naming conventions for the individual AWS geospatial information products is discussed in the  individual product sections." FOLDED="true"/>
                            <node TEXT="Additional non-standard geospatial information products should follow a similar naming  convention and must only contain numeric values from 0 to 9, characters from A to Z and  underscores." FOLDED="true"/>
                            <node TEXT="File extensions are defined by the ESRI supported file exchange format chosen. Examples of  ESRI supported file formats accepted by the FI Portal are: " FOLDED="true"/>
                            <node TEXT="&lt;ol&gt; &lt;li&gt;Shapefiles: the shapefile consists of 4 mandatory file extensions (.shp, .shx., .dbf, .prj). For example:   &lt;ul&gt;  &lt;li&gt;MU123_28SHR00.shp&lt;/li&gt;  &lt;li&gt;MU123_28SHR00.shx&lt;/li&gt;  &lt;li&gt;MU123_28SHR00.dbf&lt;/li&gt;  &lt;li&gt;MU123_28SHR00.prj&lt;/li&gt; &lt;/ul&gt; &lt;/li&gt; &lt;li&gt;File Geodatabase (FGDB): is a container that can hold single or multiple feature classes.  All feature classes must be in the root of the FGDB. For example: &lt;ul&gt;&lt;li&gt;MU123_28SHR.gdb (single feature class in a FGDB)  &lt;ul&gt;  &lt;li&gt;MU123_28SHR00&lt;/li&gt;  &lt;/ul&gt;         &lt;/li&gt;         &lt;li&gt;MU123_AWS.gdb (mulitple feature classes in FGDB)    &lt;ul&gt;  &lt;li&gt;MU123_28SHR00&lt;/li&gt;  &lt;li&gt;MU12328AOC000&lt;/li&gt;&lt;/ul&gt;         &lt;/li&gt; &lt;/li&gt;&lt;/ul&gt; &lt;li&gt;ESRI ArcInfo interchange file (E00) is a proprietary ESRI file format intended to support  the transfer between ESRI systems of different types of geospatial data used in ESRI  software.   For example:  &lt;ul&gt;  &lt;li&gt;MU123_28SHR00.E00  &lt;/li&gt;  &lt;/ul&gt;  &lt;ul&gt;  &lt;li&gt;MU123_28SHR01.E00 (first multiple layer submitted)&lt;/li&gt;  &lt;li&gt;MU123_28SHR02.E00 (second multiple layer submitted) &lt;/li&gt;  &lt;/ul&gt; &lt;/li&gt; &lt;/ol&gt; " FOLDED="true"/>
                        </node>
                        <node TEXT="Metadata" FOLDED="true">
                            <node TEXT="Metadata requirements include the use of standard naming conventions and submission details that are collected when AWS files are submitted to the FI Portal.  " FOLDED="true"/>
                        </node>
                        <node TEXT="Format" FOLDED="true">
                            <node TEXT="Geospatial information and associated tabular attributes are to be submitted in an ESRI  supported file format. This format will be consistent with the formats defined by the FI  Portal. A single ESRI supported file format will be used within the submission." FOLDED="true"/>
                            <node TEXT="Each geospatial data layer must contain a defined projection. The selected projection is  to be used for all spatial products associated with an AWS." FOLDED="true"/>
                            <node TEXT="Information managed in the UTM projection, where management units span more than  one UTM zone, must be projected to a single UTM zone." FOLDED="true"/>
                            <node TEXT="Information is to be provided in a projection recognized by a well-known spatial  reference system standards body. Typical projection choices will be EPSG: 26915 - EPSG: 26918 (UTM Zones 15-18, NAD83 Datum), or EPSG: 3161 (NAD83 / Ontario MNR Lambert)." FOLDED="true"/>
                            <node TEXT="Geospatial information will be submitted in a seamless format or as a map-joined  product with or without the tile lines removed (dissolved).  " FOLDED="true"/>
                            <node TEXT="Additional attributes can be appended to the tabular file. The inclusion of additional  attributes in the individual layers is a decision of the appropriate task team. It is  recommended that a brief metadata be provided to describe the additional attribution.  " FOLDED="true"/>
                            <node TEXT="Format requirements specific to each product are discussed in the individual product  sections.  " FOLDED="true"/>
                            <node TEXT="Geospatial data layers will respect spatial integrity." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation" FOLDED="true">
                            <node TEXT="Stage 1 validation routines assess AWS product submission files for meeting mandatory  requirements. The process will assess the entire submission file to identify as many non- compliance instances as possible. These instances will be provided in a Stage 1 report. A non- compliance will result in a required alteration and resubmission." FOLDED="true">
                                <icon BUILTIN="full-1"/>
                            </node>
                            <node TEXT="Stage 2 validation routines assess AWS product submission files for anomalies and uncommon  data relationships. These warnings will be provided in a Stage 2 report. Anomalies identified at  Stage 2 do not result in an automatic rejection or required alterations of the submission. The  MNRF will use the Stage 2 validation report to inform additional manual validation or a  discussion if required. " FOLDED="true">
                                <icon BUILTIN="full-2"/>
                            </node>
                        </node>
                        <node TEXT="Data Transfer and Schedule" FOLDED="true">
                            <node TEXT="The scheduled operations geospatial information is a required component of the AWS  submission file and is subject to those timelines. Refer to Section 5.5 for more information. " FOLDED="true"/>
                        </node>
                        <node TEXT="Review and Approval" FOLDED="true">
                            <node TEXT="Review and approval of the scheduled operations information is performed as part of AWS  review. Refer to Section 5.6 for more information. " FOLDED="true"/>
                        </node>
                        <node TEXT="SHR" FOLDED="true"/>
                        <node TEXT="SAC" FOLDED="true"/>
                        <node TEXT="SRP" FOLDED="true"/>
                        <node TEXT="SRC" FOLDED="true"/>
                        <node TEXT="SOR" FOLDED="true"/>
                        <node TEXT="SRA" FOLDED="true"/>
                        <node TEXT="SWC" FOLDED="true"/>
                        <node TEXT="SAG" FOLDED="true"/>
                        <node TEXT="SSP" FOLDED="true"/>
                        <node TEXT="SRG" FOLDED="true"/>
                        <node TEXT="STT" FOLDED="true"/>
                        <node TEXT="SPT" FOLDED="true"/>
                        <node TEXT="AGP" FOLDED="true"/>
                        <node TEXT="SEA" FOLDED="true"/>
                        <node TEXT="SHR" FOLDED="true"/>
                    </node>
                    <node TEXT="Map Specifications:" FOLDED="true">
                        <icon BUILTIN="help"/>
                        <node TEXT="Description, Intent and Intended Use" FOLDED="true">
                            <node TEXT="Maps are required for portraying information about operations that were previously planned  and approved in the FMP, and are scheduled for implementation during the fiscal year, as well  as information on operations that are planned and approved in the AWS (e.g., water crossings).  All maps, except for the public notice maps, will be submitted as described in Section 5.0,  Submission File. " FOLDED="true"/>
                            <node TEXT="It is not a requirement to produce French language versions of all maps for areas designated  under the French Language Services Act. A French language version of the public notice map is  required for areas designated under the French Language Services Act. A French language  version of the summary map is required for all areas." FOLDED="true"/>
                            <node TEXT="Information about when each map is required, and for what purpose, is provided in the  detailed map description sections (Sections 4.3.5 - 4.3.8). " FOLDED="true"/>
                        </node>
                        <node TEXT="Packaging and Naming Convention" FOLDED="true">
                            <node TEXT="Maps that are a required component of an AWS submission file will use a standard naming  convention. A standard naming convention must be used to permit an automated validation of  the information product. Standardized naming of files also facilitates internet viewing, file  retention and data discovery. The file name is composed of the following parts:  " FOLDED="true"/>
                            <node TEXT="MU&lt;management unit&gt;_&lt;year&gt;_AWS_MAP_&lt;description&gt;_&lt;file number&gt;.pdf , where:  " FOLDED="true"/>
                            <node TEXT="Sample naming conventions for the individual map file components are provided in the detailed  map descriptions below. For maps that are not a required component of an AWS submission file, a standard name has  not been provided. " FOLDED="true"/>
                        </node>
                        <node TEXT="Metadata" FOLDED="true">
                            <node TEXT="Metadata requirements for map products are met by the required information contained in the  map surround, use of a standard naming convention, as well as the submission details that are  captured when AWS submission files are submitted via the FI Portal." FOLDED="true"/>
                        </node>
                        <node TEXT="Format:" FOLDED="true">
                            <node TEXT="Maps that are a required component of an AWS submission file will be produced in an Adobe  portable document format (PDF) that does not exceed 100 MB in file size with the fonts and  symbols successfully imbedded." FOLDED="true"/>
                            <node TEXT="Note: Some problems have been encountered when generating PDF files, ensure that the ESRI  fonts and symbols have been imbedded properly by viewing the file on a computer that does  not have the font file installed. " FOLDED="true"/>
                            <node TEXT="Map Scale Standards:" FOLDED="true">
                                <node TEXT="Each map produced for inclusion in the AWS must be prepared according to one of three map  scale types." FOLDED="true"/>
                                <node TEXT="Operational Map Scale" FOLDED="true">
                                    <node TEXT="Acceptable operational map scales range from 1:10,000 to 1:50,000. Operational scale maps are also referred to as large scale maps." FOLDED="true"/>
                                </node>
                                <node TEXT="Composite Map Scale" FOLDED="true">
                                    <node TEXT="Acceptable composite map scales range from 1:50,000 to 1:250,000. The composite  scale chosen must allow for easy, clear interpretation of map themes and ease of  reproduction. The scale chosen for these small scale maps should be one that minimizes  the number of maps required to display the entire management unit.  " FOLDED="true"/>
                                </node>
                                <node TEXT="Summary Map Scale" FOLDED="true">
                                    <node TEXT='Acceptable summary map scales generally allow for portrayal of the target area on an 11"x 17"  or smaller sheet of paper and allow for the appropriate resolution of information and ease of  reproduction. These very small scale maps are designed and created for public distribution. ' FOLDED="true"/>
                                    <node TEXT="The detailed map description sections (Sections 4.3.5 - 4.3.8) identify a required map scale type  for each map as operational, composite, or summary. The scales chosen from the operational  and composite scale ranges for the FMP maps are also to be used for the AWS. If the plan  author feels it appropriate to produce some maps at scales other than the one chosen for the  FMP, these maps will be in addition to the maps produced at the chosen scale. Use of a  consistent scale for the summary map(s) is not required. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="Map Surround Standards:" FOLDED="true">
                                <node TEXT="All maps will have a similar map surround. Where particular features of these map surround  standards do not apply to a map, it will be noted in the detailed map descriptions. " FOLDED="true"/>
                                <node TEXT="Additional  guidance can be obtained from the Map Design Considerations for Accessibility.   " FOLDED="true"/>
                                <node TEXT="Map surround components are as follows" FOLDED="true"/>
                                <node TEXT="table:" FOLDED="true">
                                    <node TEXT="name" FOLDED="true">
                                        <node TEXT="Component" FOLDED="true"/>
                                    </node>
                                    <node TEXT="desctext" FOLDED="true">
                                        <node TEXT="Description" FOLDED="true"/>
                                    </node>
                                    <node TEXT="Logo:" FOLDED="true">
                                        <node TEXT="desctext" FOLDED="true">
                                            <node TEXT="Ontario Government logo or forest company logo (or combination) as appropriate." FOLDED="true"/>
                                        </node>
                                    </node>
                                    <node TEXT="Title Block:" FOLDED="true">
                                        <node TEXT="desctext" FOLDED="true">
                                            <node TEXT=" includes the management unit name, the term of the FMP, and the map name. For  operational maps, the mapsheet identifier must also be included. The naming standard for the  map is indicated in the detailed map descriptions. " FOLDED="true"/>
                                        </node>
                                    </node>
                                    <node TEXT="Index Map:" FOLDED="true">
                                        <node TEXT="desctext" FOLDED="true">
                                            <node TEXT=" indicates the extent of the area shown on the map in relation to a larger area.  Composite maps will show their extent in relation to the rest of Ontario. Operational scale  maps (1:20,000, 1:10,000) will show their extent in relation to the management unit. " FOLDED="true"/>
                                        </node>
                                    </node>
                                    <node TEXT="Legend:" FOLDED="true">
                                        <node TEXT="desctext" FOLDED="true">
                                            <node TEXT="provides a list of map symbols used for theme and base features" FOLDED="true"/>
                                        </node>
                                    </node>
                                    <node TEXT="Disclaimer:" FOLDED="true">
                                        <node TEXT="desctext" FOLDED="true">
                                            <node TEXT="required for safeguarding against liability on the part of the MNRF or the forest  industry companies. A disclaimer is of particular importance with the take-home summary  maps.    " FOLDED="true"/>
                                            <node TEXT="Example:   " FOLDED="true"/>
                                            <node TEXT="This map should not be relied on as a precise indicator of routes or locations, nor as a  guide to navigation. The Ontario Ministry of Natural Resources and Forestry shall not be  liable in any way for the use of, or reliance upon, this map or any information on this  map. " FOLDED="true"/>
                                        </node>
                                    </node>
                                    <node TEXT="Scale Bar or Statement:" FOLDED="true">
                                        <node TEXT="desctext" FOLDED="true">
                                            <node TEXT="provides the relationship between map distance and true  (ground) distance. Both a scale bar and text scale statement are required. " FOLDED="true"/>
                                        </node>
                                    </node>
                                    <node TEXT="Map Publication Date:" FOLDED="true">
                                        <node TEXT="desctext" FOLDED="true">
                                            <node TEXT=" indicates the date the map was created. The date will display the  month in text and the year in four digits. " FOLDED="true"/>
                                        </node>
                                    </node>
                                    <node TEXT="Datum:" FOLDED="true">
                                        <node TEXT="desctext" FOLDED="true">
                                            <node TEXT=" identifies the projection and datum of the map information " FOLDED="true"/>
                                        </node>
                                    </node>
                                    <node TEXT="Copyright" FOLDED="true">
                                        <node TEXT="desctext" FOLDED="true">
                                            <node TEXT=" indicates who maintains ownership of the data/information or a contact name for  more information on copyright applicable to the map data. " FOLDED="true"/>
                                        </node>
                                    </node>
                                    <node TEXT="North Arrow:" FOLDED="true">
                                        <node TEXT="desctext" FOLDED="true">
                                            <node TEXT="grid north direction indicator. This information is not required if map is oriented  with north to the top of page. " FOLDED="true"/>
                                        </node>
                                    </node>
                                    <node TEXT="Border:" FOLDED="true">
                                        <node TEXT="desctext" FOLDED="true">
                                            <node TEXT="map frame" FOLDED="true"/>
                                        </node>
                                    </node>
                                </node>
                            </node>
                            <node TEXT="Symbology" FOLDED="true"/>
                            <node TEXT="Sensitive and Confidential Information" FOLDED="true"/>
                            <node TEXT="Page Size Standards" FOLDED="true"/>
                        </node>
                        <node TEXT="Public Notice Map" FOLDED="true"/>
                        <node TEXT="Annual Work Schedule Index Map" FOLDED="true"/>
                        <node TEXT="Annual Work Schedule Operations Maps" FOLDED="true"/>
                        <node TEXT="AWS Summary Map" FOLDED="true"/>
                    </node>
                    <node TEXT="AWS Text and Tables:" FOLDED="true"/>
                </node>
                <node TEXT="Submission Files" FOLDED="true"/>
                <node TEXT="Appendix 1 Data Identification Form" FOLDED="true"/>
            </node>
            <node TEXT="ScheduledHarvest:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledHarvest" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SHR" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Harvest (SHR)" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AWS_YR:" FOLDED="true">
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AWS_YR" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AWS fiscal year" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="None" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value indicates an area not scheduled" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="There cannot be any values less than the FMP plan period start year (except for 0) or greater than the plan period end year minus 1." FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be greater than or equal to ${planyear} and less than or equal to ${planyear} + 10' ${:} ${NINN} and row['${name}'] not in [ '0' ] if ( int(row['${name}']) &lt; int(${planyear})) or (int(row['${name}']) &gt;= (int(${planyear}+10) ) )   ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':0}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="{'${name}':2050}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be greater than or equal to ${planyear} and less than or equal to ${planyear} + 10" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="BLOCKID:" FOLDED="true">
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="BLOCKID" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="25" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Block ID" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The harvest block identifier attribute is a unique user defined label associated with polygons scheduled for harvest that are in proximity of each other for practical implementation of operations. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory where plan start is greater than or equal to 2019" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not valid when plan start is greater than or equal to 2019" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null if plan start year is greater than or equal to 2019' ${:} ${NIN} and ${planyear} &gt; 2018 ]" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null if plan start year is greater than or equal to 2019" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="SILVSYS:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Silvicultural System" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SILVSYS" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="SILVSYS" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silviculture system attribute indicates the process by which a productive forest stand will be managed for timber production purposes. The process/system is classified according to the method of harvesting that will be used. " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                <icon BUILTIN="full-1"/>
                            </node>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                <icon BUILTIN="full-2"/>
                            </node>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true">
                                <icon BUILTIN="full-3"/>
                            </node>
                            <node TEXT="The population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true">
                                <icon BUILTIN="full-4"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: SILVSYS can only be null when FUELWOOD = Y " FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null unless FUELWOOD equals Y' ${:} ${NIN} and str(row['AWS_YR']) == str(${year}) and row['AWS_YR'] not in ${list_NULL}  and row['FUELWOOD'] &lt;&gt; 'Y'  ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'FUELWOOD':'N' , 'AWS_YR':${year} }" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null unless FUELWOOD equals Y" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="CC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CC" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="clearcut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A system of regenerating an even-aged forest stand in which new seedlings become established in fully exposed micro-environments after most or all of the existing trees have been removed. Regeneration is artificial or natural." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="selection" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An uneven aged system where mature and/or undesirable trees are removed individually or in small groups over the whole area. Regeneration is generally natural." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="shelterwood" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An even-aged system where mature trees are harvested in a series of two or more cuts for the purpose of obtaining regeneration under the shelter of residual trees, whether by cutting uniformly over the entire stand area or in narrow strips or groups. Regeneration is natural or artificial." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="HARVCAT:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Harvest Category" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="HARVCAT" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="HARVCAT" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The harvest category attribute indicates the planned type of harvest that is being scheduled." FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                <icon BUILTIN="full-1"/>
                            </node>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true">
                                <icon BUILTIN="full-2"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="HARVCAT can only be null when FUELWOOD = Y" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: Bridging (HARVCAT = BRIDGING) is only available when the AWS start year is equal to the first year of the 10 year plan period." FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: If the harvest category is second pass (HARVCAT = SCNDPASS), then the silvicultural system must be clearcut (SILVSYS = CC)" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null unless FUELWOOD equals Y' ${:} ${NIN} and str(row['AWS_YR']) == str(${year})  and row['FUELWOOD'] != 'Y' ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be bridging unless AWS_YR = First Plan Year' ${:N} str(row['AWS_YR']) == str(${year})  and row['${name}'] == 'BRIDGING' and ${year} != ${planyear}   ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST1: If &lt;mark&gt;${name}&lt;/mark&gt; is second pass (SCNDPASS) then SILVSYS must be clearcut (CC)' ${:} (row['${name}'] == 'SCNDPASS') and row['SILVSYS'] != 'CC' ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'', 'FUELWOOD':'N', 'AWS_YR':${year}}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null unless FUELWOOD equals Y" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'BRIDGING', 'AWS_YR':${planyear} }" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be bridging unless AWS_YR = First Plan Year" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'SCNDPASS', 'SILVSYS':'SH', 'AWS_YR':${year} }" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST1: If &lt;mark&gt;${name}&lt;/mark&gt; is second pass (SCNDPASS) then SILVSYS must be clearcut (CC)" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="REGULAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REGULAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="regular harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Harvest areas categorized as regular under the FMP." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="BRIDGING:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BRIDGING" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="bridging harvest areas" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Areas which were scheduled for harvest in the current FMP, but were not harvested. The bridging operations are limited to three months harvest area from the current approved FMP and harvest of bridging area must be completed by March 31 of the first year of the FMP." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="CONTNGNT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CONTNGNT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="contingency harvest area" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The area set aside to accommodate unforeseeable circumstances (e.g., wildfire). Contingency area will serve as replacement for harvest area, and only be used if needed. The area must be sufficient to provide for a minimum of one year and a maximum of two years of harvest operations." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="REDIRECT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REDIRECT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="redirected harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Areas to be harvested under an insect pest management plan and count against the available harvest area of the FMP." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ACCELER:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ACCELER" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="accelerated harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Areas to be harvested under an insect pest management plan and are areas in addition to the available harvest area of the FMP." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FRSTPASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FRSTPASS" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="modified cut: first pass" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="For areas managed using the clearcut silvicultural system, harvest may be planned in two passes. This is normally when species within the stand are harvested and utilized by different logger/contractor/forest resource licensee in different years (e.g., first pass is conifer and second pass is hardwood). The first pass should be recorded if merchantable tree species will remain in the forest stands which have been allocated for harvest, but not yet harvested." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SCNDPASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCNDPASS" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Second-pass harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="For areas managed using the clearcut silvicultural system, harvest may be planned in two passes. This is normally when species within the stand are harvested and utilized by different logger/contractor/licensee in different years (e.g., first pass is conifer and second pass is hardwood). Second pass harvest should be identified when merchantable tree species will be harvested from forest stands which have been previously reported as harvested." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SALVAGE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SALVAGE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="salvage harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The salvage harvest is the areas where the recovery or harvesting of timber that has been killed or damaged by natural causes (such as fire, wind, flood, insects, and disease) are planned. The salvage area, as defined in the FMPM, does not contribute to the available harvest area." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="FUELWOOD:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="fuelwood area" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="FUELWOOD" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The fuelwood area attribute identifies areas where non-commercial fuelwood can be obtained by the public for their personal use during the AWS operating year." FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                <icon BUILTIN="full-1"/>
                            </node>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true">
                                <icon BUILTIN="full-2"/>
                            </node>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                <icon BUILTIN="full-3"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: If the FUELWOOD attribute is Y then the SILVSYS and HARVCAT attributes can be null" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <icon BUILTIN="password"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true">
                        <node TEXT="[ 'Field %s not found in table'%(f)  for f in [ 'AWS_YR','BLOCKID','SILVSYS','HARVCAT','FUELWOOD' ] if f not in fields ] " FOLDED="true"/>
                    </node>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
                <node TEXT="Description" FOLDED="true">
                    <node TEXT="The scheduled harvest layer identifies areas scheduled for harvest operations during the year. In order to provide flexibility for unforeseen circumstances, up to two years of the average annual available harvest area by forest unit may be identified. Areas will be identified by  silvicultural system, harvest category, and non-commercial fuelwood availability. " FOLDED="true"/>
                    <node TEXT="Licensees must include all harvest areas in the approved FMP including all approved amendments received prior to December 1st (e.g. December 1, 2019 for the 2020-2021 AWS). An attribute identifying the AWS fiscal year will distinguish the areas scheduled in the applicable AWS.   " FOLDED="true"/>
                    <node TEXT="For FMPs where the planned harvest layer was created under the 2009 FMPM there is no requirement for a block identifier to be populated. " FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ScheduledAreaofConcern:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledAreaofConcern" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Area of Concern (SAC)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SAC" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AOCID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AOC Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="15" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AOCID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The AOC identifier attribute is the label assigned to a specific AOC prescription which must correspond to the label on FMP and AWS Areas Selected for Operations maps and the area of concern prescriptions in table FMP-11. The prescription can represent either a group of areas of concern with a common prescription or an individual area of concern with a unique prescription." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT=" ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT=" ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT=" ST1: The value must be a code from table FMP-10 and the supplementary documentation" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="AOCTYPE:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AOCTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AOC Type" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="AOCTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The AOC type attribute indicates the type of AOC prescription as either modified or reserved." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="M:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="M" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="modified" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Areas which are scheduled for operations but have specific conditions or restrictions on operations as required by an AOC prescription." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="R:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="R" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="reserved" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Areas which are reserved (prohibited) from operations as required by an AOC prescription." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
                <node TEXT="Description" FOLDED="true">
                    <node TEXT="The areas of concern (AOC) in scheduled operations layer is submitted as one or more geospatial data layers. Examples of multiple layers may include but are not limited to: Individual layers based on the area of concern identification (e.g. eagle nest, fisheries values, etc.) Individual layers based on the area of concern type (reserve or modified) " FOLDED="true"/>
                    <node TEXT="This layer(s) includes AOCs associated with scheduled areas of operations (harvest, renewal and maintenance, road construction, water crossings, existing roads planned to be used for forest management purposes, aggregate pits, and aggregate extraction areas). Licensees must include all AOCs in the approved FMP including all approved amendments received prior to December 1st (e.g. December 1, 2019 for the 2020-2021 AWS). Any changes to AOCs in scheduled areas of operations, as a result of changes to values will also be reflected in the layer(s).    " FOLDED="true"/>
                    <node TEXT="Areas of concern for renewal and maintenance activities are normally only required for modified operations or where a value may be impacted by renewal and maintenance activities (e.g., timing restrictions, herbicide application restrictions or site disturbance restrictions). " FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ScheduledResidualPatches:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledResidualPatches" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Residual Patches (SRP)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SRP" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="RESID:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="10" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="RESID" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Residual Patch Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The residual patch identifier attribute is a number, label or name assigned to a residual patch(es) as defined in the FMP or AWS text. " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true" VSHIFT="3">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true">
                        <node TEXT="[ 'Field %s not found in table'%(f)  for f in [ 'RESID' ] if f not in fields ] " FOLDED="true"/>
                    </node>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
                <node TEXT="Description" FOLDED="true">
                    <node TEXT="TThe Scheduled Residual Patches layer will identify areas within the planned harvest that are not part of the allowable harvest area. The text of the FMP will describe the conditions applied to the residual areas. This layer is required if stand level residual requirements were identified in the FMP to be addressed during the implementation of operations. " FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ScheduledRoadCorridors:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledRoadCorridors" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Road Corridors (SRC)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SRC" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AWS_YR:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AWS fiscal year" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AWS_YR" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The AWS fiscal year attribute identifies the fiscal year to which the AWS applies. Only those areas scheduled in the AWS require this field to be populated. " FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value indicates an area not scheduled" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="There cannot be any values less than the FMP plan period start year (except for 0) or greater than the plan period end year minus 1." FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be greater than or equal to ${planyear} and less than or equal to ${planyear} + 10' ${:} ${NINN} and row['${name}'] not in [ '0' ] if ( int(row['${name}']) &lt; int(${planyear})) or (int(row['${name}']) &gt;= (int(${planyear}+10) ) )   ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':1000}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be greater than or equal to ${planyear} and less than or equal to ${planyear} + 10" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':1000}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;AWS_YR&lt;/mark&gt; must be greater than or equal to ${planyear} and less than or equal to ${planyear} + 10" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':0}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ROADID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ROADID" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="30" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road identifier attribute is the unique number, label or name assigned to the forest access road that this polygon is a part of." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory." FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="ROADCLAS:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ROADCLAS" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Class" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="ROADCLAS" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road class attribute identifies the class of the proposed forest access road or road network, in terms of the road use management strategy in the FMP." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory." FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="B:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="B" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="branch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="O:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="O" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Operational" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="P:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="P" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Primary" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRANS:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRANS" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road transfer year" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road transfer year attribute indicates a four-digit number representing the first year of the 10 year planning period that the transfer of responsibility to the MNRF is anticipated to take effect. If there is no intent to transfer responsibility to MNRF during the future 20-year plan period there is no need to specify a year. The presence of this field is to facilitate the update of the spatial operational FMP layers as a result of approved amendments. " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory. (conflicts with the next validation line)" FOLDED="true">
                                    <icon BUILTIN="button_cancel"/>
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A zero or null value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: If road transfer year does not equal zero (TRANS not equal to zero) then INTENT must be populated (This is being checked at INTENT field validation)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <icon BUILTIN="idea"/>
                                </node>
                                <node TEXT="ST1: The value must be greater than or equal to the 10 year plan period start year" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be greater or equal to FMP start year, if populated.'${:N}  row['${name}'] &lt; ${planyear} ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="{'${name}':1000}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be greater or equal to FMP start year, if populated." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="ACYEAR:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ACYEAR" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Access control year" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The access control year attribute indicates a four-digit number representing the expected fiscal year (April 1 to March 31) that the access control is anticipated to take effect. The presence of this field is to facilitate the update of the spatial operational FMP layers as a result of approved amendments." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory. (conflicts with the next validation line)" FOLDED="true">
                                    <icon BUILTIN="button_cancel"/>
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A zero or null value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The value must be greater than or equal to the plan period start year and less than or equal to the plan end year" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: If access control year does not equal zero (ACYEAR does not equal 0) then access control must not be null. " FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <icon BUILTIN="idea"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be greater or equal to FMP start year and must be less than or equal to plan end year, if populated' ${:} ${NINN} if  int(row['${name}']) not in ( range(${planyear}, ${planyear} + 11) + [0, None] ) ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="[ 'ST1: When &lt;mark&gt;${name}&lt;/mark&gt; is populated, ACCESS must also be populated.' ${:N}   row['ACCESS'] in ${list_NULL} ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':1000}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be greater or equal to FMP start year and must be less than or equal to plan end year, if populated" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'ACYEAR':2020 }" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: When &lt;mark&gt;${name}&lt;/mark&gt; is populated, ACCESS must also be populated." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="banana" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ACCESS:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Access Control" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="9" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true"/>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ACCESS" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The access control attribute identifies where new access control activities are scheduled to be implemented during the year on primary or branch roads that will be constructed during the year. This attribute is to be used when scheduled activities will restrict road use for the purposes other than meeting the conditions required for transferring responsibility for the road to the Crown." FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory." FOLDED="true">
                                <icon BUILTIN="full-1"/>
                                <icon BUILTIN="list"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: where AWS_YR = fiscal year to which the AWS applies:" FOLDED="true"/>
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory." FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: When the road access control status is apply or both (ACCES = APPLY or BOTH) then the control type must be a code other than null (CONTROL1 is not null). (This is being checked at CONTROL1.)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <icon BUILTIN="idea"/>
                                </node>
                                <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM is not NULL or MAINTAIN = Y or MONITOR = Y or [ACCESS = APPLY or ACCESS = REMOVE or ACCESS = BOTH]) when AWS_YR is the current year." FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST2: When the road access control status is remove (ACCESS = REMOVE) then the control type should be null (CONTROL1 = null and CONTROL2 = null) (This is being checked at CONTROL1 and 2)" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <icon BUILTIN="idea"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record.' ${:} str(row['AWS_YR']) == str(${year}) if row['DECOM'] in ${list_NULL} and row['MAINTAIN'] != 'Y' and row['MONITOR'] != 'Y' and row['${name}'] in ${list_NULL} ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'', 'DECOM':None, 'MAINTAIN':'N', 'MONITOR':'N', 'AWS_YR':${year} }" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="APPLY:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="APPLY" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This indicates that an access control is being applied to the road segment." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="REMOVE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REMOVE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This indicates that an access control is being removed from the road segment." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="BOTH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BOTH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This indicates that an access control is being applied and removed from the road segment in the same year and will be reported on in the same annual report year." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="DECOM:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Decommissioning type" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="DECOM" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The decommissioning type attribute identifies where decommissioning activities are scheduled to occur during the year on primary or branch roads that will be constructed during the year. " FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory." FOLDED="true">
                                <icon BUILTIN="full-1"/>
                                <icon BUILTIN="list"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: where AWS_YR = fiscal year to which the AWS applies" FOLDED="true"/>
                                <node TEXT="ST1: The presence of DECOM in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM is not null or MAINTAIN = Y or MONITOR = Y or [ACCESS = APPLY or ACCESS = REMOVE or ACCESS = BOTH]) (This is being checked during ACCESS validation)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <icon BUILTIN="password"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BERM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BERM" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="berm and/or ditch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="scarify and/or plant road" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SLSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SLSH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pile slash" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="WATX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="water crossing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="INTENT:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="30" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="INTENT" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="MNRF Intent" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true"/>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The MNRF Intent attribute indicates the MNRF's future management intent for the road corridor as identified in table FMP-18. The presence of this field is to facilitate the update of the spatial operational FMP layers as a result of approved amendments. " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of INTENT in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is a valid code (no need to validate this)" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: If TRANS value is populated, then INTENT must be populated" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be populated if TRANS is populated.' ${:} ${NIN} and row['TRANS'] not in ${list_NULL}  ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None, 'TRANS':${year} }" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be populated if TRANS is populated." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="MAINTAIN:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MAINTAIN" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Maintenance" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true"/>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road maintenance attribute is a field used to identify where road maintenance activities are planned to occur. The presence of this field is to facilitate the update of the spatial operational FMP layers as a result of approved amendments." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory." FOLDED="true">
                                <icon BUILTIN="full-1"/>
                                <icon BUILTIN="list"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of MAINTAIN in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM is not null or MAINTAIN = Y or MONITOR = Y or [ACCESS = APPLY or ACCESS = REMOVE or ACCESS = BOTH]) (This is being checked during ACCESS validation)" FOLDED="true">
                                    <icon BUILTIN="idea"/>
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="MONITOR:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MONITOR" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Monitoring" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true"/>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road monitoring attribute is a field used to identify where road monitoring activities are planned to occur. The presence of this field is to facilitate the update of the spatial operational FMP layers as a result of approved amendments." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of MONITOR in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM is not null or MAINTAIN = Y or MONITOR = Y or [ACCESS = APPLY or ACCESS = REMOVE or ACCESS = BOTH]) (This is being checked during ACCESS validation)" FOLDED="true">
                                    <icon BUILTIN="idea"/>
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="CONTROL1:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CONTROL1" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Access Control Type 1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="CONTROL" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road access control type attributes indicate the method of access control to be implemented on primary or branch roads that will be constructed during the year." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of CONTROL1 or CONTROL2 in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of CONTROL1 or CONTROL2 is mandatory where ACCESS = BOTH or ACCESS = APPLY" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is a valid code (unless ACYEAR is populated or ACCESS = BOTH or APPLY)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST2: When the road access control status is remove (ACCESS = REMOVE) then the control type should be null (CONTROL1 = null and CONTROL2 = null) (Transferred from ACCESS) " FOLDED="true">
                                    <icon BUILTIN="idea"/>
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be null if ACCESS = BOTH or APPLY.' for row in [row] if str(row['AWS_YR']) == str(${year}) and row['AWS_YR'] not in [ None, '', ' ', 0 ] if row['${name}'] in [None,'',' '] and row['ACCESS'] in ['BOTH', 'APPLY']   ]" FOLDED="true">
                                    <icon BUILTIN="idea"/>
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="[ 'ST2: &lt;mark&gt;${name}&lt;/mark&gt; should be null if ACCESS = REMOVE.' ${:} ${NINN} and  row['ACCESS'] == 'REMOVE' ]" FOLDED="true">
                                    <icon BUILTIN="idea"/>
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'BERM', 'AWS_YR':${year}, 'ACCESS':'REMOVE' }" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST2: &lt;mark&gt;${name}&lt;/mark&gt; should be null if ACCESS = REMOVE." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BERM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BERM" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="berm and/or ditch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="GATE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GATE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="gated / physical barrier" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="scarify and/or plant and /or seed road" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIGN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIGN" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="signed" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PRIV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRIV" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="private land" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SLSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SLSH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pile slash" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="WATX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="water crossing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="CONTROL2:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CONTROL2" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Access Control Type 2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="CONTROL" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is NOT mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of CONTROL1 or CONTROL2 in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of CONTROL1 or CONTROL2 is mandatory where ACCESS = BOTH or ACCESS = APPLY (This statement will be applied to CONTROL1 only)" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is a valid code (unless ACYEAR is populated or ACCESS = BOTH or APPLY)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST2: When the road access control status is remove (ACCESS = REMOVE) then the control type should be null (CONTROL1 = null and CONTROL2 = null) (transferred from ACCESS)" FOLDED="true">
                                    <icon BUILTIN="idea"/>
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST2: &lt;mark&gt;${name}&lt;/mark&gt; should be null if ACCESS = REMOVE.' ${:} ${NINN} and  row['ACCESS'] == 'REMOVE' ]" FOLDED="true">
                                    <icon BUILTIN="idea"/>
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'BERM', 'AWS_YR':${year}, 'ACCESS':'REMOVE' }" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST2: &lt;mark&gt;${name}&lt;/mark&gt; should be null if ACCESS = REMOVE." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BERM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BERM" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="berm and/or ditch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="GATE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GATE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="gated / physical barrier" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PRIV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRIV" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="private land" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="scarify and/or plant and /or seed road" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIGN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIGN" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="signed" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SLSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SLSH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pile slash" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="WATX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="water crossing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
                <node TEXT="Description" FOLDED="true">
                    <node TEXT="The scheduled roads corridors layer contains primary and branch road corridors in which road construction is scheduled to occur. This layer also identifies which corridors are scheduled to have access controls implemented or removed in the same year as construction.  " FOLDED="true"/>
                    <node TEXT="Licensees must include all road corridors in the approved FMP including all approved amendments received prior to December 1 (e.g. December 1, 2019 for the 2020-2021 AWS). An attribute identifying the AWS fiscal year will distinguish the areas scheduled for activities in the applicable AWS. The attributes identified for this AWS year may not be identical to the Planned Road Corridors layer from the approved FMP. For example, if multiple activities are planned in the approved FMP the layer may only show the activities scheduled for the fiscal year to which the AWS applies. (e.g. ACCESS = BOTH in the FMP while ACCESS = APPLY in the AWS)      " FOLDED="true"/>
                    <node TEXT="Monitoring and maintenance activities are not required to be identified in the same year as construction. Newly constructed roads are not the responsibility of the Crown until a formal transfer of responsibility has occurred. According to Part A Section 1.3.6.7 of the FMPM, if the responsibility of a road is to be transferred during the implementation of the FMP, an amendment to the FMP will be required (FMPM Part C, Section 2.0).  " FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ScheduledOperationalRoadBoundaries:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledOperationalRoadBoundaries" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Operational Road Boundaries (SOR)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SOR" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AWS_YR:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AWS fiscal year" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AWS_YR" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The annual work schedule fiscal year attribute identifies the fiscal year to which the AWS applies. Only those areas scheduled in the AWS require this field to be populated." FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value indicates an area not scheduled" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="There cannot be any values less than the FMP plan period start year (except for 0) or greater than the plan period end year minus 1." FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be greater than or equal to ${planyear} and less than or equal to ${planyear} + 10' ${:} ${NINN} and row['${name}'] not in [ '0' ] if ( int(row['${name}']) &lt; int(${planyear})) or (int(row['${name}']) &gt;= (int(${planyear}+10) ) )   ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':1000}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be greater than or equal to ${planyear} and less than or equal to ${planyear} + 10" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':1000}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;AWS_YR&lt;/mark&gt; must be greater than or equal to ${planyear} and less than or equal to ${planyear} + 10" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="ORBID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Operational Road Boundary ID" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="20" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ORBID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The operational road boundary identifier attribute indicates the user defined unique number, label or name assigned to the operational road boundaries. The operational road boundary is the perimeter of, the scheduled harvest area plus the area from an existing road or scheduled road corridor to the harvest area within which an operational road is scheduled to be constructed." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true">
                        <node TEXT="[ 'ST1: Field %s not found in table'%(f)  for f in [ 'AWS_YR','ORBID'] if f not in fields ] " FOLDED="true"/>
                    </node>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
                <node TEXT="Description" FOLDED="true">
                    <node TEXT="The scheduled operational road boundaries layer will establish the limits within which areas where new operational roads and forestry aggregate pits may be constructed/established during the year. An operational road boundary (ORB) includes the planned harvest area plus the area from an existing road or planned road corridor to the same harvest area within which an operational road is planned to be constructed. This layer may also include area between harvest operations or between planned/existing roads and harvest operations. The intent is not to remove water features or AOCs from the ORB layer. This layer only includes Crown land. The operational road boundary name (ORBID) will be linked to the road use management strategy.  " FOLDED="true"/>
                    <node TEXT="Licensees must include all operational road boundaries in the approved FMP including all approved amendments received prior to December 1(e.g. December 1, 2019 for the 2020-2021 AWS). An attribute identifying the AWS fiscal year will distinguish the areas scheduled for activities during the year.  " FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ScheduledExistingRoadActivities:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledExistingRoadActivities" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polyline" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Existing Road Activities (SRA)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SRA" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AWS_YR:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AWS_YR" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AWS fiscal year" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The annual work schedule fiscal year attribute identifies the fiscal year to which the AWS applies. Only those water crossing activities scheduled to be implemented during the year require this field to be populated." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="There cannot be any values less than the FMP plan period start year (except for 0) or greater than the plan period end year minus 1." FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A blank or null value indicates an area not scheduled" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be greater than or equal to ${planyear} and less than or equal to ${planyear} + 10' ${:} ${NINN} if ( int(row['${name}']) &lt; int(${planyear})) or (int(row['${name}']) &gt;= (int(${planyear}+10) ) ) ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':0}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="{'${name}':1000}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;AWS_YR&lt;/mark&gt; must be greater than or equal to ${planyear} and less than or equal to ${planyear} + 10" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="ROADID:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="30" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ROADID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road identifier attribute is the unique label or name assigned to the road or network of roads that the water crossing feature is located on. For plans prepared under the 2009 FMPM and the 2017 FMPM, this value must match a ROADID in table FMP-18, Road Construction and Use Management If the road segment that this crossing is associated with has been included in the road corridor layer, then the ROADID values must match" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory." FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ROADCLAS:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Class" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ROADCLAS" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="ROADCLAS" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road class attribute identifies the class of the existing forest access road or road network, in terms of the road use management strategy in the FMP." FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory." FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="B:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="B" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="branch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A branch road is a road, other than a primary road, that branches off an existing or new primary or branch road, providing access to, through or between areas of operations on a management unit" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="O:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="O" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Operational" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Operational roads are roads within operational road boundaries, other than primary or branch roads, that provide short-term access for harvest, renewal and tending operations. Operational roads are normally not maintained after they are no longer required for forest management purposes, and are often site prepared and regenerated." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="P:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="P" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Primary" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Primary roads are roads that provide principal access for the management unit, and are constructed, maintained and used as part of the main road system on the management unit. Primary roads are normally permanent roads." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRANS:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRANS" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Transfer" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT=" The road transfer attribute indicates that the transfer of responsibility to the  MNRF is scheduled in this AWS year. " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="ACCESS:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="9" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ACCESS" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="ACCESS" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Access Control" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The access control attribute is a field used to identify where access control activities are scheduled or already in effect. " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory." FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: When the road access control status is APPLY or ADD, BOTH or ADDREMOVE then the control type must be a code other than null (CONTROL1 is not null). (This is being checked at CONTROL1.)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <icon BUILTIN="password"/>
                                </node>
                                <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM is not null or MAINTAIN = Y or MONITOR = Y or ACCESS is not null)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST2: When the road access control status is remove (ACCESS = REMOVE) then the control type should be null (CONTROL1 = null and CONTROL2 = null) (This is being checked at CONTROL1 and 2)" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <icon BUILTIN="password"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow coding (APPLY, REMOVE, ADD, EXISTING, BOTH, ADDREMOVE)' for row in [row] if str(row['AWS_YR']) == str(${year}) and row['AWS_YR'] not in [ None, '', ' ', 0 ] if row['${name}'] not in ['APPLY', 'REMOVE', 'ADD', 'EXISTING', 'BOTH', 'ADDREMOVE' , None, '' , ' ' ]  ]" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="[ 'ST1: At a minimum, one of DECOM, MAINTAIN, MONITOR or &lt;mark&gt;ACCESS&lt;/mark&gt; must occur for each record.' ${:} str(row['AWS_YR']) == str(${year}) and row['AWS_YR'] not in ${list_NULL} if row['${name}'] in ${list_NULL} and row['DECOM'] in ${list_NULL} and row['MAINTAIN'] in ${list_NULL} + ['N'] and row['MONITOR'] in ${list_NULL} +['N'] ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'DECOM':None,'MAINTAIN':'N','MONITOR':'N', 'AWS_YR':${year} }" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: At a minimum, one of DECOM, MAINTAIN, MONITOR or &lt;mark&gt;ACCESS&lt;/mark&gt; must occur for each record." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'DECOM':None,'MAINTAIN':'N','MONITOR':'N', 'AWS_YR':0 }" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="APPLY:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="apply" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This indicates that an access control is being applied to the road segment." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="REMOVE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="remove" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This indicates that an access control is being removed from the road segment." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ADD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ADD" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="additional" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This indicates that an access control exists on the road segment and that a new access control is being applied to the road segment." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="EXISTING:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="EXISTING" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="existing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This indicates that an access control exists on the road segment." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="BOTH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="both" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This indicates that an access control is being applied and removed from the road segment in the same year and will be reported on in the same annual report year." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ADDREMOVE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ADDREMOVE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="additional with removal" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This indicates that an access control exists on the road segment, that a new access control is being applied to the road segment and that an access control is being removed from the road segment in the plan period" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="DECOM:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="DECOM" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="DECOM" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Decommissioning type" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The decommissioning type attribute identifies where decommissioning activities are scheduled to occur during the year on primary or branch roads that will be constructed during the year." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of DECOM in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM is not null or MAINTAIN = Y or MONITOR = Y or ACCESS is not null) - Implemented in ACCESS" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <icon BUILTIN="password"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BERM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BERM" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="berm and/or ditch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="scarify and/or plant road" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SLSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SLSH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pile slash" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="WATX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="water crossing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="MAINTAIN:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MAINTAIN" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="MAINTAIN" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Maintenance" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road maintenance attribute identifies where road maintenance activities are scheduled to occur on existing roads during the year." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of MAINTAIN in the file structure of the layer is optional" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: If present, the population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is NOT a valid code - conflicts with 1 - ignored" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <icon BUILTIN="help"/>
                                </node>
                                <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM is not null or MAINTAIN = Y or MONITOR = Y or [ACCESS = APPLY or ACCESS = REMOVE or ACCESS = BOTH]) (This is being checked during ACCESS validation)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <icon BUILTIN="password"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="MONITOR:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MONITOR" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="MONITOR" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Monitoring" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road monitoring attribute identifies where road monitoring activities are scheduled to occur during the year on existing roads." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of MONITOR in the file structure of the layer is optional" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: If present, the population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is NOT a valid code - conflicts with 1- ignored" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <icon BUILTIN="help"/>
                                </node>
                                <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM is not null or MAINTAIN = Y or MONITOR = Y or ACCESS is not null) - Handled in ACCESS" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <icon BUILTIN="password"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="conflict" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="CONTROL1:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CONTROL1" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Access Control Type 1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="CONTROL" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road access control type attributes indicate the method of access control to be implemented during the year on existing roads." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of CONTROL1 or CONTROL2 in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="A blank or null value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BERM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BERM" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="berm and/or ditch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="GATE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GATE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="gated / physical barrier" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PRIV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRIV" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="private land" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="scarify and/or plant and /or seed road" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIGN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIGN" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="signed" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SLSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SLSH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pile slash" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="WATX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="water crossing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="CONTROL2:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CONTROL2" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Access Control Type 2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="CONTROL" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of CONTROL1 or CONTROL2 in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="A blank or null value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BERM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BERM" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="berm and/or ditch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="GATE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GATE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="gated / physical barrier" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PRIV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRIV" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="private land" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="scarify and/or plant and /or seed road" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIGN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIGN" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="signed" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SLSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SLSH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pile slash" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="WATX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="water crossing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true">
                        <node TEXT="[ 'ST1: Field %s not found in table'%(f)  for f in [ 'AWS_YR','ROADID','ROADCLAS','TRANS','ACCESS','DECOM','CONTROL1'] if f not in fields ] " FOLDED="true"/>
                    </node>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
                <node TEXT="Description" FOLDED="true">
                    <node TEXT="The scheduled existing road activities layer identifies existing roads or existing road segments where use management activities are scheduled to occur. It will also identify existing roads that are scheduled to be transferred to MNRF and/or decommissioned during the AWS year.  " FOLDED="true"/>
                    <node TEXT="This layer is essentially a subset of the Existing Road Use Management Strategy layer in the FMP. Licensees may include all existing roads identified in the FMP in this layer. An attribute identifying the AWS fiscal year will distinguish the roads scheduled for activities in the applicable AWS.  " FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ScheduledWaterCrossingActivities:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledWaterCrossingActivities" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Point" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Water Crossings Activities (SWC)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="Description" FOLDED="true">
                    <node TEXT="The scheduled water crossing activities layer contains point features only. The actual water crossing location may be constructed within 100 metres of the point location identified. " FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SWC" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AWS_YR:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AWS_YR" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AWS fiscal year" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The annual work schedule fiscal year attribute identifies the fiscal year to which the AWS applies. Only those water crossing activities scheduled to be implemented during the year require this field to be populated." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="There cannot be any values less than the FMP plan period start year (except for 0) or greater than the plan period end year minus 1." FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A blank or null value indicates an area not scheduled" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be greater than or equal to ${planyear} and less than or equal to ${planyear} + 10' ${:} ${NINN} if ( int(row['${name}']) &lt; int(${planyear})) or (int(row['${name}']) &gt;= (int(${planyear}+10) ) ) ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':0}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="{'${name}':1000}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;AWS_YR&lt;/mark&gt; must be greater than or equal to ${planyear} and less than or equal to ${planyear} + 10" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="WATXID:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="12" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="WATXID" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Water Crossing Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The water crossing identifier attribute is a unique identifier label assigned to the crossing location. This water crossing ID will be unique in perpetuity." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="WATXTYPE:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="5" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="WATXTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Water Crossing Type" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="WATXTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The water crossing type attribute identifies the type of water crossing structure being scheduled." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BRID:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BRID" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="bridge" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="TEMP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="TEMP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Temporary" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CULV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CULV" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="culvert" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="MULTI:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MULTI" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Multiple Culvert" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="FORD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FORD" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="engineered ford" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="ICE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ICE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ice crossing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="BOX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BOX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="box culvert" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="ARCH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ARCH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="open bottom arch culvert" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="CONSTRCT:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CONSTRCT" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Construction" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The water crossing construction attribute identifies water crossings are scheduled to be constructed during the operating year of the AWS." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is optional." FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="If present, the attribute population must follow the correct coding scheme where AWS_YR equals the fiscal year to which the AWS applies or the following year." FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="At a minimum, one of Construction, Monitoring, Remove or Replace must occur for each record (CONSTRCT = Y or MONITOR = Y or REMOVE = Y or REPLACE = Y) where AWS_YR equals the fiscal year to which the AWS applies or the following year." FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: At a minimum, one of Construction, Monitoring, Remove or Replace must occur for each record (CONSTRCT = Y or MONITOR = Y or REMOVE = Y or REPLACE = Y) where AWS_YR equals the fiscal year to which the AWS applies or the following year. '  ${:}   (   row['CONSTRCT'] != 'Y'    and row['MONITOR'] != 'Y'    and row['REMOVE'] != 'Y'    and row['REPLACE'] != 'Y'   )  and   row['AWS_YR']  in [ ${year}, ${year} + 1 ]  ]  " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'N', 'CONSTRCT':'N', 'MONITOR':None, 'MONITOR':None, 'REMOVE':None, 'REPLACE':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRANS:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRANS" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Water Crossing Transfer" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The water crossing transfer attribute indicates that the transfer of responsibility to the MNRF is scheduled in this AWS year." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="MONITOR:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MONITOR" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Monitoring" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The water crossing monitoring attribute identifies water crossings scheduled to be monitored during the operating year of the AWS." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is optional." FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="If present, the attribute population must follow the correct coding scheme where AWS_YR equals the fiscal year to which the AWS applies or the following year." FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="At a minimum, one of Construction, Monitoring, Remove or Replace must occur for each record (CONSTRCT = Y or MONITOR = Y or REMOVE = Y or REPLACE = Y) where AWS_YR equals the fiscal year to which the AWS applies or the following year." FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: At a minimum, one of Construction, Monitoring, Remove or Replace must occur for each record (CONSTRCT = Y or MONITOR = Y or REMOVE = Y or REPLACE = Y) where AWS_YR equals the fiscal year to which the AWS applies or the following year.'  ${:}   (   row['CONSTRCT'] != 'Y'    and row['MONITOR'] != 'Y'    and row['REMOVE'] != 'Y'    and row['REPLACE'] != 'Y'   )  and   row['AWS_YR']  in [ ${year}, ${year} + 1 ]  ]  " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'CONSTRCT':'N', 'MONITOR':None, 'REMOVE':None, 'REPLACE':None, 'AWS_YR':${year} }" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: At a minimum, one of Construction, Monitoring, Remove or Replace must occur for each record (CONSTRCT = Y or MONITOR = Y or REMOVE = Y or REPLACE = Y) where AWS_YR equals the fiscal year to which the AWS applies or the following year." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="REMOVE:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="REMOVE" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Removal" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The water crossing removal attribute identifies water crossings scheduled to be removed during the operating year of the AWS." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is optional." FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="If present, the attribute population must follow the correct coding scheme where AWS_YR equals the fiscal year to which the AWS applies or the following year." FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="At a minimum, one of Construction, Monitoring, Remove or Replace must occur for each record (CONSTRCT = Y or MONITOR = Y or REMOVE = Y or REPLACE = Y) where AWS_YR equals the fiscal year to which the AWS applies or the following year." FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: At a minimum, one of Construction, Monitoring, Remove or Replace must occur for each record (CONSTRCT = Y or MONITOR = Y or REMOVE = Y or REPLACE = Y) where AWS_YR equals the fiscal year to which the AWS applies or the following year. '  ${:}   (   row['CONSTRCT'] != 'Y'    and row['MONITOR'] != 'Y'    and row['REMOVE'] != 'Y'    and row['REPLACE'] != 'Y'   )  and   row['AWS_YR']  in [ ${year}, ${year} + 1 ]  ]  " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'N', 'CONSTRCT':'N', 'MONITOR':None, 'MONITOR':None, 'REMOVE':None, 'REPLACE':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="REPLACE:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="REPLACE" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Replacement" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The water crossing replacement attribute identifies water crossings scheduled to be replaced during the operating year of the AWS." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is optional." FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="If present, the attribute population must follow the correct coding scheme where AWS_YR equals the fiscal year to which the AWS applies or the following year." FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="At a minimum, one of Construction, Monitoring, Remove or Replace must occur for each record (CONSTRCT = Y or MONITOR = Y or REMOVE = Y or REPLACE = Y) where AWS_YR equals the fiscal year to which the AWS applies or the following year." FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: At a minimum, one of Construction, Monitoring, Remove or Replace must occur for each record (CONSTRCT = Y or MONITOR = Y or REMOVE = Y or REPLACE = Y) where AWS_YR equals the fiscal year to which the AWS applies or the following year. '  ${:}   (   row['CONSTRCT'] != 'Y'    and row['MONITOR'] != 'Y'    and row['REMOVE'] != 'Y'    and row['REPLACE'] != 'Y'   )  and   row['AWS_YR']  in [ ${year}, ${year} + 1 ]  ]  " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'N', 'CONSTRCT':'N', 'MONITOR':None, 'MONITOR':None, 'REMOVE':None, 'REPLACE':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="ROADID:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="30" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ROADID" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road identifier attribute is the unique label or name assigned to the road or network of roads that the water crossing feature is located on. For plans prepared under the 2009 FMPM and the 2017 FMPM, this value must match a ROADID in table FMP-18, Road Construction and Use Management If the road segment that this crossing is associated with has been included in the road corridor layer, then the ROADID values must match" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory." FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
                <node TEXT="Description" FOLDED="true">
                    <node TEXT="The scheduled water crossing activities layer contains the locations of water crossings that will be constructed or replaced during the year. Water crossings planned to be constructed or replaced in the following year may be submitted to provide MNRF with an ice-free season to conduct a review with respect to the Fisheries Act (FMPM Part D Section 3.2.5.1). The scheduled water crossing activities layer will also contain the locations of water crossings to be removed during the year to enable a review by MNRF with respect to the Fisheries Act. Water crossings scheduled to be monitored must also be identified.  " FOLDED="true"/>
                    <node TEXT="An attribute identifying the AWS fiscal year will distinguish the water crossings scheduled for activities in the applicable AWS. " FOLDED="true"/>
                    <node TEXT="The scheduled water crossing activities layer contains point features only. The actual water crossing location may be constructed within 100 metres of the point location identified. " FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ScheduledAggregateExtraction:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledAggregateExtraction" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Aggregate Extraction (SAG)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SAG" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AWS_YR:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AWS_YR" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AWS fiscal year" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The AWS fiscal year attribute identifies the fiscal year to which the scheduled activity applies. Only those aggregate extraction areas scheduled to have forestry aggregate pits established during the year require this field to be populated." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value indicates an area not scheduled" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="There cannot be any values less than the FMP plan period start year (except for 0) or greater than the plan period end year minus 1." FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be greater than or equal to ${planyear} and less than or equal to ${planyear} + 10' ${:} ${NINN} if ( int(row['${name}']) &lt; int(${planyear})) or (int(row['${name}']) &gt;= (int(${planyear}+10) ) ) ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':0}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="{'${name}':1000}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;AWS_YR&lt;/mark&gt; must be greater than or equal to ${planyear} and less than or equal to ${planyear} + 10" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="AGAREAID:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="15" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AGAREAID" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Aggregate Extraction Area Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The aggregate extraction area identifier attribute indicates the unique identifier for the area where forestry aggregate pits are scheduled to be established." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="A  blank or null value is a valid code. " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true">
                        <node TEXT="[ 'ST1: Field %s not found in table'%(f)  for f in [ 'AWS_YR','AGAREAID'] if f not in fields ]" FOLDED="true"/>
                    </node>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
                <node TEXT="Description" FOLDED="true">
                    <node TEXT="The scheduled aggregate extraction areas layer contains areas where forestry aggregate pits are scheduled to be established. Aggregate extraction areas are areas outside of road corridors, harvest areas and operational road boundaries where the Licensee has scheduled to extract aggregate material. An aggregate extraction area is defined as an individual polygon depicting a planned pit location within 500 meters of an existing access road.  " FOLDED="true"/>
                    <node TEXT="Licensees must include all aggregate extraction areas in the approved FMP including all approved amendments received prior to December 1(e.g. December 1, 2019 for the 2020-2021 AWS). An attribute identifying the AWS fiscal year will distinguish the areas scheduled for activities in the applicable AWS.  " FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ScheduledSitePreparationTreatments:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledSitePreparationTreatments" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Site Preparation Treatments (SSP)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SSP" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AWS_YR:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AWS_YR" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AWS fiscal year" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The AWS fiscal year attribute identifies the fiscal year to which the AWS applies. Only those areas scheduled in the AWS require this field to be populated. " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value indicates an area not scheduled" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="There cannot be any values less than the FMP plan period start year (except for 0) or greater than the plan period end year minus 1." FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be greater than or equal to ${planyear} and less than or equal to ${planyear} + 10' ${:} ${NINN} if ( int(row['${name}']) &lt; int(${planyear})) or (int(row['${name}']) &gt;= (int(${planyear}+10) ) ) ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':0}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="{'${name}':1000}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;AWS_YR&lt;/mark&gt; must be greater than or equal to ${planyear} and less than or equal to ${planyear} + 10" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD1:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD1" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment method attribute indicates the general type of silvicultural activity and the specific treatment or method to be applied to the site." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: For TRTMTHD1, the presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: For TRTMTHD1 or TRTMTHD2 or TRTMTHD3, the population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be null or blank when AWS_YR is ${year}' ${:} ( ${NIN} and row['TRTMTHD2'] in ${list_NULL} and row['TRTMTHD3'] in ${list_NULL}) and row['AWS_YR'] == ${year}    ]" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None, 'TRTMTHD2':None, 'TRTMTHD3':None, 'AWS_YR':2025}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="SIPMECH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPMECH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="mechanical" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIPCHEMA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPCHEMA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical : aerial application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIPCHEMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical : ground application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIPPB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPPB" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="prescribed burn - conventional burn / high complexity burn" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD2:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD2" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment method attribute indicates the general type of silvicultural activity and the specific treatment or method to be applied to the site." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="SIPMECH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPMECH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="mechanical" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIPCHEMA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPCHEMA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical : aerial application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIPCHEMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical : ground application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIPPB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPPB" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="prescribed burn - conventional burn / high complexity burn" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD3:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD3" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 3" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment method attribute indicates the general type of silvicultural activity and the specific treatment or method to be applied to the site." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="SIPMECH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPMECH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="mechanical" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIPCHEMA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPCHEMA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical : aerial application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIPCHEMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical : ground application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIPPB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPPB" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="prescribed burn - conventional burn / high complexity burn" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true">
                        <node TEXT="[ 'ST1: Field %s not found in table'%(f)  for f in [ 'AWS_YR','TRTMTHD1'] if f not in fields ] " FOLDED="true"/>
                    </node>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
                <node TEXT="Description" FOLDED="true">
                    <node TEXT="The scheduled site preparation treatments layer is one of four geospatial data layers that identify the areas where renewal and maintenance operations are scheduled during the year. This layer will identify renewal and maintenance operations related to site preparation treatments. The treatment method will be identified for each area. " FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ScheduledRegenerationTreatments:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledRegenerationTreatments" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Regeneration Treatments (SRG)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SRG" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AWS_YR:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AWS_YR" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AWS fiscal year" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The AWS fiscal year attribute identifies the fiscal year to which the AWS applies. Only those areas scheduled in the AWS require this field to be populated. " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value indicates an area not scheduled" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="There cannot be any values less than the FMP plan period start year (except for 0) or greater than the plan period end year minus 1." FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be greater than or equal to ${planyear} and less than or equal to ${planyear} + 10' ${:} ${NINN} if ( int(row['${name}']) &lt; int(${planyear})) or (int(row['${name}']) &gt;= (int(${planyear}+10) ) ) ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':0}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="{'${name}':1000}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;AWS_YR&lt;/mark&gt; must be greater than or equal to ${planyear} and less than or equal to ${planyear} + 10" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD1:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD1" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Silvicultural Treatment Method 1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD-REGN" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment method attribute indicates the general type of silvicultural activity and the specific treatment or method scheduled to be applied to the site." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: For TRTMTHD1, the presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: For TRTMTHD1 or TRTMTHD2 or TRTMTHD3, the population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be null or blank when AWS_YR is ${year}' ${:} ( ${NIN} and row['TRTMTHD2'] in ${list_NULL} and row['TRTMTHD3'] in ${list_NULL}) and row['AWS_YR'] == ${year}    ]" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None, 'TRTMTHD2':None, 'TRTMTHD3':None, 'AWS_YR':2025}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="PLANT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLANT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - planting" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCARIFY:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCARIFY" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - scarification" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SEED:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEED" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - seeding" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SEEDSIP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDSIP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - seeding with site preparation" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD2:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD2" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Silvicultural Treatment Method 2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD-REGN" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment method attribute indicates the general type of silvicultural activity and the specific treatment or method scheduled to be applied to the site." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="PLANT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLANT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - planting" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCARIFY:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCARIFY" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - scarification" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SEED:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEED" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - seeding" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SEEDSIP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDSIP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - seeding with site preparation" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD3:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD3" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Silvicultural Treatment Method 3" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD-REGN" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment method attribute indicates the general type of silvicultural activity and the specific treatment or method scheduled to be applied to the site." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="PLANT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLANT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - planting" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCARIFY:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCARIFY" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - scarification" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SEED:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEED" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - seeding" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SEEDSIP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDSIP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - seeding with site preparation" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true">
                        <node TEXT="[ 'ST1: Field %s not found in table'%(f)  for f in [ 'AWS_YR','TRTMTHD1'] if f not in fields ] " FOLDED="true"/>
                    </node>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
                <node TEXT="Description" FOLDED="true">
                    <node TEXT="The scheduled regeneration treatments layer is one of four geospatial data layers that identify the areas where renewal and maintenance operations are scheduled during the year. This layer will identify renewal and maintenance operations related to regeneration treatments. The treatment method will be identified for each area. " FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ScheduledTendingTreatments:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledTendingTreatments" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Tending Treatments (STT)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="STT" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AWS_YR:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AWS_YR" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AWS fiscal year" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The AWS fiscal year attribute identifies the fiscal year to which the AWS applies. Only those areas scheduled in the AWS require this field to be populated. " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value indicates an area not scheduled" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="There cannot be any values less than the FMP plan period start year (except for 0) or greater than the plan period end year minus 1." FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be greater than or equal to ${planyear} and less than or equal to ${planyear} + 10' ${:} ${NINN} if ( int(row['${name}']) &lt; int(${planyear})) or (int(row['${name}']) &gt;= (int(${planyear}+10) ) ) ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':0}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="{'${name}':1000}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;AWS_YR&lt;/mark&gt; must be greater than or equal to ${planyear} and less than or equal to ${planyear} + 10" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD1:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD1" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Silvicultural Treatment Method 1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD-TND" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment method attribute indicates the general type of silvicultural activity and the specific treatment or method scheduled to be applied to the site." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: For TRTMTHD1, the presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: For TRTMTHD1 or TRTMTHD2 or TRTMTHD3, the population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be null or blank when AWS_YR is ${year}' ${:} ( ${NIN} and row['TRTMTHD2'] in ${list_NULL} and row['TRTMTHD3'] in ${list_NULL}) and row['AWS_YR'] == ${year}    ]" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="CLCHEMA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLCHEMA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - chemical : aerial application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CLCHEMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - chemical : ground application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CLMANUAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLMANUAL" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - manual" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CLMECH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLMECH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - mechanical" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CLPB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLPB" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - prescribed burn / high complexity prescribed burn" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="IMPROVE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="IMPROVE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spacing / pre-commercial thin / improvement cut - uneven-aged" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="THINPRE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="THINPRE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spacing / pre-commercial thin / improvement cut - even aged" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CULTIVAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CULTIVAT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cultivation" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PRUNE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRUNE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pruning" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD2:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD2" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Silvicultural Treatment Method 2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD-TND" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment method attribute indicates the general type of silvicultural activity and the specific treatment or method scheduled to be applied to the site." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="CLCHEMA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLCHEMA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - chemical : aerial application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CLCHEMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - chemical : ground application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CLMANUAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLMANUAL" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - manual" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CLMECH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLMECH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - mechanical" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CLPB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLPB" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - prescribed burn / high complexity prescribed burn" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="IMPROVE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="IMPROVE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spacing / pre-commercial thin / improvement cut - uneven-aged" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="THINPRE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="THINPRE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spacing / pre-commercial thin / improvement cut - even aged" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CULTIVAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CULTIVAT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cultivation" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PRUNE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRUNE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pruning" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD3:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD3" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Silvicultural Treatment Method 3" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD-TND" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment method attribute indicates the general type of silvicultural activity and the specific treatment or method scheduled to be applied to the site." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="CLCHEMA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLCHEMA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - chemical : aerial application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CLCHEMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - chemical : ground application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CLMANUAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLMANUAL" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - manual" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CLMECH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLMECH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - mechanical" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CLPB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLPB" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - prescribed burn / high complexity prescribed burn" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="IMPROVE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="IMPROVE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spacing / pre-commercial thin / improvement cut - uneven-aged" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="THINPRE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="THINPRE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spacing / pre-commercial thin / improvement cut - even aged" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CULTIVAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CULTIVAT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cultivation" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PRUNE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRUNE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pruning" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true">
                        <node TEXT="[ 'ST1: Field %s not found in table'%(f)  for f in [ 'AWS_YR','TRTMTHD1'] if f not in fields ] " FOLDED="true"/>
                    </node>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
                <node TEXT="Description" FOLDED="true">
                    <node TEXT="The scheduled tending treatments layer is one of four geospatial data layers that identify the areas where renewal and maintenance operations are scheduled during the year. This layer will identify renewal and maintenance operations related to tending treatments. The treatment method will be identified for each area. " FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ScheduledProtectionTreatment:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledProtectionTreatment" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Protection Treatment (SPT)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SPT" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AWS_YR:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AWS_YR" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AWS fiscal year" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The AWS fiscal year attribute identifies the fiscal year to which the AWS applies. Only those areas scheduled in the AWS require this field to be populated. " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value indicates an area not scheduled" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="There cannot be any values less than the FMP start year (except for 0 on areas not scheduled) or greater than the plan end year minus 1. " FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be greater than or equal to ${planyear} and less than or equal to ${planyear} + 10' ${:} ${NINN} if ( int(row['${name}']) &lt; int(${planyear})) or (int(row['${name}']) &gt;= (int(${planyear}+10) ) ) ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':0}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="{'${name}':1000}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;AWS_YR&lt;/mark&gt; must be greater than or equal to ${planyear} and less than or equal to ${planyear} + 10" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD1:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD1" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Silvicultural Treatment Method 1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD-PROTECT" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment method attribute indicates the general type of silvicultural activity and the specific treatment or method scheduled to be applied to the site." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: For TRTMTHD1, the  presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: For TRTMTHD1 or TRTMTHD2 or TRTMTHD3, the population of this attribute is mandatory where AWS_YR equals the fiscal year to which the AWS applies." FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be null or blank when AWS_YR is ${year}' ${:} ${NIN} and row['AWS_YR'] == ${year}    ]" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="PCHEMA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PCHEMA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical - aerial spraying" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Application of chemicals from an aircraft to prevent, control or manage the spread of, and/or the damage caused by, insects and disease." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PCHEMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical - ground insecticide" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Ground application of chemicals to prevent, control or manage the spread of, and/or the damage caused by, insects and disease." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PMANUAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PMANUAL" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="manual" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of (healthy) trees or tree limbs as a preventative measure to reduce the risk of a specific insect or disease occurring in an area, or the removal of infected trees or tree limbs from an area to clean the area and reduce the spread of insects or disease." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD2:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD2" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Silvicultural Treatment Method 2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD-PROTECT" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment method attribute indicates the general type of silvicultural activity and the specific treatment or method scheduled to be applied to the site." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="PCHEMA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PCHEMA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical - aerial spraying" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Application of chemicals from an aircraft to prevent, control or manage the spread of, and/or the damage caused by, insects and disease." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PCHEMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical - ground insecticide" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Ground application of chemicals to prevent, control or manage the spread of, and/or the damage caused by, insects and disease." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PMANUAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PMANUAL" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="manual" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of (healthy) trees or tree limbs as a preventative measure to reduce the risk of a specific insect or disease occurring in an area, or the removal of infected trees or tree limbs from an area to clean the area and reduce the spread of insects or disease." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD3:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD3" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Silvicultural Treatment Method 3" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD-PROTECT" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment method attribute indicates the general type of silvicultural activity and the specific treatment or method scheduled to be applied to the site." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="PCHEMA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PCHEMA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical - aerial spraying" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Application of chemicals from an aircraft to prevent, control or manage the spread of, and/or the damage caused by, insects and disease." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PCHEMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical - ground insecticide" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Ground application of chemicals to prevent, control or manage the spread of, and/or the damage caused by, insects and disease." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PMANUAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PMANUAL" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="manual" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of (healthy) trees or tree limbs as a preventative measure to reduce the risk of a specific insect or disease occurring in an area, or the removal of infected trees or tree limbs from an area to clean the area and reduce the spread of insects or disease." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true">
                        <node TEXT="[ 'ST1: Field %s not found in table'%(f)  for f in [ 'AWS_YR','TRTMTHD1'] if f not in fields ] " FOLDED="true"/>
                    </node>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
                <node TEXT="Description" FOLDED="true">
                    <node TEXT="The scheduled protection treatments layer is one of five geospatial data layers that identify the areas where renewal and maintenance operations are scheduled during the year. This layer will identify renewal and maintenance operations related to scheduled protection treatments. The treatment method will be identified for each area.  " FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ExistingForestryAggregatePits:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ExistingForestryAggregatePits" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Point" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Existing Forestry Aggregate Pits (AGP)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="AGP" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="PITID:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="15" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="PITID" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Aggregate Pit ID" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true"/>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The aggregate pit identifier attribute is the unique identifier / label assigned to an existing forestry aggregate pit." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The PIT_ID attribute must contain a unique Value" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="I thin it is stupid this is not included..." FOLDED="true">
                                <node TEXT="{}" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="PITOPEN:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="9" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="PITOPEN" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="aggregate pit opening date" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The aggregate pit opening date attribute is the date when the forestry aggregate pit was established." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" the date will be recorded as year/month/day following this format: YYYYMMMDD (e.g., 2028MAR01)" FOLDED="true"/>
                            <node TEXT="the day will always be recorded as a two digit number padded left with zero. (e.g.,01, 04)" FOLDED="true"/>
                            <node TEXT="the month will always be recorded as a three letter abbreviation" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code - Added to increase clarity " FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;mark&gt;${name}&lt;/mark&gt; is not a valid date in YYYYMMMDD (i.e. 2028MAR01) format'  ${:N}  fimdate(row['${name}']) is None  ] " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; is not a valid date in YYYYMMMDD (i.e. 2028MAR01) format" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="PITCLOSE:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="PITCLOSE" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="aggregate pit closure" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The aggregate pit closure attribute identifies that a pit will be closed and will receive final rehabilitation during this AWS year.  " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: If PITCLOSE is Y, CAT9APP must be null." FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: If &lt;mark&gt;${name}&lt;/mark&gt; is Y, CAT9APP must be null'  ${:} row['${name}'] =='Y' and row['CAT9APP'] not in [None, '', ' '] ] " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'Y', 'CAT9APP':'2028MAR31' }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: If &lt;mark&gt;${name}&lt;/mark&gt; is Y, CAT9APP must be null" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="CAT9APP:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="9" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CAT9APP" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Category 9 application date" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The Category 9 application date attribute is the projected date within this annual work schedule when an application will be made for a Category 9 Aggregate Pit Permit. " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" the date will be recorded as year/month/day following this format: YYYYMMMDD (e.g., 2028MAR01)" FOLDED="true"/>
                            <node TEXT="the day will always be recorded as a two digit number padded left with zero. (e.g.,01, 04)" FOLDED="true"/>
                            <node TEXT="the month will always be recorded as a three letter abbreviation" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="This field may contain null values" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT='ST1: The attribute population must follow the date format "%Y%b%d"' FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The date cannot be greater than 10 years from PITOPEN date" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: If CAT9APP is not null, PITCLOSE must be NO (Tested on PITCLOSE)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the date format &quot;%Y%b%d&quot;'  ${:N} fimdate(row['${name}']) is None  ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be within 10 years of pit open date'  ${:N}  (fimdate(row['${name}']) is not None )  if   ( fimdate(row['${name}']).year - (fimdate(row['PITOPEN']) ).year  )  &gt; 10 ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="[ 'ST1: PITCLOSE must be N when &lt;mark&gt;CAT9APP&lt;/mark&gt; is not null' ${:N} row['PITCLOSE'] !='N' ] " FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT='ST1: &lt;mark&gt;${name}&lt;/mark&gt; must follow the date format "%Y%b%d"' FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'2028MAR31', 'PITOPEN':'2017MAR31' }" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be within 10 years of pit open date" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
                <node TEXT="Description" FOLDED="true">
                    <node TEXT="The existing forestry aggregate pits layer contains the locations of all existing forestry aggregate  pits on the forest management unit. Expired pits are only to be included if a final rehabilitation  has not yet occurred. Aggregate resources can be removed from forestry aggregate pits by the  forest industry without the requirement for an aggregate permit under the Aggregate  Resources Act.  " FOLDED="true"/>
                    <node TEXT="The locations of new forestry aggregate pits established prior to submission of this AWS (e.g.,  January 1) will be identified in this layer. Pits established after submission of the AWS will  appear in the next AWS. New forestry aggregate pits will be identified in the applicable annual  report. Expired and closed pits are not eligible for aggregate extraction and therefore should  not be included in this layer. " FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ScheduledEstablishmentAssessment:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ScheduledEstablishmentAssessment" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Scheduled Establishment Assessment (SEA)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualWorkSchedules" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SEA" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AWS_YR:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AWS_YR" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AWS fiscal year" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The AWS fiscal year attribute identifies the fiscal year to which the AWS applies. Only those areas scheduled in the AWS require this field to be populated. " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value indicates an area not scheduled" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="There cannot be any values less than the FMP plan period start year (except for 0) or greater than the plan period end year minus 1." FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be greater than or equal to ${planyear} and less than or equal to ${planyear} + 10' ${:} row['${name}'] not in [None, '', ' '] if int(row['${name}']) &gt; 0 and ( int(row['${name}']) &lt; int(${planyear})) or (int(row['${name}']) &gt;= (int(${planyear}+10) ) ) ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':0}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="{'${name}':1000}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;AWS_YR&lt;/mark&gt; must be greater than or equal to ${planyear} and less than or equal to ${planyear} + 10" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="YRDEP:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="YRDEP" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Year of Last Disturbance" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The year of last disturbance attribute indicates a four digit number of the fiscal year that a productive forest area was disturbed, completely or partially, by harvest or by natural causes. For the shelterwood silvicultural system, the year of last disturbance is the year of stand initiation (regeneration cut). Although subsequent cuts are reported as harvest the regenerating stand age is normally based on the original regeneration cut. This value will contribute to the determination of the regeneration delay for the applicable silvicultural stratum in the FMP. " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="The inventories will be populated with all disturbances (harvest and natural) which are identified in annual reports. The year of last disturbance for each productive forest stand must correspond to the year that the disturbance occurred, as recorded in the applicable annual reports.  Forest stands that are managed under the selection or shelterwood silvicultural systems must also have a year of last disturbance. The year of last disturbance provides an estimate of the stage of forest stand development within a cutting cycle for selection stands or within the current stage of a shelterwood system.  In the case of a partial harvest where certain tree species have been removed, such as a first pass where merchantable trees remain in a forest stand, the year that the first pass was carried out must be entered as the year of last disturbance. If an additional harvest or pass is conducted, the year of last disturbance is changed to the year in which the most recent harvest/pass operation was carried out.  Commercial thinning is a mid-rotation treatment that is recorded as a disturbance and the stand area is considered as disturbed area in forest management planning. Forest stands that have received a commercial thinning must show the applicable year of last disturbance in the forest polygon coverage.  The year of last disturbance is also used to identify other stand improvement operations. A stand improvement operation is normally associated with the selection system.   Year of last disturbance must not be used to indicate tending operations, such as a chemical or manual release which are required to bring a forest stand to regeneration standards. " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A zero or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TARGETFU:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="15" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TARGETFU" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Target Forest Unit" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The target forest unit attribute contains the short form label used to reference the forest unit in the future condition section of the associated SGR applied to the area. " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TARGETYD:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="10" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TARGETYD" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Target Yield" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The target yield attribute contains the same label used in the base model inventory from the YIELD attribute used in the approved forest model of the FMP at the time of harvest or SGR update. This provides an indicator of productivity and the expected growth and development pattern. " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="Format: user defined (e.g. PRSNT, High, Med, and Low) must be a YIELD defined in the FMP at the time of harvest or SGR update Target Yields apply only to even-aged forest stands that are managed under the clear-cut silvicultural system and the shelterwood silvicultural system. For forest stands that are managed under the shelterwood silvicultural system, the stage of development, understorey, and next stage attributes, describe the silvicultural regimes and provide equivalent information to indicate silvicultural intensity. Similarly, for stands managed under the selection silvicultural system, the stage of development, acceptable growing stock, unacceptable growing stock, and next stage attributes describe the silvicultural regimes and provide equivalent information to indicate silvicultural intensity." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
                <node TEXT="Description" FOLDED="true">
                    <node TEXT="This geospatial layer denotes harvested areas including salvage that are scheduled to be assessed for establishment. Establishment is defined in the regeneration standards as the early indicator of observable measures of a regenerating area to provide confidence that the target (i.e. mature) stand condition can be achieved. " FOLDED="true"/>
                    <node TEXT="The identified areas will be used by MNRF to complete sampling within the same operational season as the licensee. This layer will be submitted as part of an AWS changes submission (Regeneration Assessment) by June 1st to accommodate the completion of business processes required to identify areas for establishment surveys in the Annual Work Schedule. " FOLDED="true"/>
                    <node TEXT="For plans developed under the 2009 FMPM this layer will be submitted to identify areas scheduled for a free to grow survey. This includes 2019 FMPs that do not have regeneration standards as defined in the 2017 FMPM. Target yield attribute for the polygon will be populated with the Silviculture Intensity attribute described in the SGR from the approved FMP. " FOLDED="true"/>
                </node>
            </node>
            <node TEXT="metadata:" FOLDED="true" POSITION="left">
                <node TEXT="submission_type" FOLDED="true">
                    <node TEXT="AWS_2018" FOLDED="true"/>
                </node>
                <node TEXT="submission_min_year" FOLDED="true">
                    <node TEXT="2017" FOLDED="true"/>
                </node>
                <node TEXT="submission_max_year" FOLDED="true">
                    <node TEXT="2030" FOLDED="true"/>
                </node>
                <node TEXT="id" FOLDED="true">
                    <node TEXT="fi_portal_id" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="{fmu}\{product}\{year}" FOLDED="true"/>
                </node>
                <node TEXT="Internal_zipfile" FOLDED="true">
                    <node TEXT="mu{fmu_id}_{year}_AWS.zip" FOLDED="true"/>
                    <node STYLE="fork" TEXT="*.zip" FOLDED="true"/>
                </node>
                <node TEXT="Maps" FOLDED="true">
                    <node TEXT="MU{fmu_id}_{planyear}_{product}_{phase}_MAP*.eps" FOLDED="true"/>
                    <node TEXT="*.eps" FOLDED="true"/>
                    <node TEXT="*_MAP*.pdf" FOLDED="true"/>
                    <node TEXT="*_MAP*.eps" FOLDED="true"/>
                </node>
                <node TEXT="Document" FOLDED="true">
                    <node STYLE="fork" TEXT="MU{fmu_id}_{year}_{product}_TXT_Text.pdf" FOLDED="true"/>
                    <node TEXT="MU{fmu_id}_{year}_{product}_TBL_Tables.pdf" FOLDED="true"/>
                    <node TEXT="MU{fmu_id}_{year}_{product}_MAP_Sum.pdf" FOLDED="true"/>
                    <node TEXT="MU{fmu_id}_{year}_{product}_MAP_SumFR.pdf" FOLDED="true"/>
                    <node TEXT="MU*_MAP_SUM*.pdf" FOLDED="true"/>
                    <node STYLE="fork" TEXT="MU*TXT*.pdf" FOLDED="true"/>
                </node>
                <node TEXT="Layer" FOLDED="true">
                    <node TEXT="*.e00" FOLDED="true"/>
                    <node TEXT="*.shp" FOLDED="true"/>
                    <node TEXT="*.dbf" FOLDED="true"/>
                    <node TEXT="*.shx" FOLDED="true"/>
                    <node TEXT="*.prj" FOLDED="true"/>
                    <node TEXT="*.shp.xml" FOLDED="true"/>
                </node>
                <node TEXT="Layer_Filename" FOLDED="true">
                    <node TEXT="MU{fmu_id}_{year}{product}" FOLDED="true"/>
                </node>
                <node TEXT="Document_Directory" FOLDED="true">
                    <node TEXT="" FOLDED="true"/>
                </node>
                <node TEXT="DeferredLayerGPKG" FOLDED="true">
                    <node TEXT="*.gpkg" FOLDED="true"/>
                </node>
                <node TEXT="DeferredLayerGPKG_Directory" FOLDED="true">
                    <node TEXT="LAYERS" FOLDED="true"/>
                </node>
                <node TEXT="Maps_Directory" FOLDED="true">
                    <node STYLE="fork" TEXT="Maps" FOLDED="true"/>
                </node>
                <node TEXT="Internal_zipfile_Directory" FOLDED="true">
                    <node TEXT="" FOLDED="true"/>
                </node>
                <node TEXT="Catchall" FOLDED="true">
                    <node TEXT="" FOLDED="true"/>
                </node>
                <node TEXT="Catchall_Directory" FOLDED="true">
                    <node TEXT="" FOLDED="true"/>
                </node>
                <node TEXT="Trash" FOLDED="true">
                    <node TEXT="Thumbs.db" FOLDED="true"/>
                    <node TEXT="*.sbn" FOLDED="true"/>
                    <node TEXT="*.sbx" FOLDED="true"/>
                    <node TEXT="*.cpg" FOLDED="true"/>
                </node>
                <node TEXT="DeferredLayerGDB" FOLDED="true">
                    <node TEXT="*.atx" FOLDED="true"/>
                    <node TEXT="*.spx" FOLDED="true"/>
                    <node TEXT="*.gdbindexes" FOLDED="true"/>
                    <node TEXT="*.gdbtablx" FOLDED="true"/>
                    <node TEXT="*.gdbtable" FOLDED="true"/>
                    <node TEXT="*.freelist" FOLDED="true"/>
                    <node STYLE="fork" TEXT="*/gdb" FOLDED="true"/>
                    <node TEXT="*/timestamps" FOLDED="true"/>
                </node>
                <node TEXT="DeferredLayerGDB_Directory" FOLDED="true">
                    <node TEXT="LAYERS/mu{fmu_id}_{year}_{product}.gdb" FOLDED="true"/>
                </node>
            </node>
        </node>
    </map>
